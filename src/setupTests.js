// config file used by Enzyme, see: https://airbnb.io/enzyme/docs/installation/
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
jest.spyOn(Date, "now").mockImplementation(() => 1479427200000);
