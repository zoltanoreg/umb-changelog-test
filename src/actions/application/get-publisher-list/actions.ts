import { Dispatch } from "redux";

import { ApplicationService } from "../../../services/ApplicationService";
import { editModalCloseAction } from "../../modal/edit/actions";
import { TEditModalActions } from "../../modal/edit/types";
import { popupOpenAction } from "../../popup/actions";
import { TPopupActions } from "../../popup/types";

import {
  GetPublisherListTypes,
  IGetPublisherListErrorAction,
  IGetPublisherListRequestAction,
  IGetPublisherListSuccessAction,
  TGetPublisherListActions,
} from "./types";

export const GetPublisherListRequestAction = (data: any): IGetPublisherListRequestAction => ({
  payload: data,
  type: GetPublisherListTypes.REQUEST,
});
export const GetPublisherListSuccessAction = (data: any): IGetPublisherListSuccessAction => ({
  payload: data,
  type: GetPublisherListTypes.SUCCESS,
});
export const GetPublisherListErrorAction = (error: any): IGetPublisherListErrorAction => ({
  payload: error,
  type: GetPublisherListTypes.ERROR,
});

export const getPublisherList = (config: any, cb?: any) => (
  dispatch: Dispatch<TPopupActions | TGetPublisherListActions | TEditModalActions>
) => {
  dispatch(GetPublisherListRequestAction(config));
  ApplicationService.getApplicationTopicList(config).then(
    (data: { data: any }) => {
      const mapped = data.data.data.content.filter(
        (obj: any, index: any, self: any) =>
          index === self.findIndex((el: any) => el.topic.topicId === obj.topic.topicId)
      );

      // tslint:disable-next-line: ban-ts-ignore
      // @ts-ignore
      dispatch(
        GetPublisherListSuccessAction({
          content: mapped,
          count: mapped.length,
        })
      );
    },
    (error: any) => {
      dispatch(GetPublisherListErrorAction(error));
    }
  );
};
