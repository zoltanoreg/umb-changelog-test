// Type definitions for next-components 3.0.0
// Project:
// Definitions by: Bence Darabos <bence.darabos@blackswan.com>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 3.6.2

/// <reference types="react" />

declare module "next-components" {
  export import NumberInput = __NextComponents.NumberInput;
  export import Input = __NextComponents.Input;
  export import Badge = __NextComponents.Badge;
  export import BadgeVariant = __NextComponents.BadgeVariant;
  export import ButtonIcon = __NextComponents.ButtonIcon;
  export import ButtonPrimary = __NextComponents.ButtonPrimary;
  export import ButtonPrimaryIcon = __NextComponents.ButtonPrimaryIcon;
  export import ButtonSecondary = __NextComponents.ButtonSecondary;
  export import ButtonGhost = __NextComponents.ButtonGhost;
  export import ButtonGhostInverted = __NextComponents.ButtonGhostInverted;
  export import Card = __NextComponents.Card;
  export import Chip = __NextComponents.Chip;
  export import Grid = __NextComponents.Grid;
  export import GridItem = __NextComponents.GridItem;
  export import Text = __NextComponents.Text;
  export import Link = __NextComponents.Link;
  export import Heading = __NextComponents.Heading;
  export import Row = __NextComponents.Row;
  export import SortableHeaderCell = __NextComponents.SortableHeaderCell;
  export import TextCell = __NextComponents.TextCell;
  export import TablePagination = __NextComponents.TablePagination;
  export import OverlayFullscreen = __NextComponents.OverlayFullscreen;
  export import ProgressIndicatorLinear = __NextComponents.ProgressIndicatorLinear;
  export import StackItem = __NextComponents.StackItem;
  export import CloudBar = __NextComponents.CloudBar;
  export import ToolMenu = __NextComponents.ToolMenu;
  export import ToolMenuItem = __NextComponents.ToolMenuItem;
  export import ToolMenuChild = __NextComponents.ToolMenuChild;
  export import SingleDatePicker = __NextComponents.SingleDatePicker;
  export import DateRangePicker = __NextComponents.DateRangePicker;
  export import Dropdown = __NextComponents.Dropdown;
  export import OverlayFullscreen = __NextComponents.OverlayFullscreen;
  export import TabContainer = __NextComponents.TabContainer;
  export import Tab = __NextComponents.Tab;
  export import ProgressIndicatorCircular = __NextComponents.ProgressIndicatorCircular;
  export import ModalInfo = __NextComponents.ModalInfo;
  export import ModalConfirmation = __NextComponents.ModalConfirmation;
  export import ModalBase = __NextComponents.ModalBase;
  export import Breadcrumb = __NextComponents.Breadcrumb;
  export import Tile = __NextComponents.Tile;
  export import Checkbox = __NextComponents.Checkbox;
  export import Textarea = __NextComponents.Textarea;
  export import BatchActionActive = __NextComponents.BatchActionActive;
  export import DataCell = __NextComponents.DataCell;
  export import BatchActionGridBase = __NextComponents.BatchActionGridBase;

  export import IconArrowLeft16 = __NextComponents.IconArrowLeft16;
  export import IconArrowLeft20 = __NextComponents.IconArrowLeft20;
  export import IconArrowLeft24 = __NextComponents.IconArrowLeft24;
  export import IconArrowLeft32 = __NextComponents.IconArrowLeft32;
  export import IconAdd16 = __NextComponents.IconAdd16;
  export import IconAdd20 = __NextComponents.IconAdd20;
  export import IconAdd24 = __NextComponents.IconAdd24;
  export import IconAdd32 = __NextComponents.IconAdd32;
  export import IconHome16 = __NextComponents.IconHome16;
  export import IconHome20 = __NextComponents.IconHome20;
  export import IconHome24 = __NextComponents.IconHome24;
  export import IconHome32 = __NextComponents.IconHome32;
  export import IconInfo16 = __NextComponents.IconInfo16;
  export import IconInfo20 = __NextComponents.IconInfo20;
  export import IconInfo24 = __NextComponents.IconInfo24;
  export import IconInfo32 = __NextComponents.IconInfo32;
  export import IconClose16 = __NextComponents.IconClose16;
  export import IconClose20 = __NextComponents.IconClose20;
  export import IconClose24 = __NextComponents.IconClose24;
  export import IconClose32 = __NextComponents.IconClose32;
  export import IconDelete16 = __NextComponents.IconDelete16;
  export import IconDelete20 = __NextComponents.IconDelete20;
  export import IconDelete24 = __NextComponents.IconDelete24;
  export import IconDelete32 = __NextComponents.IconDelete32;
  export import IconCheckmark16 = __NextComponents.IconCheckmark16;
  export import IconCheckmark20 = __NextComponents.IconCheckmark20;
  export import IconCheckmark24 = __NextComponents.IconCheckmark24;
  export import IconCheckmark32 = __NextComponents.IconCheckmark32;
  export import IconWarning16 = __NextComponents.IconWarning16;
  export import IconWarning20 = __NextComponents.IconWarning20;
  export import IconWarning24 = __NextComponents.IconWarning24;
  export import IconWarning32 = __NextComponents.IconWarning32;
  export import IconNotification16 = __NextComponents.IconNotification16;
  export import IconNotification20 = __NextComponents.IconNotification20;
  export import IconNotification24 = __NextComponents.IconNotification24;
  export import IconNotification32 = __NextComponents.IconNotification32;
  export import IconAvatar16 = __NextComponents.IconAvatar16;
  export import IconAvatar20 = __NextComponents.IconAvatar20;
  export import IconAvatar24 = __NextComponents.IconAvatar24;
  export import IconAvatar32 = __NextComponents.IconAvatar32;
  export import IconSettings16 = __NextComponents.IconSettings16;
  export import IconSettings20 = __NextComponents.IconSettings20;
  export import IconSettings24 = __NextComponents.IconSettings24;
  export import IconSettings32 = __NextComponents.IconSettings32;
  export import IconAppSwitcher16 = __NextComponents.IconAppSwitcher16;
  export import IconAppSwitcher20 = __NextComponents.IconAppSwitcher20;
  export import IconAppSwitcher24 = __NextComponents.IconAppSwitcher24;
  export import IconAppSwitcher32 = __NextComponents.IconAppSwitcher32;
  export import Menu16 = __NextComponents.Menu16;
  export import Menu20 = __NextComponents.Menu20;
  export import Menu24 = __NextComponents.Menu24;
  export import Menu32 = __NextComponents.Menu32;

  export import colors = __NextComponents.colors;
}

declare namespace __NextComponents {
  export class getSize {}

  interface INumberInput {
    disabled?: boolean;
    min?: number;
    max?: number;
    value?: number;
    placeholder?: string;
    onChange(value: number): void;
    step?: number;
  }
  export class NumberInput extends React.Component<HTMLAttributes & INumberInput> {}

  interface IInput {
    disabled?: boolean;
    status?: string;
    icon?: string;
    onChange(event: React.ChangeEvent<HTMLInputElement>): void;
    feedback?: string;
    value?: string;
    readOnly?: boolean;
    className?: string;
    maxLength?: number;
  }
  export class Input extends React.Component<HTMLAttributes & IInput> {}

  // namespace BadgeVariant {
  //   type variant = "default" | "active" | "notification" | "alert";
  // }

  export enum BadgeVariant {
    default = "default",
    active = "active",
    notification = "notification",
    alert = "alert",
  }

  interface IBadge {
    children?: node;
    variant: BadgeVariant;
  }

  export class Badge extends React.Component<HTMLAttributes & IBadge> {}

  namespace ButtonEnum {
    type size = "default" | "small";
  }

  interface IButtonIcon {
    icon: node;
    onClick?(event: React.MouseEvent<HTMLElement>): void;
    size?: ButtonEnum.size;
  }
  export class ButtonIcon extends React.Component<HTMLAttributes & IButtonIcon> {}

  interface IButton {
    onClick?(event: React.MouseEvent<HTMLElement>): void;
    size?: ButtonEnum.size;
    disabled?: boolean;
    children?: Node;
  }
  export class ButtonPrimary extends React.Component<HTMLAttributes & IButton> {}
  export class ButtonPrimaryIcon extends React.Component<HTMLAttributes & IButton> {}
  export class ButtonSecondary extends React.Component<HTMLAttributes & IButton> {}
  export class ButtonGhost extends React.Component<HTMLAttributes & IButton> {}
  export class ButtonGhostInverted extends React.Component<HTMLAttributes & IButton> {}

  interface ICard {
    children?: node;
    image?: string | node;
    title?: string;
    background?: string;
  }
  export class Card extends React.Component<HTMLAttributes & ICard> {}

  interface IChip {
    children?: node;
    onClick?(event: React.MouseEvent<HTMLElement>): void;
    label?: string;
    disabled?: boolean;
  }

  export class Chip extends React.Component<HTMLAttributes & IChip> {}

  namespace GridEnum {
    type autoFlow = "column" | "row";
  }

  interface IGrid {
    children?: node;
    columns?: string;
    rows?: string;
    height?: string;
    alignItems?: string;
    alignSelf?: string;
    justifyItems?: string;
    justifyContent?: string;
    justifyColumns?: string;
    gap?: string;
    gapRow?: string;
    gapColumn?: string;
    templateAreas?: string;
    autoFlow?: GridEnum.autoFlow;
    padding?: string;
    paddingTop?: string;
    paddingRight?: string;
    paddingBottom?: string;
    paddingLeft?: string;
  }
  export class Grid extends React.Component<HTMLAttributes & IGrid> {}

  namespace GridItemEnum {
    type autoFlow = "column" | "row";
  }
  interface IGridItem {
    children?: node;
    column?: string;
    row?: string;
    justifySelf?: string;
    alignSelf?: string;
    area?: string;
  }
  export class GridItem extends React.Component<HTMLAttributes & IGridItem> {}

  namespace TextEnum {
    type variant = "label" | "uiText" | "monoText" | "captionText";
    type align = "left" | "center" | "right";
  }
  interface IText {
    variant: TextEnum.variant;
    color?: string;
    children?: node;
    align?: TextEnum.align;
    ellipsis?: boolean;
  }
  export class Text extends React.Component<HTMLAttributes & IText> {}

  namespace HeadingEnum {
    type headingVariant = "heading1" | "heading2" | "heading3" | "listHeading";
  }
  interface IHeading {
    variant?: HeadingEnum.headingVariant;
    color?: string;
    children?: node;
  }
  export class Heading extends React.Component<HTMLAttributes & IHeading> {}

  // DATA TABLE BASE
  namespace DataTableBaseEnum {
    type direction = "none" | "ascending" | "descending";
    type justifyItems = "start" | "center" | "end";
    type striped = "none" | "even" | "odd";
    type align = "left" | "center" | "right";
  }

  interface IRow {
    columns?: string;
    gap?: string;
    striped?: DataTableBaseEnum.striped;
    selected?: boolean;
    isHeader?: boolean;
    paddingLeft?: string;
    paddingRight?: string;
  }
  export class Row extends React.Component<HTMLAttributes & IRow> {}

  interface ISortableHeaderCell {
    value: string;
    sortDirection?: DataTableBaseEnum.direction;
    justifyItems?: DataTableBaseEnum.justifyItems;
    onClick(event: React.MouseEvent<HTMLElement>): void;
  }
  export class SortableHeaderCell extends React.Component<HTMLAttributes & ISortableHeaderCell> {}

  interface ITextCell {
    value: string;
    justifyItems?: DataTableBaseEnum.justifyItems;
    onClick(): void;
  }
  export class TextCell extends React.Component<HTMLAttributes & ITextCell> {}

  interface ITablePagination {
    itemCount?: number;
    paging?: {
      count?: number;
      page?: number;
    };
    onChangePage(value: number): void;
  }
  export class TablePagination extends React.Component<HTMLAttributes & ITablePagination> {}

  interface IOverlayFullscreen {
    opacity?: number;
    children?: node;
  }
  export class OverlayFullscreen extends React.Component<HTMLAttributes & IOverlayFullscreen> {}

  namespace ProgressIndicatorEnum {
    type size = "default" | "small";
  }

  interface IProgressIndicatorLinear {
    size?: ProgressIndicatorEnum.size;
  }
  export class ProgressIndicatorLinear extends React.Component<
    HTMLAttributes & IProgressIndicatorLinear
  > {}

  interface IStackItem {
    children?: node;
    draggable?: boolean;
    merging?: boolean;
  }
  export class StackItem extends React.Component<HTMLAttributes & IStackItem> {}

  namespace CloudBarEnum {}
  interface ICloudBarUser {
    name?: string;
    profileUrl?: string;
    notificationsUrl?: string;
    settingsUrl?: string;
  }
  interface ICloudBarLastUsedTools {
    name?: string;
    icon?: string;
    path?: string;
    selected?: boolean;
  }

  interface ICloudBarOtherTools {
    name?: string;
    icon?: string;
    path?: string;
    selected?: boolean;
  }

  interface ICloudBar {
    cloudHomeUrl: string;
    user: ICloudBarUser;
    notificationCount?: number;
    lastUsedTools?: Array<ICloudBarLastUsedTools>;
    otherTools?: Array<ICloudBarOtherTools>;
  }
  export class CloudBar extends React.Component<HTMLAttributes & ICloudBar> {}

  interface IToolMenu {
    title?: string;
    children?: node;
    isClosed?: boolean;
    handleClosed(): void;
    handleCloudbar(): void;
  }
  export class ToolMenu extends React.Component<HTMLAttributes & IToolMenu> {}

  interface IToolMenuItem {
    as?: any;
    icon?: string;
    title?: string;
    children?: node;
    isActive?: boolean;
    href?: string;
    onClick?(event: React.MouseEvent<HTMLElement>, target: string): void;
  }
  export class ToolMenuItem extends React.Component<HTMLAttributes & IToolMenuItem> {}

  interface IToolMenuChild {
    as?: any;
    icon?: string;
    title?: string;
    children?: node;
    isActive?: boolean;
    href?: string;
    onToggle?(event: React.MouseEvent<HTMLElement>, target: string): void;
    onClick?(event: React.MouseEvent<HTMLElement>, target: string): void;
  }
  export class ToolMenuChild extends React.Component<HTMLAttributes & IToolMenuChild> {}

  namespace DayOfWeekEnum {
    type day = 0 | 1 | 2 | 3 | 4 | 5 | 6;
  }
  interface ISingleDatePicker {
    disabled?: boolean;
    placeholder?: string;
    numberOfMonths?: number;
    date?: object;
    onDateChange?(): void;
    displayFormat?: string;
    firstDayOfWeek?: DayOfWeekEnum.day;
    enableOutsideDays?: boolean;
    isDayBlocked?(): void;
    isOutsideRange?(): void;
    isDayHighlighted?(): void;
    onClick?(event: React.MouseEvent<HTMLElement>, target: string): void;
  }
  export class SingleDatePicker extends React.Component<HTMLAttributes & ISingleDatePicker> {}

  interface IDateRangePicker {
    startDatePlaceholderText?: string;
    endDatePlaceholderText?: string;
    startDate?: object;
    endDate?: object;
    disabled?: boolean;
    placeholder?: string;
    numberOfMonths?: number;
    date?: object;
    onDatesChange(): void;
    displayFormat?: string;
    firstDayOfWeek?: DayOfWeekEnum.day;
    enableOutsideDays?: boolean;
    isDayBlocked?(): void;
    isOutsideRange?(): void;
    isDayHighlighted?(): void;
    onClick?(event: React.MouseEvent<HTMLElement>, target: string): void;
  }
  export class DateRangePicker extends React.Component<HTMLAttributes & IDateRangePicker> {}

  namespace DropdownEnum {
    type size = "default" | "small";
  }
  interface IDropdownOptions {
    label: Node;
    value: any;
  }
  interface IDropdown {
    size?: dropdownEnum.size;
    label?: string;
    options?: IDropdownOptions[];
    onChange?(): void;
    onFilter?(): void;
    filterLabel?: string;
    selected?: any;
    disabled?: boolean;
    tabIndex?: number;
  }
  export class Dropdown extends React.Component<HTMLAttributes & IDropdown> {}

  interface IOverlayFullscreen {
    opacity?: number;
    children?: Node;
  }
  export class OverlayFullscreen extends React.Component<HTMLAttributes & IOverlayFullscreen> {}

  interface ITabContainer {
    children?: Node;
  }
  export class TabContainer extends React.Component<HTMLAttributes & ITabContainer> {}

  interface ITab {
    active?: boolean;
    children?: Node;
    badgeContent?: Node;
  }
  export class Tab extends React.Component<HTMLAttributes & ITab> {}

  namespace ProgressIndicatorEnum {
    type size = "default" | "small";
  }

  interface IProgressIndicatorCircular {
    size?: ProgressIndicatorEnum.size;
  }
  export class ProgressIndicatorCircular extends React.Component<
    HTMLAttributes & IProgressIndicatorCircular
  > {}

  interface IModalInfo {
    label?: string;
    title: string | {};
    text: string;
    onClose?(): void;
  }
  export class ModalInfo extends React.Component<HTMLAttributes & IModalInfo> {}

  interface IModalConfirmation {
    confirmText?: string;
    icon?: Node;
    iconColor?: string;
    text: {};
    title: {};
    onCancel(): void;
    onConfirm(): void;
  }

  export class ModalConfirmation extends React.Component<HTMLAttributes & IModalConfirmation> {}

  interface IModalBase {
    children?: Node;
  }
  export class ModalBase extends React.Component<HTMLAttributes & IModalBase> {}

  interface IBreadcrumbLink {
    as: string;
    href: string;
    title: string;
    active: boolean;
  }

  interface IBreadcrumb {
    links: IBreadcrumbLink[];
  }
  export class Breadcrumb extends React.Component<HTMLAttributes & IBreadcrumb> {}

  namespace TileElevationEnum {
    type elevation = "none" | "small" | "medium";
  }

  interface ITile {
    children: Node;
    elevation?: TileElevationEnum.elevation;
    bgColor?: string;
  }
  export class Tile extends React.Component<HTMLAttributes & ITile> {}

  namespace CheckboxOptions {
    type marked = "unselected" | "selected" | "semi";
  }

  interface ICheckbox {
    disabled?: boolean;
    marked?: CheckboxOptions.marked;
  }
  export class Checkbox extends React.Component<HTMLAttributes & ICheckbox> {}

  interface ITextarea {
    placeholder?: string;
    disabled?: boolean;
    readOnly?: boolean;
    value?: string;
    onChange?(): void;
  }
  export class Textarea extends React.Component<HTMLAttributes & ITextarea> {}

  export class BatchActionActive extends React.Component<HTMLAttributes> {}
  export class BatchActionGridBase extends React.Component<HTMLAttributes> {}

  export class DataCell extends React.Component<HTMLAttributes> {}

  export let IconArrowLeft16: IconArrowLeft16;
  export let IconArrowLeft20: IconArrowLeft20;
  export let IconArrowLeft24: IconArrowLeft24;
  export let IconArrowLeft32: IconArrowLeft32;

  export let IconAdd16: IconAdd16;
  export let IconAdd20: IconAdd20;
  export let IconAdd24: IconAdd24;
  export let IconAdd32: IconAdd32;

  export let IconHome16: IconHome16;
  export let IconHome20: IconHome20;
  export let IconHome24: IconHome24;
  export let IconHome32: IconHome32;

  export let IconInfo16: IconInfo16;
  export let IconInfo20: IconInfo20;
  export let IconInfo24: IconHome24;
  export let IconInfo32: IconInfo32;

  export let IconClose16: IconClose16;
  export let IconClose20: IconClose20;
  export let IconClose24: IconClose24;
  export let IconClose32: IconClose32;

  export let IconNotification16: IconNotification16;
  export let IconNotification20: IconNotification20;
  export let IconNotification24: IconNotification24;
  export let IconNotification32: IconNotification32;

  export let IconAvatar16: IconAvatar16;
  export let IconAvatar20: IconAvatar20;
  export let IconAvatar24: IconAvatar24;
  export let IconAvatar32: IconAvatar32;

  export let IconSettings16: IconSettings16;
  export let IconSettings20: IconSettings20;
  export let IconSettings24: IconSettings24;
  export let IconSettings32: IconSettings32;

  export let IconAppSwitcher16: IconAppSwitcher16;
  export let IconAppSwitcher20: IconAppSwitcher20;
  export let IconAppSwitcher24: IconAppSwitcher24;
  export let IconAppSwitcher32: IconAppSwitcher32;

  export let IconCheckmark16: IconCheckmark16;
  export let IconCheckmark20: IconCheckmark20;
  export let IconCheckmark24: IconCheckmark24;
  export let IconCheckmark32: IconCheckmark32;

  export let IconWarning16: IconWarning16;
  export let IconWarning20: IconWarning20;
  export let IconWarning24: IconWarning24;
  export let IconWarning32: IconWarning32;

  export let Menu16: Menu16;
  export let Menu20: Menu20;
  export let Menu24: Menu24;
  export let Menu32: Menu32;

  interface colors {
    primary: {
      default: string;
      darken10: string;
      darken20: string;
      darken30: string;
      lighten10: string;
      lighten20: string;
      lighten30: string;
      sl1: string;
      sl2: string;
      sl3: string;
    };
    secondary: {
      default: string;
      darken10: string;
      darken20: string;
      darken30: string;
      lighten10: string;
      lighten20: string;
      lighten30: string;
    };
    tertiary1: {
      default: string;
      darken10: string;
      lighten10: string;
    };
    tertiary2: {
      default: string;
      darken10: string;
      lighten10: string;
    };
    tertiary3: {
      default: string;
      darken10: string;
      lighten10: string;
    };
    notification: {
      default: string;
      darken10: string;
      lighten10: string;
    };
    error: {
      default: string;
      darken10: string;
      lighten10: string;
    };
    success: {
      default: string;
      darken10: string;
      lighten10: string;
    };
    greys: {
      default: string;
      darken10: string;
      darken20: string;
      darken30: string;
      lighten20: string;
      lighten25: string;
      lighten30: string;
    };
    general: {
      white: string;
      black: string;
    };
  }
  export let colors: colors;
}
