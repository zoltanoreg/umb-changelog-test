import Sinon from "sinon";

import * as brokerTypeListActions from "../../../../actions/broker-type/get-filtered-list/actions";
import { BrokerTypeListType } from "../../../../actions/broker-type/get-filtered-list/types";
import { BrokerTypeService } from "../../../../services/BrokerTypeService";

describe("brokerTypeListActions/", () => {
  it('should have the property  "type"', () => {
    const actions = [
      brokerTypeListActions.brokerTypeListErrorAction,
      brokerTypeListActions.brokerTypeListRequestAction,
      brokerTypeListActions.brokerTypeListSuccessAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("brokerTypeList should be a promise", () => {
    expect(brokerTypeListActions.getBrokerTypeList).toBeDefined();
  });

  it("test dispatch", async () => {
    const conf = {
      filter: {},
      limit: 0,
      orderBy: "test",
      orderType: "test",
      page: 0,
    };
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(brokerTypeListActions.getBrokerTypeList(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "GET_BROKERTYPE_LIST_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getBrokerTypeList", () => {
    let brokerTypeServiceStub: any;
    const dispatch = jest.fn();
    const listObj = { data: { data: [] } };
    const expectedAction = [{ payload: listObj.data.data, type: BrokerTypeListType.SUCCESS }];

    beforeAll(() => {
      brokerTypeServiceStub = Sinon.stub(BrokerTypeService, "getBrokerTypeList").callsFake(
        (config: any) => Promise.resolve(listObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await brokerTypeListActions.getBrokerTypeList({} as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      brokerTypeServiceStub.restore();
    });
  });

  describe("getBrokerTypeList", () => {
    let brokerTypeServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedActions = [{ payload: errorObj, type: BrokerTypeListType.ERROR }];

    beforeAll(() => {
      brokerTypeServiceStub = Sinon.stub(BrokerTypeService, "getBrokerTypeList").callsFake(
        (config: any) => Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        brokerTypeListActions.getBrokerTypeList({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedActions);
      }
    });

    afterAll(() => {
      brokerTypeServiceStub.restore();
    });
  });
});
