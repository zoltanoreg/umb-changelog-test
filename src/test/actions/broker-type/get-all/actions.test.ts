import Sinon from "sinon";

import * as allBrokerTypeListActions from "../../../../actions/broker-type/get-all/actions";
import { AllBrokerTypeListType } from "../../../../actions/broker-type/get-all/types";
import { IBrokerTypeListingTableConfig } from "../../../../models/IBrokerTypeListingTableConfig";
import { BrokerTypeService } from "../../../../services/BrokerTypeService";

describe("#getAllBrokerType", () => {
  describe("#allBrokerTypeListRequestAction", () => {
    it("should exist", () => {
      expect(allBrokerTypeListActions.allBrokerTypeListRequestAction()).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(allBrokerTypeListActions.allBrokerTypeListRequestAction().type).toEqual(
        AllBrokerTypeListType.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(allBrokerTypeListActions.allBrokerTypeListRequestAction().payload).not.toBeDefined();
    });
  });

  describe("#allBrokerTypeListSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(
        allBrokerTypeListActions.allBrokerTypeListSuccessAction(responseOnSuccess)
      ).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(
        allBrokerTypeListActions.allBrokerTypeListSuccessAction(responseOnSuccess).type
      ).toEqual(AllBrokerTypeListType.SUCCESS);
    });

    it("should not have a property 'payload'", () => {
      expect(
        allBrokerTypeListActions.allBrokerTypeListSuccessAction(responseOnSuccess).payload
      ).toEqual(responseOnSuccess);
    });
  });

  describe("#allBrokerTypeListErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(allBrokerTypeListActions.allBrokerTypeListErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(allBrokerTypeListActions.allBrokerTypeListErrorAction(responseOnError).type).toEqual(
        AllBrokerTypeListType.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(
        allBrokerTypeListActions.allBrokerTypeListErrorAction(responseOnError).payload
      ).toEqual(responseOnError);
    });
  });

  describe("#getAllBrokerType", () => {
    let config: IBrokerTypeListingTableConfig;

    beforeAll(() => {
      config = {
        filter: {},
        limit: 0,
        orderBy: "brokerTypeName",
        orderType: "ASC",
        page: 0,
      };
    });

    it("should exist", () => {
      expect(allBrokerTypeListActions.getAllBrokerType(config)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(allBrokerTypeListActions.getAllBrokerType(config)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "GET_ALLBROKERTYPE_LIST_REQUEST" });
      expect(dispatch).toBeCalledTimes(1);
    });
  });

  describe("getAllBrokerType", () => {
    let brokerTypeServiceStub: any;
    const dispatch = jest.fn();
    const listObj = { data: { data: {} } };
    const expectedAction = [{ payload: listObj.data.data, type: AllBrokerTypeListType.SUCCESS }];

    beforeAll(() => {
      brokerTypeServiceStub = Sinon.stub(BrokerTypeService, "getBrokerTypeList").callsFake(
        (config: any) => Promise.resolve(listObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await allBrokerTypeListActions.getAllBrokerType({} as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      brokerTypeServiceStub.restore();
    });
  });

  describe("getAllBrokerType", () => {
    let brokerTypeServiceStub: any;
    const dispatch = jest.fn();
    const expectedAction = [{ payload: {}, type: AllBrokerTypeListType.ERROR }];

    beforeAll(() => {
      brokerTypeServiceStub = Sinon.stub(BrokerTypeService, "getBrokerTypeList").callsFake(
        (config: any) => Promise.reject({} as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        allBrokerTypeListActions.getAllBrokerType({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      brokerTypeServiceStub.restore();
    });
  });
});
