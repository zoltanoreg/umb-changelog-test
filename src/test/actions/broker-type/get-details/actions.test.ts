import Sinon from "sinon";

import * as getBrokerTypeDetailsActions from "../../../../actions/broker-type/get-details/actions";
import { TGetBrokerTypeDetails } from "../../../../actions/broker-type/get-details/types";
import { BrokerTypeService } from "../../../../services/BrokerTypeService";

describe("getBrokerTypeDetailsActions/", () => {
  it('should have the property  "type"', () => {
    const actions = [
      getBrokerTypeDetailsActions.getBrokerTypeDetailsErrorAction,
      getBrokerTypeDetailsActions.getBrokerTypeDetailsRequestAction,
      getBrokerTypeDetailsActions.getBrokerTypeDetailsSuccessAction,
    ];
    actions.forEach(action => {
      expect(action({})).toHaveProperty("type");
    });
  });

  it("getBrokerTypeDetails should be a promise", () => {
    expect(getBrokerTypeDetailsActions.getBrokerTypeDetails).toBeDefined();
  });

  it("test dispatch", async () => {
    const brokerTypeId = "test";
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getBrokerTypeDetailsActions.getBrokerTypeDetails(brokerTypeId)(
      dispatch
    ) as any);
    expect(dispatch).toBeCalledWith({ type: "BROKER_TYPE_GETDETAILS_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getBrokerTypeDetails", () => {
    let getBrokerTypeDetailsStub: any;
    const dispatch = jest.fn();
    const detailObj = { data: {} };
    const expectedAction = [{ payload: detailObj.data, type: TGetBrokerTypeDetails.SUCCESS }];

    beforeAll(() => {
      getBrokerTypeDetailsStub = Sinon.stub(BrokerTypeService, "getBrokerTypeDetails").callsFake(
        (config: any) => Promise.resolve(detailObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await getBrokerTypeDetailsActions.getBrokerTypeDetails("testId")(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      getBrokerTypeDetailsStub.restore();
    });
  });

  describe("getBrokerTypeDetails", () => {
    let getBrokerTypeDetailsStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: TGetBrokerTypeDetails.ERROR }];

    beforeAll(() => {
      getBrokerTypeDetailsStub = Sinon.stub(BrokerTypeService, "getBrokerTypeDetails").callsFake(
        (config: any) => Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", async () => {
      // tslint:disable-next-line
      await getBrokerTypeDetailsActions.getBrokerTypeDetails("testId")(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      getBrokerTypeDetailsStub.restore();
    });
  });
});
