import Sinon from "sinon";

import * as topologyActions from "../../../actions/topology/actions";
import { GetTopologyListType } from "../../../actions/topology/types";
import { OrderType } from "../../../models/ITableConfig";
import { ITopologyTableConfig } from "../../../models/ITopologyConfig";
import { TopologyService } from "../../../services/TopologyService";

describe("topology action", () => {
  describe("#topologyListRequestAction", () => {
    it("should exist", () => {
      expect(topologyActions.topologyListRequestAction()).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(topologyActions.topologyListRequestAction().type).toEqual(GetTopologyListType.REQUEST);
    });

    it("should not have a property 'payload'", () => {
      expect(topologyActions.topologyListRequestAction().payload).toEqual(undefined);
    });
  });

  describe("#topologyListSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(topologyActions.topologyListSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(topologyActions.topologyListSuccessAction(responseOnSuccess).type).toEqual(
        GetTopologyListType.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(topologyActions.topologyListSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#topologyListErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(topologyActions.topologyListErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(topologyActions.topologyListErrorAction(responseOnError).type).toEqual(
        GetTopologyListType.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(topologyActions.topologyListErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#getTopologies", () => {
    const config: ITopologyTableConfig = {
      limit: 10,
      orderBy: "brokerTopologyName",
      orderType: OrderType.ASC,
      page: 0,
    };

    it("should exist", () => {
      expect(topologyActions.getTopologies(config)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(topologyActions.getTopologies(config)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: GetTopologyListType.REQUEST });
      expect(dispatch).toBeCalledTimes(1);
    });

    describe("returns with success", () => {
      let topologyServiceStub: any;
      const dispatch = jest.fn();
      const payloadObj = {
        content: {},
      };
      const topologyObj = {
        data: payloadObj,
      };
      const expectedAction = [{ payload: payloadObj, type: GetTopologyListType.SUCCESS }];

      beforeAll(() => {
        topologyServiceStub = Sinon.stub(TopologyService, "getTopologies").callsFake(
          (stubConfig: ITopologyTableConfig) => Promise.resolve(topologyObj as any)
        );
      });

      it("should call dispatch", async () => {
        // tslint:disable-next-line
        await topologyActions.getTopologies(config)(dispatch);
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      });

      afterAll(() => {
        topologyServiceStub.restore();
      });
    });

    describe("returns with error", () => {
      let topologyServiceStub: any;
      const dispatch = jest.fn();
      const errorObj = {};
      const expectedAction = [{ payload: errorObj, type: GetTopologyListType.ERROR }];

      beforeAll(() => {
        topologyServiceStub = Sinon.stub(TopologyService, "getTopologies").callsFake(
          (stubConfig: ITopologyTableConfig) => Promise.reject(errorObj as any)
        );
      });

      it("should call error dispatch", () => {
        try {
          topologyActions.getTopologies(config)(dispatch);
        } catch (e) {
          expect(dispatch.mock.calls).toContainEqual(expectedAction);
        }
      });

      afterAll(() => {
        topologyServiceStub.restore();
      });
    });
  });
});
