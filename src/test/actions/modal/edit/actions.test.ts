import { editModalCloseAction, editModalOpenAction } from "../../../../actions/modal/edit/actions";

describe("popupActions/", () => {
  it('should have the property  "type"', () => {
    const actions = [editModalCloseAction, editModalOpenAction];
    actions.forEach(action => {
      expect(action({})).toHaveProperty("type");
    });
  });
});
