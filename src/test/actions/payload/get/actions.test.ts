import Sinon from "sinon";

import * as getPayloadActions from "../../../../actions/payload/get/actions";
import { GetPayloadTypes } from "../../../../actions/payload/get/types";
import { TopicService } from "../../../../services/TopicService";

describe("#getPayload", () => {
  describe("#GetPayloadRequestAction", () => {
    it("should exist", () => {
      expect(getPayloadActions.GetPayloadRequestAction({})).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(getPayloadActions.GetPayloadRequestAction({}).type).toEqual(GetPayloadTypes.REQUEST);
    });

    it("should not have a property 'payload'", () => {
      expect(getPayloadActions.GetPayloadRequestAction({}).payload).toEqual({});
    });
  });

  describe("#GetPayloadSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(getPayloadActions.GetPayloadSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(getPayloadActions.GetPayloadSuccessAction(responseOnSuccess).type).toEqual(
        GetPayloadTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(getPayloadActions.GetPayloadSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#GetPayloadErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(getPayloadActions.GetPayloadErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(getPayloadActions.GetPayloadErrorAction(responseOnError).type).toEqual(
        GetPayloadTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(getPayloadActions.GetPayloadErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#getPayload", () => {
    const cb = jest.fn();
    const config = {};

    it("should exist", () => {
      expect(getPayloadActions.getPayload(config)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(getPayloadActions.getPayload(config, cb)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "GET_PAYLOAD_REQUEST", payload: config });
      expect(dispatch).toBeCalledTimes(1);
    });
  });

  describe("getPayload", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();
    const payloadObj = {
      data: {
        data: {
          content: {},
        },
      },
    };

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getPayload").callsFake((config: any) =>
        Promise.resolve(payloadObj as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await getPayloadActions.getPayload({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("getPayload", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getPayload").callsFake((config: any) =>
        Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        getPayloadActions.getPayload({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
