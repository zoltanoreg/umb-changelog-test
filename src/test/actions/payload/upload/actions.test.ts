import Sinon from "sinon";

import * as uploadPayloadActions from "../../../../actions/payload/upload/actions";
import { UploadPayloadTypes } from "../../../../actions/payload/upload/types";
import { TopicService } from "../../../../services/TopicService";

describe("#uploadPayload", () => {
  describe("#uploadPayloadRequestAction", () => {
    it("should exist", () => {
      expect(uploadPayloadActions.uploadPayloadRequestAction({})).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(uploadPayloadActions.uploadPayloadRequestAction({}).type).toEqual(
        UploadPayloadTypes.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(uploadPayloadActions.uploadPayloadRequestAction({}).payload).toEqual({});
    });
  });

  describe("#uploadPayloadSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(uploadPayloadActions.uploadPayloadSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(uploadPayloadActions.uploadPayloadSuccessAction(responseOnSuccess).type).toEqual(
        UploadPayloadTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(uploadPayloadActions.uploadPayloadSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#uploadPayloadErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(uploadPayloadActions.uploadPayloadErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(uploadPayloadActions.uploadPayloadErrorAction(responseOnError).type).toEqual(
        UploadPayloadTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(uploadPayloadActions.uploadPayloadErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#uploadPayload", () => {
    const cb = jest.fn();
    const config = {};

    it("should exist", () => {
      expect(uploadPayloadActions.uploadPayload(config)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(uploadPayloadActions.uploadPayload(config, cb)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "UPLOAD_PAYLOAD_REQUEST", payload: config });
      expect(dispatch).toBeCalledTimes(1);
    });
  });

  describe("uploadPayload", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();
    const payloadObj = {
      data: {
        data: {
          content: {},
        },
      },
    };

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "uploadPayload").callsFake((config: any) =>
        Promise.resolve(payloadObj as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await uploadPayloadActions.uploadPayload({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("uploadPayload", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "uploadPayload").callsFake((config: any) =>
        Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        uploadPayloadActions.uploadPayload({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
