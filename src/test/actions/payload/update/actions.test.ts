import Sinon from "sinon";

import * as updatePayloadActions from "../../../../actions/payload/update/actions";
import { UpdatePayloadTypes } from "../../../../actions/payload/update/types";
import { TopicService } from "../../../../services/TopicService";

describe("#updatePayload", () => {
  describe("#updatePayloadRequestAction", () => {
    it("should exist", () => {
      expect(updatePayloadActions.updatePayloadRequestAction({})).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(updatePayloadActions.updatePayloadRequestAction({}).type).toEqual(
        UpdatePayloadTypes.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(updatePayloadActions.updatePayloadRequestAction({}).payload).toEqual({});
    });
  });

  describe("#updatePayloadSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(updatePayloadActions.updatePayloadSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(updatePayloadActions.updatePayloadSuccessAction(responseOnSuccess).type).toEqual(
        UpdatePayloadTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(updatePayloadActions.updatePayloadSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#updatePayloadErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(updatePayloadActions.updatePayloadErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(updatePayloadActions.updatePayloadErrorAction(responseOnError).type).toEqual(
        UpdatePayloadTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(updatePayloadActions.updatePayloadErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#updatePayload", () => {
    const cb = jest.fn();
    const config = {};

    it("should exist", () => {
      expect(updatePayloadActions.updatePayload(config, cb)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(updatePayloadActions.updatePayload(config, cb)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "UPDATE_PAYLOAD_REQUEST", payload: config });
      expect(dispatch).toBeCalledTimes(1);
    });
  });

  describe("updatePayload", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();
    const payloadObj = {
      data: {
        data: {
          content: {},
        },
      },
    };

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "updatePayload").callsFake((config: any) =>
        Promise.resolve(payloadObj as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await updatePayloadActions.updatePayload({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("updatePayload", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();
    const payloadObj = {
      data: {
        data: {
          content: {},
        },
      },
    };

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "updatePayload").callsFake((config: any) =>
        Promise.resolve(payloadObj as any)
      );
    });

    it("should not call callback", async () => {
      // tslint:disable-next-line
      await updatePayloadActions.updatePayload({})(dispatch);
      expect(callbackMockFn).not.toBeCalled();
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("updatePayload", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "updatePayload").callsFake((config: any) =>
        Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        updatePayloadActions.updatePayload({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
