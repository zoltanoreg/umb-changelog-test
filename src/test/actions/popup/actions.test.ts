import { popupCloseAction, popupOpenAction } from "../../../actions/popup/actions";

describe("popupActions/", () => {
  it('should have the property  "type"', () => {
    const actions = [popupCloseAction, popupOpenAction];
    actions.forEach(action => {
      expect(action({})).toHaveProperty("type");
    });
  });
});
