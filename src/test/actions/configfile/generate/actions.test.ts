import Sinon from "sinon";

import * as getConfigGenerateActions from "../../../../actions/configfile/generate/actions";
import { GetConfigGenerateType } from "../../../../actions/configfile/generate/types";
import { ConfigService } from "../../../../services/ConfigService";

describe("ConfigGenerateActions/", () => {
  it('should have the property  "type"', () => {
    const actions = [
      getConfigGenerateActions.getConfigGenerateRequestAction,
      getConfigGenerateActions.getConfigGenerateSuccessAction,
      getConfigGenerateActions.getConfigGenerateErrorAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("generateConfig should be a promise", () => {
    expect(getConfigGenerateActions.generateConfig).toBeDefined();
  });

  it("getConfigGenerateRequestAction should be defined", () => {
    getConfigGenerateActions.generateConfig();
    expect(getConfigGenerateActions.getConfigGenerateRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getConfigGenerateActions.generateConfig()(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "GENERATE_CONFIG_REQUEST" });
  });

  describe("generateConfig", () => {
    let configServiceStub: any;
    const dispatch = jest.fn();
    const listObj = { data: { data: {} } };
    const expectedAction = [{ payload: listObj.data.data, type: GetConfigGenerateType.SUCCESS }];

    beforeAll(() => {
      configServiceStub = Sinon.stub(ConfigService, "generateConfig").callsFake(() =>
        Promise.resolve(listObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await getConfigGenerateActions.generateConfig()(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      configServiceStub.restore();
    });
  });

  describe("generateConfig", () => {
    let configServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: GetConfigGenerateType.ERROR }];

    beforeAll(() => {
      configServiceStub = Sinon.stub(ConfigService, "generateConfig").callsFake(() =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        getConfigGenerateActions.generateConfig()(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      configServiceStub.restore();
    });
  });
});
