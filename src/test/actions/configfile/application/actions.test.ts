import Sinon from "sinon";

import * as getConfigApplicationActions from "../../../../actions/configfile/application/actions";
import { GetConfigApplicationType } from "../../../../actions/configfile/application/types";
import { ConfigService } from "../../../../services/ConfigService";

describe("ConfigApplicationActions/", () => {
  const conf = {};

  it('should have the property  "type"', () => {
    const actions = [
      getConfigApplicationActions.getConfigApplicationRequestAction,
      getConfigApplicationActions.getConfigApplicationSuccessAction,
      getConfigApplicationActions.getConfigApplicationErrorAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("getConfigApplication should be a promise", () => {
    expect(getConfigApplicationActions.getConfigApplication).toBeDefined();
  });

  it("getConfigApplicationRequestAction should be defined", () => {
    getConfigApplicationActions.getConfigApplication(conf);
    expect(getConfigApplicationActions.getConfigApplicationRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getConfigApplicationActions.getConfigApplication(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "GET_CONFIG_APPLICATION_REQUEST" });
  });

  describe("getConfigApplication", () => {
    let configServiceStub: any;
    const dispatch = jest.fn();
    const listObj = { data: { data: {} } };
    const expectedAction = [{ payload: listObj.data.data, type: GetConfigApplicationType.SUCCESS }];

    beforeAll(() => {
      configServiceStub = Sinon.stub(ConfigService, "getConfigApplication").callsFake(
        (config: any) => Promise.resolve(listObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await getConfigApplicationActions.getConfigApplication({} as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      configServiceStub.restore();
    });
  });

  describe("getConfigApplication", () => {
    let configServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: GetConfigApplicationType.ERROR }];

    beforeAll(() => {
      configServiceStub = Sinon.stub(ConfigService, "getConfigApplication").callsFake(
        (config: any) => Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        getConfigApplicationActions.getConfigApplication({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      configServiceStub.restore();
    });
  });
});
