import Sinon from "sinon";

import * as getConfigBrokerActions from "../../../../actions/configfile/broker/actions";
import { GetConfigBrokerType } from "../../../../actions/configfile/broker/types";
import { ConfigService } from "../../../../services/ConfigService";

describe("ConfigBrokerActions/", () => {
  const conf = {};

  it('should have the property  "type"', () => {
    const actions = [
      getConfigBrokerActions.getConfigBrokerRequestAction,
      getConfigBrokerActions.getConfigBrokerSuccessAction,
      getConfigBrokerActions.getConfigBrokerErrorAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("getConfigBroker should be a promise", () => {
    expect(getConfigBrokerActions.getConfigBroker).toBeDefined();
  });

  it("getConfigBrokerRequestAction should be defined", () => {
    getConfigBrokerActions.getConfigBroker(conf);
    expect(getConfigBrokerActions.getConfigBrokerRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getConfigBrokerActions.getConfigBroker(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "GET_CONFIG_BROKER_REQUEST" });
  });

  describe("getConfigBroker", () => {
    let configServiceStub: any;
    const dispatch = jest.fn();
    const listObj = { data: { data: {} } };
    const expectedAction = [{ payload: listObj.data.data, type: GetConfigBrokerType.SUCCESS }];

    beforeAll(() => {
      configServiceStub = Sinon.stub(ConfigService, "getConfigBroker").callsFake((config: any) =>
        Promise.resolve(listObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await getConfigBrokerActions.getConfigBroker({} as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      configServiceStub.restore();
    });
  });

  describe("getConfigBroker", () => {
    let configServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: GetConfigBrokerType.ERROR }];

    beforeAll(() => {
      configServiceStub = Sinon.stub(ConfigService, "getConfigBroker").callsFake((config: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        getConfigBrokerActions.getConfigBroker({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      configServiceStub.restore();
    });
  });
});
