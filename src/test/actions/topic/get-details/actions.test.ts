import Sinon from "sinon";

import * as getTopicDetailsActions from "../../../../actions/topic/get-details/actions";
import { TGetTopicDetails } from "../../../../actions/topic/get-details/types";
import { TopicService } from "../../../../services/TopicService";

describe("getTopicDetailsActions/", () => {
  it('should have the property  "type"', () => {
    const actions = [
      getTopicDetailsActions.getTopicDetailsErrorAction,
      getTopicDetailsActions.getTopicDetailsRequestAction,
      getTopicDetailsActions.getTopicDetailsSuccessAction,
    ];
    actions.forEach(action => {
      expect(action({})).toHaveProperty("type");
    });
  });

  it("getTopicDetails should be a promise", () => {
    expect(getTopicDetailsActions.getTopicDetails).toBeDefined();
  });

  it("test dispatch", async () => {
    const topicId = "test";
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getTopicDetailsActions.getTopicDetails(topicId)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "TOPIC_GETDETAILS_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getTopicDetails", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const topicObj = { data: {} };
    const expectedAction = [{ payload: topicObj.data, type: TGetTopicDetails.SUCCESS }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getTopicDetails").callsFake((topicId: any) =>
        Promise.resolve(topicObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await getTopicDetailsActions.getTopicDetails({} as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("getTopicDetails", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: TGetTopicDetails.ERROR }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getTopicDetails").callsFake((topicId: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        getTopicDetailsActions.getTopicDetails({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toEqual(expectedAction);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
