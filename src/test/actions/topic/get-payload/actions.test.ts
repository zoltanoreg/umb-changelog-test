import Sinon from "sinon";

import * as getTopicPayloadActions from "../../../../actions/topic/get-payload/actions";
import { GetTopicPayloadTypes } from "../../../../actions/topic/get-payload/types";
import { TopicService } from "../../../../services/TopicService";

describe("TopicPayloadActions/", () => {
  const conf = {};

  it('should have the property  "type"', () => {
    const actions = [
      getTopicPayloadActions.GetTopicPayloadRequestAction,
      getTopicPayloadActions.GetTopicPayloadSuccessAction,
      getTopicPayloadActions.GetTopicPayloadErrorAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("getTopicPayload should be a promise", () => {
    expect(getTopicPayloadActions.getTopicPayload).toBeDefined();
  });

  it("GetTopicPayloadRequestAction should be defined", () => {
    getTopicPayloadActions.getTopicPayload(conf);
    expect(getTopicPayloadActions.GetTopicPayloadRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getTopicPayloadActions.getTopicPayload(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "GET_TOPICPAYLOAD_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getTopicPayload", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const topicObj = {
      data: {
        data: {},
      },
    };
    const expectedAction = [{ payload: topicObj.data.data, type: GetTopicPayloadTypes.SUCCESS }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getPayload").callsFake((config: any) =>
        Promise.resolve(topicObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await getTopicPayloadActions.getTopicPayload({})(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("getTopicPayload", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: GetTopicPayloadTypes.ERROR }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getPayload").callsFake((config: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        getTopicPayloadActions.getTopicPayload({})(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
