import Sinon from "sinon";

import * as appTopicListActions from "../../../../actions/topic/get-clients/actions";
import { TopicClientListType } from "../../../../actions/topic/get-clients/types";
import { ITopicListingTableConfig } from "../../../../models/ITopicConfig";
import { TopicService } from "../../../../services/TopicService";

describe("getTopicClientList/", () => {
  const conf = {
    filter: {
      key: "test",
    },
    key: "test",
    limit: 2,
    orderBy: "test",
    orderType: "test",
    page: 2,
  };

  it('should have the property  "type"', () => {
    const actions = [
      appTopicListActions.appTopicListRequestAction,
      appTopicListActions.appTopicListSuccessAction,
      appTopicListActions.appTopicListErrorAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("getTopicClientList should be a promise", () => {
    expect(appTopicListActions.getTopicClientList).toBeDefined();
  });

  it("appTopicListRequestAction should be defined", () => {
    appTopicListActions.getTopicClientList(conf);
    expect(appTopicListActions.appTopicListRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(appTopicListActions.getTopicClientList(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "TOPIC_CLIENT_LIST_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getTopicClientList", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const topicObj = {
      data: {
        data: {
          content: {},
        },
      },
    };
    const expectedAction = [{ payload: topicObj.data.data, type: TopicClientListType.SUCCESS }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getTopicClientList").callsFake((config: any) =>
        Promise.resolve(topicObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await appTopicListActions.getTopicClientList({} as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("getTopicClientList", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: TopicClientListType.ERROR }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getTopicClientList").callsFake((config: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        appTopicListActions.getTopicClientList({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
