import Sinon from "sinon";

import * as editTopicActions from "../../../../actions/topic/edit/actions";
import { EditTopicTypes } from "../../../../actions/topic/edit/types";
import { TopicService } from "../../../../services/TopicService";

describe("#editTopic", () => {
  describe("#EditTopicRequestAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(editTopicActions.EditTopicRequestAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(editTopicActions.EditTopicRequestAction(responseOnSuccess).type).toEqual(
        EditTopicTypes.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(editTopicActions.EditTopicRequestAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#EditTopicSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(editTopicActions.EditTopicSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(editTopicActions.EditTopicSuccessAction(responseOnSuccess).type).toEqual(
        EditTopicTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(editTopicActions.EditTopicSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#EditTopicErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(editTopicActions.EditTopicErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(editTopicActions.EditTopicErrorAction(responseOnError).type).toEqual(
        EditTopicTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(editTopicActions.EditTopicErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#editTopic", () => {
    let config: any;
    let id: any;
    const cb = jest.fn();
    beforeAll(() => {
      config = {};
      id = "test";
    });

    it("should exist", () => {
      expect(editTopicActions.editTopic(config, id, cb)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(editTopicActions.editTopic(config, id, cb)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "EDIT_TOPIC_REQUEST", payload: config });
      expect(dispatch).toBeCalledTimes(1);
    });
  });

  describe("editTopic", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();
    const topicObj = {
      data: {
        data: {
          content: {},
        },
      },
    };

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "updateTopic").callsFake((config: any, id: any) =>
        Promise.resolve(topicObj as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await editTopicActions.editTopic({}, {}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("editTopic", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "updateTopic").callsFake((config: any, id: any) =>
        Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        editTopicActions.editTopic({}, {}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
