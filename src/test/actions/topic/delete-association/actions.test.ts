import Sinon from "sinon";

import * as deleteAssociationActions from "../../../../actions/topic/delete-association/actions";
import { TDeleteAssociation } from "../../../../actions/topic/delete-association/types";
import { MQTTService } from "../../../../services/MQTTService";

describe("#deleteAssociation", () => {
  describe("#deleteAssociationRequestAction", () => {
    it("should exist", () => {
      expect(deleteAssociationActions.deleteAssociationRequestAction()).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(deleteAssociationActions.deleteAssociationRequestAction().type).toEqual(
        TDeleteAssociation.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(deleteAssociationActions.deleteAssociationRequestAction().payload).toEqual(undefined);
    });
  });

  describe("#deleteAssociationSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(
        deleteAssociationActions.deleteAssociationSuccessAction(responseOnSuccess)
      ).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(
        deleteAssociationActions.deleteAssociationSuccessAction(responseOnSuccess).type
      ).toEqual(TDeleteAssociation.SUCCESS);
    });

    it("should not have a property 'payload'", () => {
      expect(
        deleteAssociationActions.deleteAssociationSuccessAction(responseOnSuccess).payload
      ).toEqual(responseOnSuccess);
    });
  });

  describe("#deleteAssociationErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(deleteAssociationActions.deleteAssociationErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(deleteAssociationActions.deleteAssociationErrorAction(responseOnError).type).toEqual(
        TDeleteAssociation.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(
        deleteAssociationActions.deleteAssociationErrorAction(responseOnError).payload
      ).toEqual(responseOnError);
    });
  });

  describe("#deleteAssociation", () => {
    const cb = jest.fn();
    const config = {};

    it("should exist", () => {
      expect(deleteAssociationActions.deleteAssociation(config)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(deleteAssociationActions.deleteAssociation(config, cb)(
        dispatch
      ) as any);
      expect(dispatch).toBeCalledWith({ type: "TOPIC_DELETEASSOCIATION_REQUEST" });
    });
  });

  describe("deleteAssociation", () => {
    let mqttServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      mqttServiceStub = Sinon.stub(MQTTService, "deleteAssociation").callsFake(
        (topicMqttClientId: any) => Promise.resolve({ data: {} } as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await deleteAssociationActions.deleteAssociation({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      mqttServiceStub.restore();
    });
  });

  describe("deleteAssociation", () => {
    let mqttServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      mqttServiceStub = Sinon.stub(MQTTService, "deleteAssociation").callsFake(
        (topicMqttClientId: any) => Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        deleteAssociationActions.deleteAssociation({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      mqttServiceStub.restore();
    });
  });
});
