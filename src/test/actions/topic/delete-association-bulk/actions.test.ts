import Sinon from "sinon";

import * as deleteAssociationBulkActions from "../../../../actions/topic/delete-association-bulk/actions";
import { TDeleteAssociationBulk } from "../../../../actions/topic/delete-association-bulk/types";
import { MQTTService } from "../../../../services/MQTTService";

describe("#deleteAssociationBulk", () => {
  describe("#deleteAssociationBulkRequestAction", () => {
    it("should exist", () => {
      expect(deleteAssociationBulkActions.deleteAssociationBulkRequestAction()).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(deleteAssociationBulkActions.deleteAssociationBulkRequestAction().type).toEqual(
        TDeleteAssociationBulk.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(deleteAssociationBulkActions.deleteAssociationBulkRequestAction().payload).toEqual(
        undefined
      );
    });
  });

  describe("#deleteAssociationBulkSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(
        deleteAssociationBulkActions.deleteAssociationBulkSuccessAction(responseOnSuccess)
      ).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(
        deleteAssociationBulkActions.deleteAssociationBulkSuccessAction(responseOnSuccess).type
      ).toEqual(TDeleteAssociationBulk.SUCCESS);
    });

    it("should not have a property 'payload'", () => {
      expect(
        deleteAssociationBulkActions.deleteAssociationBulkSuccessAction(responseOnSuccess).payload
      ).toEqual(responseOnSuccess);
    });
  });

  describe("#deleteAssociationBulkErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(
        deleteAssociationBulkActions.deleteAssociationBulkErrorAction(responseOnError)
      ).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(
        deleteAssociationBulkActions.deleteAssociationBulkErrorAction(responseOnError).type
      ).toEqual(TDeleteAssociationBulk.ERROR);
    });

    it("should not have a property 'payload'", () => {
      expect(
        deleteAssociationBulkActions.deleteAssociationBulkErrorAction(responseOnError).payload
      ).toEqual(responseOnError);
    });
  });

  describe("#deleteAssociationBulk", () => {
    const cb = jest.fn();
    const config: string[] = [];

    it("should exist", () => {
      expect(deleteAssociationBulkActions.deleteAssociationBulk(config)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(deleteAssociationBulkActions.deleteAssociationBulk(config, cb)(
        dispatch
      ) as any);
      expect(dispatch).toBeCalledWith({ type: "TOPIC_DELETEASSOCIATIONBULK_REQUEST" });
    });
  });

  describe("deleteAssociationBulk", () => {
    let mqttServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      mqttServiceStub = Sinon.stub(MQTTService, "deleteAssociationBulk").callsFake(
        (topicMqttClientId: any) => Promise.resolve({ data: {} } as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await deleteAssociationBulkActions.deleteAssociationBulk(
        { mqttClientType: "test", mqttIds: [] },
        callbackMockFn
      )(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      mqttServiceStub.restore();
    });
  });

  describe("deleteAssociationBulk", () => {
    let mqttServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      mqttServiceStub = Sinon.stub(MQTTService, "deleteAssociationBulk").callsFake(
        (topicMqttClientId: any) => Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        deleteAssociationBulkActions.deleteAssociationBulk(
          { mqttClientType: "test", mqttIds: [] },
          callbackMockFn
        )(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      mqttServiceStub.restore();
    });
  });
});
