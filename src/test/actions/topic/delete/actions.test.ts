import Sinon from "sinon";

import * as deleteTopicActions from "../../../../actions/topic/delete/actions";
import { TDeleteTopic } from "../../../../actions/topic/delete/types";
import { TopicService } from "../../../../services/TopicService";

describe("#deleteTopic", () => {
  describe("#deleteTopicRequestAction", () => {
    it("should exist", () => {
      expect(deleteTopicActions.deleteTopicRequestAction()).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(deleteTopicActions.deleteTopicRequestAction().type).toEqual(TDeleteTopic.REQUEST);
    });

    it("should not have a property 'payload'", () => {
      expect(deleteTopicActions.deleteTopicRequestAction().payload).not.toBeDefined();
    });
  });

  describe("#deleteTopicSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(deleteTopicActions.deleteTopicSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(deleteTopicActions.deleteTopicSuccessAction(responseOnSuccess).type).toEqual(
        TDeleteTopic.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(deleteTopicActions.deleteTopicSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#deleteTopicErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(deleteTopicActions.deleteTopicErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(deleteTopicActions.deleteTopicErrorAction(responseOnError).type).toEqual(
        TDeleteTopic.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(deleteTopicActions.deleteTopicErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#deleteTopic", () => {
    let topicId: any;
    beforeAll(() => {
      topicId = "test";
    });

    it("should exist", () => {
      expect(deleteTopicActions.deleteTopic(topicId)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(deleteTopicActions.deleteTopic(topicId)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "TOPIC_DELETE_REQUEST" });
      expect(dispatch).toBeCalledTimes(1);
    });
  });

  describe("deleteTopic", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "deleteTopic").callsFake((topicId: any) =>
        Promise.resolve({ data: {} } as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await deleteTopicActions.deleteTopic({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("deleteTopic", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "deleteTopic").callsFake((topicId: any) =>
        Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        deleteTopicActions.deleteTopic({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
