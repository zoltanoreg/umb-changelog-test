import Sinon from "sinon";

import * as getTopicListActions from "../../../../actions/topic/get-list/actions";
import { TopicListType } from "../../../../actions/topic/get-list/types";
import { TopicService } from "../../../../services/TopicService";

describe("topicListActions/", () => {
  const conf = {
    filter: {
      key: "test",
    },
    key: "test",
    limit: 2,
    orderBy: "test",
    orderType: "test",
    page: 2,
  };

  it('should have the property  "type"', () => {
    const actions = [
      getTopicListActions.topicListRequestAction,
      getTopicListActions.topicListSuccessAction,
      getTopicListActions.topicListErrorAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("getTopicList should be a promise", () => {
    expect(getTopicListActions.getTopicList).toBeDefined();
  });

  it("topicListRequestAction should be defined", () => {
    getTopicListActions.getTopicList(conf);
    expect(getTopicListActions.topicListRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getTopicListActions.getTopicList(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "GET_TOPIC_LIST_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getTopicList", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const confObj = {
      statuses: ["1"],
    };
    const confObj2 = {};

    const topicObj = {
      data: {
        data: {
          content: [{ status: "1" }],
        },
      },
    };
    const expectedAction = [{ payload: topicObj.data.data, type: TopicListType.SUCCESS }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getTopicList").callsFake((config: any) =>
        Promise.resolve(topicObj as any)
      );
    });

    it("should appear topic status in config", async () => {
      // tslint:disable-next-line
      await getTopicListActions.getTopicList(confObj as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    it("should not appear topic status in config", async () => {
      // tslint:disable-next-line
      await getTopicListActions.getTopicList(confObj2 as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("getTopicList", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: TopicListType.ERROR }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getTopicList").callsFake((config: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        getTopicListActions.getTopicList({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toEqual(expectedAction);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
