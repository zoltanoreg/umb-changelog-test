import Sinon from "sinon";

import * as getTopicListActions from "../../../../actions/topic/filter-list/actions";
import { FilterTopicListType } from "../../../../actions/topic/filter-list/types";
import { TopicService } from "../../../../services/TopicService";

describe("filterTopicListActions", () => {
  const conf = {
    filter: {
      topicName: "test",
    },
    key: "test",
    limit: 2,
    orderBy: "test",
    orderType: "test",
    page: 2,
  };

  it('should have the property  "type"', () => {
    const actions = [
      getTopicListActions.filterTopicListRequestAction,
      getTopicListActions.filterTopicListSuccessAction,
      getTopicListActions.filterTopicListErrorAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("getFilteredTopicList should be a promise", () => {
    expect(getTopicListActions.getFilteredTopicList).toBeDefined();
  });

  it("filterTopicListRequestAction should be defined", () => {
    getTopicListActions.getFilteredTopicList(conf);
    expect(getTopicListActions.filterTopicListRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getTopicListActions.getFilteredTopicList(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: FilterTopicListType.REQUEST });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getTopicList", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const confObj = {
      statuses: ["1"],
    };
    const confObj2 = {};

    const topicObj = {
      data: {
        data: {
          content: [{ status: "1" }],
        },
      },
    };
    const expectedAction = [{ payload: topicObj.data.data, type: FilterTopicListType.SUCCESS }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getTopicList").callsFake((config: any) =>
        Promise.resolve(topicObj as any)
      );
    });

    it("should appear topic status in config", async () => {
      // tslint:disable-next-line
      await getTopicListActions.getFilteredTopicList(confObj as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    it("should not appear topic status in config", async () => {
      // tslint:disable-next-line
      await getTopicListActions.getFilteredTopicList(confObj2 as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("getFilteredTopicList", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: FilterTopicListType.ERROR }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getTopicList").callsFake((config: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        getTopicListActions.getFilteredTopicList({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toEqual(expectedAction);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
