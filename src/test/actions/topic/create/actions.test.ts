import Sinon from "sinon";

import * as createTopicActions from "../../../../actions/topic/create/actions";
import { CreateTopicTypes } from "../../../../actions/topic/create/types";
import { TopicService } from "../../../../services/TopicService";

describe("#createTopic", () => {
  describe("#createTopicRequestAction", () => {
    it("should exist", () => {
      expect(createTopicActions.createTopicRequestAction({})).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createTopicActions.createTopicRequestAction({}).type).toEqual(
        CreateTopicTypes.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(createTopicActions.createTopicRequestAction({}).payload).toEqual({});
    });
  });

  describe("#createTopicSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(createTopicActions.createTopicSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createTopicActions.createTopicSuccessAction(responseOnSuccess).type).toEqual(
        CreateTopicTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(createTopicActions.createTopicSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#createTopicErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(createTopicActions.createTopicErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createTopicActions.createTopicErrorAction(responseOnError).type).toEqual(
        CreateTopicTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(createTopicActions.createTopicErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#createTopic", () => {
    const cb = jest.fn();
    const config = {};

    it("should exist", () => {
      expect(createTopicActions.createTopic(config)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(createTopicActions.createTopic(config, cb)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "CREATE_TOPIC_REQUEST", payload: config });
      expect(dispatch).toBeCalledTimes(1);
    });
  });

  describe("createTopic", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();
    const topicObj = {
      data: {
        data: {
          content: {},
        },
      },
    };

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "createTopic").callsFake((config: any) =>
        Promise.resolve(topicObj as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await createTopicActions.createTopic({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("createTopic", () => {
    let topicServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "createTopic").callsFake((config: any) =>
        Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        createTopicActions.createTopic({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
