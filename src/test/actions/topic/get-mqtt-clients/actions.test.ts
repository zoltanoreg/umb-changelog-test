import Sinon from "sinon";

import * as getMqttClientsActions from "../../../../actions/topic/get-mqtt-clients/actions";
import { MQTTClientType } from "../../../../actions/topic/get-mqtt-clients/types";
import { MQTTService } from "../../../../services/MQTTService";

describe("getMqttClientsActions/", () => {
  const conf = {};

  it('should have the property  "type"', () => {
    const actions = [
      getMqttClientsActions.MQTTClientRequestAction,
      getMqttClientsActions.MQTTClientSuccessAction,
      getMqttClientsActions.MQTTClientErrorAction,
      getMqttClientsActions.MQTTClientClearAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("getMqttClients should be a promise", () => {
    expect(getMqttClientsActions.getMqttClients).toBeDefined();
  });

  it("MQTTClientRequestAction should be defined", () => {
    getMqttClientsActions.getMqttClients(conf);
    expect(getMqttClientsActions.MQTTClientRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getMqttClientsActions.getMqttClients(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "GET_MQTT_CLIENT_REQUEST" });
  });

  describe("getMqttClients", () => {
    let mqttServiceStub: any;
    const dispatch = jest.fn();
    const topicObj = {
      data: {
        data: {},
      },
    };
    const expectedAction = [{ payload: topicObj.data.data, type: MQTTClientType.SUCCESS }];

    beforeAll(() => {
      mqttServiceStub = Sinon.stub(MQTTService, "getMqttClients").callsFake((config: any) =>
        Promise.resolve(topicObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await getMqttClientsActions.getMqttClients({})(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      mqttServiceStub.restore();
    });
  });

  describe("getMqttClients", () => {
    let mqttServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: MQTTClientType.ERROR }];

    beforeAll(() => {
      mqttServiceStub = Sinon.stub(MQTTService, "getMqttClients").callsFake((config: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        getMqttClientsActions.getMqttClients({})(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      mqttServiceStub.restore();
    });
  });
});
