import Sinon from "sinon";

import * as topicBridgesActions from "../../../../actions/topic/get-bridges/actions";
import { TGetTopicBridges } from "../../../../actions/topic/get-bridges/types";
import { TopicService } from "../../../../services/TopicService";

describe("getTopicBridges/", () => {
  const conf = {
    filter: {
      topicId: "test",
    },
    limit: 2,
    orderBy: "test",
    orderType: "test",
    page: 2,
  };

  it('should have the property  "type"', () => {
    const actions = [
      topicBridgesActions.getTopicBridgesRequestAction,
      topicBridgesActions.getTopicBridgesSuccessAction,
      topicBridgesActions.getTopicBridgesErrorAction,
      topicBridgesActions.clearTopicBridgesAction,
    ];
    actions.forEach(action => {
      expect(action({})).toHaveProperty("type");
    });
  });

  it("getTopicBridges should be a promise", () => {
    expect(topicBridgesActions.getTopicBridges).toBeDefined();
  });

  it("getTopicBridgesRequestAction should be defined", () => {
    topicBridgesActions.getTopicBridges(conf);
    expect(topicBridgesActions.getTopicBridgesRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(topicBridgesActions.getTopicBridges(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "TOPIC_GETBRIDGES_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(topicBridgesActions.getTopicBridges(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "TOPIC_GETBRIDGES_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getTopicBridges", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const topicObj = {
      data: {
        data: {
          content: {},
        },
      },
    };
    const expectedAction = [{ payload: topicObj.data.data, type: TGetTopicBridges.SUCCESS }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getBridges").callsFake((config: any) =>
        Promise.resolve(topicObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await topicBridgesActions.getTopicBridges({})(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });

  describe("getTopicBridges", () => {
    let topicServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: TGetTopicBridges.ERROR }];

    beforeAll(() => {
      topicServiceStub = Sinon.stub(TopicService, "getBridges").callsFake((config: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        topicBridgesActions.getTopicBridges({})(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      topicServiceStub.restore();
    });
  });
});
