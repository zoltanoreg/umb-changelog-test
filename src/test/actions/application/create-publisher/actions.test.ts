import Sinon from "sinon";

import * as createPublisherActions from "../../../../actions/application/create-publisher/actions";
import { CreatePublisherTypes } from "../../../../actions/application/create-publisher/types";
import { MQTTService } from "../../../../services/MQTTService";

describe("#createPublisher", () => {
  describe("#CreatePublisherRequestAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(createPublisherActions.CreatePublisherRequestAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createPublisherActions.CreatePublisherRequestAction(responseOnSuccess).type).toEqual(
        CreatePublisherTypes.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(
        createPublisherActions.CreatePublisherRequestAction(responseOnSuccess).payload
      ).toEqual(responseOnSuccess);
    });
  });

  describe("#CreatePublisherSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(createPublisherActions.CreatePublisherSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createPublisherActions.CreatePublisherSuccessAction(responseOnSuccess).type).toEqual(
        CreatePublisherTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(
        createPublisherActions.CreatePublisherSuccessAction(responseOnSuccess).payload
      ).toEqual(responseOnSuccess);
    });
  });

  describe("#CreatePublisherErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(createPublisherActions.CreatePublisherErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createPublisherActions.CreatePublisherErrorAction(responseOnError).type).toEqual(
        CreatePublisherTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(createPublisherActions.CreatePublisherErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#createPublisher", () => {
    const cb = jest.fn();

    it("should exist", () => {
      const config = {};
      expect(createPublisherActions.createPublisher(config, cb)).toBeDefined();
    });

    it("test dispatch", async () => {
      const config = {};

      const dispatch = jest.fn();

      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(createPublisherActions.createPublisher(config, cb)(dispatch) as any);

      expect(dispatch).toBeCalledWith({ type: "CREATE_PUBLISHER_REQUEST", payload: config });
      expect(dispatch).toBeCalledTimes(1);
    });
  });

  describe("createPublisher", () => {
    let createPublisherStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      createPublisherStub = Sinon.stub(MQTTService, "createPublisher").callsFake((config: any) =>
        Promise.resolve({ config: {}, data: {} } as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await createPublisherActions.createPublisher({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      createPublisherStub.restore();
    });
  });

  describe("createPublisher", () => {
    let createPublisherStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      createPublisherStub = Sinon.stub(MQTTService, "createPublisher").callsFake((config: any) =>
        Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        createPublisherActions.createPublisher({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      createPublisherStub.restore();
    });
  });
});
