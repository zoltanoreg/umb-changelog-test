import Sinon from "sinon";

import * as createSubscriberActions from "../../../../actions/application/create-subscriber/actions";
import { CreateSubscriberTypes } from "../../../../actions/application/create-subscriber/types";
import { MQTTService } from "../../../../services/MQTTService";

describe("#createSubscriber", () => {
  describe("#CreateSubscriberRequestAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(
        createSubscriberActions.CreateSubscriberRequestAction(responseOnSuccess)
      ).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createSubscriberActions.CreateSubscriberRequestAction(responseOnSuccess).type).toEqual(
        CreateSubscriberTypes.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(
        createSubscriberActions.CreateSubscriberRequestAction(responseOnSuccess).payload
      ).toEqual(responseOnSuccess);
    });
  });

  describe("#CreateSubscriberSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(
        createSubscriberActions.CreateSubscriberSuccessAction(responseOnSuccess)
      ).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createSubscriberActions.CreateSubscriberSuccessAction(responseOnSuccess).type).toEqual(
        CreateSubscriberTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(
        createSubscriberActions.CreateSubscriberSuccessAction(responseOnSuccess).payload
      ).toEqual(responseOnSuccess);
    });
  });

  describe("#CreateSubscriberErrorAction", () => {
    const responseOnError = "Error message";

    it("should exist", () => {
      expect(createSubscriberActions.CreateSubscriberErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createSubscriberActions.CreateSubscriberErrorAction(responseOnError).type).toEqual(
        CreateSubscriberTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(createSubscriberActions.CreateSubscriberErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#createSubscriber", () => {
    const cb = jest.fn();
    const config = {};

    it("should exist", () => {
      expect(createSubscriberActions.createSubscriber(config, cb)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();

      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(createSubscriberActions.createSubscriber(config, cb)(dispatch) as any);

      expect(dispatch).toBeCalledWith({ type: "CREATE_APPLICATION_REQUEST", payload: config });
    });

    it("test dispatch error", async () => {
      const dispatch = jest.fn();

      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(createSubscriberActions.createSubscriber(config, cb)(dispatch) as any);

      expect(dispatch).lastCalledWith({
        payload: {
          content: "Cannot read property 'topicId' of undefined",
          title: "Error",
          type: "Info",
        },
        type: "POPUP_OPEN",
      });
    });
  });

  describe("createSubscriber", () => {
    let createSubscriberStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      createSubscriberStub = Sinon.stub(MQTTService, "createSubscriber").callsFake((config: any) =>
        Promise.resolve({ config: {}, data: {} } as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await createSubscriberActions.createSubscriber({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      createSubscriberStub.restore();
    });
  });

  describe("createSubscriber", () => {
    let createSubscriberStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      createSubscriberStub = Sinon.stub(MQTTService, "createSubscriber").callsFake((config: any) =>
        Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        createSubscriberActions.createSubscriber({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      createSubscriberStub.restore();
    });
  });
});
