import Sinon from "sinon";

import * as appTopicListActions from "../../../../actions/application/get-topics/actions";
import { AppTopicListType } from "../../../../actions/application/get-topics/types";
import { ITopicListingTableConfig } from "../../../../models/ITopicConfig";
import { ApplicationService } from "../../../../services/ApplicationService";

describe("getApplicationTopicList/", () => {
  it('should have the property  "type"', () => {
    const actions = [
      appTopicListActions.appTopicListErrorAction,
      appTopicListActions.appTopicListRequestAction,
      appTopicListActions.appTopicListSuccessAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("getApplicationTopicList should be a promise", () => {
    expect(appTopicListActions.getApplicationTopicList).toBeDefined();
  });

  it("test dispatch", async () => {
    const conf = {
      filter: {},
      limit: 0,
      orderBy: "test",
      orderType: "test",
      page: 0,
    };
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(appTopicListActions.getApplicationTopicList(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "APP_TOPIC_LIST_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getApplicationTopicList", () => {
    let applicationServiceStub: any;
    const dispatch = jest.fn();
    const topicListObj = { data: { data: {} } };
    const expectedAction = [{ payload: topicListObj.data.data, type: AppTopicListType.SUCCESS }];

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationTopicList").callsFake(
        (config: any) => Promise.resolve(topicListObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await appTopicListActions.getApplicationTopicList({} as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });

  describe("getApplicationTopicList", () => {
    let applicationServiceStub: any;
    const dispatch = jest.fn();
    const expectedAction = [{ payload: {}, type: AppTopicListType.ERROR }];

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationTopicList").callsFake(
        (config: any) => Promise.reject({} as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        appTopicListActions.getApplicationTopicList({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });
});
