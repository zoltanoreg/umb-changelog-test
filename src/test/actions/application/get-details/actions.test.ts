import Sinon from "sinon";

import * as getApplicationDetailsActions from "../../../../actions/application/get-details/actions";
import { TGetApplicationDetails } from "../../../../actions/application/get-details/types";
import { ApplicationService } from "../../../../services/ApplicationService";

describe("getApplicationDetailsActions/", () => {
  it('should have the property  "type"', () => {
    const actions = [
      getApplicationDetailsActions.getApplicationDetailsErrorAction,
      getApplicationDetailsActions.getApplicationDetailsRequestAction,
      getApplicationDetailsActions.getApplicationDetailsSuccessAction,
    ];
    actions.forEach(action => {
      expect(action({})).toHaveProperty("type");
    });
  });

  it("getApplicationDetails should be a promise", () => {
    expect(getApplicationDetailsActions.getApplicationDetails).toBeDefined();
  });

  it("test dispatch", async () => {
    const appId = "test";
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getApplicationDetailsActions.getApplicationDetails(appId)(
      dispatch
    ) as any);
    expect(dispatch).toBeCalledWith({ type: "APPLICATION_GETDETAILS_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getApplicationDetails", () => {
    let getApplicationDetailsStub: any;
    const dispatch = jest.fn();
    const detailObj = { data: {} };
    const expectedAction = [{ payload: detailObj.data, type: TGetApplicationDetails.SUCCESS }];

    beforeAll(() => {
      getApplicationDetailsStub = Sinon.stub(ApplicationService, "getApplicationDetails").callsFake(
        (config: any) => Promise.resolve(detailObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await getApplicationDetailsActions.getApplicationDetails("testId")(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      getApplicationDetailsStub.restore();
    });
  });

  describe("getApplicationDetails", () => {
    let getApplicationDetailsStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: TGetApplicationDetails.ERROR }];

    beforeAll(() => {
      getApplicationDetailsStub = Sinon.stub(ApplicationService, "getApplicationDetails").callsFake(
        (config: any) => Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", async () => {
      // tslint:disable-next-line
      await getApplicationDetailsActions.getApplicationDetails("testId")(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      getApplicationDetailsStub.restore();
    });
  });
});
