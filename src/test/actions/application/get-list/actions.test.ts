import Sinon from "sinon";

import * as applicationActions from "../../../../actions/application/get-list/actions";
import { ApplicationListType } from "../../../../actions/application/get-list/types";
import { ApplicationService } from "../../../../services/ApplicationService";

describe("getApplicationList/", () => {
  it('should have the property  "type"', () => {
    const actions = [
      applicationActions.applicationErrorAction,
      applicationActions.applicationRequestAction,
      applicationActions.applicationSuccessAction,
    ];
    actions.forEach(action => {
      expect(action("")).toHaveProperty("type");
    });
  });

  it("applicationList should be a promise", () => {
    expect(applicationActions.getApplicationList).toBeDefined();
  });

  it("test dispatch", async () => {
    const conf = {
      filter: {},
      limit: 0,
      orderBy: "test",
      orderType: "test",
      page: 0,
    };
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(applicationActions.getApplicationList(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "GET_APPLICATION_LIST_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getApplicationList", () => {
    let applicationServiceStub: any;
    const dispatch = jest.fn();
    const listObj = { data: { data: {} } };
    const expectedAction = [{ payload: listObj.data.data, type: ApplicationListType.SUCCESS }];

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationList").callsFake(
        (config: any) => Promise.resolve(listObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await applicationActions.getApplicationList({} as any)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });

  describe("getApplicationList", () => {
    let applicationServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: ApplicationListType.ERROR }];

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationList").callsFake(
        (config: any) => Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        applicationActions.getApplicationList({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });
});
