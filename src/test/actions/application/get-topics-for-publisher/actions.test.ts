import Sinon from "sinon";

import * as getTopicsForPublisherActions from "../../../../actions/application/get-topics-for-publisher/actions";
import { ApplicationService } from "../../../../services/ApplicationService";

describe("#getTopicsForPublisher", () => {
  const cb = jest.fn();

  it("should exist", () => {
    const config = {};
    expect(getTopicsForPublisherActions.getTopicsForPublisher(config, cb)).toBeDefined();
  });

  it("test dispatch error", async () => {
    const config = {};

    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getTopicsForPublisherActions.getTopicsForPublisher(config, cb)(
      dispatch
    ) as any);
    expect(dispatch).toBeCalledWith({
      payload: {
        content: "Cannot convert undefined or null to object",
        title: "Error",
        type: "Info",
      },
      type: "POPUP_OPEN",
    });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getTopicsForPublisher", () => {
    let applicationServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();
    const topicListsObj = {
      data: {
        data: {
          content: [{ topic: { topicId: "1" } }],
          count: 1,
        },
      },
    };

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationTopicList").callsFake(
        (config: any) => Promise.resolve(topicListsObj as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await getTopicsForPublisherActions.getTopicsForPublisher({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });

  describe("getTopicsForPublisher", () => {
    let applicationServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationTopicList").callsFake(
        (config: any) => Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        getTopicsForPublisherActions.getTopicsForPublisher({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });
});
