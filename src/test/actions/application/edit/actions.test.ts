import Sinon from "sinon";

import * as updateActions from "../../../../actions/application/edit/actions";
import { EditApplicationTypes } from "../../../../actions/application/edit/types";
import { ApplicationService } from "../../../../services/ApplicationService";

describe("#EditApplication", () => {
  describe("#EditApplicationRequestAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(updateActions.EditApplicationRequestAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(updateActions.EditApplicationRequestAction(responseOnSuccess).type).toEqual(
        EditApplicationTypes.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(updateActions.EditApplicationRequestAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#EditApplicationSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(updateActions.EditApplicationSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(updateActions.EditApplicationSuccessAction(responseOnSuccess).type).toEqual(
        EditApplicationTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(updateActions.EditApplicationSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#EditApplicationErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(updateActions.EditApplicationErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(updateActions.EditApplicationErrorAction(responseOnError).type).toEqual(
        EditApplicationTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(updateActions.EditApplicationErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#EditApplication", () => {
    const cb = jest.fn();

    it("should exist", () => {
      const config = {};
      expect(updateActions.EditApplication(config, cb)).toBeDefined();
    });

    it("test dispatch", async () => {
      const config = {
        appName: "",
        appVersion: "",
        authenticationType: "",
        brokerType: "",
        fullName: "",
        password: "",
        pathClientCa: "",
        pathToCa: "",
        systemName: "",
        user: {
          email: "",
          firstName: "",
          lastName: "",
          role: "",
          userId: "",
        },
        userName: "",
      };

      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(updateActions.EditApplication(config, cb)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "EDIT_APPLICATION_REQUEST", payload: config });
      expect(dispatch).toBeCalledTimes(1);
    });

    it("test dispatch error", async () => {
      const config = {};
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(updateActions.EditApplication(config, cb)(dispatch) as any);
      expect(dispatch).lastCalledWith({
        payload: {
          content: "Cannot read property 'brokerTypeId' of undefined",
          title: "Error",
          type: "Info",
        },
        type: "POPUP_OPEN",
      });
    });
  });

  describe("editApplication", () => {
    let updateApplicationStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      updateApplicationStub = Sinon.stub(ApplicationService, "updateApplication").callsFake(
        (config: any) => Promise.resolve({ data: {} } as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await updateActions.EditApplication({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      updateApplicationStub.restore();
    });
  });

  describe("editApplication", () => {
    let updateApplicationStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      updateApplicationStub = Sinon.stub(ApplicationService, "updateApplication").callsFake(
        (config: any) => Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        updateActions.EditApplication({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      updateApplicationStub.restore();
    });
  });
});
