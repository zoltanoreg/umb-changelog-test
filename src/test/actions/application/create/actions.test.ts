import Sinon from "sinon";

import * as createActions from "../../../../actions/application/create/actions";
import { CreateApplicationTypes } from "../../../../actions/application/create/types";
import {
  EditModalActions,
  IEditModalOpenAction,
  TEditModalActions,
} from "../../../../actions/modal/edit/types";
import { ApplicationService } from "../../../../services/ApplicationService";

describe("#createApplication", () => {
  describe("CreateApplicationRequestAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(createActions.CreateApplicationRequestAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createActions.CreateApplicationRequestAction(responseOnSuccess).type).toEqual(
        CreateApplicationTypes.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(createActions.CreateApplicationRequestAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("CreateApplicationSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(createActions.CreateApplicationSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createActions.CreateApplicationSuccessAction(responseOnSuccess).type).toEqual(
        CreateApplicationTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(createActions.CreateApplicationSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("CreateApplicationErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(createActions.CreateApplicationErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(createActions.CreateApplicationErrorAction(responseOnError).type).toEqual(
        CreateApplicationTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(createActions.CreateApplicationErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("createApplication", () => {
    const cb = jest.fn();

    it("should exist", () => {
      const config = {};
      expect(createActions.createApplication(config, cb)).toBeDefined();
    });

    it("should call request", async () => {
      const config = {
        appName: "",
        appVersion: "",
        authenticationType: "",
        brokerType: "",
        fullName: "",
        password: "",
        pathClientCa: "",
        pathToCa: "",
        systemName: "",
        user: {
          email: "",
          firstName: "",
          lastName: "",
          role: "",
          userId: "",
        },
        userName: "",
      };

      const dispatch = jest.fn();

      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(createActions.createApplication(config, cb)(dispatch) as any);

      expect(dispatch).toBeCalledWith({ type: "CREATE_APPLICATION_REQUEST", payload: config });
      expect(dispatch).toBeCalledTimes(1);
    });

    it("should throw error", async () => {
      const config = {};
      const dispatch = jest.fn();

      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(createActions.createApplication(config, cb)(dispatch) as any);

      expect(dispatch).lastCalledWith({
        payload: {
          content: "Cannot read property 'brokerTypeId' of undefined",
          title: "Error",
          type: "Info",
        },
        type: "POPUP_OPEN",
      });
    });
  });

  describe("createApplication", () => {
    let applicationServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "createApplication").callsFake(
        (config: any) => Promise.resolve({ data: {} } as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await createActions.createApplication({}, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });

  describe("createApplication", () => {
    let applicationServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "createApplication").callsFake(
        (config: any) => Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        createActions.createApplication({}, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });
});
