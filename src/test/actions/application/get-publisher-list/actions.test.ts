import Sinon from "sinon";

import * as getPublisherListActions from "../../../../actions/application/get-publisher-list/actions";
import { GetPublisherListTypes } from "../../../../actions/application/get-publisher-list/types";
import { ApplicationService } from "../../../../services/ApplicationService";

describe("#getPublisherList", () => {
  describe("#GetPublisherListRequestAction", () => {
    it("should exist", () => {
      expect(getPublisherListActions.GetPublisherListRequestAction({})).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(getPublisherListActions.GetPublisherListRequestAction({}).type).toEqual(
        GetPublisherListTypes.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(getPublisherListActions.GetPublisherListRequestAction({}).payload).toEqual({});
    });
  });

  describe("#GetPublisherListSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(
        getPublisherListActions.GetPublisherListSuccessAction(responseOnSuccess)
      ).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(getPublisherListActions.GetPublisherListSuccessAction(responseOnSuccess).type).toEqual(
        GetPublisherListTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(
        getPublisherListActions.GetPublisherListSuccessAction(responseOnSuccess).payload
      ).toEqual(responseOnSuccess);
    });
  });

  describe("#GetPublisherListErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(getPublisherListActions.GetPublisherListErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(getPublisherListActions.GetPublisherListErrorAction(responseOnError).type).toEqual(
        GetPublisherListTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(getPublisherListActions.GetPublisherListErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#getPublisherList", () => {
    const cb = jest.fn();
    const config = {};

    it("should exist", () => {
      expect(getPublisherListActions.getPublisherList(config)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(getPublisherListActions.getPublisherList(config, cb)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "GET_PUBLISHER_LIST_REQUEST", payload: config });
    });
  });

  describe("getPublisherList", () => {
    let applicationServiceStub: any;
    const dispatch = jest.fn();
    const topicListsObj = {
      data: {
        data: {
          content: [{ topic: { topicId: "1" } }, { topic: { topicId: "2" } }],
          count: 2,
        },
      },
    };

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationTopicList").callsFake(
        (config: any) => Promise.resolve(topicListsObj as any)
      );
    });

    it("should call callback", async () => {
      // tslint:disable-next-line
      await getPublisherListActions.getPublisherList({})(dispatch);
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });
});
