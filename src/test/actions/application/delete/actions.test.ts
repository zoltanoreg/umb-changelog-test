import Sinon from "sinon";

import * as deleteActions from "../../../../actions/application/delete/actions";
import { TDeleteApplication } from "../../../../actions/application/delete/types";
import { ApplicationService } from "../../../../services/ApplicationService";

describe("#deleteApplication", () => {
  describe("#deleteApplicationRequestAction", () => {
    it("should exist", () => {
      expect(deleteActions.deleteApplicationRequestAction()).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(deleteActions.deleteApplicationRequestAction().type).toEqual(
        TDeleteApplication.REQUEST
      );
    });

    it("should not have a property 'payload'", () => {
      expect(deleteActions.deleteApplicationRequestAction().payload).not.toBeDefined();
    });
  });

  describe("#deleteApplicationSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(deleteActions.deleteApplicationSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(deleteActions.deleteApplicationSuccessAction(responseOnSuccess).type).toEqual(
        TDeleteApplication.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(deleteActions.deleteApplicationSuccessAction(responseOnSuccess).payload).toEqual(
        responseOnSuccess
      );
    });
  });

  describe("#deleteApplicationErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(deleteActions.deleteApplicationErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(deleteActions.deleteApplicationErrorAction(responseOnError).type).toEqual(
        TDeleteApplication.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(deleteActions.deleteApplicationErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#deleteApplication", () => {
    let appId: any;
    beforeAll(() => {
      appId = "test";
    });

    it("should exist", () => {
      expect(deleteActions.deleteApplication(appId)).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(deleteActions.deleteApplication(appId)(dispatch) as any);
      expect(dispatch).toBeCalledWith({ type: "APPLICATION_DELETE_REQUEST" });
      expect(dispatch).toBeCalledTimes(1);
    });
  });

  describe("deleteApplication", () => {
    let applicationServiceStub: any;
    const dispatch = jest.fn();
    const applicationObj = {
      data: {
        data: {},
      },
    };
    const expectedAction = [{ payload: { data: {} }, type: TDeleteApplication.SUCCESS }];

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "deleteApplication").callsFake(
        (appId: any) => Promise.resolve(applicationObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await deleteActions.deleteApplication("testId")(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });

  describe("deleteApplication", () => {
    let applicationServiceStub: any;
    const dispatch = jest.fn();
    const expectedAction = [{ payload: {}, type: TDeleteApplication.ERROR }];

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "deleteApplication").callsFake(
        (appId: any) => Promise.reject({} as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        deleteActions.deleteApplication("testId")(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      applicationServiceStub.restore();
    });
  });
});
