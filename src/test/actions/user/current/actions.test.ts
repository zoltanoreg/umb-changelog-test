import Sinon from "sinon";

import * as getLoggedinUserActions from "../../../../actions/user/current/actions";
import { CurrentUserTypes } from "../../../../actions/user/current/types";
import { UserService } from "../../../../services/UserService";

describe("#getLoggedinUser", () => {
  describe("#getLoggedinUserSuccessAction", () => {
    let responseOnSuccess: { data: any[] };

    beforeAll(() => {
      responseOnSuccess = {
        data: [],
      };
    });

    it("should exist", () => {
      expect(getLoggedinUserActions.getLoggedinUserSuccessAction(responseOnSuccess)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(getLoggedinUserActions.getLoggedinUserSuccessAction(responseOnSuccess).type).toEqual(
        CurrentUserTypes.SUCCESS
      );
    });

    it("should not have a property 'payload'", () => {
      expect(
        getLoggedinUserActions.getLoggedinUserSuccessAction(responseOnSuccess).payload
      ).toEqual(responseOnSuccess);
    });
  });

  describe("#getLoggedinUserErrorAction", () => {
    let responseOnError: string;

    beforeAll(() => {
      responseOnError = "Error message";
    });

    it("should exist", () => {
      expect(getLoggedinUserActions.getLoggedinUserErrorAction(responseOnError)).toBeDefined();
    });

    it("should have a property 'type'", () => {
      expect(getLoggedinUserActions.getLoggedinUserErrorAction(responseOnError).type).toEqual(
        CurrentUserTypes.ERROR
      );
    });

    it("should not have a property 'payload'", () => {
      expect(getLoggedinUserActions.getLoggedinUserErrorAction(responseOnError).payload).toEqual(
        responseOnError
      );
    });
  });

  describe("#getLoggedinUser", () => {
    let token: string;

    beforeAll(() => {
      token = "test";
    });

    it("should exist", () => {
      expect(getLoggedinUserActions.getLoggedinUser()).toBeDefined();
    });

    it("test dispatch", async () => {
      const dispatch = jest.fn();
      // tslint:disable-next-line: no-void-expression
      await Promise.resolve(getLoggedinUserActions.getLoggedinUser()(dispatch) as any);
      expect(dispatch).toBeCalledTimes(0);
    });
  });

  describe("getLoggedinUser", () => {
    let userServiceStub: any;
    const dispatch = jest.fn();
    const token = "token";
    const userObj = { data: {} };
    const expectedAction = [{ payload: userObj, type: CurrentUserTypes.SUCCESS }];

    beforeAll(() => {
      userServiceStub = Sinon.stub(UserService, "getLoggedinUser").callsFake((config: any) =>
        Promise.resolve(userObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await getLoggedinUserActions.getLoggedinUser()(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      userServiceStub.restore();
    });
  });

  describe("getLoggedinUser", () => {
    let userServiceStub: any;
    const dispatch = jest.fn();
    const token = "token";
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: CurrentUserTypes.ERROR }];

    beforeAll(() => {
      userServiceStub = Sinon.stub(UserService, "getLoggedinUser").callsFake((config: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error dispatch", () => {
      try {
        getLoggedinUserActions.getLoggedinUser()(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      userServiceStub.restore();
    });
  });
});
