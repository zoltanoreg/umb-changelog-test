import Sinon from "sinon";

import * as getUserDetailsActions from "../../../../actions/user/get-details/actions";
import { UserService } from "../../../../services/UserService";

describe("getUserDetailsActions/", () => {
  const userId = "test";

  it('should have the property  "type"', () => {
    const actions = [
      getUserDetailsActions.getUsersDatilsRequestAction,
      getUserDetailsActions.getUsersDatilsSuccessAction,
      getUserDetailsActions.getUsersDatilsErrorAction,
    ];
    actions.forEach((action: any) => {
      expect(action({})).toHaveProperty("type");
    });
  });

  it("getUserDetails should be a promise", () => {
    expect(getUserDetailsActions.getUserDetails(userId)).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getUserDetailsActions.getUserDetails(userId)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "USER_GET_DETAILS_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getUserDetails", () => {
    let userServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();
    const userObj = {
      data: {
        data: {},
      },
    };

    beforeAll(() => {
      userServiceStub = Sinon.stub(UserService, "getUserDetails").callsFake((config: any) =>
        Promise.resolve(userObj as any)
      );
    });

    it("should call callback if exists", async () => {
      // tslint:disable-next-line
      await getUserDetailsActions.getUserDetails(userId, callbackMockFn)(dispatch);
      expect(callbackMockFn).toBeCalled();
    });

    afterAll(() => {
      userServiceStub.restore();
    });
  });

  describe("getUserDetails", () => {
    let userServiceStub: any;
    const callbackMockFn = jest.fn();
    const dispatch = jest.fn();

    beforeAll(() => {
      userServiceStub = Sinon.stub(UserService, "getUserDetails").callsFake((config: any) =>
        Promise.reject({} as any)
      );
    });

    it("should call error branch because of rejection", () => {
      try {
        getUserDetailsActions.getUserDetails(userId, callbackMockFn)(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      userServiceStub.restore();
    });
  });
});
