import Sinon from "sinon";

import * as getUserListActions from "../../../../actions/user/get-list/actions";
import { GetUserListType } from "../../../../actions/user/get-list/types";
import { IUserListingTableConfig } from "../../../../models/IUserListingTableConfig";
import { UserService } from "../../../../services/UserService";

describe("userListActions/", () => {
  const conf = {
    filter: {
      email: "string",
      firstName: "string",
      key: "any",
      lastName: "string",
      role: "string",
    },
    key: "test",
    limit: 2,
    orderBy: "string",
    orderType: "string",
    page: 2,
    roles: ["ADMINISTRATOR", "APPLICATION_OWNER"],
  };

  it('should have the property  "type"', () => {
    const actions = [
      getUserListActions.userListRequestAction,
      getUserListActions.userListSuccessAction,
      getUserListActions.userListErrorAction,
    ];
    actions.forEach(action => {
      expect(action({})).toHaveProperty("type");
    });
  });

  it("getUserlist should be a promise", () => {
    expect(getUserListActions.getUserList).toBeDefined();
  });

  it("getUserlist should be defined", () => {
    getUserListActions.getUserList(conf);
    expect(getUserListActions.userListRequestAction()).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(getUserListActions.getUserList(conf)(dispatch) as any);
    expect(dispatch).toBeCalledWith({ type: "GET_USER_LIST_REQUEST" });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("getUserList", () => {
    let userServiceStub: any;
    const dispatch = jest.fn();
    const confObj = {
      roles: ["1"],
    };
    const confObj2 = {};
    const userObj = {
      data: {
        data: {
          content: [{ role: "1" }],
        },
      },
    };
    const expectedAction = [{ payload: userObj.data, type: GetUserListType.SUCCESS }];

    beforeAll(() => {
      userServiceStub = Sinon.stub(UserService, "getUserList").callsFake((config: any) =>
        Promise.resolve(userObj as any)
      );
    });

    it("should appear user role in config", async () => {
      // tslint:disable-next-line
      await getUserListActions.getUserList(confObj as IUserListingTableConfig)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    it("should not appear topic status in config", async () => {
      // tslint:disable-next-line
      await getUserListActions.getUserList(confObj2 as IUserListingTableConfig)(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      userServiceStub.restore();
    });
  });

  describe("getUserList", () => {
    let userServiceStub: any;
    const dispatch = jest.fn();
    const errorObj = {};
    const expectedAction = [{ payload: errorObj, type: GetUserListType.ERROR }];

    beforeAll(() => {
      userServiceStub = Sinon.stub(UserService, "getUserList").callsFake((config: any) =>
        Promise.reject(errorObj as any)
      );
    });

    it("should call error branch because of rejection", () => {
      try {
        getUserListActions.getUserList({} as any)(dispatch);
      } catch (e) {
        expect(dispatch.mock.calls).toContainEqual(expectedAction);
      }
    });

    afterAll(() => {
      userServiceStub.restore();
    });
  });
});
