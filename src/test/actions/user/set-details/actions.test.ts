import Sinon from "sinon";

import * as setUserDetailsActions from "../../../../actions/user/set-details/actions";
import { UserSetDetailsTypes } from "../../../../actions/user/set-details/types";
import { UserService } from "../../../../services/UserService";

describe("userListActions/", () => {
  it('should have the property  "type"', () => {
    const actions = [
      setUserDetailsActions.userSetDetailsRequestAction,
      setUserDetailsActions.userSetDetailsSuccessAction,
      setUserDetailsActions.userSetDetailsErrorAction,
    ];
    actions.forEach(action => {
      expect(action({})).toHaveProperty("type");
    });
  });

  it("setUserDetails should be a promise", () => {
    expect(setUserDetailsActions.setUserDetails).toBeDefined();
  });

  it("userSetDetailsRequestAction should be a defined", () => {
    setUserDetailsActions.setUserDetails({ test: "test" });
    expect(setUserDetailsActions.userSetDetailsRequestAction({ test: "test" })).toBeDefined();
  });

  it("test dispatch", async () => {
    const dispatch = jest.fn();
    // tslint:disable-next-line: no-void-expression
    await Promise.resolve(setUserDetailsActions.setUserDetails({ test: "test" })(dispatch) as any);
    expect(dispatch).toBeCalledWith({
      payload: { test: "test" },
      type: "USER_SET_DETAILS_REQUEST",
    });
    expect(dispatch).toBeCalledTimes(1);
  });

  describe("setUserDetails", () => {
    let userServiceStub: any;
    const dispatch = jest.fn();
    const userObj = { data: {} };
    const expectedAction = [{ payload: userObj.data, type: UserSetDetailsTypes.SUCCESS }];

    beforeAll(() => {
      userServiceStub = Sinon.stub(UserService, "setUserDetails").callsFake((userData: any) =>
        Promise.resolve(userObj as any)
      );
    });

    it("should call dispatch", async () => {
      // tslint:disable-next-line
      await setUserDetailsActions.setUserDetails({})(dispatch);
      expect(dispatch.mock.calls).toContainEqual(expectedAction);
    });

    afterAll(() => {
      userServiceStub.restore();
    });
  });

  describe("setUserDetails", () => {
    let userServiceStub: any;
    const dispatch = jest.fn();

    beforeAll(() => {
      userServiceStub = Sinon.stub(UserService, "setUserDetails").callsFake((userData: any) =>
        Promise.reject({} as any)
      );
    });

    it("should fallback to empty string if there is no error message", () => {
      try {
        setUserDetailsActions.setUserDetails({})(dispatch);
      } catch (e) {
        expect(Object.keys(e).length).toEqual(0);
      }
    });

    afterAll(() => {
      userServiceStub.restore();
    });
  });
});
