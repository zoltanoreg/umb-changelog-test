import * as sessionHelper from "../../helper/sessionHelper";

describe("#sessionHelper", () => {
  describe("isExpired", () => {
    it("should return false for not expired timestamp", () => {
      expect(sessionHelper.isExpired("7287926400")).toBeFalsy();
    });
    it("should return true for expired timestamp", () => {
      expect(sessionHelper.isExpired("692496000")).toBeTruthy();
    });
  });
  describe("redirect", () => {
    it("should redirect to PAC login", () => {
      const mockAssign = jest.fn();
      window.location.assign = mockAssign;
      sessionHelper.redirect();
      expect(mockAssign).toHaveBeenCalledWith("https://login.nextcloud.aero/");
    });
  });
});
