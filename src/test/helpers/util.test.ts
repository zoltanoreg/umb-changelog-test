import { getFormattedDate, isCurrentUserRole } from "../../helper/util";
import { ADMINISTRATOR, READ } from "../../models/UserTypes";

describe("util #isCurrentUserRole", () => {
  let user: any;

  beforeAll(() => {
    user = {
      data: {
        role: READ,
      },
    };
  });

  it("should return true, if the role is in the allowed roles", () => {
    expect(isCurrentUserRole(user, [ADMINISTRATOR, READ])).toEqual(true);
  });

  it("should return false, if the role is not in the allowed roles", () => {
    expect(isCurrentUserRole(user, [ADMINISTRATOR])).toEqual(false);
  });

  it("should return false, if the provided user hasn't got the correct structure", () => {
    expect(isCurrentUserRole({}, [ADMINISTRATOR])).toEqual(false);
  });
});
