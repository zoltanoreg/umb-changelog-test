import { getRedirectUrl } from "../../helper/redirectHelper";
import {
  ADMINISTRATOR,
  APPLICATION_OWNER,
  READ,
  UMBT_IT_SUPPORT_TEAM,
  UMBT_OPERATIONAL_SUPPORT_TEAM,
} from "../../models/UserTypes";

describe("RedirectHelper", () => {
  const currentUser = {
    authenticated: true,
    data: {
      email: "testEmail",
      firstName: "testFirstName",
      fullName: "testFullName",
      jwt: "testJwt",
      lastName: "testLastName",
      role: "REPLACE_ROLE",
      userId: "testUserId",
    },
    error: false,
    loading: false,
  };

  it("no user data provided", () => {
    const currentCurrentUser = {};
    expect("/error").toEqual(getRedirectUrl(currentCurrentUser));
  });

  it("specifies which URL the Administrator goes to", () => {
    const currentCurrentUser = { ...currentUser };
    currentCurrentUser.data.role = ADMINISTRATOR;
    expect("/applicationClients").toEqual(getRedirectUrl(currentCurrentUser));
  });

  it("specifies which URL the Application owner goes to", () => {
    const currentCurrentUser = { ...currentUser };
    currentCurrentUser.data.role = APPLICATION_OWNER;
    expect("/applicationClients").toEqual(getRedirectUrl(currentCurrentUser));
  });

  it("specifies which URL the Readonly goes to", () => {
    const currentCurrentUser = { ...currentUser };
    currentCurrentUser.data.role = READ;
    expect("/applicationClients").toEqual(getRedirectUrl(currentCurrentUser));
  });

  it("specifies which URL the IT support team goes to", () => {
    const currentCurrentUser = { ...currentUser };
    currentCurrentUser.data.role = UMBT_IT_SUPPORT_TEAM;
    expect("/users").toEqual(getRedirectUrl(currentCurrentUser));
  });

  it("specifies which URL the Operational support team goes to", () => {
    const currentCurrentUser = { ...currentUser };
    currentCurrentUser.data.role = UMBT_OPERATIONAL_SUPPORT_TEAM;
    expect("/configurationFiles").toEqual(getRedirectUrl(currentCurrentUser));
  });
});
