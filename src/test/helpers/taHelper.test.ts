import { getTAClass, TA_TYPES } from "../../helper/taHelper";

describe("Automation class helper", () => {
  it("should exist", () => {
    expect(getTAClass).toBeDefined();
  });

  it("should create class with page and type", () => {
    const className = getTAClass("pageName", TA_TYPES.BUTTON);
    expect(className).toEqual(`ta-pageName-${TA_TYPES.BUTTON}`);
  });

  it("should create class with page, type and container", () => {
    const className = getTAClass("pageName", TA_TYPES.BUTTON, "container");
    expect(className).toEqual(`ta-pageName-${TA_TYPES.BUTTON}-container`);
  });

  it("should create class with page, type, container and subcontainer", () => {
    const className = getTAClass("pageName", TA_TYPES.BUTTON, "container", "subcontainer");
    expect(className).toEqual(`ta-pageName-${TA_TYPES.BUTTON}-container-subcontainer`);
  });

  it("should create class with page, type, container, subcontainer and specialization", () => {
    const className = getTAClass(
      "pageName",
      TA_TYPES.BUTTON,
      "container",
      "subcontainer",
      "specialization"
    );
    expect(className).toEqual(
      `ta-pageName-${TA_TYPES.BUTTON}-container-subcontainer-specialization`
    );
  });

  it("should create class with page, type, container, subcontainer, specialization and index", () => {
    const className = getTAClass(
      "pageName",
      TA_TYPES.BUTTON,
      "container",
      "subcontainer",
      "specialization",
      "index"
    );
    expect(className).toEqual(
      `ta-pageName-${TA_TYPES.BUTTON}-container-subcontainer-specialization-index`
    );
  });
});
