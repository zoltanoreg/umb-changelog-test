import { mount, ReactWrapper } from "enzyme";
import { ButtonPrimary, Checkbox, Link as NextLink } from "next-components";
import React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon, { SinonStubbedInstance } from "sinon";

import { PopupActions } from "../../actions/popup/types";
import { TDeleteAssociation } from "../../actions/topic/delete-association/types";
import { AppTopicsList } from "../../components/AppTopicsList";
import { BatchActionActive } from "../../components/next-component-fork";
import { ADMINISTRATOR, APPLICATION_OWNER } from "../../models/UserTypes";
import { ApplicationService } from "../../services/ApplicationService";
import { BrokerTypeService } from "../../services/BrokerTypeService";
import { MQTTService } from "../../services/MQTTService";
import * as testUtils from "../testUtils";

describe("AppTopicsList [delete]", () => {
  let wrapper: ReactWrapper;
  const mockStore = createMockStore([thunk]);

  let brokerTypeServiceStub: SinonStubbedInstance<any>;
  const brokerTypesObj = {
    data: {
      data: {
        content: [{}],
        count: 1,
      },
    },
  };

  beforeAll(() => {
    brokerTypeServiceStub = Sinon.stub(BrokerTypeService, "getBrokerTypeList").callsFake(
      (config: any) => Promise.resolve(brokerTypesObj as any)
    );
  });

  afterAll(() => {
    brokerTypeServiceStub.restore();
  });

  describe("on PUBLISHER page", () => {
    describe("with APPLICATION_OWNER role", () => {
      const appClientIdIntentional = "";
      const userIdTest = "testUserId";
      const brokertype = "testBrokerType";

      let applicationServiceStub: SinonStubbedInstance<any>;
      let mqttServiceStub: SinonStubbedInstance<any>;

      const topicListsObj = {
        data: {
          data: {
            content: [{ topic: { topicId: "1" } }, { topic: { topicId: "2" } }],
          },
        },
      };

      const deleteAssociationObj = {
        data: {},
      };

      const mStore = mockStore({
        allBrokerType: {
          content: [{}],
        },
        applicationDetails: {
          content: {
            appClientId: appClientIdIntentional,
            appOwner: {
              userId: userIdTest,
            },
          },
        },
        applicationTopicList: {
          content: [
            {
              appClient: {
                appOwner: {
                  userId: "userId",
                },
              },
              brokerTopology: {
                parentBrokerType: {
                  brokerType: brokertype,
                },
                remoteBrokerType: {
                  brokerType: brokertype,
                },
              },
              topic: {
                appClient: {
                  appBrokerType: {
                    brokerType: brokertype,
                  },
                  appClientId: appClientIdIntentional,
                  appName: "testAppName",
                },
                topicId: "testTopicId",
              },
            },
          ],
        },
        currentUser: {
          data: {
            role: APPLICATION_OWNER,
            userId: userIdTest,
          },
        },
      });

      beforeEach(() => {
        wrapper = mount(
          <MemoryRouter initialEntries={[{ key: "testKey" }]}>
            <Provider store={mStore}>
              <AppTopicsList btnClick={jest.fn()} type="PUBLISHER" btnDisabled={true} />
            </Provider>
          </MemoryRouter>
        );

        applicationServiceStub = Sinon.stub(
          ApplicationService,
          "getApplicationTopicList"
        ).callsFake((config: any) => Promise.resolve(topicListsObj as any));

        mqttServiceStub = Sinon.stub(MQTTService, "deleteAssociation").callsFake((config: any) =>
          Promise.resolve(deleteAssociationObj as any)
        );
      });

      afterEach(() => {
        applicationServiceStub.restore();
        mqttServiceStub.restore();
      });

      it("should open confirmation dialog if clicking on dustbin icon", async () => {
        const input = wrapper.find(NextLink).first();
        await act(async () => {
          await input.prop("onClick")();
        });

        const confirmObj = {
          payload: {
            type: "Confirmation",
          },
          type: PopupActions.OPEN,
        };
        const lastDispatch = mStore.getActions()[mStore.getActions().length - 1];
        expect(lastDispatch.payload.type).toEqual(confirmObj.payload.type);
        expect(lastDispatch.type).toEqual(confirmObj.type);

        // Simulate when the user clicked to 'confirm' button
        await lastDispatch.payload.onConfirm();

        const expectedObj = {
          payload: deleteAssociationObj.data,
          type: TDeleteAssociation.SUCCESS,
        };
        expect(mStore.getActions()).toContainEqual(expectedObj);
      });
    });

    describe("with ADMINISTRATOR role", () => {
      const brokertype = "testBrokerType";
      let mStore: any;
      let applicationServiceStub: SinonStubbedInstance<any>;
      let mqttServiceStubBulkDelete: SinonStubbedInstance<any>;
      let mqttServiceStubDelete: SinonStubbedInstance<any>;

      const applicationDetailsAppClientId = "testAppClientId";
      const originalStoreObj = {
        allBrokerType: {
          content: [{}],
        },
        applicationDetails: {
          content: {
            appClientId: applicationDetailsAppClientId,
            appOwner: {
              userId: "userIdTest1",
            },
          },
        },
        applicationTopicList: {
          content: [
            {
              appClient: {
                appOwner: {
                  userId: "userId",
                },
              },
              brokerTopology: {
                parentBrokerType: {
                  brokerType: brokertype,
                },
                remoteBrokerType: {
                  brokerType: brokertype,
                },
              },
              topic: {
                appClient: {
                  appBrokerType: {
                    brokerType: brokertype,
                  },
                  appClientId: "appClientIdTest1",
                  appName: "appNameTest1",
                },
                topicId: "selectedTopicId1",
              },
            },
            {
              appClient: {
                appOwner: {
                  userId: "userId",
                },
              },
              brokerTopology: {
                parentBrokerType: {
                  brokerType: brokertype,
                },
                remoteBrokerType: {
                  brokerType: brokertype,
                },
              },
              topic: {
                appClient: {
                  appBrokerType: {
                    brokerType: brokertype,
                  },
                  appClientId: "appClientIdTest2",
                  appName: "appNameTest2",
                },
                topicId: "selectedTopicId2",
              },
            },
          ],
        },
        currentUser: {
          data: {
            role: ADMINISTRATOR,
            userId: "userIdTest2",
          },
        },
      };

      const responsesExample = {
        emptySubscriberOfLastPublisher: {
          data: {
            data: {
              content: [],
              count: 0,
            },
          },
        },
        existingSubscriberOfLastPublisher: {
          data: {
            data: {
              content: [
                {
                  appClient: {
                    appClientId: applicationDetailsAppClientId,
                  },
                  topic: { topicId: "subscriberTopicId" },
                },
              ],
              count: 1,
            },
          },
        },
        lastPublisher: {
          data: {
            data: {
              content: [
                {
                  appClient: {
                    appClientId: applicationDetailsAppClientId,
                  },
                  topic: {
                    topicId: "1",
                  },
                },
              ],
              count: 1,
            },
          },
        },
        notlastPublisher: {
          data: {
            data: {
              content: [
                {
                  appClient: {
                    appClientId: applicationDetailsAppClientId,
                  },
                  topic: { topicId: "2" },
                },
                {
                  appClient: {
                    appClientId: applicationDetailsAppClientId,
                  },
                  topic: {
                    topicId: "3",
                  },
                },
              ],
              count: 2,
            },
          },
        },
      };

      beforeEach(async () => {
        // Jest timeout increased
        jest.setTimeout(10000);

        mStore = mockStore({ ...originalStoreObj });

        applicationServiceStub = Sinon.stub(
          ApplicationService,
          "getApplicationTopicList"
        ).callsFake((config: any) => Promise.resolve({ data: { data: {} } } as any));

        mqttServiceStubDelete = Sinon.stub(MQTTService, "deleteAssociation").callsFake(
          (topicMqttClientId: string) => Promise.resolve({ data: {} } as any)
        );

        mqttServiceStubBulkDelete = Sinon.stub(MQTTService, "deleteAssociationBulk").callsFake(
          (config: any) => Promise.resolve({ data: {} } as any)
        );

        wrapper = mount(
          <MemoryRouter initialEntries={[{ key: "testKey" }]}>
            <Provider store={mStore}>
              <AppTopicsList btnClick={jest.fn()} type="PUBLISHER" btnDisabled={true} />
            </Provider>
          </MemoryRouter>
        );
        await testUtils.updateWrapper(wrapper);
      });

      afterEach(() => {
        applicationServiceStub.restore();
        mqttServiceStubDelete.restore();
        mqttServiceStubBulkDelete.restore();
      });

      it("[trash icon delete 1x item] -> should open a confirm dialog when trying to delete one row as a last publisher", async () => {
        applicationServiceStub.callsFake((config: any) =>
          Promise.resolve(responsesExample.lastPublisher as any)
        );

        const input = wrapper.find(NextLink).first();
        await act(async () => {
          await input.prop("onClick")();
        });

        const confirmObj = {
          payload: {
            content:
              "You are trying to remove the last publisher to the topic. Last publisher can only be removed if there are no subscribers to the topic. Do you want to continue?",
            type: "Confirmation",
          },
          type: PopupActions.OPEN,
        };

        const popupAction = mStore
          .getActions()
          .filter(
            action =>
              action.payload &&
              action.payload.content === confirmObj.payload.content &&
              action.type === confirmObj.type
          );
        expect(popupAction.length).toEqual(1);
      });

      it("[bulk delete 1x item] -> should not open a confirm dialog when trying to delete one row which is not last publisher", async () => {
        applicationServiceStub.callsFake((config: any) =>
          Promise.resolve(responsesExample.notlastPublisher as any)
        );

        act(() => {
          wrapper
            .find(".ta-application-details-row")
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });

        const confirmObj = {
          payload: {
            content:
              "You are trying to remove the last publisher to the topic. Last publisher can only be removed if there are no subscribers to the topic. Do you want to continue?",
            type: "Confirmation",
          },
          type: PopupActions.OPEN,
        };

        const popupAction = mStore
          .getActions()
          .filter(
            action =>
              action.payload &&
              action.payload.content === confirmObj.payload.content &&
              action.type === confirmObj.type
          );
        expect(popupAction.length).toEqual(0);
        expect(mqttServiceStubDelete.getCalls().length).toEqual(1);
      });

      it("[bulk delete 1x item] -> existing subscriber checking throws exception", async () => {
        // Reset history in order to onCall(0) work as expected
        applicationServiceStub.resetHistory();

        applicationServiceStub
          .onCall(0)
          .callsFake((config: any) => Promise.resolve(responsesExample.lastPublisher as any));
        applicationServiceStub
          .onCall(1)
          .callsFake((config: any) => Promise.reject("Subscriber check rejetion" as any));

        act(() => {
          wrapper
            .find(".ta-application-details-row")
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });

        expect(mqttServiceStubBulkDelete.getCalls().length).toEqual(0);
      });

      it("[bulk delete 2x item] -> trying to delete those rows which had already been deleted", async () => {
        const otherAppClientId = "otherAppClientId";
        const lastPublisherResponses = [
          {
            data: {
              data: {
                content: [
                  {
                    appClient: {
                      appClientId: otherAppClientId,
                    },
                    topic: {
                      topicId: "1",
                    },
                  },
                ],
                count: 1,
              },
            },
          },
          {
            data: {
              data: {
                content: [
                  {
                    appClient: {
                      appClientId: otherAppClientId,
                    },
                    topic: { topicId: "2" },
                  },
                  {
                    appClient: {
                      appClientId: otherAppClientId,
                    },
                    topic: {
                      topicId: "3",
                    },
                  },
                ],
                count: 2,
              },
            },
          },
        ];
        // Reset history in order to onCall(0) work as expected
        applicationServiceStub.resetHistory();

        applicationServiceStub
          .onCall(0)
          .callsFake((config: any) => Promise.resolve(lastPublisherResponses[0] as any));
        applicationServiceStub
          .onCall(1)
          .callsFake((config: any) => Promise.resolve(lastPublisherResponses[1] as any));

        act(() => {
          wrapper
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });
        expect(mqttServiceStubDelete.getCalls().length).toEqual(0);
      });

      it("[bulk delete 2x item] -> should delete both associations [none of them are last publisher]", async () => {
        // Reset history in order to onCall(0) work as expected
        applicationServiceStub.resetHistory();

        applicationServiceStub
          .onCall(0)
          .callsFake((config: any) => Promise.resolve(responsesExample.notlastPublisher as any));
        applicationServiceStub
          .onCall(1)
          .callsFake((config: any) => Promise.resolve(responsesExample.notlastPublisher as any));

        act(() => {
          wrapper
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });

        expect(mqttServiceStubBulkDelete.getCall(0).args[0].mqttIds.length).toEqual(2);
      });

      it("[bulk delete 2x item] -> should delete both associations [one of them is last publisher without subscribers]", async () => {
        // Reset history in order to onCall(0) work as expected
        applicationServiceStub.resetHistory();

        applicationServiceStub
          .onCall(0)
          .callsFake((config: any) => Promise.resolve(responsesExample.lastPublisher as any));
        applicationServiceStub
          .onCall(1)
          .callsFake((config: any) => Promise.resolve(responsesExample.notlastPublisher as any));
        applicationServiceStub
          .onCall(2)
          .callsFake((config: any) =>
            Promise.resolve(responsesExample.emptySubscriberOfLastPublisher as any)
          );

        act(() => {
          wrapper
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });

        expect(mqttServiceStubBulkDelete.getCall(0).args[0].mqttIds.length).toEqual(2);
      });

      it("[bulk delete 2x item] -> should delete that association [one of them is last publisher with subscribers]", async () => {
        // Reset history in order to onCall(0) work as expected
        applicationServiceStub.resetHistory();

        applicationServiceStub
          .onCall(0)
          .callsFake((config: any) => Promise.resolve(responsesExample.lastPublisher as any));
        applicationServiceStub
          .onCall(1)
          .callsFake((config: any) => Promise.resolve(responsesExample.notlastPublisher as any));
        applicationServiceStub
          .onCall(2)
          .callsFake((config: any) =>
            Promise.resolve(responsesExample.existingSubscriberOfLastPublisher as any)
          );

        act(() => {
          wrapper
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });

        expect(mqttServiceStubBulkDelete.getCall(0).args[0].mqttIds.length).toEqual(1);
      });

      it("[bulk delete 2x item] -> last publisher checking throws exception", async () => {
        // Reset history in order to onCall(0) work as expected
        applicationServiceStub.resetHistory();

        applicationServiceStub
          .onCall(0)
          .callsFake((config: any) => Promise.resolve(responsesExample.notlastPublisher as any));
        applicationServiceStub
          .onCall(1)
          .callsFake((config: any) => Promise.reject("Last publisher rejection" as any));

        act(() => {
          wrapper
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });

        expect(mqttServiceStubBulkDelete.getCalls().length).toEqual(0);
      });

      it("[bulk delete 2x item] -> existing subscriber checking throws exception", async () => {
        // Reset history in order to onCall(0) work as expected
        applicationServiceStub.resetHistory();

        applicationServiceStub
          .onCall(0)
          .callsFake((config: any) => Promise.resolve(responsesExample.lastPublisher as any));
        applicationServiceStub
          .onCall(1)
          .callsFake((config: any) => Promise.resolve(responsesExample.notlastPublisher as any));
        applicationServiceStub
          .onCall(2)
          .callsFake((config: any) => Promise.reject("Subscriber check rejetion" as any));

        act(() => {
          wrapper
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });

        expect(mqttServiceStubBulkDelete.getCalls().length).toEqual(0);
      });
    });
  });

  describe("on SUBSCRIBER page", () => {
    describe("with APPLICATION_OWNER role", () => {
      const appClientIdTest = "appClientId";
      const userIdTest = "testUserId";
      const brokertype = "testBrokerType";

      let applicationServiceStub: SinonStubbedInstance<any>;
      let mqttServiceStub: SinonStubbedInstance<any>;

      const topicListsObj = {
        data: {
          data: {
            content: [{ topic: { topicId: "1" } }],
          },
        },
      };

      const deleteAssociationObj = {
        data: {},
      };

      const mStore = mockStore({
        allBrokerType: {
          content: [{}],
        },
        applicationDetails: {
          content: {
            appClientId: appClientIdTest,
            appOwner: {
              userId: userIdTest,
            },
          },
        },
        applicationTopicList: {
          content: [
            {
              appClient: {
                appOwner: {
                  userId: "userId",
                },
              },
              brokerTopology: {
                parentBrokerType: {
                  brokerType: brokertype,
                },
                remoteBrokerType: {
                  brokerType: brokertype,
                },
              },
              topic: {
                appClient: {
                  appBrokerType: {
                    brokerType: brokertype,
                  },
                  appClientId: appClientIdTest,
                  appName: "testAppName",
                },
                topicId: "testTopicId",
              },
            },
          ],
        },
        currentUser: {
          data: {
            role: APPLICATION_OWNER,
            userId: userIdTest,
          },
        },
      });

      beforeEach(() => {
        wrapper = mount(
          <MemoryRouter initialEntries={[{ key: "testKey" }]}>
            <Provider store={mStore}>
              <AppTopicsList btnClick={jest.fn()} type="SUBSCRIBER" btnDisabled={true} />
            </Provider>
          </MemoryRouter>
        );

        applicationServiceStub = Sinon.stub(
          ApplicationService,
          "getApplicationTopicList"
        ).callsFake((config: any) => Promise.resolve(topicListsObj as any));

        mqttServiceStub = Sinon.stub(MQTTService, "deleteAssociation").callsFake((config: any) =>
          Promise.resolve(deleteAssociationObj as any)
        );
      });

      afterEach(() => {
        applicationServiceStub.restore();
        mqttServiceStub.restore();
      });

      it("should open confirmation dialog if clicking on dustbin icon", async () => {
        const input = wrapper.find(NextLink).first();
        await act(async () => {
          await input.prop("onClick")();
        });

        const confirmObj = {
          payload: {
            type: "Confirmation",
          },
          type: PopupActions.OPEN,
        };

        const popupAction = mStore
          .getActions()
          .filter(action => action.payload && action.type === confirmObj.type);
        expect(popupAction.length).toEqual(1);

        // Simulate when the user clicked to 'confirm' button
        await popupAction[0].payload.onConfirm();

        const expectedObj = {
          payload: deleteAssociationObj.data,
          type: TDeleteAssociation.SUCCESS,
        };
        expect(mStore.getActions()).toContainEqual(expectedObj);
      });
    });

    describe("with ADMINISTRATOR role", () => {
      const brokertype = "testBrokerType";
      let mStore: any;
      let applicationServiceStub: SinonStubbedInstance<any>;
      const originalStoreObj = {
        allBrokerType: {
          content: [{}],
        },
        applicationDetails: {
          content: {
            appClientId: "",
            appOwner: {
              userId: "userIdTest1",
            },
          },
        },
        applicationTopicList: {
          content: [
            {
              appClient: {
                appOwner: {
                  userId: "userId",
                },
              },
              brokerTopology: {
                parentBrokerType: {
                  brokerType: brokertype,
                },
                remoteBrokerType: {
                  brokerType: brokertype,
                },
              },
              topic: {
                appClient: {
                  appBrokerType: {
                    brokerType: brokertype,
                  },
                  appClientId: "appClientIdTest1",
                  appName: "appNameTest1",
                },
                topicId: "selectedTopicId1",
              },
            },
            {
              appClient: {
                appOwner: {
                  userId: "userId",
                },
              },
              brokerTopology: {
                parentBrokerType: {
                  brokerType: brokertype,
                },
                remoteBrokerType: {
                  brokerType: brokertype,
                },
              },
              topic: {
                appClient: {
                  appBrokerType: {
                    brokerType: brokertype,
                  },
                  appClientId: "appClientIdTest2",
                  appName: "appNameTest2",
                },
                topicId: "selectedTopicId2",
              },
            },
          ],
        },
        currentUser: {
          data: {
            role: ADMINISTRATOR,
            userId: "userIdTest2",
          },
        },
      };

      beforeEach(async () => {
        mStore = mockStore({ ...originalStoreObj });

        wrapper = mount(
          <MemoryRouter initialEntries={[{ key: "testKey" }]}>
            <Provider store={mStore}>
              <AppTopicsList btnClick={jest.fn()} type="SUBSCRIBER" btnDisabled={true} />
            </Provider>
          </MemoryRouter>
        );
        await testUtils.updateWrapper(wrapper);
      });

      it("[bulk delete 2x item] -> should delete both associations", async () => {
        const topicObj = {
          data: {
            data: {
              content: [{ topic: { topicId: "1" } }, { topic: { topicId: "2" } }],
              count: 2,
            },
          },
        };
        const mqttServiceStub = Sinon.stub(MQTTService, "deleteAssociationBulk").callsFake(
          (config: any) => Promise.resolve({ data: {} } as any)
        );

        applicationServiceStub = Sinon.stub(
          ApplicationService,
          "getApplicationTopicList"
        ).callsFake((config: any) => Promise.resolve(topicObj as any));

        act(() => {
          wrapper
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        expect(wrapper.find(BatchActionActive).length);

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });

        expect(mqttServiceStub.getCall(0).args[0].mqttIds.length).toEqual(
          originalStoreObj.applicationTopicList.content.length
        );

        mqttServiceStub.restore();
        applicationServiceStub.restore();
      });

      it("[bulk delete 1x item] -> should not open a confirm dialog when trying to delete one row which is not last publisher", async () => {
        const lastPublisherResponses = [
          {
            data: {
              data: {
                content: [
                  {
                    appClient: {
                      appClientId: "applicationDetailsAppClientId",
                    },
                    topic: { topicId: "2" },
                  },
                  {
                    appClient: {
                      appClientId: "applicationDetailsAppClientId",
                    },
                    topic: {
                      topicId: "3",
                    },
                  },
                ],
                count: 2,
              },
            },
          },
        ];
        applicationServiceStub.callsFake((config: any) =>
          Promise.resolve(lastPublisherResponses[0] as any)
        );

        const mqttServiceStubDelete = Sinon.stub(MQTTService, "deleteAssociation").callsFake(
          (topicMqttClientId: string) => Promise.resolve({ data: {} } as any)
        );

        act(() => {
          wrapper
            .find(".ta-application-details-row")
            .find(Checkbox)
            .first()
            .prop("onClick")();
          wrapper.setProps({}); // Re-render
        });

        const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);
        await act(async () => {
          await deleteButtonWithoutIcon.prop("onClick")();
        });

        const confirmObj = {
          payload: {
            content:
              "You are trying to remove the last publisher to the topic. Last publisher can only be removed if there are no subscribers to the topic. Do you want to continue?",
            type: "Confirmation",
          },
          type: PopupActions.OPEN,
        };

        const popupAction = mStore
          .getActions()
          .filter(
            (action: any) =>
              action.payload &&
              action.payload.content === confirmObj.payload.content &&
              action.type === confirmObj.type
          );
        expect(popupAction.length).toEqual(0);
        expect(mqttServiceStubDelete.getCalls().length).toEqual(1);
        mqttServiceStubDelete.restore();
      });
    });
  });
});
