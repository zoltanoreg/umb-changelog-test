import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";

import { TopologyList } from "../../components/TopologyList";
import store from "../../store";

describe("TopologyList component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopologyList />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <TopologyList />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
