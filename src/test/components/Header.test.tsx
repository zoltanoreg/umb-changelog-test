import { mount, shallow } from "enzyme";
import { ToolMenu, ToolMenuItem } from "next-components";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter, withRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import { Store } from "redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { Header } from "../../components/Header";
import {
  ADMINISTRATOR,
  APPLICATION_OWNER,
  READ,
  UMBT_IT_SUPPORT_TEAM,
  UMBT_OPERATIONAL_SUPPORT_TEAM,
} from "../../models/UserTypes";
import store from "../../store";

const HeaderWithRouter = withRouter(Header);
const mockStore = createMockStore([thunk]);

describe("Header component", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <MemoryRouter initialEntries={[{ pathname: "/applicationClients", key: "testKey" }]}>
          <HeaderWithRouter></HeaderWithRouter>
        </MemoryRouter>
      </Provider>
    );
  });

  const wrapperHeader = renderer.create(
    <Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: "/applicationClients", key: "testKey" }]}>
        <HeaderWithRouter></HeaderWithRouter>
      </MemoryRouter>
    </Provider>
  );

  it("should match snapshot", () => {
    expect(wrapperHeader).toMatchSnapshot();
  });

  describe("Header component", () => {
    let wrapper: any;
    const { location } = window;

    beforeEach(() => {
      wrapper = mount(
        <Provider store={store}>
          <MemoryRouter initialEntries={[{ pathname: "/applicationClients", key: "testKey" }]}>
            <HeaderWithRouter></HeaderWithRouter>
          </MemoryRouter>
        </Provider>
      );

      delete window.location;
      window.location = { reload: jest.fn() };
    });

    afterEach(() => {
      window.location = location;
    });
  });

  describe("navigates as", () => {
    let wrapper: any;
    let historySpy: Sinon.SinonSpy;

    afterEach(() => {
      if (historySpy) {
        historySpy.restore();
      }
    });

    const simulateNavigation = (mStore: Store) => {
      wrapper = mount(
        <MemoryRouter initialEntries={[{ pathname: "/applicationClients", key: "testKey" }]}>
          <Provider store={mStore}>
            <HeaderWithRouter></HeaderWithRouter>
          </Provider>
        </MemoryRouter>
      );
      historySpy = Sinon.spy(wrapper.instance().history, "push");

      wrapper
        .find(ToolMenu)
        .find("button")
        .simulate("click");

      wrapper
        .find(ToolMenuItem)
        .filter('[className^="ta-menu-item-"]')
        .forEach((item: any, index: number) => {
          if (index > 0) {
            wrapper
              .find(ToolMenu)
              .find("button")
              .simulate("click"); // Reopen
          }
          wrapper
            .find(ToolMenuItem)
            .at(index)
            .simulate("click");
        });
      const historySpyArgs: any[] = [];
      historySpy.getCalls().map(call => historySpyArgs.push(call.args[0]));

      return historySpyArgs;
    };

    it("Administrator", () => {
      const accessibleUrls = [
        "/applicationClients",
        "/topics",
        "/brokerTypes",
        "/configurationFiles",
        "/users",
        "/topology",
      ];
      const mStore = mockStore({
        allBrokerType: {
          content: [{}],
        },
        currentUser: {
          data: {
            role: ADMINISTRATOR,
            userId: "test",
          },
        },
      });
      const historySpyArgs = simulateNavigation(mStore);
      expect(historySpyArgs).toEqual(accessibleUrls);
    });

    it("Application Owner", () => {
      const accessibleUrls = [
        "/applicationClients",
        "/topics",
        "/brokerTypes",
        "/configurationFiles",
        "/topology",
      ];
      const mStore = mockStore({
        allBrokerType: {
          content: [{}],
        },
        currentUser: {
          data: {
            role: APPLICATION_OWNER,
            userId: "test",
          },
        },
      });
      const historySpyArgs = simulateNavigation(mStore);
      expect(historySpyArgs).toEqual(accessibleUrls);
    });

    it("ReadOnly", () => {
      const accessibleUrls = [
        "/applicationClients",
        "/topics",
        "/brokerTypes",
        "/configurationFiles",
      ];
      const mStore = mockStore({
        allBrokerType: {
          content: [{}],
        },
        currentUser: {
          data: {
            role: READ,
            userId: "test",
          },
        },
      });
      const historySpyArgs = simulateNavigation(mStore);
      expect(historySpyArgs).toEqual(accessibleUrls);
    });

    it("Umbt_it_support_team", () => {
      const accessibleUrls = ["/users"];
      const mStore = mockStore({
        allBrokerType: {
          content: [{}],
        },
        currentUser: {
          data: {
            role: UMBT_IT_SUPPORT_TEAM,
            userId: "test",
          },
        },
      });
      const historySpyArgs = simulateNavigation(mStore);
      expect(historySpyArgs).toEqual(accessibleUrls);
    });

    it("Umbt_it_support_team", () => {
      const accessibleUrls = ["/configurationFiles"];
      const mStore = mockStore({
        allBrokerType: {
          content: [{}],
        },
        currentUser: {
          data: {
            role: UMBT_OPERATIONAL_SUPPORT_TEAM,
            userId: "test",
          },
        },
      });
      const historySpyArgs = simulateNavigation(mStore);
      expect(historySpyArgs).toEqual(accessibleUrls);
    });
  });
});
