import { shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { StatusIcon } from "../../components/StatusIcon";
import store from "../../store";

describe("status icon component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <StatusIcon check={true} />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <StatusIcon check={true} />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
