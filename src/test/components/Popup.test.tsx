import { mount, shallow } from "enzyme";
import { ModalConfirmation, ModalInfo } from "next-components";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import * as popupActions from "../../actions/popup/actions";
import { Popup } from "../../components/Popup";
import store from "../../store";

const mockStore = createMockStore([thunk]);

describe("popup component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <Popup />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <Popup />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe("Popup component", () => {
    const popupCloseActionSpy = jest.spyOn(popupActions, "popupCloseAction");
    let wrapper: any;

    it("calls close action", () => {
      const mStore = mockStore({
        popupData: {
          content: "content",
          onConfirm: jest.fn(),
          open: true,
          title: "title",
          type: "",
        },
      });

      wrapper = mount(
        <Provider store={mStore}>
          <Popup />
        </Provider>
      );
      wrapper.find(ModalInfo).prop("onClose")();
      expect(popupCloseActionSpy).toHaveBeenCalled();
    });

    it("pops up a confirmation window", () => {
      const mStore = mockStore({
        popupData: {
          content: "content",
          onConfirm: jest.fn(),
          open: true,
          title: "title",
          type: "Confirmation",
        },
      });

      wrapper = mount(
        <Provider store={mStore}>
          <Popup />
        </Provider>
      );
      expect(wrapper.find(ModalConfirmation)).toHaveLength(1);
      wrapper.find(ModalConfirmation).prop("onCancel")();
      expect(popupCloseActionSpy).toHaveBeenCalled();
    });
  });
});
