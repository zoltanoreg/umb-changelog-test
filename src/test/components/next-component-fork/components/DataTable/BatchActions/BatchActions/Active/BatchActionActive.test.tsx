import { mount, shallow } from "enzyme";
import {
  ButtonGhostInverted,
  ButtonPrimary,
  ButtonPrimaryIcon,
  IconDelete16,
} from "next-components";
import * as React from "react";

import { BatchActionActive } from "../../../../../../../../components/next-component-fork";

describe("BatchActionActive component test", () => {
  it("renders without crashing", () => {
    const props = {
      label: "test",
      onCancel: () => null,
    };
    shallow(<BatchActionActive {...props} />);
  });

  it("check if matches snapshot", () => {
    const props = {
      label: "test",
      onCancel: () => null,
    };
    const wrapper = mount(<BatchActionActive {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it("invokes callback functions when click on function buttons", () => {
    const callbackButton1 = jest.fn();
    const callbackButton2 = jest.fn();

    const props = {
      actions: [
        {
          icon: <IconDelete16 />,
          onClick: callbackButton1,
          title: "buttonWithIcon",
        },
        {
          onClick: callbackButton2,
          title: "buttonWithoutIcon",
        },
      ],
      label: "test",
      onCancel: () => null,
    };
    mount(<BatchActionActive {...props} />);
    expect(ButtonPrimaryIcon.length).toEqual(props.actions.filter(button => button.icon).length);
    expect(ButtonPrimary.length).toEqual(props.actions.filter(button => !button.icon).length);
  });

  it("clicks on cancel", () => {
    const callbackCancel = jest.fn();
    const props = {
      actions: [
        {
          icon: <IconDelete16 />,
          onClick: () => null,
          title: "buttonWithIcon",
        },
      ],
      label: "test",
      onCancel: callbackCancel,
    };
    const wrapper = mount(<BatchActionActive {...props} />);
    wrapper
      .find(ButtonGhostInverted)
      .first()
      .simulate("click");
    expect(callbackCancel).toBeCalled();
  });
});
