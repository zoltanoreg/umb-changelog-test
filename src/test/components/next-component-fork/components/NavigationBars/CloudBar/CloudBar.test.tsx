import { mount, shallow } from 'enzyme';
import { IconAppSwitcher32, IconClose32 } from 'next-components';
import React from 'react';
import { act } from 'react-dom/test-utils';

import CloudBar from '../../../../../../components/next-component-fork/components/NavigationBars/CloudBar/CloudBar';
import { LargeSideBarContainer } from '../../../../../../components/next-component-fork/components/NavigationBars/CloudBar/CloudBar.style';
import { ITools } from '../../../../../../components/next-component-fork/components/NavigationBars/CloudBar/ICloudBar';
import { CloseButton } from '../../../../../../components/next-component-fork/components/NavigationBars/CloudBar/SideBar.style';
import SideBarItem from '../../../../../../components/next-component-fork/components/NavigationBars/CloudBar/SideBarItem';

describe('#CloudBar component', () => {
  it("renders without crashing", () => {
    shallow(
      <CloudBar
        cloudHomeUrl={``}
        user={{
          name: '',
          notificationsLink: '',
          profileLink: '',
          settingsLink: '',
        }} />
    );
  });

  it("should show large sidebar", () => {
    const wrapper = mount(
      <CloudBar
        cloudHomeUrl={``}
        user={{
          name: '',
          notificationsLink: '',
          profileLink: '',
          settingsLink: '',
        }}
        lastUsedTools={[
          // tslint:disable-next-line:no-object-literal-type-assertion
          {
            icon: 'https://next-components.s3.eu-central-1.amazonaws.…',
            name: 'Insights',
            path: '#',
          } as ITools,
        ]}
        />
    );

    expect(wrapper.find(LargeSideBarContainer).length).toBe(0);
    
    act(() => {
      wrapper.find(IconAppSwitcher32).parents(SideBarItem).simulate('click');
      wrapper.setProps({});
    });
    
    expect(wrapper.find(LargeSideBarContainer).length).toBeGreaterThan(0);
    
    act(() => {
      wrapper.find(IconClose32).parents(CloseButton).simulate('click')
      wrapper.setProps({});
    });
  });

  it("should render sidebaritem as node", () => {
    const wrapper = mount(
      <CloudBar
        cloudHomeUrl={``}
        user={{
          name: '',
          notificationsLink: (props: any) => <div className="test-class-name-for-node">{props.children}</div>,
          profileLink: () => (props: any) => <>{props.children}</>,
          settingsLink: () => (props: any) => <>{props.children}</>,
        }}
        />
    );
    expect(wrapper.find(".test-class-name-for-node")).toBeDefined();
  });
});