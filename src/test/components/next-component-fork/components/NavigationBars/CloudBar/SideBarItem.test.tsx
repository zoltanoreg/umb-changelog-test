import { shallow } from 'enzyme';
import React from 'react';

import SideBarItem from '../../../../../../components/next-component-fork/components/NavigationBars/CloudBar/SideBarItem';

describe('#SideBarItemNode component', () => {
  it("renders without crashing", () => {
    shallow(
      <SideBarItem
        badge={() => <></>}
        icon={() => <></>}
        showLabel={true}
        label={`Test`}
      />
    );
  });
});