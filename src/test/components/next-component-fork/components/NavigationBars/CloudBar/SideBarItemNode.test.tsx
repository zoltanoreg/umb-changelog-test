import { shallow } from 'enzyme';
import React from 'react';

import SideBarItemNode from '../../../../../../components/next-component-fork/components/NavigationBars/CloudBar/SideBarItemNode';

describe('#SideBarItemNode component', () => {
  it("renders without crashing", () => {
    shallow(
      <SideBarItemNode
        badge={() => <></>}
        icon={() => <></>}
        showLabel={true}
        label={`Test`}
      />
    );
  });
});