import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";

import { RouteUnauthenticated } from "../../components/RouteUnauthenticated";
import store from "../../store";

describe("RouteAuthenticated component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <RouteUnauthenticated />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <MemoryRouter>
        <Provider store={store}>
          <RouteUnauthenticated />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
