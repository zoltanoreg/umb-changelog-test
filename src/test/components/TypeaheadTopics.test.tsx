import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { Typeahead } from "../../components/Typeahead";
import { TypeaheadTopics } from "../../components/TypeaheadTopics";
import { topicStatusEnum } from "../../models/ITopicConfig";
import store from "../../store";

describe("type ahead user component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TypeaheadTopics />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      value: {
        topicName: "test",
        topicStatus: topicStatusEnum.DRAFT,
      },
    };
    const wrapper = mount(
      <Provider store={store}>
        <TypeaheadTopics {...setValues} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it("check draft chiplabel", () => {
    const setValues = {
      pageSelector: "test",
      value: {
        topicId: "test",
        topicName: "test",
        topicStatus: topicStatusEnum.DRAFT,
      },
    };
    const wrapper = mount(
      <Provider store={store}>
        <TypeaheadTopics {...setValues} />
      </Provider>
    );

    wrapper.find(Typeahead).prop("chipLabel")(setValues);
    expect(
      wrapper
        .find(".ta-test-typeahead-topic-chip")
        .first()
        .prop("label")
    ).toEqual("test ---- Topic is a draft ⚠️");
  });

  it("check onhold chiplabel", () => {
    const setValues = {
      pageSelector: "test",
      value: {
        topicId: "test",
        topicName: "test",
        topicStatus: topicStatusEnum.ONHOLD,
      },
    };
    const wrapper = mount(
      <Provider store={store}>
        <TypeaheadTopics {...setValues} />
      </Provider>
    );

    wrapper.find(Typeahead).prop("chipLabel")(setValues);
    expect(
      wrapper
        .find(".ta-test-typeahead-topic-chip")
        .first()
        .prop("label")
    ).toEqual("test ---- Topic is on hold ⚠️");
  });
});
