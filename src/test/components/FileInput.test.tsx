import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { FileInput } from "../../components/FileInput";
import store from "../../store";
import { SHiddenInput } from "../../styles/styles";

describe("FileInput component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <FileInput />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <FileInput />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("should handle file input", () => {
    const mockFn = jest.fn();
    const props = {
      onChange: mockFn,
    };
    const wrapper = mount(
      <Provider store={store}>
        <FileInput {...props} />
      </Provider>
    );
    const event = {
      target: {
        files: [{ name: "fileName" }],
      },
    };
    wrapper.find(SHiddenInput).simulate("change", event);
    expect(mockFn).toHaveBeenCalled();
    expect(mockFn.mock.calls).toContainEqual([event.target.files[0]]);
  });

  it("should not handle file input if there is no entered file", () => {
    const mockFn = jest.fn();
    const props = {
      onChange: mockFn,
    };
    const wrapper = mount(
      <Provider store={store}>
        <FileInput {...props} />
      </Provider>
    );
    const event = {
      target: {
        files: [],
      },
    };
    wrapper.find(SHiddenInput).simulate("change", event);
    expect(mockFn).not.toHaveBeenCalled();
  });
});
