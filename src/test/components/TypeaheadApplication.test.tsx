import { shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { TypeaheadApplication } from "../../components/TypeaheadApplication";
import store from "../../store";

describe("type ahead user component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TypeaheadApplication />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <TypeaheadApplication />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
