import { shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { TopicDetailsTabs } from "../../../../components/pages/TopicDetails/TopicDetailsTabs";
import { tabEnum } from "../../../../models/ITopicDetailsConfig";
import store from "../../../../store";

describe("TopicDetailsTabs component test", () => {
  it("renders without crashing", () => {
    const fakeFc = jest.fn();
    shallow(
      <Provider store={store}>
        <TopicDetailsTabs onChange={fakeFc} activeTab={tabEnum.DETAILS} />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const fakeFc = jest.fn();
    const wrapper = renderer.create(
      <Provider store={store}>
        <TopicDetailsTabs onChange={fakeFc} activeTab={tabEnum.DETAILS} />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
