import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { TopicDetailsBridgesList } from "../../../../components/pages/TopicDetails/TopicBridgesList";
import { ADMINISTRATOR } from "../../../../models/UserTypes";
import store from "../../../../store";

const mockStore = createMockStore([thunk]);

describe("TopicDetailsBridgesList component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopicDetailsBridgesList />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setvalues = {
      data: {
        topicId: "test",
      },
    };
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <TopicDetailsBridgesList {...setvalues} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe("TopicDetailsBridgesList [ordering and filtering] ", () => {
    const topicIdTest = "testTopicId";
    /* let useStateSpy: any; */

    const props = {
      data: {
        topicId: topicIdTest,
      },
    };
    let wrapper: any;
    const mStore = mockStore({
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
      topicBridges: {
        content: [
          {
            brokerTopology: {
              brokerTopologyName: "brokerTopologyName",
              parentBrokerType: {
                brokerTypeName: "brokerTypeName",
              },
              remoteBrokerType: {
                brokerTypeName: "brokerTypeName1",
              },
            },
            createdAt: null,
            createdBy: {
              fullName: "fullName",
            },
            direction: "direction",
            localPrefix: "localPrefix",
            qosLevel: "qosLevel",
            remotePrefix: "remotePrefix",
            secure: "secure",
          },
        ],
        loading: false,
      },
    });

    beforeEach(() => {
      /* useStateSpy = Sinon.spy(React, "useState"); */
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <TopicDetailsBridgesList {...props} />
          </Provider>
        </MemoryRouter>
      );
    });

    afterEach(() => {
      /* useStateSpy.restore(); */
    });

    it("should change sorting direction", () => {
      let header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("none");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("ascending");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("descending");
    });
  });
});
