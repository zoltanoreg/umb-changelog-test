import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { TopicDetailsMainDetails } from "../../../../components/pages/TopicDetails/TopicDetailsMainDetails";
import store from "../../../../store";

const mockStore = createMockStore([thunk]);

describe("TopicDetailsMainDetails component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopicDetailsMainDetails data="" />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <TopicDetailsMainDetails data="" />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("should present details about the topic", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
    });
    const fullNameTest = "testFullName";
    const statusTest = "testStatus";
    const props = {
      data: {
        appClient: {
          appClientId: "appClientId",
          appOwner: {
            fullName: fullNameTest,
          },
        },
        status: statusTest,
      },
    };

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <TopicDetailsMainDetails {...props} />
        </Provider>
      </MemoryRouter>
    );
    expect(
      wrapper
        .find(".ta-topicDetails-text-status")
        .first()
        .text()
        .indexOf(statusTest)
    ).toBeGreaterThanOrEqual(0);
    expect(
      wrapper
        .find(".ta-topicDetails-text-ownerAdmin")
        .first()
        .text()
        .indexOf(fullNameTest)
    ).toBeGreaterThanOrEqual(0);
  });
});
