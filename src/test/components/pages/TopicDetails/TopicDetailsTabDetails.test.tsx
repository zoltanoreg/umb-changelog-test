import { shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { TopicDetailsTabDetails } from "../../../../components/pages/TopicDetails/TopicDetailsTabDetails";
import store from "../../../../store";

describe("TopicDetailsTabDetails component test", () => {
  const content = {
    appClient: {
      appBrokerType: {
        brokerType: "test",
        brokerTypeId: "test",
      },
      appClientId: "test",
      appName: "test",
      appOwner: {
        fullName: "test",
        userId: "test",
      },
      appVersion: "test",
      modifiedAt: "test",
    },
    createdAt: "test",
    createdBy: {
      fullName: "test",
      userId: "test",
    },
    dependencies: "test",
    description: "test",
    featureName: "test",
    isSecure: false,
    modifiedAt: "test",
    modifiedBy: {
      fullName: "test",
      userId: "test",
    },
    obfuscationRequirement: 0,
    offloadType: "test",
    priority: 0,
    qosLevel: "test",
    retainRequired: true,
    serviceName: "test",
    status: "test",
    topicCategory: "test",
    topicId: "test",
    topicName: "test",
    topicType: "test",
    versionId: "test",
  };
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopicDetailsTabDetails data={content} />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <TopicDetailsTabDetails data={content} />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
