import { mount, shallow } from "enzyme";
import moment from "moment-timezone";
import * as React from "react";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { TopicDetailsPayload } from "../../../../components/pages/TopicDetails/TopicDetailsPayload";
import store from "../../../../store";

const mockStore = createMockStore([thunk]);

describe("TopicDetailsPayload component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopicDetailsPayload payloadId="test" />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    moment.tz.setDefault("America/Los_Angeles");
    const mStore = mockStore({
      topicDetails: { content: { topic: { topicPayloadId: "test" } } },
      topicPayload: { content: {} },
    });
    const wrapper = mount(
      <Provider store={mStore}>
        <TopicDetailsPayload payloadId="test" />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
