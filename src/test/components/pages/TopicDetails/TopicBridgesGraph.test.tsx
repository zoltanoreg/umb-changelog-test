import { mount, shallow } from "enzyme";
// tslint:disable-next-line: no-import-side-effect
import "jest-canvas-mock";
import * as React from "react";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { TopicBridgesGraph } from "../../../../components/pages/TopicDetails/TopicBridgesGraph";
import { TopologyGraphVis } from "../../../../components/TopologyGraphVis";
import store from "../../../../store";

const mockStore = createMockStore([thunk]);

describe("TopicBridgesGraph component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopicBridgesGraph />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = shallow(
      <Provider store={store}>
        <TopicBridgesGraph />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("should generate graph if topicBridge information are present", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      topicBridges: {
        content: [
          {
            brokerTopology: {
              parentBrokerType: {
                brokerType: "1",
              },
              remoteBrokerType: {
                brokerType: "1",
              },
            },
          },
        ],
        loading: false,
      },
      topicMQTTClients: {
        content: [],
      },
    });
    const props = {
      data: {
        appClient: {
          appBrokerType: {
            brokerType: "brokerType",
          },
        },
        topicId: "testTopicId",
      },
    };

    const wrapper = mount(
      <Provider store={mStore}>
        <TopicBridgesGraph {...props} />
      </Provider>
    );
    expect(wrapper.find(TopologyGraphVis).length).toEqual(1);
  });

  it("should generate graph if topicMQTTClients information are present", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      topicBridges: {
        content: [],
        loading: false,
      },
      topicMQTTClients: {
        content: [
          {
            needBridge: "0",
          },
        ],
      },
    });
    const props = {
      data: {
        appClient: {
          appBrokerType: {
            brokerType: "brokerType",
          },
        },
        topicId: "testTopicId",
      },
    };

    const wrapper = mount(
      <Provider store={mStore}>
        <TopicBridgesGraph {...props} />
      </Provider>
    );
    expect(wrapper.find(TopologyGraphVis).length).toEqual(1);
  });
});
