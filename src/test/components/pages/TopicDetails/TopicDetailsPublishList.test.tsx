import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { TopicDetailsPublishList } from "../../../../components/pages/TopicDetails/TopicDetailsPublishList";
import store from "../../../../store";

const mockStore = createMockStore([thunk]);

describe("TopicDetailsPublishList component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopicDetailsPublishList />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          userId: "test",
        },
      },
      topicClientList: {
        content: [
          {
            appClient: {
              appBrokerType: { brokerType: "test" },
              appName: "test",
              appOwner: { fullName: "test" },
            },
            modifiedBy: { fullName: "test" },
          },
        ],
        loading: "test",
      },
      topicDetails: {
        content: {
          topic: "test",
        },
        error: "test",
      },
    });
    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <TopicDetailsPublishList />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
