import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { Error } from "../../../components/pages/Error";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("error component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <Error />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <MemoryRouter>
        <Provider store={store}>
          <Error />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("should present error", () => {
    const messageTest = "TestMessage";
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          message: messageTest,
        },
        error: true,
      },
    });

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <Error />
        </Provider>
      </MemoryRouter>
    );
    expect(
      wrapper
        .find(Error)
        .first()
        .text()
        .indexOf(messageTest)
    ).toBeGreaterThanOrEqual(0);
  });
});
