import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";

import * as applicationClientDetailsActions from "../../../actions/application/get-details/actions";
import { ApplicationClientsDetails } from "../../../components/pages/ApplicationClientsDetails";
import store from "../../../store";

describe("application clients details component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ApplicationClientsDetails />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      match: {
        params: {
          appId: "test",
        },
      },
    };
    const wrapper = renderer.create(
      <MemoryRouter>
        <Provider store={store}>
          <ApplicationClientsDetails {...setValues} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("renders without crashing on undefined id, but not getting details", () => {
    const router = {
      match: {
        params: {
          appId: undefined,
        },
      },
    };

    const spyFn = jest.spyOn(applicationClientDetailsActions, "getApplicationDetails");

    const wrongWrapper = mount(
      <MemoryRouter>
        <Provider store={store}>
          <ApplicationClientsDetails {...router} />
        </Provider>
      </MemoryRouter>
    );

    expect(spyFn).not.toHaveBeenCalled();

    spyFn.mockRestore();
  });
});
