import { mount, ReactWrapper, shallow } from "enzyme";
// tslint:disable-next-line:no-implicit-dependencies
import { createBrowserHistory } from "history";
import * as React from "react";
import { Provider } from "react-redux";
// tslint:disable-next-line:no-implicit-dependencies
import { MemoryRouter, Router } from "react-router";
import renderer, { act } from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { BrokerTypesDetailsTabAppClient } from "../../../../components/pages/BrokerTypesDetails/BrokerTypesDetailsTabAppClient";
import { ApplicationService } from "../../../../services/ApplicationService";
import store from "../../../../store";
jest.useFakeTimers();

const mockStore = createMockStore([thunk]);

const copyObject = (obj: any, max: number) => {
  const arr = [];
  for (let i = 0; i < max; i = i + 1) {
    arr.push({ ...obj, appClientId: i });
  }

  return arr;
};

describe("BrokerTypesDetailsTabAppClient component test", () => {
  const setValues = {
    content: {
      brokerType: "test",
    },
  };

  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <BrokerTypesDetailsTabAppClient {...setValues} />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const fakeFc = jest.fn();
    const wrapper = renderer.create(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <BrokerTypesDetailsTabAppClient {...setValues} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe("testing filters", () => {
    let wrapper: ReactWrapper;
    let mStore: any;
    const mockHistory = createBrowserHistory();

    beforeAll(() => {
      mStore = mockStore({
        applicationList: {
          content: copyObject(
            {
              appName: "testName",
              appOwner: "testOwner",
              status: "ACTIVE",
            },
            11
          ),
          count: 11,
        },
      });

      wrapper = mount(
        <Router history={mockHistory}>
          <Provider store={mStore}>
            <BrokerTypesDetailsTabAppClient {...setValues} />
          </Provider>
        </Router>
      );
    });

    it("should change sorting direction", () => {
      let header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect((header2.props() as any).sortDirection).toEqual("none");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect((header2.props() as any).sortDirection).toEqual("ascending");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect((header2.props() as any).sortDirection).toEqual("descending");
    });

    it("should filter", () => {
      const enteredInput = "test";

      let input = wrapper.find(".ta-brokerTypesDetailsAppClients-filter-appNameAndVersion").first();
      const handler = input.prop("onChange");

      if (handler) {
        act(() => {
          handler({ target: { value: enteredInput } } as any);
        });
      }
      wrapper.update();
      jest.runAllTimers();

      input = wrapper.find(".ta-brokerTypesDetailsAppClients-filter-appNameAndVersion").first();
      expect(input.prop("value")).toEqual(enteredInput);
    });

    it("should paginate", () => {
      const pager = wrapper.find(".ta-brokerTypesDetailsAppClients-pagination button").last();
      const spyAppList = jest.spyOn(ApplicationService, "getApplicationList");

      act(() => {
        pager.simulate("click");
      });

      expect(spyAppList).toBeCalled();

      spyAppList.mockRestore();
    });

    it("should navigate", () => {
      const pager = wrapper.find(".ta-brokerTypesDetailsAppClients-row").first();

      const pushSpy = Sinon.spy(mockHistory, "push");

      act(() => {
        pager.simulate("click");
      });

      expect(pushSpy.called).toEqual(true);
      expect(pushSpy.args[0]).toEqual([`/applicationClients/0`]);

      pushSpy.restore();
    });
  });
});
