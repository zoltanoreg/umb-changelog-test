import { shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { BrokerTypesDetailsTabDetails } from "../../../../components/pages/BrokerTypesDetails/BrokerTypesDetailsTabDetails";
import store from "../../../../store";

describe("BrokerTypesDetailsTabDetails component test", () => {
  const setValues = {
    content: {
      brokerType: "test",
      createdAt: "2020-01-20T16:00:55.000Z",
      createdBy: { fullName: "test" },
      pathToCa: "test",
      pathToCbaIca: "test",
      pathToPrivate: "test",
      systemType: "test",
    },
  };

  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <BrokerTypesDetailsTabDetails {...setValues} />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <BrokerTypesDetailsTabDetails {...setValues} />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
