import { mount, ReactWrapper, shallow } from "enzyme";
// tslint:disable-next-line: no-implicit-dependencies
import { createBrowserHistory } from "history";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter, Router } from "react-router-dom";
import renderer, { act } from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { BrokerTypesDetailsTabsTopicList } from "../../../../components/pages/BrokerTypesDetails/BrokerTypesDetailsTabsTopicList";
import store from "../../../../store";
jest.useFakeTimers();

describe("BrokerTypesDetailsTabsTopicList component test", () => {
  it("renders without crashing", () => {
    shallow(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <BrokerTypesDetailsTabsTopicList brokerType="CB" type="PUBLISHER" />
        </Provider>
      </MemoryRouter>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <BrokerTypesDetailsTabsTopicList brokerType="CB" type="PUBLISHER" />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe("BrokerTypesDetails AppTopicsList", () => {
    let wrapper: ReactWrapper;
    const mockStore = createMockStore([thunk]);

    const mockHistory = createBrowserHistory();
    let pushSpy: any;

    const mStore = mockStore({
      applicationTopicList: {
        content: [
          {
            appClient: {
              appName: "testAppName",
              appVersion: "testAppVersion",
            },
            topic: {
              appClient: {
                appBrokerType: {
                  brokerTypeId: "testBrokerTypeId",
                },
              },
              topicId: "testTopicId",
              topicName: "testTopicName",
            },
          },
        ],
      },
    });
    beforeEach(() => {
      pushSpy = Sinon.spy(mockHistory, "push");

      wrapper = mount(
        <Router history={mockHistory}>
          <Provider store={mStore}>
            <BrokerTypesDetailsTabsTopicList brokerType="CB" type="PUBLISHER" />
          </Provider>
        </Router>
      );
    });

    afterEach(() => {
      pushSpy.restore();
    });

    it("should filter", () => {
      const enteredInput = "test";

      let input = wrapper.find(".ta-brokerTypeDetails-filter-topicName").first();
      const handler = input.prop("onChange");

      if (handler) {
        act(() => {
          handler({ target: { value: enteredInput } } as any);
        });
      }
      wrapper.update();
      jest.runAllTimers();

      input = wrapper.find(".ta-brokerTypeDetails-filter-topicName").first();
      expect(input.prop("value")).toEqual(enteredInput);
    });

    it("should call navigate", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const prefix = "ta-broker-type-details-column-";
      getElementByClassName(`${prefix}topicName`).simulate("click");
      expect(pushSpy.called).toEqual(true);
      expect(pushSpy.args[0]).toEqual(["/topics/testTopicId"]);
    });
  });
});
