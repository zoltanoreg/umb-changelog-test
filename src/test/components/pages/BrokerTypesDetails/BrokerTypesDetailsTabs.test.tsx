import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { BrokerTypesDetailsTabs } from "../../../../components/pages/BrokerTypesDetails/BrokerTypesDetailsTabs";
import { tabBrokerTypesDetailsEnum } from "../../../../models/IBrokerTypesDetailsConfig";
import store from "../../../../store";

describe("BrokerTypesDetailsTabs component test", () => {
  it("renders without crashing", () => {
    const fakeFc = jest.fn();
    shallow(
      <Provider store={store}>
        <BrokerTypesDetailsTabs onChange={fakeFc} activeTab="" />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const fakeFc = jest.fn();
    const wrapper = renderer.create(
      <Provider store={store}>
        <BrokerTypesDetailsTabs onChange={fakeFc} activeTab="" />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("changes tabs by clicking", () => {
    const fakeFc = jest.fn();
    const wrapper = mount(
      <Provider store={store}>
        <BrokerTypesDetailsTabs onChange={fakeFc} activeTab="" />
      </Provider>
    );

    wrapper
      .find(".ta-broker-types-details-tab-app-clients")
      .first()
      .simulate("click");
    wrapper
      .find(".ta-broker-types-details-tab-topics-published")
      .first()
      .simulate("click");
    wrapper
      .find(".ta-broker-types-details-tab-topics-subscribed")
      .first()
      .simulate("click");
    wrapper
      .find(".ta-broker-types-details-tab-details")
      .first()
      .simulate("click");

    expect(fakeFc.mock.calls.length).toEqual(4);
    expect(fakeFc.mock.calls[0]).toEqual([tabBrokerTypesDetailsEnum.APPCLIENTS]);
    expect(fakeFc.mock.calls[1]).toEqual([tabBrokerTypesDetailsEnum.PUBLISHED]);
    expect(fakeFc.mock.calls[2]).toEqual([tabBrokerTypesDetailsEnum.SUBSCRIBED]);
    expect(fakeFc.mock.calls[3]).toEqual([tabBrokerTypesDetailsEnum.DETAILS]);
  });
});
