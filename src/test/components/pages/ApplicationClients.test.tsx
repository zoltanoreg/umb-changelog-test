import { mount, shallow } from "enzyme";
import { Row } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { EditModalActions } from "../../../actions/modal/edit/types";
import { BrokerTypeDropdown } from "../../../components/BrokerTypeDropdown";
import { ApplicationClients } from "../../../components/pages/ApplicationClients";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import store from "../../../store";
jest.useFakeTimers();

let container: any;
const mockStore = createMockStore([thunk]);

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = undefined;
});

describe("application clients component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ApplicationClients />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <MemoryRouter>
        <Provider store={store}>
          <ApplicationClients />
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper).toMatchSnapshot();
  });

  describe("ApplicationClients [ordering and filtering] ", () => {
    /* let useStateSpy: any; */

    let wrapper: any;
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      applicationList: {
        loading: false,
      },
      applicationTopicList: {
        content: [],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      /* useStateSpy = Sinon.spy(React, "useState"); */

      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <ApplicationClients />
          </Provider>
        </MemoryRouter>
      );
    });

    afterEach(() => {
      /* useStateSpy.restore(); */
    });

    it("should change sorting direction", () => {
      let header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("none");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("ascending");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("descending");
    });

    it("should call filter if filtering against string-based column", () => {
      const enteredInput = "randomly added filter string";
      const columns = [
        "ta-applicationClients-filter-appName",
        "ta-applicationClients-filter-version",
        "ta-applicationClients-filter-appOwner",
      ];

      for (const column of columns) {
        let input = wrapper.find(`.${column}`).first();
        act(() => {
          input.prop("onChange")({ target: { value: enteredInput } });
        });

        wrapper.update();
        jest.runAllTimers();

        input = wrapper.find(`.${column}`).first();
        expect(input.prop("value")).toEqual(enteredInput);
      }
    });

    it("should not call filter if filtering with empty string", () => {
      const enteredInput = " ";
      const expectedValue = "";
      let input = wrapper.find(".ta-applicationClients-filter-appName").first();
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
        wrapper.update();
      });
      input = wrapper.find(".ta-applicationClients-filter-appName").first();
      expect(input.prop("value")).toEqual(expectedValue);
    });

    it("should call filter if filtering against date-based column", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const enteredInput = { startDate: null, endDate: null };
      const columns = [
        "ta-applicationClients-filter-createdDate",
        "ta-applicationClients-filter-modifiedDate",
      ];
      for (const column of columns) {
        let input = getElementByClassName(column);
        act(() => {
          input.prop("onDatesChange")(enteredInput);
        });
        wrapper.update();
        input = getElementByClassName(column);
        expect(input.prop("startDate")).toEqual(enteredInput.startDate);
        expect(input.prop("endDate")).toEqual(enteredInput.endDate);
      }
    });

    it("should call filter if filtering against dropdown column", () => {
      const enteredInput = "brokerType";
      let input = wrapper.find(BrokerTypeDropdown).first();
      act(() => {
        input.prop("onChange")(enteredInput);
      });
      wrapper.update();

      input = wrapper.find(BrokerTypeDropdown).first();
      expect(input.prop("selected")).toEqual(enteredInput);
    });

    it("should clear all filter values if any had been already entered", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const classNameOfInput = "ta-applicationClients-filter-appName";
      const classNameOfDeleteIcon = "ta-applicationClients-button-clearFilter";

      const enteredInput = "randomly added filter string";
      let input = getElementByClassName(classNameOfInput);
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
      });
      wrapper.update();
      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual(enteredInput);

      wrapper.setProps({}); // Re-render in order to property change (disable) come into live

      const icon = getElementByClassName(classNameOfDeleteIcon);
      expect(icon.prop("disabled")).toBe(false);
      icon.simulate("click");

      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual("");
    });

    // TODO
    /* it("should call keypress event on pressing Enter", () => {
      const getElementByClassName = (className: string) => wrapper.find("." + className).first();
      const enteredInput = "randomly added filter string";
      let input = getElementByClassName("ta-applicationClients-filter-topicClient-appName");
      input.prop("onChange")({ target: { value: enteredInput } });
      
      console.info(useStateSpy.args);
      const callsBefore = useStateSpy.args.length;
      input.prop("onKeyPress")({ key: "Enter" });
      wrapper.update();
      input = getElementByClassName("ta-applicationClients-filter-topicClient-appName");

      const expectedObj = [
        {
          clientType: 'PUBLISHER',
          modifiedDate: {},
          mqttConnectedAppClientBrokerType: '',
          mqttConnectedAppClientId: '',
          mqttConnectedAppClientName: '',
          mqttConnectedAppClientOwnerFullName: '',
          mqttConnectedAppClientVersion: '',
          topicId: ''
        }
      ];
      expect(useStateSpy.args[useStateSpy.args.length-1]).toEqual(expectedObj);
    }); */
  });

  describe("ApplicationClients", () => {
    let wrapper: any;
    const appClientIdTest = "appClientId";
    const mockHistoryPush = jest.fn();
    const mRouter = {
      history: {
        push: mockHistoryPush,
      },
    };
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      applicationList: {
        content: [
          {
            appBrokerType: {
              brokerType: "brokerType",
            },
            appClientId: appClientIdTest,
            appName: "appName",
            appOwner: {
              fullName: "fullName",
            },
            appVersion: "appversion",
            createdAt: null,
            createdBy: {
              fullName: "fullName",
            },
            modifiedAt: null,
            modifiedBy: {
              fullName: "fullName",
            },
            status: "status",
          },
        ],
        loading: false,
      },
      applicationTopicList: {
        content: [],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <ApplicationClients {...mRouter} />
          </Provider>
        </MemoryRouter>
      );
    });

    it("should call navigate to other route", () => {
      const tbodyRow = wrapper.find(Row).at(1);
      tbodyRow.simulate("click");
      expect(mockHistoryPush).toHaveBeenCalledWith(`applicationClients/${appClientIdTest}`);
    });

    it("should open edit modal", () => {
      const openModalButton = wrapper.find(".ta-applicationClients-button-createNew").first();

      openModalButton.simulate("click");
      const expectedObj = {
        payload: {
          type: "NEWAPP",
        },
        type: EditModalActions.OPEN,
      };
      expect(mStore.getActions()).toContainEqual(expectedObj);
    });
  });
});
