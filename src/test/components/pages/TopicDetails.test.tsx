import { mount, shallow } from "enzyme";
import { Tab } from "next-components";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import * as editModalActions from "../../../actions/modal/edit/actions";
import { EditTopicTypes } from "../../../actions/topic/edit/types";
import * as topicDetailsActions from "../../../actions/topic/get-details/actions";
import { ProgressIndicatorCircular } from "../../../components/next-component-fork";
import { TopicDetails } from "../../../components/pages/TopicDetails";
import { TopicBridgesGraph } from "../../../components/pages/TopicDetails/TopicBridgesGraph";
import { TopicDetailsBridgesList } from "../../../components/pages/TopicDetails/TopicBridgesList";
import { TopicDetailsPayload } from "../../../components/pages/TopicDetails/TopicDetailsPayload";
import { TopicDetailsPublishList } from "../../../components/pages/TopicDetails/TopicDetailsPublishList";
import { TopicDetailsSubscriberList } from "../../../components/pages/TopicDetails/TopicDetailsSubscribeList";
import { TopicDetailsTabDetails } from "../../../components/pages/TopicDetails/TopicDetailsTabDetails";
import { PageError } from "../../../components/PageState";
import { modalTypeEnum } from "../../../models/IEditModalConfig";
import { topicStatusEnum } from "../../../models/ITopicConfig";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import { TopicService } from "../../../services/TopicService";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("topic details component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopicDetails match={undefined} />
      </Provider>
    );
  });

  it("renders without crashing on undefined id, but not getting details", () => {
    const router = {
      match: {
        params: {
          topicId: undefined,
        },
      },
    };

    const spyFn = jest.spyOn(topicDetailsActions, "getTopicDetails");

    const wrongWrapper = mount(
      <MemoryRouter>
        <Provider store={store}>
          <TopicDetails {...router} />
        </Provider>
      </MemoryRouter>
    );

    expect(spyFn).not.toHaveBeenCalled();

    spyFn.mockRestore();
  });

  it("check if matches snapshot", () => {
    const setValues = {
      match: {
        params: {
          topicId: "test",
        },
      },
    };
    const wrapper = renderer.create(
      <MemoryRouter>
        <Provider store={store}>
          <TopicDetails {...setValues} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("should present overlay", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
        },
      },
      editModal: {
        open: true,
        topicData: {},
        type: modalTypeEnum.EDIT_TOPIC,
      },
      topicDetails: {
        content: {
          topic: {
            appClient: {
              appOwner: {
                userId: "userId",
              },
            },
            status: topicStatusEnum.ACTIVE,
          },
        },
        loading: true,
      },
    });
    const router = {
      match: {
        params: {
          topicId: "topicId",
        },
      },
    };

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <TopicDetails {...router} />
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper.find(ProgressIndicatorCircular).length).toEqual(1);
  });

  it("should present error", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
        },
      },
      editModal: {
        open: true,
        topicData: {},
        type: modalTypeEnum.EDIT_TOPIC,
      },
      topicDetails: {
        content: {
          topic: {
            appClient: {
              appOwner: {
                userId: "userId",
              },
            },
            status: topicStatusEnum.ACTIVE,
          },
        },
        error: true,
        loading: false,
      },
    });
    const router = {
      match: {
        params: {
          topicId: "topicId",
        },
      },
    };

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <TopicDetails {...router} />
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper.find(PageError).length).toEqual(1);
  });

  it("should change tab contents while changing tabs", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
        },
      },
      editModal: {
        type: modalTypeEnum.EDIT_TOPIC,
      },
      topicBridges: {
        content: [],
        loading: false,
      },
      topicClientList: {
        content: [],
        loading: false,
      },
      topicDetails: {
        content: {
          loading: false,
          topic: {
            appClient: {
              appOwner: {
                userId: "userId",
              },
            },
            status: topicStatusEnum.ACTIVE,
          },
        },
      },
      topicMQTTClients: {
        content: [],
        error: false,
        loading: false,
      },
      topicPayload: {
        content: {},
      },
    });
    const router = {
      match: {
        params: {
          topicId: "topicId",
        },
      },
    };

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <TopicDetails {...router} />
        </Provider>
      </MemoryRouter>
    );

    const tabNames = {
      "ta-topic-tab-bridg": TopicDetailsBridgesList,
      "ta-topic-tab-details": TopicDetailsTabDetails,
      "ta-topic-tab-payload": TopicDetailsPayload,
      "ta-topic-tab-publ": TopicDetailsPublishList,
      "ta-topic-tab-subsc": TopicDetailsSubscriberList,
      "ta-topic-tab-topology": TopicBridgesGraph,
    } as any;

    Object.keys(tabNames).forEach((key: string) => {
      wrapper
        .find(Tab)
        .find(`.${key}`)
        .first()
        .simulate("click");
      expect(wrapper.find(tabNames[key]).length).toEqual(1);
    });
  });

  it("should open an edit modal dialog", () => {
    const userIdTest = "testUserId";
    const topicPayloadIdTest = "testTopicPayloadId";
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
        },
      },
      editModal: {},
      topicBridges: {
        loading: false,
      },
      topicClientList: {
        content: [],
        loading: false,
      },
      topicDetails: {
        content: {
          loading: false,
          topic: {
            appClient: {
              appOwner: {
                userId: "testUserId",
              },
            },
            status: topicStatusEnum.ACTIVE,
          },
          topicPayloadId: topicPayloadIdTest,
        },
      },
      topicPayload: {
        content: {},
      },
    });
    const router = {
      match: {
        params: {
          topicId: "topicId",
        },
      },
    };

    const expectedObj = {
      appData: {
        appOwner: {
          userId: userIdTest,
        },
      },
      topicData: {
        appClient: {
          appOwner: {
            userId: userIdTest,
          },
        },
        status: topicStatusEnum.ACTIVE,
        topicPayloadId: topicPayloadIdTest,
      },
      type: modalTypeEnum.EDIT_TOPIC,
    };

    const spyFn = jest.spyOn(editModalActions, "editModalOpenAction");
    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <TopicDetails {...router} />
        </Provider>
      </MemoryRouter>
    );
    wrapper
      .find(".ta-topicDetails-button-edit")
      .first()
      .simulate("click");
    expect(spyFn.mock.calls).toContainEqual([expectedObj]);
  });

  describe("TopicDetails", () => {
    let updateTopicStub: any;
    let getTopicDetailsStub: any;
    let wrapper: any;
    const userIdTest = "userId";
    const originalInitObj = {
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: userIdTest,
        },
      },
      editModal: {
        open: false,
        type: modalTypeEnum.EDIT_TOPIC,
      },
      topicBridges: {
        loading: false,
      },
      topicClientList: {
        content: [],
        loading: false,
      },
      topicDetails: {
        content: {
          loading: false,
          topic: {
            appClient: {
              appOwner: {
                userId: userIdTest,
              },
            },
            status: topicStatusEnum.ACTIVE,
          },
        },
      },
      topicPayload: {
        content: {},
      },
    };
    const router = {
      match: {
        params: {
          topicId: "topicId",
        },
      },
    };

    beforeEach(() => {
      updateTopicStub = Sinon.stub(TopicService, "updateTopic").callsFake((config, id) =>
        Promise.resolve({ data: { data: { content: {} } } } as any)
      );
      getTopicDetailsStub = Sinon.stub(TopicService, "getTopicDetails").callsFake(topicId =>
        Promise.resolve({ data: {} } as any)
      );
    });

    afterEach(() => {
      updateTopicStub.restore();
      getTopicDetailsStub.restore();
    });

    it("should put from ACTIVE to ONHOLD", async () => {
      const expectedObj = {
        payload: {
          data: {
            content: {},
          },
        },
        type: EditTopicTypes.SUCCESS,
      };

      const initObj = { ...originalInitObj };
      const mStore = mockStore(initObj);

      wrapper = mount(
        <MemoryRouter>
          <Provider store={mStore}>
            <TopicDetails {...router} />
          </Provider>
        </MemoryRouter>
      );

      await wrapper
        .find(".ta-topicDetails-button-onhold")
        .first()
        .simulate("click");
      expect(mStore.getActions()).toContainEqual(expectedObj);
    });

    it("should put from ONHOLD to ACTIVE", async () => {
      const expectedObj = {
        payload: {
          data: {
            content: {},
          },
        },
        type: EditTopicTypes.SUCCESS,
      };

      const initObj = { ...originalInitObj };
      initObj.topicDetails.content.topic.status = topicStatusEnum.ONHOLD;
      const mStore = mockStore(initObj);

      wrapper = mount(
        <MemoryRouter>
          <Provider store={mStore}>
            <TopicDetails {...router} />
          </Provider>
        </MemoryRouter>
      );

      await wrapper
        .find(".ta-topicDetails-button-onhold")
        .first()
        .simulate("click");
      expect(mStore.getActions()).toContainEqual(expectedObj);
    });

    /* it("should delete topic", async () => {
      
      const deleteTopicStub = Sinon.stub(TopicService, "deleteTopic").callsFake(
        (id) => Promise.resolve({ data: {data: {}} } as any)
      );

      const expectedObj = {
        payload: {
          data: {
            content: {},
          },
        },
        type: EditTopicTypes.SUCCESS,
      };

      const initObj = {...originalInitObj};
      const mStore = mockStore(initObj);

      wrapper = mount(
        <MemoryRouter>
          <Provider store={mStore}>
            <TopicDetails {...router}/>
          </Provider>
        </MemoryRouter>
      );

      // TODO
      await wrapper.find(".ta-topicDetails-button-delete").first().simulate("click");
      console.info(wrapper.debug());
      Miért nincs modalConfirmation az oldalon?? 
      console.info("van modalconfirmation? ", wrapper.find(ModalConfirmation).length);
      expect(mStore.getActions()).toContainEqual(expectedObj);

      deleteTopicStub.restore();

      // false = !(isAdmin || isTopicsAppOwner) || isDeleted 
    }); */
  });
});
