import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { ApplicationDetailsTabs } from "../../../../components/pages/ApplicationClientDetails/ApplicationDetailsTabs";
import { tabAppDetailsEnum } from "../../../../models/IAppDetailsConfig";
import store from "../../../../store";

describe("ApplicationDetailsTabs component test", () => {
  it("renders without crashing", () => {
    const fakeFc = jest.fn();
    shallow(
      <Provider store={store}>
        <ApplicationDetailsTabs onChange={fakeFc} activeTab="" />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const fakeFc = jest.fn();
    const wrapper = renderer.create(
      <Provider store={store}>
        <ApplicationDetailsTabs onChange={fakeFc} activeTab="" />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("changes tabs by clicking", () => {
    const fakeFc = jest.fn();
    const wrapper = mount(
      <Provider store={store}>
        <ApplicationDetailsTabs onChange={fakeFc} activeTab="" />
      </Provider>
    );

    wrapper
      .find(".ta-application-details-tab-topic-subscribe")
      .first()
      .simulate("click");
    wrapper
      .find(".ta-application-details-tab-topic-publish")
      .first()
      .simulate("click");
    wrapper
      .find(".ta-application-details-tab-details")
      .first()
      .simulate("click");

    expect(fakeFc.mock.calls.length).toEqual(3);
    expect(fakeFc.mock.calls[0]).toEqual([tabAppDetailsEnum.SUBSCRIBE]);
    expect(fakeFc.mock.calls[1]).toEqual([tabAppDetailsEnum.PUBLISH]);
    expect(fakeFc.mock.calls[2]).toEqual([tabAppDetailsEnum.DETAILS]);
  });
});
