import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { TDeleteApplication } from "../../../../actions/application/delete/types";
import { EditModalActions } from "../../../../actions/modal/edit/types";
import { ApplicationDetailsTopSection } from "../../../../components/pages/ApplicationClientDetails/ApplicationDetailsTopSection";
import { modalTypeEnum } from "../../../../models/IEditModalConfig";
import { ADMINISTRATOR } from "../../../../models/UserTypes";
import { ApplicationService } from "../../../../services/ApplicationService";
import store from "../../../../store";

const mockStore = createMockStore([thunk]);

describe("ApplicationDetailsTopSection component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ApplicationDetailsTopSection />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      appContent: {
        appName: "test",
      },
    };
    const wrapper = renderer.create(
      <Provider store={store}>
        <ApplicationDetailsTopSection {...setValues} />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("edit modal appears (user is appOwner)", () => {
    const userIdTest = "testUserId";
    const props = {
      appContent: {
        appName: "appNameTest",
        appOwner: {
          userId: userIdTest,
        },
        status: "ACTIVE",
      },
      userData: {
        role: "userHasNoSpecialRole",
        userId: userIdTest,
      },
    };
    const mStore = mockStore({
      editModal: {
        appData: {},
      },
    });
    const expectedObj = {
      payload: {
        appData: {
          ...props.appContent,
        },
        type: modalTypeEnum.EDIT_APP,
      },
      type: EditModalActions.OPEN,
    };
    const wrapper = mount(
      <Provider store={mStore}>
        <ApplicationDetailsTopSection {...props} />
      </Provider>
    );
    wrapper
      .find(".ta-applicationDetails-button-edit")
      .first()
      .simulate("click");
    expect(mStore.getActions()).toContainEqual(expectedObj);
  });

  it("asks confirmation when deleting (user is ADMINISTRATOR)", async () => {
    let applicationServiceStub: any;
    const props = {
      appContent: {
        appBrokerType: {
          brokerType: "brokerTypeTest",
        },
        appName: "appNameTest",
        appOwner: {
          userId: "testUserId1",
        },
        status: "ACTIVE",
      },
      userData: {
        role: ADMINISTRATOR,
        userId: "differentTestUserId",
      },
    };
    const mStore = mockStore({
      editModal: {
        appData: {},
      },
    });
    const applicationListsObj = {
      data: {
        content: [{}],
        count: 1,
      },
    };

    applicationServiceStub = Sinon.stub(ApplicationService, "deleteApplication").callsFake(
      (appId: any) => Promise.resolve(applicationListsObj as any)
    );
    const wrapper = mount(
      <Provider store={mStore}>
        <ApplicationDetailsTopSection {...props} />
      </Provider>
    );
    wrapper
      .find(".ta-applicationDetails-button-delete")
      .first()
      .simulate("click");
    wrapper.setProps({}); // Re-render

    // Simulate when the user clicked to 'confirm' button
    await mStore.getActions()[0].payload.onConfirm();

    const expectedObj = {
      payload: applicationListsObj.data,
      type: TDeleteApplication.SUCCESS,
    };
    expect(mStore.getActions()).toContainEqual(expectedObj);
    applicationServiceStub.restore();
  });
});
