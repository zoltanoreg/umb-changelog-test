import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { EditModalActions } from "../../../../actions/modal/edit/types";
import { AppTopicsList } from "../../../../components/AppTopicsList";
import { TopicListSubscribe } from "../../../../components/pages/ApplicationClientDetails/TopicListSubscribe";
import { modalTypeEnum } from "../../../../models/IEditModalConfig";
import store from "../../../../store";

const mockStore = createMockStore([thunk]);

describe("TopicListSubscribe component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopicListSubscribe />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <MemoryRouter>
        <Provider store={store}>
          <TopicListSubscribe />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("opens new subscribe modal (user is appOwner)", () => {
    const userIdTest = "testUserId";
    const props = {
      appContent: {
        appName: "appNameTest",
        appOwner: {
          userId: userIdTest,
        },
        status: "ACTIVE",
      },
      userData: {
        role: "userHasNoSpecialRole",
        userId: userIdTest,
      },
    };
    const mStore = mockStore({
      allBrokerType: {
        content: [],
      },
      applicationDetails: {
        content: {
          appClientId: "test",
        },
      },
      applicationTopicList: {
        loading: false,
      },
      editModal: {
        appData: {},
      },
    });
    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <TopicListSubscribe {...props} />
        </Provider>
      </MemoryRouter>
    );
    const topicFilterObj = {};
    const func = wrapper
      .find(AppTopicsList)
      .first()
      .prop("btnClick");
    if (func) {
      func(topicFilterObj);
    }
    const expectedObj = {
      payload: {
        appData: { ...props.appContent },
        topicFilter: { ...topicFilterObj },
        type: modalTypeEnum.CREATE_TOPIC_SUBSCRIBE,
      },
      type: EditModalActions.OPEN,
    };
    expect(mStore.getActions()).toContainEqual(expectedObj);
  });
});
