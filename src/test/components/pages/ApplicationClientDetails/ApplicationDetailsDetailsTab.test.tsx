import { shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { ApplicationDetailsDetailsTab } from "../../../../components/pages/ApplicationClientDetails/ApplicationDetailsDetailsTab";
import store from "../../../../store";

describe("ApplicationDetailsDetailsTab component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ApplicationDetailsDetailsTab />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      appContent: {
        createdAt: "",
        createdBy: { fullName: "test" },
        modifiedAt: "",
        modifiedBy: { fullName: "test" },
      },
    };
    const wrapper = renderer.create(
      <Provider store={store}>
        <ApplicationDetailsDetailsTab {...setValues} />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
