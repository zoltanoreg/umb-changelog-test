import { shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { ConfigurationFiles } from "../../../components/pages/ConfigurationFiles";
import store from "../../../store";

describe("configuration files component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ConfigurationFiles />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <ConfigurationFiles />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
