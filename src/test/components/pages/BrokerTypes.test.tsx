import { mount, shallow } from "enzyme";
import { Row } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { BrokerTypes } from "../../../components/pages/BrokerTypes";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import store from "../../../store";
jest.useFakeTimers();

const mockStore = createMockStore([thunk]);

describe("broker types component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <BrokerTypes />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <BrokerTypes />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe("BrokerTypes [ordering and filtering] ", () => {
    const appClientIdTest = "appClientId";
    /* let useStateSpy: any; */

    let wrapper: any;
    const mStore = mockStore({
      allBrokerType: {
        content: [],
      },
      brokerTypeList: {
        content: [
          {
            appClientId: appClientIdTest,
            createdBy: {
              fullName: "testFullName",
            },
            modifiedBy: {
              fullName: "testFullName",
            },
          },
        ],
        loading: false,
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      /* useStateSpy = Sinon.spy(React, "useState"); */
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <BrokerTypes />
          </Provider>
        </MemoryRouter>
      );
    });

    afterEach(() => {
      /* useStateSpy.restore(); */
    });

    it("should change sorting direction", () => {
      let header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("none");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("ascending");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("descending");
    });

    it("should call filter if filtering against string-based column", () => {
      const enteredInput = "randomly added filter string";
      const columns = ["ta-brokerTypes-filter-brokerTypeName", "ta-brokerTypes-filter-systemType"];
      for (const column of columns) {
        let input = wrapper.find(`.${column}`).first();
        act(() => {
          input.prop("onChange")({ target: { value: enteredInput } });
        });
        wrapper.update();
        jest.runAllTimers();

        input = wrapper.find(`.${column}`).first();
        expect(input.prop("value")).toEqual(enteredInput);
      }
    });

    it("should not call filter if filtering with empty string", () => {
      const enteredInput = " ";
      const expectedValue = "";
      let input = wrapper.find(".ta-brokerTypes-filter-brokerTypeName").first();
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
      });
      wrapper.update();
      input = wrapper.find(".ta-brokerTypes-filter-brokerTypeName").first();
      expect(input.prop("value")).toEqual(expectedValue);
    });

    it("should clear all filter values if any had been already entered", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const classNameOfInput = "ta-brokerTypes-filter-brokerTypeName";
      const classNameOfDeleteIcon = "ta-brokerTypes-button-clearFilter";

      const enteredInput = "randomly added filter string";
      let input = getElementByClassName(classNameOfInput);
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
      });
      wrapper.update();
      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual(enteredInput);

      wrapper.setProps({}); // Re-render in order to property change (disable) come into live

      const icon = getElementByClassName(classNameOfDeleteIcon);
      expect(icon.prop("disabled")).toBe(false);
      icon.simulate("click");

      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual("");
    });

    // TODO
    /* it("should call keypress event on pressing Enter", () => {
      const getElementByClassName = (className: string) => wrapper.find("." + className).first();
      const enteredInput = "randomly added filter string";
      let input = getElementByClassName("ta-brokerTypes-filter-topicClient-appName");
      input.prop("onChange")({ target: { value: enteredInput } });
      
      console.info(useStateSpy.args);
      const callsBefore = useStateSpy.args.length;
      input.prop("onKeyPress")({ key: "Enter" });
      wrapper.update();
      input = getElementByClassName("ta-brokerTypes-filter-topicClient-appName");

      const expectedObj = [
        {
          clientType: 'PUBLISHER',
          modifiedDate: {},
          mqttConnectedAppClientBrokerType: '',
          mqttConnectedAppClientId: '',
          mqttConnectedAppClientName: '',
          mqttConnectedAppClientOwnerFullName: '',
          mqttConnectedAppClientVersion: '',
          topicId: ''
        }
      ];
      expect(useStateSpy.args[useStateSpy.args.length-1]).toEqual(expectedObj);
    }); */
  });

  describe("BrokerTypes navigation test", () => {
    let wrapper: any;
    const brokerTypeTestId = "brokerTypesId";
    const mockHistoryPush = jest.fn();
    const mRouter = {
      history: {
        push: mockHistoryPush,
      },
    };
    const brokerTypesIdTest = "brokerTypesId";

    const mStore = mockStore({
      allBrokerType: {
        content: [],
      },
      brokerTypeList: {
        content: [
          {
            brokerTypeId: brokerTypeTestId,
            brokerTypeName: "Central Broker",
          },
        ],
        loading: false,
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <BrokerTypes {...mRouter} />
          </Provider>
        </MemoryRouter>
      );
    });

    it("should call navigate to other route", () => {
      const tbodyRow = wrapper.find(Row).at(1);
      tbodyRow.simulate("click");
      expect(mockHistoryPush).toHaveBeenCalledWith(`brokerTypes/${brokerTypesIdTest}`);
    });
  });
});
