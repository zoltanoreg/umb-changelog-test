import { mount, shallow } from "enzyme";
// tslint:disable-next-line:no-implicit-dependencies
import { createBrowserHistory } from "history";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon, { SinonStubbedInstance } from "sinon";

import { EditModalActions } from "../../../../actions/modal/edit/types";
import * as userDetailsActions from "../../../../actions/user/get-details/actions";
import { UserDetails } from "../../../../components/pages/Users/UserDetails";
import { PageError, PageLoading } from "../../../../components/PageState";
import { modalTypeEnum } from "../../../../models/IEditModalConfig";
import { PermissionActionEnum } from "../../../../models/IPermissionActions";
import { userRoleStatusEnum } from "../../../../models/IUserRoleStatus";
import {
  ADMINISTRATOR,
  APPLICATION_OWNER,
  READ,
  UMBT_IT_SUPPORT_TEAM,
} from "../../../../models/UserTypes";
import { UserService } from "../../../../services/UserService";
import store from "../../../../store";

const mockStore = createMockStore([thunk]);

const router = {
  match: {
    params: {
      userId: undefined,
    },
  },
};

describe("user details component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <UserDetails match={undefined} history={undefined} />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      match: {
        params: {
          userId: "test",
        },
      },
    };

    const wrapper = renderer.create(
      <MemoryRouter>
        <Provider store={store}>
          <UserDetails {...setValues} history={history} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("renders without crashing on undefined id, but not getting details", () => {
    const spyFn = jest.spyOn(userDetailsActions, "getUserDetails");

    const wrongWrapper = mount(
      <MemoryRouter>
        <Provider store={store}>
          <UserDetails history={history} {...router} />
        </Provider>
      </MemoryRouter>
    );

    expect(spyFn).not.toHaveBeenCalled();

    spyFn.mockRestore();
  });

  it("should render error", () => {
    const mStore = mockStore({
      currentUser: {
        data: {},
      },
      userDetails: {
        content: {},
        error: true,
        loading: false,
      },
      userSetDetails: {
        content: {
          lastName: "test lastname",
        },
        error: false,
        loading: false,
      },
    });

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <UserDetails history={history} {...router} />
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper.find(PageError).length).toEqual(1);
  });

  it("should render loading", () => {
    const mStore = mockStore({
      currentUser: {
        data: {},
      },
      userDetails: {
        content: {
          lastName: "test lastname",
        },
        error: false,
        loading: true,
      },
      userSetDetails: {
        content: {
          lastName: "test lastname",
        },
        error: false,
        loading: true,
      },
    });

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <UserDetails history={history} {...router} />
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper.find(PageLoading).length).toEqual(1);
  });

  describe("should render full page", () => {
    let userServiceStub: SinonStubbedInstance<any>;
    beforeAll(() => {
      const userObj = { data: {} };
      userServiceStub = Sinon.stub(UserService, "setUserDetails").callsFake((userData: any) =>
        Promise.resolve(userObj as any)
      );
    });

    afterEach(() => {
      userServiceStub.resetHistory();
    });

    const setValues = {
      match: {
        params: {
          userId: "test",
        },
      },
    };
    const currentUserAdmin = {
      data: {
        role: ADMINISTRATOR,
      },
    };
    const userDetails = {
      content: {
        createdBy: {
          fullName: "test",
        },
        firstName: "test firstname",
        lastName: "test lastname",
        latestChangeRequest: {
          createdAt: null,
          requestedRole: null,
          status: null,
        },
        modifiedBy: {
          fullName: "test",
        },
        role: READ,
        userId: "testUserId",
      },
      error: false,
      loading: false,
    };
    const userSetDetails = {
      content: {
        createdBy: {
          fullName: "test",
        },
        firstName: "test firstname",
        lastName: "test lastname",
        modifiedBy: {
          fullName: "test",
        },
        role: READ,
      },
      error: false,
      loading: false,
    };

    it("with no pending request", () => {
      const mStore = mockStore({
        currentUser: { ...currentUserAdmin },
        userDetails: { ...userDetails },
        userSetDetails: { ...userSetDetails },
      });
      const mockHistory = createBrowserHistory();
      const wrapper = mount(
        <MemoryRouter>
          <Provider store={{ ...mStore }}>
            <UserDetails history={mockHistory} {...setValues} />
          </Provider>
        </MemoryRouter>
      );

      expect(wrapper.find(".ta-user-details-first-name").length).toBeGreaterThan(0);
    });

    it("with pending request, and make a decision by clicking grant/deny buttons", () => {
      const mockHistory = createBrowserHistory();
      const request: any = {
        createdAt: "2020-06-18T12:48:52.000Z",
        requestedRole: APPLICATION_OWNER,
        status: userRoleStatusEnum.PENDING,
      };
      const userDetailsWithPendingRequest = { ...userDetails };
      userDetailsWithPendingRequest.content.latestChangeRequest = request;

      const mStore = mockStore({
        currentUser: { ...currentUserAdmin },
        userDetails: { ...userDetailsWithPendingRequest },
        userSetDetails: { ...userSetDetails },
      });
      const wrapper = mount(
        <MemoryRouter>
          <Provider store={mStore}>
            <UserDetails history={mockHistory} {...setValues} />
          </Provider>
        </MemoryRouter>
      );

      expect(wrapper.find(".ta-user-details-grant-btn").length).toBeGreaterThan(0);
      expect(wrapper.find(".ta-user-details-deny-btn").length).toBeGreaterThan(0);

      // Click to grant
      const grantButton = wrapper.find(".ta-user-details-grant-btn").first();
      act(() => {
        grantButton.simulate("click");
      });
      const expectedObjGrant = {
        requestStatus: PermissionActionEnum.GRANT,
        role: userDetailsWithPendingRequest.content.latestChangeRequest.requestedRole,
        userId: userDetailsWithPendingRequest.content.userId,
      };
      expect(userServiceStub.getCall(0).args[0]).toEqual(expectedObjGrant);

      // Click to deny
      const denyButton = wrapper.find(".ta-user-details-deny-btn").first();
      act(() => {
        denyButton.simulate("click");
      });
      const expectedObjDeny = {
        requestStatus: PermissionActionEnum.DENY,
        role: userDetailsWithPendingRequest.content.latestChangeRequest.requestedRole,
        userId: userDetailsWithPendingRequest.content.userId,
      };
      expect(userServiceStub.getCall(1).args[0]).toEqual(expectedObjDeny);
    });

    it("user can click to Edit button", () => {
      const mockHistory = createBrowserHistory();
      const mStore = mockStore({
        currentUser: { ...currentUserAdmin },
        userDetails: { ...userDetails },
        userSetDetails: { ...userSetDetails },
      });
      const wrapper = mount(
        <MemoryRouter>
          <Provider store={{ ...mStore }}>
            <UserDetails history={mockHistory} {...setValues} />
          </Provider>
        </MemoryRouter>
      );

      expect(wrapper.find(".ta-userDetails-button-edit").length).toBeGreaterThan(0);
      act(() => {
        wrapper
          .find(".ta-userDetails-button-edit")
          .first()
          .simulate("click");
      });

      const expectedObj = {
        payload: { type: modalTypeEnum.EDIT_USER, userData: { ...userDetails } },
        type: EditModalActions.OPEN,
      };
      expect(mStore.getActions()).toContainEqual(expectedObj);
    });

    describe("IT Support user arrives to UMB by clicking to link from email", () => {
      const permissionRequestQueryParamKey = "action";
      const request: any = {
        createdAt: "2020-06-18T12:48:52.000Z",
        requestedRole: APPLICATION_OWNER,
        status: userRoleStatusEnum.PENDING,
      };
      const userDetailsWithPendingRequest = { ...userDetails };
      userDetailsWithPendingRequest.content.latestChangeRequest = request;

      const currentUserItSupport = {
        data: {
          role: UMBT_IT_SUPPORT_TEAM,
        },
      };

      it("grant link", () => {
        const mockHistory = createBrowserHistory();
        mockHistory.location.search = `${permissionRequestQueryParamKey}=${PermissionActionEnum.GRANT}`;

        const mStore = mockStore({
          currentUser: currentUserItSupport,
          userDetails: { ...userDetailsWithPendingRequest },
          userSetDetails: { ...userSetDetails },
        });

        const wrapper = mount(
          <MemoryRouter>
            <Provider store={{ ...mStore }}>
              <UserDetails history={mockHistory} {...setValues} />
            </Provider>
          </MemoryRouter>
        );
        // Got approved without explicit click to Grant button
        const expectedObjGrant = {
          requestStatus: PermissionActionEnum.GRANT,
          role: userDetailsWithPendingRequest.content.latestChangeRequest.requestedRole,
          userId: userDetailsWithPendingRequest.content.userId,
        };
        expect(userServiceStub.getCall(0).args[0]).toEqual(expectedObjGrant);
      });

      it("deny link", () => {
        const mockHistory = createBrowserHistory();
        mockHistory.location.search = `${permissionRequestQueryParamKey}=${PermissionActionEnum.DENY}`;

        const mStore = mockStore({
          currentUser: currentUserItSupport,
          userDetails: { ...userDetailsWithPendingRequest },
          userSetDetails: { ...userSetDetails },
        });

        const wrapper = mount(
          <MemoryRouter>
            <Provider store={{ ...mStore }}>
              <UserDetails history={mockHistory} {...setValues} />
            </Provider>
          </MemoryRouter>
        );
        // Got denied without explicit click to Deny button
        const expectedObjGrant = {
          requestStatus: PermissionActionEnum.DENY,
          role: userDetailsWithPendingRequest.content.latestChangeRequest.requestedRole,
          userId: userDetailsWithPendingRequest.content.userId,
        };
        expect(userServiceStub.getCall(0).args[0]).toEqual(expectedObjGrant);
      });

      describe("there is no meaningful info in queryparam", () => {
        it("query key is wrong", () => {
          const mockHistory = createBrowserHistory();
          mockHistory.location.search = `someKey=${PermissionActionEnum.DENY}`;

          const mStore = mockStore({
            currentUser: currentUserItSupport,
            userDetails: { ...userDetailsWithPendingRequest },
            userSetDetails: { ...userSetDetails },
          });

          const wrapper = mount(
            <MemoryRouter>
              <Provider store={{ ...mStore }}>
                <UserDetails history={mockHistory} {...setValues} />
              </Provider>
            </MemoryRouter>
          );
          expect(userServiceStub.getCalls().length).toEqual(0);
        });

        it("query value is wrong", () => {
          const mockHistory = createBrowserHistory();
          mockHistory.location.search = `${permissionRequestQueryParamKey}=someValue`;

          const mStore = mockStore({
            currentUser: currentUserItSupport,
            userDetails: { ...userDetailsWithPendingRequest },
            userSetDetails: { ...userSetDetails },
          });

          const wrapper = mount(
            <MemoryRouter>
              <Provider store={{ ...mStore }}>
                <UserDetails history={mockHistory} {...setValues} />
              </Provider>
            </MemoryRouter>
          );
          expect(userServiceStub.getCalls().length).toEqual(0);
        });
      });
    });

    it("user modifies the URL in order to simulate clicking behaviour", () => {
      const permissionRequestQueryParamKey = "action";
      const mockHistory = createBrowserHistory();
      mockHistory.location.search = `${permissionRequestQueryParamKey}=${PermissionActionEnum.GRANT}`;

      const request: any = {
        createdAt: "2020-06-18T12:48:52.000Z",
        requestedRole: APPLICATION_OWNER,
        status: userRoleStatusEnum.PENDING,
      };
      const userDetailsWithPendingRequest = { ...userDetails };
      userDetailsWithPendingRequest.content.latestChangeRequest = request;

      const currentUserReadOnly = {
        data: {
          role: READ,
        },
      };
      const mStore = mockStore({
        currentUser: currentUserReadOnly,
        userDetails: { ...userDetailsWithPendingRequest },
        userSetDetails: { ...userSetDetails },
      });

      const wrapper = mount(
        <MemoryRouter>
          <Provider store={{ ...mStore }}>
            <UserDetails history={mockHistory} {...setValues} />
          </Provider>
        </MemoryRouter>
      );
      // There is no Grant/Deny buttons
      expect(wrapper.find(".ta-user-details-grant-btn").length).toEqual(0);
      expect(wrapper.find(".ta-user-details-deny-btn").length).toEqual(0);
      // No automatic invocation despite of setting the URL parameters
      expect(userServiceStub.getCalls().length).toEqual(0);
    });
  });
});
