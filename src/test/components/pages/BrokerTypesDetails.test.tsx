import { mount, shallow } from "enzyme";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { BrokerTypesDetails } from "../../../components/pages/BrokerTypesDetails";
import { BrokerTypesDetailsTabs } from "../../../components/pages/BrokerTypesDetails/BrokerTypesDetailsTabs";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("broker types details component test", () => {
  it("renders without crashing", () => {
    const setValues = {
      match: {
        params: {
          brokerTypeId: "test",
        },
      },
    };
    shallow(
      <Provider store={store}>
        <BrokerTypesDetails {...setValues} />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      match: {
        params: {
          brokerTypeId: "test",
        },
      },
    };
    const wrapper = renderer.create(
      <MemoryRouter>
        <Provider store={store}>
          <BrokerTypesDetails {...setValues} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("test state changes when clicking on a tab", () => {
    const mStore = mockStore({
      applicationList: {
        content: [],
      },
      applicationTopicList: {
        content: [],
        error: false,
        loading: true,
      },
      brokerTypeDetails: {
        content: [],
        error: false,
        loading: false,
      },
      currentUser: {
        data: {},
      },
    });

    const setValues = {
      match: {
        params: {
          brokerTypeId: "test",
        },
      },
    };

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <BrokerTypesDetails {...setValues} />
        </Provider>
      </MemoryRouter>
    );

    // Order depends on the enum's order
    const classes = [
      ".ta-broker-types-details-tab-app-clients",
      ".ta-broker-types-details-tab-details",
      ".ta-broker-types-details-tab-topics-published",
      ".ta-broker-types-details-tab-topics-subscribed",
    ];

    let i = 0;
    for (const tab of classes) {
      const t = wrapper.find(tab).first();
      act(() => {
        t.simulate("click");
      });
      wrapper.update();

      expect(wrapper.find(BrokerTypesDetailsTabs).prop("activeTab")).toEqual(i);
      i = i + 1;
    }
  });
});
