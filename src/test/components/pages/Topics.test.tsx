import { mount, shallow } from "enzyme";
import { Dropdown, Row } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { Topics } from "../../../components/pages/Topics";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import store from "../../../store";
jest.useFakeTimers();

const mockStore = createMockStore([thunk]);

describe("topics component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <Topics />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <MemoryRouter>
        <Provider store={store}>
          <Topics />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe("Topics [ordering and filtering] ", () => {
    const testAppClientId = "appClientId";

    let wrapper: any;
    const mStore = mockStore({
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
      topicList: {
        content: [
          {
            appClient: {
              appName: "testAppName",
              appVersion: "testVersion",
            },
            appClientId: testAppClientId,
            modifiedBy: {
              fullName: "testFullName",
            },
            topicId: "testTopicId",
          },
        ],
        loading: false,
      },
    });

    beforeEach(() => {
      /* useStateSpy = Sinon.spy(React, "useState"); */
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <Topics />
          </Provider>
        </MemoryRouter>
      );
    });

    afterEach(() => {
      /* useStateSpy.restore(); */
    });

    it("should change sorting direction", () => {
      let header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("none");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("ascending");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("descending");
    });

    it("should call filter if filtering against string-based column", () => {
      const enteredInput = "randomly added filter string";
      const columns = [
        "ta-topics-filter-topicName",
        "ta-topics-filter-topicOwner",
        "ta-topics-filter-modifiedBy",
      ];
      for (const column of columns) {
        let input = wrapper.find(`.${column}`).first();
        act(() => {
          input.prop("onChange")({ target: { value: enteredInput } });
        });
        wrapper.update();
        jest.runAllTimers();

        input = wrapper.find(`.${column}`).first();
        expect(input.prop("value")).toEqual(enteredInput);
      }
    });

    it("should not call filter if filtering with empty string", () => {
      const enteredInput = " ";
      const expectedValue = "";
      let input = wrapper.find(".ta-topics-filter-topicName").first();
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
      });
      wrapper.update();
      input = wrapper.find(".ta-topics-filter-topicName").first();
      expect(input.prop("value")).toEqual(expectedValue);
    });

    it("should call filter if filtering against date-based column", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const enteredInput = { startDate: null, endDate: null };
      const columns = ["ta-topics-filter-modifiedDate"];
      for (const column of columns) {
        let input = getElementByClassName(column);
        act(() => {
          input.prop("onDatesChange")(enteredInput);
        });
        wrapper.update();
        input = getElementByClassName(column);
        expect(input.prop("startDate")).toEqual(enteredInput.startDate);
        expect(input.prop("endDate")).toEqual(enteredInput.endDate);
      }
    });

    it("should call filter if filtering against dropdown column", () => {
      const enteredInput = "Status";
      let input = wrapper.find(Dropdown).first();
      act(() => {
        input.prop("onChange")(enteredInput);
      });
      wrapper.update();
      input = wrapper.find(Dropdown).first();
      expect(input.prop("selected")).toEqual(enteredInput);
    });

    it("should clear all filter values if any had been already entered", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const classNameOfInput = "ta-topics-filter-topicName";
      const classNameOfDeleteIcon = "ta-topics-button-clearFilter";

      const enteredInput = "randomly added filter string";
      let input = getElementByClassName(classNameOfInput);
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
      });
      wrapper.update();
      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual(enteredInput);

      wrapper.setProps({}); // Re-render in order to property change (disable) come into live

      const icon = getElementByClassName(classNameOfDeleteIcon);
      expect(icon.prop("disabled")).toBe(false);
      icon.simulate("click");

      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual("");
    });

    // TODO
    /* it("should call keypress event on pressing Enter", () => {
      const getElementByClassName = (className: string) => wrapper.find("." + className).first();
      const enteredInput = "randomly added filter string";
      let input = getElementByClassName("ta-topics-filter-topicClient-appName");
      input.prop("onChange")({ target: { value: enteredInput } });
      
      console.info(useStateSpy.args);
      const callsBefore = useStateSpy.args.length;
      input.prop("onKeyPress")({ key: "Enter" });
      wrapper.update();
      input = getElementByClassName("ta-topics-filter-topicClient-appName");

      const expectedObj = [
        {
          clientType: 'PUBLISHER',
          modifiedDate: {},
          mqttConnectedAppClientBrokerType: '',
          mqttConnectedAppClientId: '',
          mqttConnectedAppClientName: '',
          mqttConnectedAppClientOwnerFullName: '',
          mqttConnectedAppClientVersion: '',
          topicId: ''
        }
      ];
      expect(useStateSpy.args[useStateSpy.args.length-1]).toEqual(expectedObj);
    }); */
  });

  describe("Topics", () => {
    let wrapper: any;
    const appClientIdTest = "appClientId";
    const topicIdTest = "testTopicId";
    const mockHistoryPush = jest.fn();
    const mRouter = {
      history: {
        push: mockHistoryPush,
      },
    };
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
      topicList: {
        content: [
          {
            appClient: {
              appClientId: appClientIdTest,
              appName: "testAppName",
              appVersion: "testVersion",
            },
            modifiedBy: {
              fullName: "testFullName",
            },
            topicId: topicIdTest,
          },
        ],
        loading: false,
      },
    });

    beforeEach(() => {
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <Topics {...mRouter} />
          </Provider>
        </MemoryRouter>
      );
    });

    it("should call navigate to other route", () => {
      let cell = wrapper
        .find(Row)
        .at(1)
        .find(".ta-topics-column-topicName")
        .first();
      cell.simulate("click");
      expect(mockHistoryPush).toHaveBeenCalledWith(`topics/${topicIdTest}`);

      cell = wrapper
        .find(Row)
        .at(1)
        .find(".ta-topics-column-topicOwner")
        .first();
      cell.simulate("click");
      expect(mockHistoryPush).toHaveBeenCalledWith(`applicationClients/${appClientIdTest}`);
    });
  });
});
