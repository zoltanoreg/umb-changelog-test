import { mount, shallow } from "enzyme";
import { Dropdown } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { ConfigurationBrokerList } from "../../../../components/pages/ConfigurationFiles/ConfigurationBrokerList";
import { ADMINISTRATOR } from "../../../../models/UserTypes";
import store from "../../../../store";
jest.useFakeTimers();

const mockStore = createMockStore([thunk]);

describe("ConfigurationBrokerList component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ConfigurationBrokerList />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <Provider store={store}>
        <ConfigurationBrokerList />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe("ConfigurationBrokerList [ordering and filtering] ", () => {
    /* let useStateSpy: any; */

    let wrapper: any;
    const mStore = mockStore({
      allBrokerType: {
        content: [],
      },
      configFileBroker: {
        content: [],
      },
      configFileGenerate: {
        loading: false,
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      /* useStateSpy = Sinon.spy(React, "useState"); */
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <ConfigurationBrokerList />
          </Provider>
        </MemoryRouter>
      );
    });

    afterEach(() => {
      /* useStateSpy.restore(); */
    });

    it("should change sorting direction", () => {
      let header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("none");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("ascending");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("descending");
    });

    it("should call filter if filtering against string-based column", () => {
      const enteredInput = "randomly added filter string";
      const columns = [
        "ta-configurationBrokerList-filter-fileName",
        "ta-configurationBrokerList-filter-fileVersion",
      ];
      for (const column of columns) {
        let input = wrapper.find(`.${column}`).first();
        act(() => {
          input.prop("onChange")({ target: { value: enteredInput } });
        });
        wrapper.update();
        jest.runAllTimers();

        input = wrapper.find(`.${column}`).first();
        expect(input.prop("value")).toEqual(enteredInput);
      }
    });

    it("should not call filter if filtering with empty string", () => {
      const enteredInput = " ";
      const expectedValue = "";
      let input = wrapper.find(".ta-configurationBrokerList-filter-fileName").first();
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
      });
      wrapper.update();
      input = wrapper.find(".ta-configurationBrokerList-filter-fileName").first();
      expect(input.prop("value")).toEqual(expectedValue);
    });

    it("should call filter if filtering against date-based column", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const enteredInput = { startDate: null, endDate: null };
      const columns = ["ta-configurationBrokerList-filter-createdDate"];
      for (const column of columns) {
        let input = getElementByClassName(column);
        act(() => {
          input.prop("onDatesChange")(enteredInput);
        });
        wrapper.update();
        input = getElementByClassName(column);
        expect(input.prop("startDate")).toEqual(enteredInput.startDate);
        expect(input.prop("endDate")).toEqual(enteredInput.endDate);
      }
    });

    it("should call filter if filtering against dropdown column", () => {
      const enteredInput = "brokerType";
      const className = ".ta-configurationBrokerList-filter-brokerType";
      let input = wrapper
        .find(className)
        .find(Dropdown)
        .first();
      act(() => {
        input.prop("onChange")(enteredInput);
      });
      wrapper.update();
      input = wrapper
        .find(className)
        .find(Dropdown)
        .first();
      expect(input.prop("selected")).toEqual(enteredInput);
    });

    it("should clear all filter values if any had been already entered", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const classNameOfInput = "ta-configurationBrokerList-filter-fileName";
      const classNameOfDeleteIcon = "ta-configurationBrokerList-button-clearFilter";

      const enteredInput = "randomly added filter string";
      let input = getElementByClassName(classNameOfInput);
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
      });
      wrapper.update();
      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual(enteredInput);

      wrapper.setProps({}); // Re-render in order to property change (disable) come into live

      const icon = getElementByClassName(classNameOfDeleteIcon);
      expect(icon.prop("disabled")).toBe(false);
      icon.simulate("click");

      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual("");
    });

    // TODO
    /* it("should call keypress event on pressing Enter", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const enteredInput = "randomly added filter string";
      let input = getElementByClassName("ta-configurationBrokerList-filter-topicClient-appName");
      input.prop("onChange")({ target: { value: enteredInput } });
      
      console.info(useStateSpy.args);
      const callsBefore = useStateSpy.args.length;
      input.prop("onKeyPress")({ key: "Enter" });
      wrapper.update();
      input = getElementByClassName("ta-configurationBrokerList-filter-topicClient-appName");

      const expectedObj = [
        {
          clientType: 'PUBLISHER',
          modifiedDate: {},
          mqttConnectedAppClientBrokerType: '',
          mqttConnectedAppClientId: '',
          mqttConnectedAppClientName: '',
          mqttConnectedAppClientOwnerFullName: '',
          mqttConnectedAppClientVersion: '',
          topicId: ''
        }
      ];
      expect(useStateSpy.args[useStateSpy.args.length-1]).toEqual(expectedObj);
    }); */
  });
});
