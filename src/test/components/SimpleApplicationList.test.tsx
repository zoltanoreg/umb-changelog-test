import { mount, shallow } from "enzyme";
// tslint:disable-next-line: no-implicit-dependencies
import { createBrowserHistory } from "history";
import { Row } from "next-components";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter, Router } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { SimpleApplicationList } from "../../components/SimpleApplicationList";
import { ADMINISTRATOR } from "../../models/UserTypes";
import store from "../../store";

const mockStore = createMockStore([thunk]);

describe("simple application list component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <SimpleApplicationList />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <SimpleApplicationList />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe("SimpleApplicationList [ordering and filtering] ", () => {
    const appClientIdTest = "appClientId";
    const appNameTest = "testAppName";
    const brokertype = "testBrokerType";
    /* let useStateSpy: any; */

    let wrapper: any;
    const mStore = mockStore({
      applicationList: {
        content: [
          {
            appBrokerType: {
              brokerType: brokertype,
            },
            appClientId: appClientIdTest,
            appName: appNameTest,
          },
        ],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      /* useStateSpy = Sinon.spy(React, "useState"); */
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <SimpleApplicationList />
          </Provider>
        </MemoryRouter>
      );
    });

    afterEach(() => {
      /* useStateSpy.restore(); */
    });

    it("should change sorting direction", () => {
      let header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("none");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("ascending");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("descending");
    });
  });

  describe("SimpleApplicationList", () => {
    let wrapper: any;
    const appClientIdTest = "appClientId";
    const appNameTest = "testAppName";
    const brokertype = "testBrokerType";

    const mockHistory = createBrowserHistory();
    const pushSpy = jest.fn();
    const props = {
      history: {
        push: pushSpy,
      },
    };

    const mStore = mockStore({
      applicationList: {
        content: [
          {
            appBrokerType: {
              brokerType: brokertype,
            },
            appClientId: appClientIdTest,
            appName: appNameTest,
          },
        ],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      wrapper = mount(
        <Router history={mockHistory}>
          <Provider store={mStore}>
            <SimpleApplicationList {...props} />
          </Provider>
        </Router>
      );
    });

    it("should call navigate", () => {
      const tbodyRow = wrapper.find(Row).at(1);
      tbodyRow.simulate("click");
      expect(pushSpy.mock.calls).toContainEqual([`/applicationClients/${appClientIdTest}`]);
    });
  });
});
