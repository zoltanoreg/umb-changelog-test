import { shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { BrokerTypeDropdown } from "../../components/BrokerTypeDropdown";
import store from "../../store";

describe("broker type dropdown component test", () => {
  const fakeFc = jest.fn();
  const dropdownWithoutType = (
    <Provider store={store}>
      <BrokerTypeDropdown onChange={fakeFc} selected="" />
    </Provider>
  );

  it("renders without crashing", () => {
    shallow(dropdownWithoutType);
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(dropdownWithoutType);
    expect(wrapper).toMatchSnapshot();
  });
});
