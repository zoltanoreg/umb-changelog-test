import { mount, shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter, Redirect, Route } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import PrivateRoute from "../../components/PrivateRoute";
import { ADMINISTRATOR, READ } from "../../models/UserTypes";
import store from "../../store";

const mockStore = createMockStore([thunk]);

describe("private route component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <PrivateRoute />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <MemoryRouter>
        <Provider store={store}>
          <PrivateRoute />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("check if user is authenticated with ADMINISTRATOR right", () => {
    const props = {
      userLevelAccess: [ADMINISTRATOR],
    };
    const mStore = mockStore({
      currentUser: {
        authenticated: true,
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
        loading: false,
      },
    });

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <PrivateRoute {...props} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper.find(Route).length).toEqual(1);
  });

  it("check if route does not have predefined access", () => {
    const props = {};
    const mStore = mockStore({
      currentUser: {
        authenticated: true,
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
        loading: false,
      },
    });

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <PrivateRoute {...props} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper.find(Route).length).toEqual(1);
  });

  it("check if user has not enough permission to route", () => {
    const props = {
      userLevelAccess: [ADMINISTRATOR],
    };
    const mStore = mockStore({
      currentUser: {
        authenticated: true,
        data: {
          role: READ,
          userId: "test",
        },
        loading: false,
      },
    });

    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mStore}>
          <PrivateRoute {...props} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper.find(Redirect).length).toEqual(1);
  });
});
