import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { EditTopicTypes } from "../../../actions/topic/edit/types";
import { ModalEditTopic } from "../../../components/modals/ModalEditTopic";
import { ModalEditTopicPage1 } from "../../../components/modals/ModalEditTopicPage1";
import { ModalEditTopicPage2 } from "../../../components/modals/ModalEditTopicPage2";
import { ModalEditTopicPage3 } from "../../../components/modals/ModalEditTopicPage3";
import { retainRequiredEnum } from "../../../models/ITopicConfig";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import { TopicService } from "../../../services/TopicService";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("ModalEditTopic component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalEditTopic />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const mStore = mockStore({
      currentUser: { data: { role: "test" } },
      editModal: { topicData: { description: "test" } },
    });
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={mStore}>
          <ModalEditTopic />
        </Provider>
      </MemoryRouter>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  describe("ModalEditTopic component test", () => {
    let wrapper: any;
    let topicServiceStub: any;
    const mStore = mockStore({
      currentUser: {
        data: {
          role: ADMINISTRATOR,
        },
      },
      editModal: {
        appData: {
          appClientId: "appClientIdTest",
          appName: "appNameTest",
        },
        topicData: {
          description: "descriptionTest",
          retainRequired: false,
          topicName: "topicNameTest",
        },
      },
      payloadGet: {
        loading: false,
      },
      payloadUpload: {
        loading: true,
      },
    });
    const configPage1 = {
      appData: {
        appName: "appNameTest1",
      },
      payloadData: {
        payloadSchema: "JSON",
      },
      retainRequired: retainRequiredEnum.YES,
      topicName: "topicNameTest",
    };
    const topicIdTest = "topicIdTest";
    const editTopicContent = {
      data: {
        content: {
          topicId: topicIdTest,
        },
      },
    };
    const topicPayload = {
      data: editTopicContent,
    };

    beforeEach(() => {
      topicServiceStub = Sinon.stub(TopicService, "updateTopic").callsFake((config, id) =>
        Promise.resolve(topicPayload as any)
      );
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <ModalEditTopic />
          </Provider>
        </MemoryRouter>
      );
    });

    afterEach(() => {
      topicServiceStub.restore();
    });

    it("navigating forward on pages, and save the modified topic", async () => {
      // Page1
      expect(wrapper.find(ModalEditTopicPage1).length).toEqual(1);

      act(() => {
        wrapper.find(ModalEditTopicPage1).prop("onChange")(configPage1);
        wrapper.update();
      });
      wrapper
        .find(".ta-modal-next-button")
        .first()
        .simulate("click");

      // Page2
      expect(wrapper.find(ModalEditTopicPage2).length).toEqual(1);

      act(() => {
        wrapper.find(ModalEditTopicPage2).prop("onChange")(configPage1);
        wrapper.update();
      });

      wrapper
        .find(".ta-modal-next-button")
        .first()
        .simulate("click");

      expect(wrapper.find(".ta-modal-next-button").first().length).toEqual(0);
      expect(wrapper.find(".ta-modal-save-button").first().length).toEqual(1);

      // Page3
      expect(wrapper.find(ModalEditTopicPage3).length).toEqual(1);
      act(() => {
        wrapper.find(ModalEditTopicPage3).prop("onChange")(configPage1);
        wrapper.update();
      });

      await wrapper
        .find(".ta-modal-save-button")
        .first()
        .simulate("click");

      const expectedObj = { payload: editTopicContent, type: EditTopicTypes.SUCCESS };
      expect(mStore.getActions()).toContainEqual(expectedObj);
    });

    it("navigating back to previous page", () => {
      // Page1
      expect(wrapper.find(ModalEditTopicPage1).length).toEqual(1);

      act(() => {
        wrapper.find(ModalEditTopicPage1).prop("onChange")(configPage1);
        wrapper.update();
      });
      wrapper
        .find(".ta-modal-next-button")
        .first()
        .simulate("click");
      // Page2
      expect(wrapper.find(ModalEditTopicPage2).length).toEqual(1);

      wrapper
        .find(".ta-modal-previous-button")
        .first()
        .simulate("click");

      expect(wrapper.find(ModalEditTopicPage2).length).toEqual(0);
      expect(wrapper.find(ModalEditTopicPage1).length).toEqual(1);
    });
  });

  describe("ModalEditTopic component test", () => {
    let wrapper: any;
    let topicServiceStub: any;
    const mStore = mockStore({
      currentUser: {
        data: {
          role: ADMINISTRATOR,
        },
      },
      editModal: {
        appData: {
          appClientId: "appClientIdTest",
          appName: "appNameTest",
        },
        topicData: {
          description: "descriptionTest",
          retainRequired: true,
          topicName: "topicNameTest",
        },
      },
      payloadGet: {
        loading: false,
      },
      payloadUpload: {
        loading: true,
      },
      topicDetails: {
        content: {
          topicPayloadId: "",
        },
      },
    });
    const configPage1 = {
      appData: {
        appName: "appNameTest1",
      },
      payloadData: {
        payloadSchema: "JSON",
      },
      retainRequired: retainRequiredEnum.YES,
      topicName: "topicNameTest",
    };
    const topicIdTest = "topicIdTest";
    const editTopicContent = {
      data: {
        content: {
          topicId: topicIdTest,
        },
      },
    };
    const topicPayload = {
      data: editTopicContent,
    };

    beforeEach(() => {
      topicServiceStub = Sinon.stub(TopicService, "updateTopic").callsFake((config, id) =>
        Promise.resolve(topicPayload as any)
      );
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <ModalEditTopic />
          </Provider>
        </MemoryRouter>
      );
    });

    afterEach(() => {
      topicServiceStub.restore();
    });
  });
});
