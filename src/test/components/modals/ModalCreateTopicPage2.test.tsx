import { mount, ReactWrapper, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { ProgressIndicatorCircular } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { FileInput } from "../../../components/FileInput";
import { DropdownInput } from "../../../components/modals/Dropdownlnput";
import { ModalCreateTopicPage2 } from "../../../components/modals/ModalCreateTopicPage2";
import { PayloadSchemaEditor } from "../../../components/modals/PayloadSchemaEditor";
import { payloadFormatEnum } from "../../../models/ITopicConfig";
import { TopicService } from "../../../services/TopicService";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("ModalCreateTopicPage2 component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalCreateTopicPage2 config={{ payloadConfig: { topicPayloadId: 0 } }} />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      config: {
        isSecure: true,
        payloadConfig: {
          topicPayloadId: 0,
        },
        payloadFormat: "test",
        topicName: "test",
      },
    };
    const fakeFc = jest.fn();
    const wrapper = mount(
      <Provider store={store}>
        <ModalCreateTopicPage2 onChange={fakeFc} {...setValues} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  describe("ModalCreateTopicPage2", () => {
    it("appears loading animation", () => {
      const mStore = mockStore({
        editModal: {
          appData: {},
        },
        payloadGet: {
          loading: true,
        },
        payloadUpload: {
          loading: true,
        },
      });

      const fakeFc = jest.fn();
      const props = {
        config: {
          payloadConfig: {
            topicPayloadId: "topicPayloadId",
          },
          topic: {
            topicName: "topicNameTest",
          },
        },
      };

      const wrapper = mount(
        <Provider store={mStore}>
          <ModalCreateTopicPage2 onChange={fakeFc} {...props} />
        </Provider>
      );
      expect(wrapper.find(ProgressIndicatorCircular).length).toEqual(1);
    });

    it("change dropdown values", () => {
      const payloadConfigObj = {
        payloadFileName: "",
        topicPayloadId: "",
      };

      const fakeFc = jest.fn();
      const props = {
        config: {
          payloadConfig: payloadConfigObj,
        },
      };

      const wrapper = mount(
        <Provider store={store}>
          <ModalCreateTopicPage2 onChange={fakeFc} {...props} />
        </Provider>
      );

      const payloadFormatValue = payloadFormatEnum.JSON;
      act(() => {
        wrapper
          .find(DropdownInput)
          .at(0)
          .prop("onChange")(payloadFormatValue);
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({
        payloadConfig: { ...payloadConfigObj, payloadFormat: payloadFormatValue },
      });
    });

    it("clears file name", () => {
      const payloadConfigObj = {
        payloadFile: File,
        payloadFileName: "payloadFileNameTest",
        payloadFormat: "",
        payloadSchema: "",
        topicPayloadId: "",
      };
      const config = {
        payloadConfig: payloadConfigObj,
        topicPayloadId: "topicPayloadIdTest",
      };
      const fakeFc = jest.fn();
      const props = {
        config,
      };

      const wrapper = mount(
        <Provider store={store}>
          <ModalCreateTopicPage2 onChange={fakeFc} {...props} />
        </Provider>
      );

      act(() => {
        wrapper
          .find(".ta-chip-payloadFile")
          .first()
          .simulate("click");
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({
        ...config,
        payloadConfig: { ...payloadConfigObj, payloadFileName: "" },
      });
    });

    describe("handles file upload", () => {
      let topicServiceStub: any;
      afterEach(() => {
        topicServiceStub.restore();
      });

      it("with deficient response", () => {
        const deficientResponseObj = {
          data: {
            data: {
              content: {
                payloadId: "testId",
                /* payloadSchema: "testSchema", */
              },
            },
          },
        };
        topicServiceStub = Sinon.stub(TopicService, "uploadPayload").callsFake((configParam: any) =>
          Promise.resolve(deficientResponseObj as any)
        );

        const payloadConfigObj = {
          payloadFileName: "",
          payloadFormat: payloadFormatEnum.JSON,
          topicPayloadId: "",
        };
        const config = {
          payloadConfig: payloadConfigObj,
        };
        const props = {
          config,
        };
        const fakeFc = jest.fn();
        const wrapper = mount(
          <Provider store={store}>
            <ModalCreateTopicPage2 onChange={fakeFc} {...props} />
          </Provider>
        );
        const fileName = "testFileName";
        act(() => {
          const uploadedFile: File = {
            lastModified: new Date().getTime(),
            name: fileName,
            size: 1,
            slice: () => new Blob(),
            type: "JSON",
          };
          wrapper
            .find(FileInput)
            .first()
            .prop("onChange")(uploadedFile);
        });
        expect(fakeFc).toBeCalledTimes(1);
        expect(fakeFc).toBeCalledWith({
          ...config,
          payloadConfig: { ...payloadConfigObj, payloadFileName: fileName },
        });
      });

      it("with full response", () => {
        const responseObj = {
          data: {
            data: {
              content: {
                payloadId: "testId",
                payloadSchema: "testSchema",
              },
            },
          },
        };
        topicServiceStub = Sinon.stub(TopicService, "uploadPayload").callsFake((configParam: any) =>
          Promise.resolve(responseObj as any)
        );

        const payloadConfigObj = {
          payloadFileName: "",
          payloadFormat: payloadFormatEnum.JSON,
          topicPayloadId: "",
        };
        const config = {
          payloadConfig: payloadConfigObj,
        };
        const props = {
          config,
        };
        const fakeFc = jest.fn();
        const wrapper = mount(
          <Provider store={store}>
            <ModalCreateTopicPage2 onChange={fakeFc} {...props} />
          </Provider>
        );
        const fileName = "testFileName";
        const uploadedFile: File = {
          lastModified: new Date().getTime(),
          name: fileName,
          size: 1,
          slice: () => new Blob(),
          type: "JSON",
        };
        act(() => {
          wrapper
            .find(FileInput)
            .first()
            .prop("onChange")(uploadedFile);
        });
        expect(fakeFc).toBeCalledTimes(1);
        expect(fakeFc).toBeCalledWith({
          ...config,
          payloadConfig: { ...payloadConfigObj, payloadFileName: fileName },
        });
      });
    });

    describe("handles file upload", () => {
      let topicServiceSpy: any;
      afterEach(() => {
        topicServiceSpy.restore();
      });

      it("with full response", () => {
        topicServiceSpy = Sinon.spy(TopicService, "uploadPayload");

        const payloadConfigObj = {
          payloadFileName: "",
          payloadFormat: payloadFormatEnum.JSON,
          topicPayloadId: "",
        };
        const config = {
          payloadConfig: payloadConfigObj,
        };
        const props = {
          config,
        };
        const fakeFc = jest.fn();
        const wrapper = mount(
          <Provider store={store}>
            <ModalCreateTopicPage2 onChange={fakeFc} {...props} />
          </Provider>
        );
        const fileName = "testFileName";
        const uploadedFile: File = {
          lastModified: new Date().getTime(),
          name: fileName,
          size: 1,
          slice: () => new Blob(),
          type: "JSON",
        };
        act(() => {
          wrapper
            .find(FileInput)
            .first()
            .prop("onChange")(uploadedFile);
        });
        expect(fakeFc).toBeCalledTimes(1);
        expect(fakeFc).toBeCalledWith({
          ...config,
          payloadConfig: { ...payloadConfigObj, payloadFileName: fileName },
        });
      });
    });
  });

  describe("Without file there shouldn't be an editor rendered and file upload while not editing", () => {
    const cancel = jest.fn();
    const mStore = mockStore({
      payloadGet: {
        error: false,
        loading: false,
        payloadFile: "",
        payloadFormat: "Protobuf",
        payloadSchema: 'syntax = "proto3";\n\npackage books;\n',
      },
      payloadUpload: {
        loading: false,
      },
      topicDetails: {
        content: {
          topicPayloadId: "",
        },
        loading: false,
      },
    });
    let wrapper: ReactWrapper;
    const fakeFc = jest.fn();
    const props = {
      config: {
        payloadConfig: {
          error: false,
          loading: false,
          payloadFile: "",
          payloadFileName: "",
          payloadFormat: "JSON",
          payloadMasterId: "id",
          payloadSchema: '{"id": "1"}',
          payloadType: null,
        },
      },
      isEditing: false,
      onToggleEditing: cancel,
    };

    beforeAll(() => {
      wrapper = mount(
        <Provider store={mStore}>
          <ModalCreateTopicPage2 {...props} onChange={fakeFc} />
        </Provider>
      );
    });

    it("should render schema editor on click", () => {
      expect(wrapper.find(PayloadSchemaEditor).length).toEqual(0);
    });
  });

  describe("Cancel button click", () => {
    const cancel = jest.fn();
    const mStore = mockStore({
      payloadGet: {
        error: false,
        loading: false,
        payloadFile: "url",
        payloadFormat: "Protobuf",
        payloadMasterId: "id",
        payloadSchema: 'syntax = "proto3";\n\npackage books;\n',
      },
      payloadUpload: {
        loading: false,
      },
      topicDetails: {
        content: {
          topicPayloadId: "",
        },
        loading: false,
      },
    });
    let wrapper: ReactWrapper;
    const fakeFc = jest.fn();
    const props = {
      config: {
        payloadConfig: {
          error: false,
          loading: false,
          payloadFile: "url",
          payloadFileName: "payloadFileName1",
          payloadFormat: "JSON",
          payloadSchema: '{"id": "1"}',
        },
        topicPayloadId: "asd",
      },
      isEditing: true,
      onToggleEditing: cancel,
    };

    beforeAll(() => {
      wrapper = mount(
        <Provider store={mStore}>
          <ModalCreateTopicPage2 {...props} onChange={fakeFc} />
        </Provider>
      );
    });

    it("should render schema editor on click", () => {
      expect(wrapper.find(PayloadSchemaEditor).length).toBeGreaterThan(0);
    });

    it("should call cancel on click", () => {
      const btn = wrapper.find(".ta-payloadSchemaEditor-button-cancel").first();

      act(() => {
        btn.simulate("click");
      });

      expect(cancel).toBeCalled();
    });

    it("should clear file name", () => {
      act(() => {
        wrapper
          .find(".ta-chip-payloadFile")
          .first()
          .simulate("click");
      });
      expect(fakeFc).not.toBeCalled();
    });
  });

  describe("Open editor", () => {
    const onedit = jest.fn();
    const mStore = mockStore({
      payloadGet: {
        createdAt: "2020-04-06T12:14:39.000Z",
        error: false,
        loading: false,
        modifiedAt: "2020-04-06T12:14:51.000Z",
        payloadFile: "url",
        payloadFormat: "Protobuf",
        payloadMasterId: "id",
        payloadSchema: 'syntax = "proto3";\n\npackage books;\n',
        payloadType: null,
      },
      payloadUpload: {
        loading: false,
      },
      topicDetails: {
        loading: false,
      },
    });
    let wrapper: ReactWrapper;
    const fakeFc = jest.fn();
    const props = {
      config: {
        payloadConfig: {
          error: false,
          loading: false,
          payloadFile: "url",
          payloadFileName: "payloadFileName1",
          payloadFormat: "Protobuf",
          payloadMasterId: "id",
          payloadSchema: 'syntax = "proto3";\n\npackage books;\n',
          payloadType: null,
        },
      },
      isEditing: false,
      onToggleEditing: onedit,
    };

    beforeAll(() => {
      wrapper = mount(
        <Provider store={mStore}>
          <ModalCreateTopicPage2 {...props} onChange={fakeFc} />
        </Provider>
      );
    });

    it("should have edit schema button", () => {
      expect(wrapper.find(".ta-addTopic-button-editSchema").length).toBeGreaterThan(0);
    });

    it("should have edit schema button", () => {
      const btn = wrapper.find(".ta-addTopic-button-editSchema").first();

      act(() => {
        btn.simulate("click");
      });

      expect(onedit).toBeCalled();
    });
  });
});
