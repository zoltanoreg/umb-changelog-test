import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
// tslint:disable-next-line: no-implicit-dependencies
import { createBrowserHistory } from "history";
import { ProgressIndicatorCircular } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter, Router } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { EditApplicationTypes } from "../../../actions/application/edit/types";
import { TGetApplicationDetails } from "../../../actions/application/get-details/types";
import { BrokerTypeDropdown } from "../../../components/BrokerTypeDropdown";
import { DropdownInput } from "../../../components/modals/Dropdownlnput";
import { ModalEditApplication } from "../../../components/modals/ModalEditApplication";
import { authTypeEnum } from "../../../models/IAppDetailsConfig";
import { modalTypeEnum } from "../../../models/IEditModalConfig";
import { ADMINISTRATOR, APPLICATION_OWNER } from "../../../models/UserTypes";
import { ApplicationService } from "../../../services/ApplicationService";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("ModalEditApplication component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalEditApplication />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <ModalEditApplication />
        </Provider>
      </MemoryRouter>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it("progressIndicator appears while loading", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      applicationEdit: {
        loading: true,
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
      editModal: {
        appData: {
          appOwner: {
            userId: "testUserId",
          },
        },
        type: modalTypeEnum.EDIT_APP,
      },
    });

    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={mStore}>
          <ModalEditApplication />
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper.find(ProgressIndicatorCircular).length).toEqual(1);
  });

  describe("modify an applicationClient", () => {
    const mockHistory = createBrowserHistory();
    let applicationServiceStub: any;
    let applicationServiceStub2: any;
    const appClientIdTest = "testAppClientId";
    const applicationDetailsObj = {
      content: [],
      count: 0,
    };

    beforeAll(() => {
      applicationServiceStub = Sinon.stub(ApplicationService, "updateApplication").callsFake(
        (config: any) => Promise.resolve({ data: { appClientId: appClientIdTest } } as any)
      );
      applicationServiceStub2 = Sinon.stub(ApplicationService, "getApplicationDetails").callsFake(
        (appId: any) => Promise.resolve({ data: applicationDetailsObj } as any)
      );
    });

    afterAll(() => {
      applicationServiceStub.restore();
      applicationServiceStub2.restore();
    });
  });
});
