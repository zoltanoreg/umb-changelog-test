import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import * as React from "react";
import { Provider } from "react-redux";

import { ModalCreateBase } from "../../../components/modals/ModalCreateBase";
import store from "../../../store";

describe("ModalCreateBase component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalCreateBase />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <Provider store={store}>
        <ModalCreateBase />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
