import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import * as React from "react";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { ModalAddTopicSubscriberPage1 } from "../../../components/modals/ModalAddTopicSubscriberPage1";
import { StatusIcon } from "../../../components/StatusIcon";
import { Typeahead } from "../../../components/Typeahead";
import { topicStatusEnum } from "../../../models/ITopicConfig";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("ModalAddTopicSubscriberPage1 component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalAddTopicSubscriberPage1 />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const mStore = mockStore({
      applicationDetails: { content: { appBrokerType: { brokerType: "test" } } },
      editModal: { appData: "test" },
      topicMQTTClients: { content: {} },
    });
    const setValues = {
      config: {
        appClientId: "test",
        createBrige: "test",
        topic: {
          appClientId: "test",
          topicId: "test",
          topicName: "test",
          topicStatus: topicStatusEnum.DRAFT,
        },
      },
    };
    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriberPage1 {...setValues} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it("check success statusicon case 1", () => {
    const mStore = mockStore({
      applicationDetails: { content: { appBrokerType: { brokerType: "test" } } },
      editModal: { appData: "test" },
      topicMQTTClients: { content: {} },
    });
    const setValues = {
      config: {
        appClientId: "test",
        createBrige: "test",
        topic: {
          appClientId: "test",
          topicId: "test",
          topicName: "test",
          topicStatus: topicStatusEnum.ACTIVE,
        },
      },
    };
    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriberPage1 {...setValues} />
      </Provider>
    );
    expect(
      wrapper
        .find(StatusIcon)
        .first()
        .prop("className")
    ).toEqual("ta-status-icon-success");
  });

  it("check success statusicon case 2", () => {
    const mStore = mockStore({
      applicationDetails: { content: { appBrokerType: { brokerType: "test" } } },
      editModal: { appData: "test" },
      topicMQTTClients: { content: {} },
    });
    const setValues = {
      config: {
        appClientId: "test",
        createBrige: "test",
        topic: {
          appClientId: "test",
          topicId: "test",
          topicName: "test",
          topicStatus: topicStatusEnum.ACTIVE,
        },
      },
    };
    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriberPage1 {...setValues} />
      </Provider>
    );
    expect(
      wrapper
        .find(StatusIcon)
        .first()
        .prop("className")
    ).toEqual("ta-status-icon-success");
  });

  it("check fail statusicon", () => {
    const mStore = mockStore({
      applicationDetails: { content: { appBrokerType: { brokerType: "test" } } },
      editModal: { appData: "test" },
      topicMQTTClients: { content: {} },
    });
    const setValues = {
      config: {
        appClientId: "test1",
        createBrige: "test",
        topic: {
          appClientId: "test2",
          topicId: "test",
          topicName: "test",
          topicStatus: topicStatusEnum.ACTIVE,
        },
      },
    };
    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriberPage1 {...setValues} />
      </Provider>
    );
    expect(
      wrapper
        .find(StatusIcon)
        .first()
        .prop("className")
    ).toEqual("ta-status-icon-success");
  });

  it("check fail statusicon case 1", () => {
    const mStore = mockStore({
      applicationDetails: { content: { appBrokerType: { brokerType: "test" } } },
      editModal: { appData: "test" },
      topicMQTTClients: { content: {} },
    });
    const setValues = {
      config: {
        appClientId: "test1",
        createBrige: "test",
        topic: {
          appClientId: "test2",
          topicId: "test",
          topicName: "test",
          topicStatus: topicStatusEnum.DRAFT,
        },
      },
    };
    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriberPage1 {...setValues} />
      </Provider>
    );
    expect(
      wrapper
        .find(StatusIcon)
        .first()
        .prop("className")
    ).toEqual("ta-status-icon-fail");
  });

  it("check fail statusicon case2", () => {
    const mStore = mockStore({
      applicationDetails: { content: { appBrokerType: { brokerType: "test" } } },
      editModal: { appData: "test" },
      topicMQTTClients: { content: {} },
    });
    const setValues = {
      config: {
        appClientId: "test1",
        createBrige: "test",
        topic: {
          appClientId: "test2",
          topicId: "test",
          topicName: "test",
          topicStatus: topicStatusEnum.DRAFT,
        },
      },
    };
    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriberPage1 {...setValues} />
      </Provider>
    );
    expect(
      wrapper
        .find(StatusIcon)
        .first()
        .prop("check")
    ).toEqual(false);
  });

  it("chec draft chiplabel", () => {
    const mStore = mockStore({
      applicationDetails: { content: { appBrokerType: { brokerType: "test" } } },
      editModal: { appData: "test" },
      topicMQTTClients: { content: {} },
    });
    const setValues = {
      config: {
        appClientId: "test1",
        createBrige: "test",
        topic: {
          appClientId: "test2",
          topicId: "test",
          topicName: "test",
          topicStatus: topicStatusEnum.DRAFT,
        },
      },
    };

    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriberPage1 {...setValues} />
      </Provider>
    );
    wrapper.find(Typeahead).prop("chipLabel")(setValues);

    expect(
      wrapper
        .find(".ta-addTopicSubscriber-typeahead-topic-chip")
        .first()
        .prop("label")
    ).toEqual("test ---- Topic is a draft ⚠️");
  });

  it("check onhold chiplabel", () => {
    const mStore = mockStore({
      applicationDetails: { content: { appBrokerType: { brokerType: "test" } } },
      editModal: { appData: "test" },
      topicMQTTClients: { content: {} },
    });
    const setValues = {
      config: {
        appClientId: "test1",
        createBrige: "test",
        topic: {
          appClientId: "test2",
          topicId: "test",
          topicName: "test",
          topicStatus: topicStatusEnum.ONHOLD,
        },
      },
    };

    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriberPage1 {...setValues} />
      </Provider>
    );

    wrapper.find(Typeahead).prop("chipLabel")(setValues);
    expect(
      wrapper
        .find(".ta-addTopicSubscriber-typeahead-topic-chip")
        .first()
        .prop("label")
    ).toEqual("test ---- Topic is on hold ⚠️");
  });
});
