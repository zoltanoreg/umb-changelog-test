import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
// tslint:disable-next-line: no-implicit-dependencies
import { createBrowserHistory } from "history";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter, Router } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { CreatePublisherTypes } from "../../../actions/application/create-publisher/types";
import { FilterTopicListType } from "../../../actions/topic/filter-list/types";
import { ModalCreateTopic } from "../../../components/modals/ModalCreateTopic";
import { ModalCreateTopicPage1 } from "../../../components/modals/ModalCreateTopicPage1";
import { ModalCreateTopicPage2 } from "../../../components/modals/ModalCreateTopicPage2";
import { topicStatusEnum } from "../../../models/ITopicConfig";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import { MQTTService } from "../../../services/MQTTService";
import { TopicService } from "../../../services/TopicService";
import store from "../../../store";

jest.useFakeTimers();
const mockStore = createMockStore([thunk]);

describe("ModalCreateTopic component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalCreateTopic />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <ModalCreateTopic />
        </Provider>
      </MemoryRouter>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  describe("ModalCreateTopic", () => {
    let wrapper: any;
    let pushSpy: any;
    let topicServiceStub: any;
    let topicListStub: any;
    const mockHistory = createBrowserHistory();
    const mStore = mockStore({
      currentUser: {
        data: {
          role: ADMINISTRATOR,
        },
      },
      editModal: {
        appData: {},
      },
      filterTopicList: {
        content: [],
        count: 0,
      },
      payloadGet: {
        loading: false,
      },
      payloadUpload: {
        loading: false,
      },
    });

    const configPage1 = {
      appData: {
        appClientId: "test",
        appName: "testest",
        appVersion: "13.9",
      },
      description: "test",
      isSecure: true,
      offloadType: "test",
      payloadConfig: {},
      priority: "test",
      qosLevel: "test",
      retainRequired: "test",
      topic: {
        topicId: "testidfortesttopic",
        topicName: "test",
        topicStatus: topicStatusEnum.ACTIVE,
      },
      ttl: 1,
    };

    const topicIdTest = "testTopicId";

    const payloadObj = {
      data: {
        data: {
          content: topicIdTest,
        },
      },
    };

    const topicsList = {
      data: {
        data: {
          content: [{ topicName: "semmi" }],
          count: 0,
        },
      },
    };

    beforeEach(() => {
      pushSpy = Sinon.spy(mockHistory, "push");
      topicServiceStub = Sinon.stub(TopicService, "createTopic").callsFake(config =>
        Promise.resolve(payloadObj as any)
      );

      topicListStub = Sinon.stub(TopicService, "getTopicList").callsFake(config =>
        Promise.resolve(topicsList as any)
      );

      wrapper = mount(
        <Router history={mockHistory}>
          <Provider store={mStore}>
            <ModalCreateTopic />
          </Provider>
        </Router>
      );
    });

    afterEach(() => {
      pushSpy.restore();
      topicServiceStub.restore();
      topicListStub.restore();
    });

    it("check first page", () => {
      // Page1
      expect(wrapper.find(ModalCreateTopicPage1).length).toEqual(1);

      act(() => {
        wrapper.find(ModalCreateTopicPage1).prop("onChange")(configPage1);
        jest.runAllTimers();
        wrapper.update();
      });

      const expectedObj = { type: FilterTopicListType.SUCCESS, payload: topicsList.data.data };

      expect(mStore.getActions()).toContainEqual(expectedObj);
    });
  });
});
