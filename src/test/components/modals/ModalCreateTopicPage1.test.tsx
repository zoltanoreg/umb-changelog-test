import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { Checkbox, NumberInput } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";

import { DropdownInput } from "../../../components/modals/Dropdownlnput";
import { ModalCreateTopicPage1 } from "../../../components/modals/ModalCreateTopicPage1";
import { topicStatusEnum } from "../../../models/ITopicConfig";
import store from "../../../store";

describe("ModalCreateTopicPage1 component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalCreateTopicPage1 />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      config: {
        description: "test",
        isSecure: true,
        offloadType: "test",
        priority: "test",
        qosLevel: "test",
        topicName: "test",
        ttl: "test",
      },
    };
    const fakeFc = jest.fn();
    const wrapper = mount(
      <Provider store={store}>
        <ModalCreateTopicPage1
          onChange={fakeFc}
          {...setValues}
          setIsTopicNameValid={jest.fn()}
          isTopicNameValid={false}
        />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  describe("ModalCreateTopicPage1", () => {
    const configObj = {
      isSecure: true,
      retainRequired: "test",
      topic: {
        topicName: "test",
        topicStatus: topicStatusEnum.ACTIVE,
      },
    };
    const props = {
      appData: {
        appBrokerType: {
          brokerType: "test",
        },
        appName: "test",
      },
      config: configObj,
    };
    let fakeFc: any;
    let wrapper: any;

    beforeEach(() => {
      fakeFc = jest.fn();
      wrapper = mount(
        <Provider store={store}>
          <ModalCreateTopicPage1
            onChange={fakeFc}
            {...props}
            setIsTopicNameValid={jest.fn()}
            isTopicNameValid={false}
          />
        </Provider>
      );
    });

    afterEach(() => {
      fakeFc.mockRestore();
    });

    it("change dropdown values", () => {
      const priorityValue = "testPriority";
      act(() => {
        wrapper
          .find(DropdownInput)
          .at(0)
          .prop("onChange")(priorityValue);
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({ ...configObj, priority: priorityValue });

      const offload = "offloadValue";
      act(() => {
        wrapper
          .find(DropdownInput)
          .at(1)
          .prop("onChange")(offload);
      });
      expect(fakeFc).toBeCalledWith({ ...configObj, offloadType: offload });

      const qos = "qosValue";
      act(() => {
        wrapper
          .find(DropdownInput)
          .at(2)
          .prop("onChange")(qos);
      });
      expect(fakeFc).toBeCalledWith({ ...configObj, qosLevel: qos });
    });

    it("change NumberInput value", () => {
      const numberValue = 1;
      act(() => {
        wrapper.find(NumberInput).prop("onChange")(numberValue);
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({ ...configObj, ttl: numberValue });

      act(() => {
        wrapper.find(NumberInput).prop("onChange")(-1);
      });
      expect(fakeFc).toBeCalledWith({ ...configObj, ttl: 1 });

      act(() => {
        wrapper.find(NumberInput).prop("onChange")("NotNumberValue");
      });
      expect(fakeFc).toBeCalledWith({ ...configObj, ttl: 0 });
    });

    describe("textarea", () => {
      it("should not update textarea’s content on whitespace", () => {
        act(() => {
          wrapper
            .find(".ta-modal-input-description")
            .first()
            .prop("onChange")({ target: { value: " " } });
        });
        expect(fakeFc).not.toBeCalled();
      });

      it("should not change textarea’s content on too long string", () => {
        act(() => {
          wrapper
            .find(".ta-modal-input-description")
            .first()
            .prop("onChange")({
            target: {
              value:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            },
          });
        });
        expect(fakeFc).not.toBeCalled();
      });

      it("should change textarea’s content on adding correct string", () => {
        const textValue = "test";
        act(() => {
          wrapper
            .find(".ta-modal-input-description")
            .first()
            .prop("onChange")({ target: { value: textValue } });
        });
        expect(fakeFc).toBeCalledWith({ ...configObj, description: textValue });
      });
    });

    it("change checkbox value", () => {
      act(() => {
        wrapper.find(Checkbox).prop("onClick")();
      });
      expect(fakeFc).toBeCalledWith({ ...configObj, isSecure: false });
    });
  });
});
