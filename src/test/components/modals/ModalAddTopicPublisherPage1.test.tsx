import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { Checkbox } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";

import { ModalAddTopicPublisherPage1 } from "../../../components/modals/ModalAddTopicPublisherPage1";
import { TypeaheadTopics } from "../../../components/TypeaheadTopics";
import { topicStatusEnum } from "../../../models/ITopicConfig";
import store from "../../../store";

describe("ModalAddTopicPublisherPage1 component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalAddTopicPublisherPage1 />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      appData: {
        appBrokerType: {
          brokerType: "test",
        },
        appName: "test",
      },
      config: {
        retainRequired: "test",
        topicName: "test",
      },
    };
    const fakeFc = jest.fn();
    const wrapper = mount(
      <Provider store={store}>
        <ModalAddTopicPublisherPage1 onChange={fakeFc} {...setValues} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  describe("ModalAddTopicPublisherPage1", () => {
    const configObj = {
      retainRequired: "test",
      topic: {
        topicName: "test",
        topicStatus: topicStatusEnum.ACTIVE,
      },
    };
    const props = {
      appData: {
        appBrokerType: {
          brokerType: "test",
        },
        appName: "test",
      },
      config: configObj,
    };
    let fakeFc: any;
    let wrapper: any;

    beforeEach(() => {
      fakeFc = jest.fn();
      wrapper = mount(
        <Provider store={store}>
          <ModalAddTopicPublisherPage1 onChange={fakeFc} {...props} />
        </Provider>
      );
    });

    afterEach(() => {
      fakeFc.mockRestore();
    });

    it("change dropdown value", () => {
      const retainRequiredValue = false;
      act(() => {
        wrapper
          .find(Checkbox)
          .first()
          .prop("onClick")();
        wrapper.setProps({});
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({ ...configObj, retainRequired: retainRequiredValue });
    });

    it("change typeahead value", () => {
      const topicData = {
        topicName: "test",
      };
      act(() => {
        wrapper
          .find(TypeaheadTopics)
          .first()
          .prop("onChange")(topicData);
        wrapper.update();
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({ ...configObj, topic: topicData });
    });
  });
});
