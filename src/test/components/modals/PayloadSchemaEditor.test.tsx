import { mount, ReactWrapper, shallow } from "enzyme";
import toJson from "enzyme-to-json";
// tslint:disable-next-line: no-import-side-effect
import "jest-canvas-mock";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon, { SinonStubbedInstance } from "sinon";

import { PayloadSchemaEditor } from "../../../components/modals/PayloadSchemaEditor";
import { TopicService } from "../../../services/TopicService";

const mockStore = createMockStore([thunk]);

describe("PayloadSchemaEditor component test", () => {
  let wrapper: ReactWrapper;
  const propsWithJson = {
    callback: jest.fn(),
    cancel: () => null,
    payloadData: {
      payloadFormat: "JSON",
      payloadSchema: '{id: "1"}',
    },
    payloadId: "",
  };
  const propsWithProtobuf = {
    callback: jest.fn(),
    cancel: jest.fn(),
    payloadData: {
      payloadFormat: "Protobuf",
      payloadSchema: 'syntax = "proto3;"',
    },
    payloadId: "",
  };
  const mStore = mockStore({});

  describe("Rendering & snapshot", () => {
    it("renders without crashing", () => {
      shallow(
        <Provider store={mStore}>
          <PayloadSchemaEditor {...propsWithJson} />
        </Provider>
      );
    });

    it("check if matches snapshot", () => {
      wrapper = mount(
        <Provider store={mStore}>
          <PayloadSchemaEditor {...propsWithJson} />
        </Provider>
      );

      expect(toJson(wrapper)).toMatchSnapshot();
    });

    it("should have an update button", () => {
      expect(wrapper.find(".ta-payloadSchemaEditor-button-update").length).toBeGreaterThan(0);
    });

    it("should have a cancel button", () => {
      expect(wrapper.find(".ta-payloadSchemaEditor-button-cancel").length).toBeGreaterThan(0);
    });
  });

  describe("Update button click", () => {
    let stubbed: SinonStubbedInstance<any>;

    beforeAll(() => {
      stubbed = Sinon.stub(TopicService, "updatePayload").callsFake((config: any) =>
        Promise.resolve({} as any)
      );
    });

    afterAll(() => {
      stubbed.restore();
    });

    it("should call service", () => {
      wrapper = mount(
        <Provider store={mStore}>
          <PayloadSchemaEditor {...propsWithJson} />
        </Provider>
      );

      const button = wrapper.find(".ta-payloadSchemaEditor-button-update").first();

      act(() => {
        button.simulate("click");
      });

      expect(stubbed.called).toEqual(true);
    });
  });
});
