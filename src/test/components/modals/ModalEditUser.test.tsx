import { mount, shallow } from "enzyme";
import { Dropdown } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon, { SinonStubbedInstance } from "sinon";

import { ModalEditUser } from "../../../components/modals/ModalEditUser";
import { modalTypeEnum } from "../../../models/IEditModalConfig";
import {
  ADMINISTRATOR,
  APPLICATION_OWNER,
  getUserRole,
  READ,
  UMBT_IT_SUPPORT_TEAM,
  UMBT_OPERATIONAL_SUPPORT_TEAM,
} from "../../../models/UserTypes";
import { UserService } from "../../../services/UserService";
import store from "../../../store";

describe("ModalEditUser component test", () => {
  let userServiceStub: SinonStubbedInstance<any>;

  beforeAll(() => {
    const userObj = { data: {} };
    userServiceStub = Sinon.stub(UserService, "setUserDetails").callsFake((userData: any) =>
      Promise.resolve(userObj as any)
    );
  });

  afterEach(() => {
    userServiceStub.resetHistory();
  });

  const mockStore = createMockStore([thunk]);

  const currentUserReadOnly = {
    data: {
      role: READ,
    },
  };
  const userDetailsReadOnly = {
    content: {
      createdBy: {
        fullName: "test",
      },
      firstName: "test firstname",
      lastName: "test lastname",
      modifiedBy: {
        fullName: "test",
      },
      role: READ,
      userId: "testUserId",
    },
    error: false,
    loading: false,
  };
  const editModal = { type: modalTypeEnum.EDIT_USER };

  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalEditUser />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const mStore = mockStore({
      currentUser: currentUserReadOnly,
      // tslint:disable-next-line: object-literal-shorthand
      editModal: editModal,
      userDetails: userDetailsReadOnly,
    });
    const wrapper = mount(
      <Provider store={mStore}>
        <ModalEditUser />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("Read Only user sends a request for Application owner role", () => {
    const mStore = mockStore({
      currentUser: currentUserReadOnly,
      // tslint:disable-next-line: object-literal-shorthand
      editModal: editModal,
      userDetails: userDetailsReadOnly,
    });
    const wrapper = mount(
      <Provider store={mStore}>
        <ModalEditUser />
      </Provider>
    );

    const selectedRole = APPLICATION_OWNER;
    let dropdown = wrapper
      .find(".ta-modal-input-role")
      .find(Dropdown)
      .first();

    act(() => {
      dropdown.prop("onChange")(selectedRole);
      wrapper.setProps({});
    });

    dropdown = wrapper
      .find(".ta-modal-input-role")
      .find(Dropdown)
      .first();
    expect(dropdown.prop("selected")).toEqual(selectedRole);

    const submitButton = wrapper.find(".ta-modal-submit-button").first();
    expect(submitButton.text()).toEqual("Request change");

    act(() => {
      const onClickFunction = submitButton.prop("onClick");
      if (onClickFunction) {
        onClickFunction({} as any);
      }
    });

    const expectedObj = {
      role: selectedRole,
      userId: userDetailsReadOnly.content.userId,
    };
    expect(userServiceStub.getCall(0).args[0]).toEqual(expectedObj);
  });

  it("Admin changes role of Read Only user, without opening a request", () => {
    const currentUserAdmin = { ...currentUserReadOnly };
    currentUserAdmin.data.role = ADMINISTRATOR;

    const mStore = mockStore({
      currentUser: currentUserAdmin,
      // tslint:disable-next-line: object-literal-shorthand
      editModal: editModal,
      userDetails: userDetailsReadOnly,
    });

    const wrapper = mount(
      <Provider store={mStore}>
        <ModalEditUser />
      </Provider>
    );

    const selectedRole = UMBT_IT_SUPPORT_TEAM;
    let dropdown = wrapper
      .find(".ta-modal-input-role")
      .find(Dropdown)
      .first();

    dropdown.find("span").filterWhere((option: any) => {
      // Admin can change to any arbitrary role
      const possibleRoles: string[] = [];
      possibleRoles.push(getUserRole(ADMINISTRATOR));
      possibleRoles.push(getUserRole(APPLICATION_OWNER));
      possibleRoles.push(getUserRole(READ));
      possibleRoles.push(getUserRole(UMBT_IT_SUPPORT_TEAM));
      possibleRoles.push(getUserRole(UMBT_OPERATIONAL_SUPPORT_TEAM));
      expect(possibleRoles.indexOf(option.text())).not.toEqual(-1);
    });

    act(() => {
      dropdown.prop("onChange")(selectedRole);
      wrapper.setProps({});
    });

    dropdown = wrapper
      .find(".ta-modal-input-role")
      .find(Dropdown)
      .first();
    expect(dropdown.prop("selected")).toEqual(selectedRole);

    const submitButton = wrapper.find(".ta-modal-submit-button").first();
    expect(submitButton.text()).toEqual("Save");

    act(() => {
      const onClickFunction = submitButton.prop("onClick");
      if (onClickFunction) {
        onClickFunction({} as any);
      }
    });

    const expectedObj = {
      role: selectedRole,
      userId: userDetailsReadOnly.content.userId,
    };

    expect(userServiceStub.getCall(0).args[0]).toEqual(expectedObj);
  });
});
