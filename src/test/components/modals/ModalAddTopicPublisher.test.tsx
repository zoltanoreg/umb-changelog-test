import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
// tslint:disable-next-line: no-implicit-dependencies
import { createBrowserHistory } from "history";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter, Router } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { CreatePublisherTypes } from "../../../actions/application/create-publisher/types";
import { ModalAddTopicPublisher } from "../../../components/modals/ModalAddTopicPublisher";
import { ModalAddTopicPublisherPage1 } from "../../../components/modals/ModalAddTopicPublisherPage1";
import { topicStatusEnum } from "../../../models/ITopicConfig";
import { MQTTService } from "../../../services/MQTTService";
import { TopicService } from "../../../services/TopicService";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("ModalAddTopicPublisher component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalAddTopicPublisher />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <ModalAddTopicPublisher />
        </Provider>
      </MemoryRouter>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it("check save button is not disabled when topic is active", () => {
    const mStore = mockStore({
      editModal: { appData: { appClientId: "" } },
    });

    const setValues = {
      appClientId: "test",
      createBrige: "test",
      retainRequired: "YES",
      topic: {
        appClientId: "test",
        topicId: "test",
        topicName: "test",
        topicStatus: topicStatusEnum.ACTIVE,
      },
    };

    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={mStore}>
          <ModalAddTopicPublisher />
        </Provider>
      </MemoryRouter>
    );

    act(() => {
      wrapper.find(ModalAddTopicPublisherPage1).prop("onChange")(setValues);
      wrapper.setProps({});
    });

    expect(
      wrapper
        .update()
        .find(".ta-modal-next-button")
        .first()
        .prop("disabled")
    ).toEqual(false);
  });

  it("check save button is disabled when topic is onhold", () => {
    const mStore = mockStore({
      editModal: { appData: { appClientId: "" } },
    });

    const setValues = {
      appClientId: "test",
      createBrige: "test",
      retainRequired: "YES",
      topic: {
        appClientId: "test",
        topicId: "test",
        topicName: "test",
        topicStatus: topicStatusEnum.ONHOLD,
      },
    };

    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={mStore}>
          <ModalAddTopicPublisher />
        </Provider>
      </MemoryRouter>
    );

    act(() => {
      wrapper.find(ModalAddTopicPublisherPage1).prop("onChange")(setValues);
      wrapper.setProps({});
    });

    expect(
      wrapper
        .update()
        .find(".ta-modal-next-button")
        .first()
        .prop("disabled")
    ).toEqual(true);
  });

  it("check save button is disabled when topic is a draft and appclientIds are not matching", () => {
    const mStore = mockStore({
      editModal: { appData: { appClientId: "" } },
    });

    const setValues = {
      appClientId: "test",
      createBrige: "test",
      retainRequired: "YES",
      topic: {
        appClientId: "test",
        topicId: "test",
        topicName: "test",
        topicStatus: topicStatusEnum.DRAFT,
      },
    };

    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={mStore}>
          <ModalAddTopicPublisher />
        </Provider>
      </MemoryRouter>
    );

    act(() => {
      wrapper.find(ModalAddTopicPublisherPage1).prop("onChange")(setValues);
      wrapper.setProps({});
    });

    expect(
      wrapper
        .update()
        .find(".ta-modal-next-button")
        .first()
        .prop("disabled")
    ).toEqual(true);
  });

  describe("ModalAddTopicPublisher", () => {
    let wrapper: any;
    let pushSpy: any;
    let topicServiceStub: any;
    let mqttServiceStub: any;
    const mockHistory = createBrowserHistory();
    const mStore = mockStore({
      editModal: {
        appData: {},
      },
      payloadGet: {
        loading: false,
      },
      payloadUpload: {
        loading: false,
      },
    });
    const configPage1 = {
      retainRequired: "test",
      topic: {
        topicName: "test",
        topicStatus: topicStatusEnum.ACTIVE,
      },
    };
    const configPage2 = {
      description: "test",
      isSecure: true,
      offloadType: "test",
      payloadConfig: {},
      priority: "test",
      qosLevel: "test",
      retainRequired: "test",
      topic: {
        appClientId: "test",
        topicId: "",
        topicName: "test",
        topicStatus: topicStatusEnum.ACTIVE,
      },
      topicName: "test",
      ttl: "test",
    };
    const topicIdTest = "testTopicId";
    const payloadObj = {
      data: {
        data: {
          content: topicIdTest,
        },
      },
    };
    const topicObj = {
      data: {
        data: {},
      },
    };

    beforeEach(() => {
      pushSpy = Sinon.spy(mockHistory, "push");
      topicServiceStub = Sinon.stub(TopicService, "createTopic").callsFake(config =>
        Promise.resolve(payloadObj as any)
      );
      mqttServiceStub = Sinon.stub(MQTTService, "createPublisher").callsFake(config =>
        Promise.resolve(topicObj as any)
      );
      wrapper = mount(
        <Router history={mockHistory}>
          <Provider store={mStore}>
            <ModalAddTopicPublisher />
          </Provider>
        </Router>
      );
    });

    afterEach(() => {
      pushSpy.restore();
      topicServiceStub.restore();
      mqttServiceStub.restore();
    });
  });
});
