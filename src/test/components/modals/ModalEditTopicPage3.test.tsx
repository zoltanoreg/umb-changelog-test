import { mount, ReactWrapper, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { ProgressIndicatorCircular } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { FileInput } from "../../../components/FileInput";
import { DropdownInput } from "../../../components/modals/Dropdownlnput";
import { ModalEditTopicPage3 } from "../../../components/modals/ModalEditTopicPage3";
import { PayloadSchemaEditor } from "../../../components/modals/PayloadSchemaEditor";
import { payloadFormatEnum } from "../../../models/ITopicConfig";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import { TopicService } from "../../../services/TopicService";
import store from "../../../store";
import * as testUtils from "../../testUtils";

const mockStore = createMockStore([thunk]);

describe("ModalEditTopicPage3 component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalEditTopicPage3 />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const setValues = {
      config: {
        payloadData: {
          payloadFileNam: "test",
        },
      },
    };
    const wrapper = mount(
      <Provider store={store}>
        <ModalEditTopicPage3 {...setValues} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  describe("ModalEditTopicPage3", () => {
    it("appears loading animation", () => {
      const mStore = mockStore({
        editModal: {
          appData: {},
        },
        payloadGet: {
          loading: true,
        },
        payloadUpload: {
          loading: true,
        },
      });

      const fakeFc = jest.fn();
      const props = {
        config: {
          payloadData: {
            payloadSchema: "payloadSchemaTest",
          },
        },
      };

      const wrapper = mount(
        <Provider store={mStore}>
          <ModalEditTopicPage3 onChange={fakeFc} {...props} />
        </Provider>
      );
      expect(wrapper.find(ProgressIndicatorCircular).length).toEqual(2);
    });

    it("change dropdown values", () => {
      const props = {
        config: {
          isSecure: true,
          payloadData: {
            payloadFileName: "",
          },
        },
      };

      const mStore = mockStore({
        currentUser: {
          data: {
            role: ADMINISTRATOR,
            userId: "test",
          },
        },
        payloadGet: {
          loading: false,
        },
        payloadUpload: {
          loading: false,
        },
      });
      const fakeFc = jest.fn();
      const wrapper = mount(
        <Provider store={mStore}>
          <ModalEditTopicPage3 onChange={fakeFc} {...props} />
        </Provider>
      );

      const payloadFormatValue = "testPayloadFormat";
      act(() => {
        wrapper
          .find(DropdownInput)
          .at(0)
          .prop("onChange")(payloadFormatValue);
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({
        ...props.config,
        payloadData: { ...props.config.payloadData, payloadFormat: payloadFormatValue },
      });

      fakeFc.mockRestore();
    });

    describe("Chip fn", () => {
      const mStore = mockStore({
        payloadGet: {
          error: false,
          loading: false,
          payloadFile: "url",
          payloadFormat: "Protobuf",
          payloadSchema: 'syntax = "proto3";\n\npackage books;\n',
        },
        payloadUpload: {
          loading: false,
        },
        topicDetails: {
          loading: false,
        },
      });

      let wrapper: ReactWrapper;
      const payloadConfigObj = {
        payloadFile: File,
        payloadFileName: "payloadFileNameTest",
        payloadSchema: "",
      };
      const fakeFc = jest.fn();
      const props = {
        config: {
          payloadData: {
            payloadFileName: "payloadFileName1",
          },
        },
      };

      beforeAll(() => {
        wrapper = mount(
          <Provider store={mStore}>
            <ModalEditTopicPage3 {...props} onChange={fakeFc} />
          </Provider>
        );
      });

      it("should clear file name", () => {
        act(() => {
          wrapper
            .find(".ta-chip-payloadFile")
            .first()
            .simulate("click");
        });
        expect(fakeFc).toBeCalled();
        expect(fakeFc).toBeCalledWith({
          ...props.config,
          payloadData: { ...payloadConfigObj, payloadFileName: "" },
          topicPayloadId: null,
        });
      });
    });

    describe("Open editor", () => {
      const onedit = jest.fn();
      const mStore = mockStore({
        payloadGet: {
          createdAt: "2020-04-06T12:14:39.000Z",
          error: false,
          loading: false,
          modifiedAt: "2020-04-06T12:14:51.000Z",
          payloadFile: "url",
          payloadFormat: "Protobuf",
          payloadMasterId: "id",
          payloadSchema: 'syntax = "proto3";\n\npackage books;\n',
          payloadType: null,
        },
        payloadUpload: {
          loading: false,
        },
        topicDetails: {
          loading: false,
        },
      });
      let wrapper: ReactWrapper;
      const fakeFc = jest.fn();
      const props = {
        config: {
          payloadData: {
            createdAt: "2020-04-06T12:14:39.000Z",
            error: false,
            loading: false,
            modifiedAt: "2020-04-06T12:14:51.000Z",
            payloadFile: "url",
            payloadFileName: "payloadFileName1",
            payloadFormat: "Protobuf",
            payloadMasterId: "id",
            payloadSchema: 'syntax = "proto3";\n\npackage books;\n',
            payloadType: null,
          },
        },
        isEditing: false,
        onToggleEditing: onedit,
      };

      beforeAll(() => {
        wrapper = mount(
          <Provider store={mStore}>
            <ModalEditTopicPage3 {...props} onChange={fakeFc} />
          </Provider>
        );
      });

      it("should have edit schema button", () => {
        expect(wrapper.find(".ta-editTopic-button-editSchema").length).toBeGreaterThan(0);
      });

      it("should have edit schema button", () => {
        const btn = wrapper.find(".ta-editTopic-button-editSchema").first();

        act(() => {
          btn.simulate("click");
        });

        expect(onedit).toBeCalled();
      });
    });

    describe("Cancel button click", () => {
      const cancel = jest.fn();
      const mStore = mockStore({
        payloadGet: {
          error: false,
          loading: false,
          payloadFile: "url",
          payloadFormat: "Protobuf",
          payloadMasterId: "id",
          payloadSchema: 'syntax = "proto3";\n\npackage books;\n',
        },
        payloadUpload: {
          loading: false,
        },
        topicDetails: {
          content: {
            topicPayloadId: "",
          },
          loading: false,
        },
      });
      let wrapper: ReactWrapper;
      const fakeFc = jest.fn();
      const props = {
        config: {
          payloadData: {
            error: false,
            loading: false,
            payloadFile: "url",
            payloadFileName: "payloadFileName1",
            payloadFormat: "JSON",
            payloadSchema: '{"id": "1"}',
          },
          topicPayloadId: "",
        },
        isEditing: true,
        onToggleEditing: cancel,
      };

      beforeAll(() => {
        wrapper = mount(
          <Provider store={mStore}>
            <ModalEditTopicPage3 {...props} onChange={fakeFc} />
          </Provider>
        );
      });

      it("should render schema editor on click", () => {
        expect(wrapper.find(PayloadSchemaEditor).length).toBeGreaterThan(0);
      });

      it("should call cancel on click", () => {
        const btn = wrapper.find(".ta-payloadSchemaEditor-button-cancel").first();

        act(() => {
          btn.simulate("click");
        });

        expect(cancel).toBeCalled();
      });

      it("should clear file name", () => {
        act(() => {
          wrapper
            .find(".ta-chip-payloadFile")
            .first()
            .simulate("click");
        });
        expect(fakeFc).not.toBeCalled();
      });
    });

    describe("File upload while not editing", () => {
      let wrapper: ReactWrapper;
      const fakeFc = jest.fn();
      const cancel = jest.fn();
      const mStore = mockStore({
        payloadGet: {
          error: false,
          loading: false,
          payloadFile: "",
          payloadFormat: "JSON",
          payloadSchema: "",
        },
        payloadUpload: {
          loading: false,
        },
        topicDetails: {
          content: {
            topicPayloadId: "",
          },
          loading: false,
        },
      });
      const props = {
        config: {
          payloadData: {
            createdAt: "",
            error: false,
            loading: false,
            modifiedAt: "",
            payloadFile: "",
            payloadFileName: "",
            payloadFormat: "JSON",
            payloadMasterId: "",
            payloadSchema: "",
            payloadType: null,
          },
        },
        isEditing: false,
        onToggleEditing: cancel,
      };

      let serviceMock: any;

      beforeAll(() => {
        wrapper = mount(
          <Provider store={mStore}>
            <ModalEditTopicPage3 {...props} onChange={fakeFc} />
          </Provider>
        );
        serviceMock = jest.spyOn(TopicService, "uploadPayload");
      });

      afterAll(() => {
        serviceMock.mockRestore();
      });

      it("file upload", () => {
        const fileInput = wrapper.find(FileInput).first();

        const fileContents = '{"id": 2}';
        const file = new Blob([fileContents], { type: "text/plain" });

        const onChangeProps = { target: { files: [file] } };
        act(() => {
          fileInput.prop("onChange")(onChangeProps);
          wrapper.update();
        });

        expect(serviceMock).toBeCalled();
      });
    });

    describe("Without file there shouldn't be an editor rendered", () => {
      const cancel = jest.fn();
      const mStore = mockStore({
        payloadGet: {
          error: false,
          loading: false,
          payloadFile: "",
          payloadFormat: "Protobuf",
          payloadSchema: 'syntax = "proto3";\n\npackage books;\n',
        },
        payloadUpload: {
          loading: false,
        },
        topicDetails: {
          content: {
            topicPayloadId: "",
          },
          loading: false,
        },
      });
      let wrapper: ReactWrapper;
      const fakeFc = jest.fn();
      const props = {
        config: {
          payloadData: {
            createdAt: "2020-04-06T12:14:39.000Z",
            error: false,
            loading: false,
            modifiedAt: "2020-04-06T12:14:51.000Z",
            payloadFile: "",
            payloadFileName: "asd",
            payloadFormat: "JSON",
            payloadMasterId: "id",
            payloadSchema: '{"id": "1"}',
            payloadType: null,
          },
        },
        isEditing: false,
        onToggleEditing: cancel,
      };

      beforeAll(() => {
        wrapper = mount(
          <Provider store={mStore}>
            <ModalEditTopicPage3 {...props} onChange={fakeFc} />
          </Provider>
        );
      });

      it("should render schema editor on click", () => {
        expect(wrapper.find(PayloadSchemaEditor).length).toEqual(0);
      });
    });
  });

  describe("File upload JSON", () => {
    let wrapper: ReactWrapper;
    const fakeOnchangeFc = jest.fn();
    let jestSpy: any;
    const cancel = jest.fn();
    let serviceMock: any;
    const mStore = mockStore({
      payloadGet: {
        error: false,
        loading: false,
        payloadFile: "",
        payloadFormat: "JSON",
        payloadSchema: "",
      },
      payloadUpload: {
        loading: false,
      },
      topicDetails: {
        content: {
          topicPayloadId: "",
        },
        loading: false,
      },
    });
    const props = {
      config: {
        payloadData: {
          createdAt: "",
          error: false,
          loading: false,
          modifiedAt: "",
          payloadFile: "",
          payloadFileName: "",
          payloadFormat: "JSON",
          payloadMasterId: "",
          payloadSchema: "",
          payloadType: null,
        },
      },
      isEditing: false,
      onToggleEditing: cancel,
    };

    beforeAll(() => {
      wrapper = mount(
        <Provider store={mStore}>
          <ModalEditTopicPage3 {...props} onChange={fakeOnchangeFc} />
        </Provider>
      );

      serviceMock = Sinon.stub(TopicService, "uploadPayload").callsFake((config: any) =>
        Promise.resolve({
          data: {
            data: {
              content: {
                payloadId: "2",
                payloadSchema: '{"id": 2}',
              },
            },
          },
        })
      );

      jestSpy = jest.spyOn(TopicService, "uploadPayload");
    });

    afterAll(() => {
      serviceMock.restore();
      jestSpy.restore();
    });

    it("file upload", () => {
      const fileInput = wrapper.find(FileInput).first();

      const fileContents = '{"id": 2}';
      const file = new Blob([fileContents], { type: "text/plain" });

      const onChangeProps = { target: { files: [file] } };
      act(() => {
        fileInput.prop("onChange")(onChangeProps);
        wrapper.update();
      });

      expect(jestSpy).toBeCalled();
    });
  });
  describe("File upload Proto", () => {
    let wrapper: ReactWrapper;
    const fakeOnchangeFc = jest.fn();
    let jestSpy: any;
    const cancel = jest.fn();
    let serviceMock: any;
    const mStore = mockStore({
      payloadGet: {
        error: false,
        loading: false,
        payloadFile: "",
        payloadFormat: "Protobuf",
        payloadSchema: "",
      },
      payloadUpload: {
        loading: false,
      },
      topicDetails: {
        content: {
          topicPayloadId: "",
        },
        loading: false,
      },
    });
    const props = {
      config: {
        payloadData: {
          createdAt: "",
          error: false,
          loading: false,
          modifiedAt: "",
          payloadFile: "",
          payloadFileName: "",
          payloadFormat: "Protobuf",
          payloadMasterId: "",
          payloadSchema: "",
          payloadType: null,
        },
      },
      isEditing: false,
      onToggleEditing: cancel,
    };

    beforeAll(() => {
      wrapper = mount(
        <Provider store={mStore}>
          <ModalEditTopicPage3 {...props} onChange={fakeOnchangeFc} />
        </Provider>
      );

      serviceMock = Sinon.stub(TopicService, "uploadPayload").callsFake((config: any) =>
        Promise.resolve({
          data: {
            data: {
              content: {
                payloadId: "2",
                payloadSchema: "package a;",
              },
            },
          },
        })
      );

      jestSpy = jest.spyOn(TopicService, "uploadPayload");
    });

    afterAll(() => {
      serviceMock.restore();
      jestSpy.restore();
    });

    it("file upload", () => {
      const fileInput = wrapper.find(FileInput).first();

      const fileContents = "package a;";
      const file = new Blob([fileContents], { type: "text/plain" });

      const onChangeProps = { target: { files: [file] } };
      act(() => {
        fileInput.prop("onChange")(onChangeProps);
        wrapper.update();
      });

      expect(jestSpy).toBeCalled();
    });
  });

  describe("File upload JSON", () => {
    let wrapper: ReactWrapper;
    const fakeOnchangeFc = jest.fn();
    let serviceSpy: any;
    const cancel = jest.fn();
    const mStore = mockStore({
      payloadGet: {
        error: false,
        loading: false,
        payloadFile: "",
        payloadFormat: "JSON",
        payloadSchema: "",
      },
      payloadUpload: {
        loading: false,
      },
      topicDetails: {
        content: {
          topicPayloadId: "18723898712",
        },
        loading: false,
      },
    });
    const props = {
      config: {
        payloadData: {
          createdAt: "",
          error: false,
          loading: false,
          modifiedAt: "",
          payloadFile: "",
          payloadFileName: "",
          payloadFormat: "JSON",
          payloadMasterId: "",
          payloadSchema: "",
          payloadType: null,
        },
      },
      isEditing: false,
      onToggleEditing: cancel,
    };

    beforeAll(() => {
      wrapper = mount(
        <Provider store={mStore}>
          <ModalEditTopicPage3 {...props} onChange={fakeOnchangeFc} />
        </Provider>
      );

      serviceSpy = jest.spyOn(TopicService, "uploadPayload");
    });

    afterAll(() => {
      serviceSpy.restore();
    });

    it("file upload", () => {
      const fileInput = wrapper.find(FileInput).first();

      const fileContents = '{"id": 2}';
      const file = new Blob([fileContents], { type: "text/plain" });

      const onChangeProps = { target: { files: [file] } };
      act(() => {
        fileInput.prop("onChange")(onChangeProps);
        wrapper.update();
      });

      expect(serviceSpy).toBeCalled();
    });
  });
});
