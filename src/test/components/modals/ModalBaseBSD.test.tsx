import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import * as React from "react";
import { Provider } from "react-redux";

import { ModalBaseBSD } from "../../../components/modals/ModalBaseBSD";
import store from "../../../store";

describe("ModalBaseBSD component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalBaseBSD />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <Provider store={store}>
        <ModalBaseBSD />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
