import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { CreateApplicationTypes } from "../../../actions/application/create/types";
import { ModalAddTopicSubscriber } from "../../../components/modals/ModalAddTopicSubscriber";
import { ModalAddTopicSubscriberPage1 } from "../../../components/modals/ModalAddTopicSubscriberPage1";
import { MQTTService } from "../../../services/MQTTService";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("ModalAddTopicSubscriber component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalAddTopicSubscriber />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const mStore = mockStore({
      applicationDetails: { content: { appBrokerType: { brokerType: "test" } } },
      editModal: { appData: "test" },
      topicMQTTClients: { content: {} },
    });
    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriber />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it("creates subscriber", async () => {
    const mqttServiceStub = Sinon.stub(MQTTService, "createSubscriber").callsFake((config: any) =>
      Promise.resolve({ data: {} } as any)
    );

    const mStore = mockStore({
      applicationDetails: {
        content: {
          appBrokerType: {
            brokerType: "test",
          },
          appClientId: "initialAppClientId",
        },
      },
      editModal: {
        appData: "test",
        topicFilter: "testTopicFilter",
      },
      topic: {
        topicName: "topicNameTest",
        topicStatus: "status",
      },
      topicMQTTClients: { content: {} },
    });

    const wrapper = mount(
      <Provider store={mStore}>
        <ModalAddTopicSubscriber />
      </Provider>
    );
    /* console.info(wrapper.debug()); */

    const updatedConfig = {
      appClientId: "testAppClient",
      bridge: true,
      bridgeDisabled: true,
      retainRequired: "YES",
      topic: {
        topicId: undefined,
        topicName: "testTopicName",
        topicStatus: "ACTIVE",
      },
    };

    act(() => {
      wrapper.find(ModalAddTopicSubscriberPage1).prop("onChange")(updatedConfig);
      wrapper.setProps({}); // Re-render in order to property change (disable) come into live
    });

    // tslint:disable-next-line: await-promise
    await wrapper
      .find(".ta-modal-save-button")
      .first()
      .simulate("click");

    const expectedObj = { payload: {}, type: CreateApplicationTypes.SUCCESS };
    expect(mStore.getActions()).toContainEqual(expectedObj);

    mqttServiceStub.restore();
  });
});
