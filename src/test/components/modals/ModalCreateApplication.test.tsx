import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
// tslint:disable-next-line: no-implicit-dependencies
import { createBrowserHistory } from "history";
import { ProgressIndicatorCircular } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter, Router } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon from "sinon";

import { BrokerTypeDropdown } from "../../../components/BrokerTypeDropdown";
import { DropdownInput } from "../../../components/modals/Dropdownlnput";
import { ModalCreateApplication } from "../../../components/modals/ModalCreateApplication";
import { authTypeEnum } from "../../../models/IAppDetailsConfig";
import { modalTypeEnum } from "../../../models/IEditModalConfig";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import { ApplicationService } from "../../../services/ApplicationService";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("ModalCreateApplication component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalCreateApplication />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <ModalCreateApplication />
        </Provider>
      </MemoryRouter>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it("progressIndicator appears while loading", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      applicationCreation: {
        loading: true,
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
      editModal: {
        type: modalTypeEnum.CREATE_TOPIC_PUBLISH,
      },
    });

    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={mStore}>
          <ModalCreateApplication />
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper.find(ProgressIndicatorCircular).length).toEqual(1);
  });

  describe("creates a new applicationClient", () => {
    const mockHistory = createBrowserHistory();
    let applicationServiceStub: any;
    let pushSpy: any;
    const appClientIdTest = "testAppClientId";

    beforeAll(() => {
      pushSpy = Sinon.spy(mockHistory, "push");
      applicationServiceStub = Sinon.stub(ApplicationService, "createApplication").callsFake(
        (config: any) => Promise.resolve({ data: { appClientId: appClientIdTest } } as any)
      );
    });

    afterAll(() => {
      pushSpy.restore();
      applicationServiceStub.restore();
    });

    it("with credentials", async () => {
      const mStore = mockStore({
        allBrokerType: {
          content: [{}],
        },
        applicationCreation: {
          loading: false,
        },
        currentUser: {
          data: {
            role: ADMINISTRATOR,
            userId: "test",
          },
        },
        editModal: {
          type: modalTypeEnum.CREATE_TOPIC_PUBLISH,
        },
      });

      const wrapper = mount(
        <Router history={mockHistory}>
          <Provider store={mStore}>
            <ModalCreateApplication />
          </Provider>
        </Router>
      );

      act(() => {
        const onChangeFunc = wrapper
          .find('[name="app-name"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: " " } } as any);
          onChangeFunc({ target: { value: "testAppName" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      act(() => {
        const onChangeFunc = wrapper
          .find('[name="app-version"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testAppVersion" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      act(() => {
        wrapper
          .find(BrokerTypeDropdown)
          .first()
          .prop("onChange")("brokerTypeTest");
        wrapper.setProps({}); // Re-render
      });

      act(() => {
        const onChangeFunc = wrapper
          .find('[name="system-name"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testSystemName" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      act(() => {
        wrapper
          .find(DropdownInput)
          .first()
          .prop("onChange")(authTypeEnum.CREDENTIALS);
        wrapper.setProps({}); // Re-render
      });

      act(() => {
        const onChangeFunc = wrapper
          .find(".ta-modal-auth-container-cred")
          .find('[name="username"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testUserName" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      act(() => {
        const onChangeFunc = wrapper
          .find(".ta-modal-auth-container-cred")
          .find('[name="password"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testPassword" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      // tslint:disable-next-line: await-promise
      await wrapper
        .find(".ta-modal-submit-button")
        .first()
        .simulate("click");

      expect(pushSpy.args[0]).toEqual([`/applicationClients/${appClientIdTest}`]);
    });

    it("with certificate", async () => {
      const mStore = mockStore({
        allBrokerType: {
          content: [{}],
        },
        applicationCreation: {
          loading: false,
        },
        currentUser: {
          data: {
            role: ADMINISTRATOR,
            userId: "test",
          },
        },
        editModal: {
          type: modalTypeEnum.CREATE_TOPIC_PUBLISH,
        },
      });

      const wrapper = mount(
        <Router history={mockHistory}>
          <Provider store={mStore}>
            <ModalCreateApplication />
          </Provider>
        </Router>
      );

      act(() => {
        const onChangeFunc = wrapper
          .find('[name="app-name"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testAppName" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      act(() => {
        const onChangeFunc = wrapper
          .find('[name="app-version"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testAppVersion" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      act(() => {
        wrapper
          .find(BrokerTypeDropdown)
          .first()
          .prop("onChange")("brokerTypeTest");
        wrapper.setProps({}); // Re-render
      });

      act(() => {
        const onChangeFunc = wrapper
          .find('[name="system-name"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testSystemName" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      act(() => {
        wrapper
          .find(DropdownInput)
          .first()
          .prop("onChange")(authTypeEnum.CERTIFICATE);
        wrapper.setProps({}); // Re-render
      });

      act(() => {
        const onChangeFunc = wrapper
          .find(".ta-modal-auth-container-cert")
          .find('[name="path-to-ca"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testPathToCa" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      act(() => {
        const onChangeFunc = wrapper
          .find(".ta-modal-auth-container-cert")
          .find('[name="path-client"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testPathClient" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });

      // tslint:disable-next-line: await-promise
      await wrapper
        .find(".ta-modal-submit-button")
        .first()
        .simulate("click");

      expect(pushSpy.args[0]).toEqual([`/applicationClients/${appClientIdTest}`]);
    });
  });
});
