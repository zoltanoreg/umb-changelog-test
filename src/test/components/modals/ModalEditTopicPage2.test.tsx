import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { Checkbox, Dropdown, NumberInput } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { DropdownInput } from "../../../components/modals/Dropdownlnput";
import { ModalEditTopicPage2 } from "../../../components/modals/ModalEditTopicPage2";
import { topicStatusEnum } from "../../../models/ITopicConfig";
import { ADMINISTRATOR, APPLICATION_OWNER } from "../../../models/UserTypes";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("ModalEditTopicPage2 component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalEditTopicPage2 />
      </Provider>
    );
  });

  const setValues = {
    config: {
      isSecure: "test",
    },
  };

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <Provider store={store}>
        <ModalEditTopicPage2 {...setValues} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  describe("ModalEditTopicPage2", () => {
    let fakeFc: any;
    let wrapper: any;

    const props = {
      config: {
        isSecure: true,
      },
    };

    const mStore = mockStore({
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      fakeFc = jest.fn();
      wrapper = mount(
        <Provider store={mStore}>
          <ModalEditTopicPage2 {...props} onChange={fakeFc} />
        </Provider>
      );
    });

    afterEach(() => {
      fakeFc.mockRestore();
    });

    it("change dropdown values", () => {
      const priorityValue = "testPriority";
      act(() => {
        wrapper
          .find(DropdownInput)
          .at(0)
          .prop("onChange")(priorityValue);
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({ ...props.config, priority: priorityValue });

      const offload = "offloadValue";
      act(() => {
        wrapper
          .find(DropdownInput)
          .at(1)
          .prop("onChange")(offload);
      });
      expect(fakeFc).toBeCalledWith({ ...props.config, offloadType: offload });

      const qos = "qosValue";
      act(() => {
        wrapper
          .find(DropdownInput)
          .at(2)
          .prop("onChange")(qos);
      });
      expect(fakeFc).toBeCalledWith({ ...props.config, qosLevel: qos });
    });

    it("change NumberInput value", () => {
      const numberValue = 1;
      act(() => {
        wrapper.find(NumberInput).prop("onChange")(numberValue);
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({ ...props.config, ttl: numberValue });

      act(() => {
        wrapper.find(NumberInput).prop("onChange")(-1);
      });
      expect(fakeFc).toBeCalledWith({ ...props.config, ttl: 1 });

      act(() => {
        wrapper.find(NumberInput).prop("onChange")("NotNumberValue");
      });
      expect(fakeFc).toBeCalledWith({ ...props.config, ttl: 0 });
    });

    describe("textarea", () => {
      it("should not update textarea’s content on whitespace", () => {
        act(() => {
          wrapper
            .find(".ta-modal-input-description")
            .first()
            .prop("onChange")({ target: { value: " " } });
        });
        expect(fakeFc).not.toBeCalled();
      });

      it("should not change textarea’s content on too long string", () => {
        act(() => {
          wrapper
            .find(".ta-modal-input-description")
            .first()
            .prop("onChange")({
            target: {
              value:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            },
          });
        });
        expect(fakeFc).not.toBeCalled();
      });

      it("should change textarea’s content on adding correct string", () => {
        const textValue = "test";
        act(() => {
          wrapper
            .find(".ta-modal-input-description")
            .first()
            .prop("onChange")({ target: { value: textValue } });
        });
        expect(fakeFc).toBeCalledWith({ ...props.config, description: textValue });
      });
    });

    it("change checkbox value", () => {
      act(() => {
        wrapper
          .find(Checkbox)
          .first()
          .prop("onClick")();
      });

      expect(fakeFc).toBeCalledWith({ ...props.config, isSecure: false });
    });
  });

  describe("ModalEditTopicPage2", () => {
    let wrapper: any;
    const fakeFc = jest.fn();

    const mStore = mockStore({
      currentUser: {
        data: {
          role: APPLICATION_OWNER,
          userId: "test",
        },
      },
      editModal: {
        topicData: {
          status: topicStatusEnum.ACTIVE,
        },
      },
    });

    const props = {
      config: {
        isSecure: true,
      },
    };

    beforeEach(() => {
      wrapper = mount(
        <Provider store={mStore}>
          <ModalEditTopicPage2 {...props} onChange={fakeFc} />
        </Provider>
      );
    });

    it("should have disabled fields in page2", () => {
      expect(
        wrapper
          .find(Dropdown)
          .first()
          .prop("disabled")
      ).toEqual(true);
      expect(
        wrapper
          .find(".ta-modal-input-description")
          .first()
          .prop("disabled")
      ).toEqual(true);
      expect(
        wrapper
          .find(NumberInput)
          .first()
          .prop("disabled")
      ).toEqual(true);
    });
  });
});
