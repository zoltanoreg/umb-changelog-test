import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { Dropdown } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { DropdownInput } from "../../../components/modals/Dropdownlnput";
import { ModalEditTopicPage1 } from "../../../components/modals/ModalEditTopicPage1";
import { TypeaheadApplication } from "../../../components/TypeaheadApplication";
import { retainRequiredEnum, topicStatusEnum } from "../../../models/ITopicConfig";
import { ADMINISTRATOR, APPLICATION_OWNER } from "../../../models/UserTypes";
import store from "../../../store";

const mockStore = createMockStore([thunk]);

describe("ModalEditTopicPage1 component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <ModalEditTopicPage1 />
      </Provider>
    );
  });

  const props = {
    config: {
      appData: {
        appBrokerType: "test",
        appName: "test",
        appVersion: "test",
      },
    },
  };

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <Provider store={store}>
        <ModalEditTopicPage1 {...props} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  describe("ModalEditTopicPage1", () => {
    let fakeFc: any;
    let wrapper: any;

    const mStore = mockStore({
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      fakeFc = jest.fn();
      wrapper = mount(
        <Provider store={mStore}>
          <ModalEditTopicPage1 {...props} onChange={fakeFc} />
        </Provider>
      );
    });

    afterEach(() => {
      fakeFc.mockRestore();
    });

    it("changes textField value", () => {
      act(() => {
        const onChangeFunc = wrapper
          .find('[name="topic-name"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: "testTopicName" } } as any);
          wrapper.setProps({}); // Re-render
        }
      });
      expect(fakeFc).toHaveBeenCalled();
    });

    it("changes textField value entering a space", () => {
      act(() => {
        const onChangeFunc = wrapper
          .find('[name="topic-name"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: " " } } as any);
          wrapper.setProps({}); // Re-render
        }
      });
      expect(fakeFc).not.toHaveBeenCalled();
    });

    it("changes textField value entering a long input", () => {
      const longInput =
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.";
      act(() => {
        const onChangeFunc = wrapper
          .find('[name="topic-name"]')
          .first()
          .prop("onChange");
        if (onChangeFunc) {
          onChangeFunc({ target: { value: longInput } } as any);
          wrapper.setProps({}); // Re-render
        }
      });
      expect(fakeFc).not.toHaveBeenCalled();
    });

    it("changes typeahead value", () => {
      const appDataObj = {
        appName: "test",
      };
      act(() => {
        wrapper
          .find(TypeaheadApplication)
          .first()
          .prop("onChange")(appDataObj);
        wrapper.update();
      });
      expect(fakeFc).toBeCalled();
      expect(fakeFc).toBeCalledWith({ ...props.config, appData: appDataObj });
    });
  });

  describe("ModalEditTopicPage1", () => {
    let wrapper: any;
    const fakeFc = jest.fn();

    const mStore = mockStore({
      currentUser: {
        data: {
          role: APPLICATION_OWNER,
          userId: "test",
        },
      },
      editModal: {
        topicData: {
          status: topicStatusEnum.ACTIVE,
        },
      },
    });

    beforeEach(() => {
      wrapper = mount(
        <Provider store={mStore}>
          <ModalEditTopicPage1 {...props} onChange={fakeFc} />
        </Provider>
      );
    }); 
    
    
    it("should have disabled fields in page1", () => {
      expect(wrapper.find('.ta-editTopic-typeahead-application-chip').first().prop('disabled')).toEqual(true);
      expect(wrapper.find('.ta-modal-input-topic-name').first().prop('disabled')).toEqual(true);
    });
    
  });
});
