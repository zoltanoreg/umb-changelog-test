import { mount, ReactWrapper, shallow } from "enzyme";
// tslint:disable-next-line: no-implicit-dependencies
import { createBrowserHistory } from "history";
import { Dropdown } from "next-components";
import React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter, Router } from "react-router-dom";
import renderer from "react-test-renderer";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon, { SinonStubbedInstance } from "sinon";

import { AppTopicsList } from "../../components/AppTopicsList";
import { getTAClass, TA_TYPES } from "../../helper/taHelper";
import { ADMINISTRATOR, APPLICATION_OWNER } from "../../models/UserTypes";
import { BrokerTypeService } from "../../services/BrokerTypeService";
import store from "../../store";

describe("AppTopicsList component test", () => {
  it("renders without crashing", () => {
    const fakeFc = jest.fn();
    shallow(
      <Provider store={store}>
        <AppTopicsList btnClick={fakeFc} type="PUBLISHER" btnDisabled={true} />
      </Provider>
    );
  });
  it("check if matches snapshot", () => {
    const fakeFc = jest.fn();
    let wrapper: ReactWrapper;
    wrapper = renderer.create(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <AppTopicsList btnClick={fakeFc} type="PUBLISHER" btnDisabled={true} />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});

describe("#AppTopicsList", () => {
  let wrapper: ReactWrapper;

  let brokerTypeServiceStub: SinonStubbedInstance<any>;
  const brokerTypesObj = {
    data: {
      data: {
        content: [{}],
        count: 1,
      },
    },
  };

  beforeAll(() => {
    brokerTypeServiceStub = Sinon.stub(BrokerTypeService, "getBrokerTypeList").callsFake(
      (config: any) => Promise.resolve(brokerTypesObj as any)
    );
  });

  afterAll(() => {
    brokerTypeServiceStub.restore();
  });

  const mockStore = createMockStore([thunk]);
  const appClientIdIntentional = "";
  const st = mockStore(() => ({
    ...store.getState(),
    applicationDetails: {
      ...store.getState().applicationDetails,
      content: {
        ...store.getState().applicationDetails.content,
        appClientId: appClientIdIntentional,
      },
    },
  }));

  /* const timer = Sinon.useFakeTimers(); */
  beforeEach(() => {
    st.dispatch = jest.fn();
    wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={st}>
          <AppTopicsList btnClick={jest.fn()} type="PUBLISHER" btnDisabled={true} />
        </Provider>
      </MemoryRouter>
    );
  });

  describe("AppTopicsList [ordering and filtering] ", () => {
    const appClientIdTest = "appClientId";

    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      applicationDetails: {
        content: {
          appClientId: appClientIdTest,
        },
      },
      applicationTopicList: {
        content: [],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
    });

    beforeEach(() => {
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <AppTopicsList btnClick={jest.fn()} type="PUBLISHER" btnDisabled={true} />
          </Provider>
        </MemoryRouter>
      );
    });

    it("should change sorting direction", () => {
      let header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect((header2.props() as any).sortDirection).toEqual("none");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect((header2.props() as any).sortDirection).toEqual("ascending");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect((header2.props() as any).sortDirection).toEqual("descending");
    });

    it("should call filter if filtering against string-based column", () => {
      const enteredInput = "randomly added filter string";
      const columns = [
        "ta-applicationDetails-filter-topicName",
        "ta-applicationDetails-filter-topicOwner",
        "ta-applicationDetails-filter-topicOwnerVersion",
      ];
      for (const column of columns) {
        let input = wrapper.find(`.${column}`).first();
        const handler = input.prop("onChange");
        if (handler) {
          act(() => {
            handler({ target: { value: enteredInput } } as any);
          });
        }
        wrapper.update();
        input = wrapper.find(`.${column}`).first();
        expect(input.prop("value")).toEqual(enteredInput);
      }
    });

    it("should not call filter if filtering with empty string", () => {
      const enteredInput = " ";
      const expectedValue = "";
      let input = wrapper.find(".ta-applicationDetails-filter-topicName").first();

      const handler = input.prop("onChange");
      if (handler) {
        act(() => {
          handler({ target: { value: enteredInput } } as any);
        });
      }
      wrapper.update();
      input = wrapper.find(".ta-applicationDetails-filter-topicName").first();
      expect(input.prop("value")).toEqual(expectedValue);
    });

    it("should call filter if filtering against date-based column", () => {
      const enteredInput = { startDate: null, endDate: null };
      let input = wrapper.find(".ta-applicationDetails-filter-modifiedDate").first();
      act(() => {
        input.prop("onDatesChange")(enteredInput);
      });

      wrapper.update();
      input = wrapper.find(".ta-applicationDetails-filter-modifiedDate").first();
      expect(input.prop("startDate")).toEqual(enteredInput.startDate);
      expect(input.prop("endDate")).toEqual(enteredInput.endDate);
    });

    it("should call filter if filtering against dropdown column", () => {
      const enteredInput = "brokerType";
      const className = ".ta-applicationDetails-filter-brokerType";
      let input = wrapper
        .find(className)
        .find(Dropdown)
        .first();
      act(() => {
        input.prop("onChange")(enteredInput);
      });

      wrapper.update();
      input = wrapper
        .find(className)
        .find(Dropdown)
        .first();
      expect(input.prop("selected")).toEqual(enteredInput);
    });

    it("should clear all filter values if any had been already entered", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const classNameOfInput = "ta-applicationDetails-filter-topicName";
      const classNameOfDeleteIcon = "ta-applicationDetails-button-clearFilter";

      const enteredInput = "randomly added filter string";
      let input = getElementByClassName(classNameOfInput);
      act(() => {
        const handler = input.prop("onChange");
        if (handler) {
          handler({ target: { value: enteredInput } } as any);
        }
      });
      wrapper.update();
      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual(enteredInput);

      wrapper.setProps({}); // Re-render in order to property change (disable) come into live

      const icon = getElementByClassName(classNameOfDeleteIcon);
      expect(icon.prop("disabled")).toBe(false);
      icon.simulate("click");

      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual("");
    });
  });

  describe("AppTopicsList [navigation]", () => {
    const selectedTopicId = "testTopicId";
    const appClientIdTest = "appClientId";
    const appNameTest = "testAppName";
    const userIdTest = "testUserId";
    const brokertype = "testBrokerType";

    const mockHistory = createBrowserHistory();
    let pushSpy: any;

    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      applicationDetails: {
        content: {
          appClientId: appClientIdTest,
          appOwner: {
            userId: userIdTest,
          },
        },
      },
      applicationTopicList: {
        content: [
          {
            appClient: {
              appOwner: {
                userId: "userId",
              },
            },
            brokerTopology: {
              parentBrokerType: {
                brokerType: brokertype,
              },
              remoteBrokerType: {
                brokerType: brokertype,
              },
            },
            topic: {
              appClient: {
                appBrokerType: {
                  brokerType: brokertype,
                },
                appClientId: appClientIdTest,
                appName: appNameTest,
              },
              topicId: selectedTopicId,
            },
          },
        ],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: userIdTest,
        },
      },
    });

    beforeEach(() => {
      pushSpy = Sinon.spy(mockHistory, "push");

      wrapper = mount(
        <Router history={mockHistory}>
          <Provider store={mStore}>
            <AppTopicsList btnClick={jest.fn()} type="PUBLISHER" btnDisabled={true} />
          </Provider>
        </Router>
      );
    });

    afterEach(() => {
      pushSpy.restore();
    });

    it("should call navigate", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const prefix = "ta-application-details-column-";
      getElementByClassName(`${prefix}topicName`).simulate("click");
      expect(pushSpy.called).toEqual(true);
      expect(pushSpy.args[0]).toEqual([`/topics/${selectedTopicId}`]);

      getElementByClassName(`${prefix}topicOwnerAppClientName`).simulate("click");
      expect(pushSpy.args[1]).toEqual([`/applicationClients/${appClientIdTest}`]);
    });
  });

  describe("AppTopicsList [add new PUBLISHER by pressing 'NEW' button]", () => {
    const appClientIdTest = "appClientId";
    const userIdTest = "testUserId";
    const brokertype = "testBrokerType";

    const mockFn = jest.fn();

    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      applicationDetails: {
        content: {
          appClientId: appClientIdTest,
          appOwner: {
            userId: userIdTest,
          },
        },
      },
      applicationTopicList: {
        content: [
          {
            appClient: {
              appOwner: {
                userId: "userId",
              },
            },
            brokerTopology: {
              parentBrokerType: {
                brokerType: brokertype,
              },
              remoteBrokerType: {
                brokerType: brokertype,
              },
            },
            topic: {
              appClient: {
                appBrokerType: {
                  brokerType: brokertype,
                },
                appClientId: appClientIdTest,
                appName: "testAppName",
              },
              topicId: "testTopicId",
            },
          },
        ],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: userIdTest,
        },
      },
    });

    it("should open 'add publisher' dialog", () => {
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <AppTopicsList btnClick={mockFn} type="PUBLISHER" btnDisabled={false} />
          </Provider>
        </MemoryRouter>
      );

      const className = getTAClass("applicationDetails", TA_TYPES.BUTTON, "addPublisher");
      const buttonOfAddNewPublisher = wrapper.find(`.${className}`).first();
      buttonOfAddNewPublisher.simulate("click");
      expect(mockFn).toBeCalled();
    });

    it("should open 'add subscriber' dialog", () => {
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <AppTopicsList btnClick={mockFn} type="SUBSCRIBER" btnDisabled={false} />
          </Provider>
        </MemoryRouter>
      );

      const className = getTAClass("applicationDetails", TA_TYPES.BUTTON, "addSubscriber");
      const buttonOfAddNewPublisher = wrapper.find(`.${className}`).first();
      buttonOfAddNewPublisher.simulate("click");
      expect(mockFn).toBeCalled();
    });
  });
});
