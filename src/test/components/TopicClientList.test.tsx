import { mount, shallow } from "enzyme";
// tslint:disable-next-line: no-implicit-dependencies
import { createBrowserHistory } from "history";
import { ButtonPrimary, Checkbox, Dropdown, Link as NextLink } from "next-components";
import * as React from "react";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import { MemoryRouter, Router } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import Sinon, { SinonStubbedInstance } from "sinon";

import { BatchActionActive } from "../../components/next-component-fork";
import { TopicClientList } from "../../components/TopicClientList";
import { ADMINISTRATOR, READ } from "../../models/UserTypes";
import { ApplicationService } from "../../services/ApplicationService";
import { MQTTService } from "../../services/MQTTService";
import store from "../../store";

const mockStore = createMockStore([thunk]);

describe("TopicClientList component test", () => {
  const topicClientListType = "PUBLISHER";

  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopicClientList type="PUBLISHER" />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          userId: "test",
        },
      },
      topicClientList: {
        content: [
          {
            appClient: {
              appBrokerType: { brokerType: "test" },
              appName: "test",
              appOwner: { fullName: "test" },
            },
            modifiedBy: { fullName: "test" },
          },
        ],
        loading: "test",
      },
      topicDetails: {
        content: {
          topic: "test",
        },
        error: "test",
      },
    });
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={mStore}>
          <TopicClientList type="PUBLISHER" />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe("TopicClientList [ordering and filtering] ", () => {
    const appOwnerUserId = "testUserId";
    const selectedTopicId = "testTopicId";
    /* let useStateSpy: any; */

    let wrapper: any;
    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
      topicClientList: {
        content: [
          {
            appClient: {
              appBrokerType: { brokerType: "test" },
              appName: "test",
              appOwner: {
                fullName: "test",
                userId: appOwnerUserId,
              },
            },
            modifiedBy: { fullName: "test" },
            topic: {
              topicId: selectedTopicId,
            },
          },
        ],
        loading: "test",
      },
      topicDetails: {
        content: {
          topic: "test",
        },
        error: "test",
      },
    });

    beforeEach(() => {
      /* useStateSpy = Sinon.spy(React, "useState"); */
      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <TopicClientList type={topicClientListType} />
          </Provider>
        </MemoryRouter>
      );
    });

    afterEach(() => {
      /* useStateSpy.restore(); */
    });

    it("should change sorting direction", () => {
      let header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("none");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("ascending");

      header2 = wrapper.find("SortableHeaderCell").at(1);
      header2.simulate("click");
      expect(header2.props().sortDirection).toEqual("descending");
    });

    it("should call filter if filtering against string-based column", () => {
      const enteredInput = "randomly added filter string";
      const columns = [
        "ta-topicClients-filter-appName",
        "ta-topicClients-filter-appVersion",
        "ta-topicClients-filter-owner",
      ];
      for (const column of columns) {
        let input = wrapper.find(`.${column}`).first();
        act(() => {
          input.prop("onChange")({ target: { value: enteredInput } });
        });
        wrapper.update();
        input = wrapper.find(`.${column}`).first();
        expect(input.prop("value")).toEqual(enteredInput);
      }
    });

    it("should not call filter if filtering with empty string", () => {
      const enteredInput = " ";
      const expectedValue = "";
      let input = wrapper.find(".ta-topicClients-filter-appName").first();
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
      });
      wrapper.update();
      input = wrapper.find(".ta-topicClients-filter-appName").first();
      expect(input.prop("value")).toEqual(expectedValue);
    });

    it("should call filter if filtering against date-based column", () => {
      const enteredInput = { startDate: null, endDate: null };
      let input = wrapper.find(".ta-topicClients-filter-modifiedDate").first();
      act(() => {
        input.prop("onDatesChange")(enteredInput);
      });
      wrapper.update();
      input = wrapper.find(".ta-topicClients-filter-modifiedDate").first();
      expect(input.prop("startDate")).toEqual(enteredInput.startDate);
      expect(input.prop("endDate")).toEqual(enteredInput.endDate);
    });

    it("should call filter if filtering against dropdown column", () => {
      const enteredInput = "brokerType";
      const className = ".ta-topicClients-filter-brokerType";
      let input = wrapper
        .find(className)
        .find(Dropdown)
        .first();
      act(() => {
        input.prop("onChange")(enteredInput);
      });
      wrapper.update();
      input = wrapper
        .find(className)
        .find(Dropdown)
        .first();
      expect(input.prop("selected")).toEqual(enteredInput);
    });

    it("should clear all filter values if any had been already entered", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const classNameOfInput = "ta-topicClients-filter-appName";
      const classNameOfDeleteIcon = "ta-topicClients-button-clearFilter";

      let icon = getElementByClassName(classNameOfDeleteIcon);
      expect(icon.prop("disabled")).toBe(true);

      const enteredInput = "randomly added filter string";
      let input = getElementByClassName(classNameOfInput);
      act(() => {
        input.prop("onChange")({ target: { value: enteredInput } });
      });
      wrapper.update();
      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual(enteredInput);

      wrapper.setProps({}); // Re-render in order to property change (disable) come into live

      icon = getElementByClassName(classNameOfDeleteIcon);
      expect(icon.prop("disabled")).toBe(false);
      icon.simulate("click");

      input = getElementByClassName(classNameOfInput);
      expect(input.prop("value")).toEqual("");
    });

    /* it("should call keypress event on pressing Enter", () => {
      const getElementByClassName = (className: string) => wrapper.find("." + className).first();
      const enteredInput = "randomly added filter string";
      let input = getElementByClassName("ta-topicClients-filter-appName");
      input.prop("onChange")({ target: { value: enteredInput } });
      
      console.info(useStateSpy.args);
      const callsBefore = useStateSpy.args.length;
      input.prop("onKeyPress")({ key: "Enter" });
      wrapper.update();
      input = getElementByClassName("ta-topicClients-filter-appName");

      const expectedObj = [
        {
          clientType: 'PUBLISHER',
          modifiedDate: {},
          mqttConnectedAppClientBrokerType: '',
          mqttConnectedAppClientId: '',
          mqttConnectedAppClientName: '',
          mqttConnectedAppClientOwnerFullName: '',
          mqttConnectedAppClientVersion: '',
          topicId: ''
        }
      ];
      expect(useStateSpy.args[useStateSpy.args.length-1]).toEqual(expectedObj);
    }); */
  });

  describe("TopicClientList", () => {
    let wrapper: any;
    const appOwnerUserId = "testUserId";
    const selectedTopicId = "testTopicId";
    const appClientIdTest = "appClientId";

    const mockHistory = createBrowserHistory();
    let pushSpy: any;

    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
      topicClientList: {
        content: [
          {
            appClient: {
              appBrokerType: {
                brokerType: "test",
              },
              appClientId: appClientIdTest,
              appName: "test",
              appOwner: {
                fullName: "test",
                userId: appOwnerUserId,
              },
            },
            modifiedBy: { fullName: "test" },
            topic: {
              topicId: selectedTopicId,
            },
          },
        ],
        loading: false,
      },
      topicDetails: {
        content: {
          topic: "test",
        },
        error: "test",
      },
    });

    beforeEach(() => {
      pushSpy = Sinon.spy(mockHistory, "push");

      wrapper = mount(
        <Router history={mockHistory}>
          <Provider store={mStore}>
            <TopicClientList type={topicClientListType} />
          </Provider>
        </Router>
      );
    });

    afterEach(() => {
      pushSpy.restore();
    });
    it("should call navigate", () => {
      const getElementByClassName = (className: string) => wrapper.find(`.${className}`).first();
      const icon = getElementByClassName("ta-topicClients-column-mqttClientAppName");
      icon.simulate("click");

      expect(pushSpy.called).toEqual(true);
      expect(pushSpy.args[0]).toEqual([`/applicationClients/${appClientIdTest}`]);
    });
  });

  describe("TopicClientList", () => {
    const appOwnerUserId = "testUserId";
    const selectedTopicId = "testTopicId";
    let wrapper: any;
    let applicationServiceStub: any;
    let dispatchSpy: any;
    /* let useStateSpy: any; */

    const topicListsObj = {
      data: {
        data: {
          content: [{ topic: { topicId: "1" } }],
        },
      },
    };

    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
      },
      topicClientList: {
        content: [
          {
            appClient: {
              appBrokerType: { brokerType: "test" },
              appName: "test",
              appOwner: {
                fullName: "test",
                userId: appOwnerUserId,
              },
            },
            modifiedBy: { fullName: "test" },
            topic: {
              topicId: selectedTopicId,
            },
          },
        ],
        loading: false,
      },
      topicDetails: {
        content: {
          topic: "test",
        },
        error: "test",
      },
    });

    beforeEach(() => {
      dispatchSpy = Sinon.spy(store, "dispatch");

      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <TopicClientList type={topicClientListType} />
          </Provider>
        </MemoryRouter>
      );

      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationTopicList").callsFake(
        (config: any) => Promise.resolve(topicListsObj as any)
      );

      /* const setState = jest.fn();
      useStateSpy = jest.spyOn(React, 'useState');
      //@ts-ignore
      useStateSpy.mockImplementation((init: any) => [init, setState]); */
    });

    afterEach(() => {
      /* useStateSpy.mockRestore(); */
      applicationServiceStub.restore();
      dispatchSpy.restore();
    });

    it("should open confirmation dialog if press on dustbin icon on PUBLISHER table as a user with ADMINISTRATOR role", () => {
      const input = wrapper.find(NextLink).first();
      input.prop("onClick")();

      console.info("dispatchSpy.args ", dispatchSpy.args);
      console.info("dispatchSpy.getCall ", dispatchSpy.getCall(0));

      // Console.info("dispatchSpy.mock.calls ", dispatchSpy.mock.calls);

      // TODO
      // Expect(dispatchSpy.args).toEqual("");

      /* wrapper.update();
      input = wrapper.find(".ta-topicClients-column-delete").first();
      expect(input.prop("value")).toEqual(expectedValue); */
    });
  });

  describe("TopicClientList [bulk delete 2x item]", () => {
    const appOwnerUserId = "testUserId";
    const selectedTopicId = "testTopicId";
    let wrapper: any;
    let applicationServiceStub: any;
    let dispatchSpy: any;
    let mqttServiceStub: SinonStubbedInstance<any>;

    const topicListsObj = {
      data: {
        data: {
          content: [{ topic: { topicId: "1" } }],
        },
      },
    };

    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: READ,
          userId: appOwnerUserId,
        },
      },
      topicClientList: {
        content: [
          {
            appClient: {
              appBrokerType: { brokerType: "test" },
              appName: "test",
              appOwner: {
                fullName: "test",
                userId: appOwnerUserId,
              },
            },
            modifiedBy: { fullName: "test" },
            topic: {
              topicId: selectedTopicId,
            },
          },
          {
            appClient: {
              appBrokerType: { brokerType: "test1" },
              appName: "test1",
              appOwner: {
                fullName: "test1",
                userId: appOwnerUserId,
              },
            },
            modifiedBy: { fullName: "test1" },
            topic: {
              topicId: selectedTopicId,
            },
          },
        ],
        loading: false,
      },
      topicDetails: {
        content: {
          topic: "test",
        },
        error: "test",
      },
    });

    beforeEach(() => {
      dispatchSpy = Sinon.spy(store, "dispatch");
      mqttServiceStub = Sinon.stub(MQTTService, "deleteAssociationBulk").callsFake((config: any) =>
        Promise.resolve({ data: {} } as any)
      );

      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <TopicClientList type="SUBSCRIBER" />
          </Provider>
        </MemoryRouter>
      );

      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationTopicList").callsFake(
        (config: any) => Promise.resolve(topicListsObj as any)
      );
    });

    afterEach(() => {
      applicationServiceStub.restore();
      dispatchSpy.restore();
      mqttServiceStub.restore();
    });

    it("should open confirmation dialog if press on dustbin icon on SUBSCRIBER table as a user with READ role", () => {
      const input = wrapper.find(NextLink).first();
      input.prop("onClick")();
    });

    it("should have bulk delete option hidden", () => {
      expect(wrapper.find(BatchActionActive).length).toEqual(0);
    });

    it("should have bulk delete option shown", () => {
      act(() => {
        wrapper
          .find(Checkbox)
          .first()
          .prop("onClick")();
        wrapper.setProps({}); // Re-render
      });

      expect(wrapper.find(BatchActionActive).length).toEqual(1);
    });

    it("should have bulk delete button", () => {
      act(() => {
        wrapper
          .find(Checkbox)
          .first()
          .prop("onClick")();
        wrapper.setProps({}); // Re-render
      });

      const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);

      // Call delete
      act(() => {
        deleteButtonWithoutIcon.prop("onClick")();
      });

      expect(mqttServiceStub.getCall(0).args[0]);
    });
  });

  describe("TopicClientList [bulk delete 1x item]", () => {
    const appOwnerUserId = "testUserId";
    const selectedTopicId = "testTopicId";
    let wrapper: any;
    let applicationServiceStub: any;
    let dispatchSpy: any;
    let bulkDeleteSpy: any;

    const topicListsObj = {
      data: {
        data: {
          content: [{ topic: { topicId: "1" } }],
        },
      },
    };

    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: READ,
          userId: appOwnerUserId,
        },
      },
      topicClientList: {
        content: [
          {
            appClient: {
              appBrokerType: { brokerType: "test" },
              appName: "test",
              appOwner: {
                fullName: "test",
                userId: appOwnerUserId,
              },
            },
            modifiedBy: { fullName: "test" },
            topic: {
              topicId: selectedTopicId,
            },
          },
        ],
        loading: "test",
      },
      topicDetails: {
        content: {
          topic: "test",
        },
        error: "test",
      },
    });

    beforeEach(() => {
      dispatchSpy = Sinon.spy(store, "dispatch");
      bulkDeleteSpy = jest.spyOn(MQTTService, "deleteAssociationBulk");

      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <TopicClientList type="SUBSCRIBER" />
          </Provider>
        </MemoryRouter>
      );

      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationTopicList").callsFake(
        (config: any) => Promise.resolve(topicListsObj as any)
      );
    });

    afterEach(() => {
      applicationServiceStub.restore();
      dispatchSpy.restore();
      bulkDeleteSpy.mockRestore();
    });

    it("shouldn't call bulk action if it only has one item", () => {
      act(() => {
        wrapper
          .find(Checkbox)
          .first()
          .prop("onClick")();
        wrapper.setProps({}); // Re-render
      });

      const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);

      // Call delete
      act(() => {
        deleteButtonWithoutIcon.prop("onClick")();
      });

      expect(bulkDeleteSpy).not.toHaveBeenCalled();
    });
  });

  describe("TopicClientList [only publisher with subscriber]", () => {
    const appOwnerUserId = "testUserId";
    const selectedTopicId = "testTopicId";
    let wrapper: any;
    let applicationServiceStub: any;
    let dispatchSpy: any;
    let bulkDeleteSpy: any;

    const topicListsObj = {
      data: {
        data: {
          content: [{ topic: { topicId: "1" } }],
        },
      },
    };

    const mStore = mockStore({
      allBrokerType: {
        content: [{}],
      },
      currentUser: {
        data: {
          role: READ,
          userId: appOwnerUserId,
        },
      },
      topicClientList: {
        content: [
          {
            appClient: {
              appBrokerType: { brokerType: "test" },
              appName: "test",
              appOwner: {
                fullName: "test",
                userId: appOwnerUserId,
              },
            },
            modifiedBy: { fullName: "test" },
            topic: {
              topicId: selectedTopicId,
            },
          },
          {
            appClient: {
              appBrokerType: { brokerType: "test1" },
              appName: "test1",
              appOwner: {
                fullName: "test1",
                userId: appOwnerUserId,
              },
            },
            modifiedBy: { fullName: "test1" },
            topic: {
              topicId: selectedTopicId,
            },
          },
        ],
        count: 2,
        loading: "test",
      },
      topicDetails: {
        content: {
          topic: "test",
        },
        error: "test",
      },
    });

    beforeEach(() => {
      dispatchSpy = Sinon.spy(store, "dispatch");
      bulkDeleteSpy = jest.spyOn(MQTTService, "deleteAssociationBulk");

      wrapper = mount(
        <MemoryRouter initialEntries={[{ key: "testKey" }]}>
          <Provider store={mStore}>
            <TopicClientList type="PUBLISHER" subscriberCount={1} />
          </Provider>
        </MemoryRouter>
      );

      applicationServiceStub = Sinon.stub(ApplicationService, "getApplicationTopicList").callsFake(
        (config: any) => Promise.resolve(topicListsObj as any)
      );
    });

    afterEach(() => {
      applicationServiceStub.restore();
      dispatchSpy.restore();
      bulkDeleteSpy.mockRestore();
    });

    it("shouldn call bulk action", () => {
      act(() => {
        wrapper
          .find(Checkbox)
          .first()
          .prop("onClick")();
        wrapper.setProps({}); // Re-render
      });

      const deleteButtonWithoutIcon = wrapper.find(BatchActionActive).find(ButtonPrimary);

      // Call delete
      act(() => {
        deleteButtonWithoutIcon.prop("onClick")();
      });

      expect(bulkDeleteSpy).toHaveBeenCalled();
    });
  });
});
