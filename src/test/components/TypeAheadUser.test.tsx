import { shallow } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";

import { TypeaheadUser } from "../../components/TypeaheadUser";
import store from "../../store";

describe("type ahead user component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TypeaheadUser />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <TypeaheadUser />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
