import { mount, shallow } from "enzyme";
// tslint:disable-next-line: no-import-side-effect
import "jest-canvas-mock";
import * as React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { Topology } from "../../components/Topology";
import { TopologyGraphVis } from "../../components/TopologyGraphVis";
import { TopologyList } from "../../components/TopologyList";
import store from "../../store";

const mockStore = createMockStore([thunk]);

describe("Topology component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <Topology />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={store}>
          <Topology />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("makes the list and graph visible if data is present", () => {
    const mStore = mockStore({
      topologyList: {
        content: [
          {
            brokerTopologyId: "sz74Z97C8KkSmjoYPhzcCH",
            brokerTopologyName: "GCS FS/GCS CB",
            parentBrokerType: {
              brokerType: "GCS FSB",
              brokerTypeId: "39hzCPB8Xs62LXLj4AWbth",
            },
            remoteBrokerType: {
              brokerType: "GCS CB",
              brokerTypeId: "toZ4kBW56cuCF5xahwdUNw",
            },
          },
          {
            brokerTopologyId: "testRow",
            brokerTopologyName: "GCS FS",
            parentBrokerType: {
              brokerType: "GCS FSB",
              brokerTypeId: "39hzCPB8Xs62LXLj4AWbth",
            },
            remoteBrokerType: {
              brokerType: "GCS CB",
              brokerTypeId: "toZ4kBW56cuCF5xahwdUNw",
            },
          },
        ],
        count: 2,
      },
    });
    const wrapper = mount(
      <MemoryRouter initialEntries={[{ key: "testKey" }]}>
        <Provider store={mStore}>
          <Topology />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper.find(TopologyList).length).toEqual(1);
    expect(wrapper.find(TopologyGraphVis).length).toEqual(1);
  });
});
