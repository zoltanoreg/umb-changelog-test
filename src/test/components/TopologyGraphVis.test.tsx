import { mount, shallow } from "enzyme";
// tslint:disable-next-line: no-import-side-effect
import "jest-canvas-mock";
import * as React from "react";
import { Provider } from "react-redux";
import { Network } from "react-vis-network";

import { TopologyGraphVis } from "../../components/TopologyGraphVis";
import store from "../../store";

describe("TopologyGraphVis component test", () => {
  it("renders without crashing", () => {
    shallow(
      <Provider store={store}>
        <TopologyGraphVis />
      </Provider>
    );
  });

  it("check if matches snapshot", () => {
    const wrapper = shallow(
      <Provider store={store}>
        <TopologyGraphVis />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("should present an empty canvas", () => {
    const props = {
      rawData: [],
    };
    const wrapper = mount(
      <Provider store={store}>
        <TopologyGraphVis {...props} />
      </Provider>
    );
    expect(wrapper.find(Network).length).toEqual(0);
  });

  it("should present nodes with edges", () => {
    const props = {
      rawData: [
        {
          from: "1",
          label: "label1",
          to: "2",
        },
        {
          from: "1",
          label: "label2",
          to: "3",
        },
      ],
    };
    const wrapper = mount(
      <Provider store={store}>
        <TopologyGraphVis {...props} />
      </Provider>
    );
    expect(wrapper.find(Network).length).toEqual(1);
  });

  it("should present node with recurring edge", () => {
    const props = {
      rawData: [
        {
          from: "1",
          label: "out",
          to: "1",
        },
      ],
    };
    const wrapper = mount(
      <Provider store={store}>
        <TopologyGraphVis {...props} />
      </Provider>
    );
    expect(wrapper.find(Network).length).toEqual(1);
  });
});
