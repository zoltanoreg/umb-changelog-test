import { ReactWrapper } from "enzyme";
import { act } from "react-dom/test-utils";

// From https://github.com/airbnb/enzyme/issues/2073
const wait = async (amount: any) => new Promise(resolve => setTimeout(resolve, amount));

// Use this in your test after mounting if you want the query to finish and update the wrapper
export const updateWrapper = async (wrapper: ReactWrapper, amount = 0) => {
  await act(async () => {
    await wait(amount);
    wrapper.update();
  });
};
