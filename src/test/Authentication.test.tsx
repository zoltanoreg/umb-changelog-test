import { mount } from "enzyme";
// tslint:disable-next-line: no-implicit-dependencies
import { createBrowserHistory, createLocation } from "history";
import MockAxios from "jest-mock-axios";
import jwt from "jsonwebtoken";
import * as React from "react";
import { Provider } from "react-redux";
import { HashRouter, MemoryRouter, Router } from "react-router-dom";
import createMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import { Authentication } from "../Authentication";
import { ADMINISTRATOR } from "../models/UserTypes";

jest.mock("axios", () => MockAxios);

const mockStore = createMockStore([thunk]);
const mockHistory = createBrowserHistory();

describe("Authentication component test", () => {
  it("renders without crashing", () => {
    const mStore = mockStore({
      currentUser: {
        authenticated: false,
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
        loading: true,
      },
    });

    const wrapper = mount(
      <Provider store={mStore}>
        <Router history={mockHistory}>
          <Authentication authenticated={() => <></>} unauthenticated={() => <></>} />
        </Router>
      </Provider>
    );
    expect(wrapper.find(Authentication).children().length).toEqual(0);
  });

  let AuthenticatedComp: any;
  let UnauthenticatedComp: any;

  beforeAll(() => {
    AuthenticatedComp = () => <div></div>;
    UnauthenticatedComp = () => <div></div>;
  });

  afterEach(() => {
    MockAxios.reset();
  });

  it("authenticated", () => {
    const mStore = mockStore({
      currentUser: {
        authenticated: true,
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
        loading: false,
      },
    });

    const wrapper = mount(
      <Provider store={mStore}>
        <Router history={mockHistory}>
          <Authentication authenticated={AuthenticatedComp} unauthenticated={UnauthenticatedComp} />
        </Router>
      </Provider>
    );

    expect(wrapper.find(Authentication).find(AuthenticatedComp).length).toEqual(1);
    expect(wrapper.find(Authentication).find(UnauthenticatedComp).length).toEqual(0);
  });

  it("unauthenticated", () => {
    const mStore = mockStore({
      currentUser: {
        authenticated: false,
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
        loading: false,
      },
    });

    const wrapper = mount(
      <Provider store={mStore}>
        <Router history={mockHistory}>
          <Authentication authenticated={AuthenticatedComp} unauthenticated={UnauthenticatedComp} />
        </Router>
      </Provider>
    );

    expect(wrapper.find(Authentication).find(AuthenticatedComp).length).toEqual(0);
    expect(wrapper.find(Authentication).find(UnauthenticatedComp).length).toEqual(1);
  });

  it("should do the access token check", () => {
    const neverExpiringToken =
      "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlRlc3QiLCJleHAiOiI3Mjg3OTI2NDAwIn0.dliEdTknOicn6Qe-GrNwXo1yxBEEgU6c5oELYc6ifQn-BG_9NADGzk7IPZ2xkMlfkjhccI8kxbDroKsn_mHHKT-wEULbsw4Kxs1BqakfGVDabia_oqhMoqEHG0wgsXhll-F_PBfWfhgaqW17DgE1kbbpYWxhIw80wLF0pkLSwhMY6hpnPYLu2d_kpayM4fY0iEKxiJhu9GUthi2dOcJz1PhnwS7DSIg55ZoFXAEGFIOdSk5lCbrLWYQJF9aTiWuEjiU6QqlZdY_wO0kKHmL27imKJb1ISgDKpkFR6PmopifA9N_KW-8sQBkCm3grdIaS4dNED5Myq1w3sX17oHmjsQ";
    const alreadyExpiredToken =
      "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlRlc3QiLCJleHAiOiI2OTQwNTEyMDAifQ.UhaM0ve8pcx00UhQW5aLhOyEpdLcM6evWYD9bPLP-4P9XOFBiKyY6gwZhjF3ZfdjK26hAuBL6eOrHMIM-ZvXEg9q5-pS_0C6gjaAC_eqYQt19WeaFc-ZjP9lym2WCkhC1bZ3nUrGr2DM-v3MFxC72VQkZA85oFoo0svL1QzDskVVhdhhQTJY6X0r-H_os0IWypNElZ628uTb4JB5r0nayGTdVV1Ec4_36Wtd5TsDnxeMKHFhlfvaHKy4n4cf7lq8D1Iij0tGNjGzZUgOZlSDfoQTZw5WU6dyt4oMyu_iyttbKje1Uqy7IwCC2jGIRUu01sXqotwtNyYD-Nf7UZUxtw";

    const mStore = mockStore({
      currentUser: {
        authenticated: false,
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
        loading: false,
      },
    });

    mockHistory.push(`/#access_token=${neverExpiringToken}&refresh_token=${neverExpiringToken}`);

    const component = mount(
      <Provider store={mStore}>
        <Router history={mockHistory}>
          <Authentication authenticated={AuthenticatedComp} unauthenticated={UnauthenticatedComp} />
        </Router>
      </Provider>
    );

    expect(localStorage.getItem("UMB_AT")).toEqual(neverExpiringToken);
    expect(localStorage.getItem("UMB_RT")).toEqual(neverExpiringToken);
  });

  it("should do the access token check", () => {
    const neverExpiringToken =
      "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlRlc3QiLCJleHAiOiI3Mjg3OTI2NDAwIn0.dliEdTknOicn6Qe-GrNwXo1yxBEEgU6c5oELYc6ifQn-BG_9NADGzk7IPZ2xkMlfkjhccI8kxbDroKsn_mHHKT-wEULbsw4Kxs1BqakfGVDabia_oqhMoqEHG0wgsXhll-F_PBfWfhgaqW17DgE1kbbpYWxhIw80wLF0pkLSwhMY6hpnPYLu2d_kpayM4fY0iEKxiJhu9GUthi2dOcJz1PhnwS7DSIg55ZoFXAEGFIOdSk5lCbrLWYQJF9aTiWuEjiU6QqlZdY_wO0kKHmL27imKJb1ISgDKpkFR6PmopifA9N_KW-8sQBkCm3grdIaS4dNED5Myq1w3sX17oHmjsQ";
    const alreadyExpiredToken =
      "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlRlc3QiLCJleHAiOiI2OTQwNTEyMDAifQ.UhaM0ve8pcx00UhQW5aLhOyEpdLcM6evWYD9bPLP-4P9XOFBiKyY6gwZhjF3ZfdjK26hAuBL6eOrHMIM-ZvXEg9q5-pS_0C6gjaAC_eqYQt19WeaFc-ZjP9lym2WCkhC1bZ3nUrGr2DM-v3MFxC72VQkZA85oFoo0svL1QzDskVVhdhhQTJY6X0r-H_os0IWypNElZ628uTb4JB5r0nayGTdVV1Ec4_36Wtd5TsDnxeMKHFhlfvaHKy4n4cf7lq8D1Iij0tGNjGzZUgOZlSDfoQTZw5WU6dyt4oMyu_iyttbKje1Uqy7IwCC2jGIRUu01sXqotwtNyYD-Nf7UZUxtw";

    const mStore = mockStore({
      currentUser: {
        authenticated: false,
        data: {
          role: ADMINISTRATOR,
          userId: "test",
        },
        loading: false,
      },
    });

    const component = mount(
      <Provider store={mStore}>
        <MemoryRouter initialEntries={[`/?access_token=${alreadyExpiredToken}`]}>
          <Authentication authenticated={AuthenticatedComp} unauthenticated={UnauthenticatedComp} />
        </MemoryRouter>
      </Provider>
    );

    expect(document.cookie.includes("UMB_AT")).toBeFalsy();
    expect(document.cookie.includes("UMB_RT")).toBeFalsy();
  });
});
