import { AnyAction } from "redux";

import { UserGetDetailTypes } from "../../../actions/user/get-details/types";
import { userDetails } from "../../../reducers/user/UserDetailsReducer";

const initialState = {
  content: {},
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("user details reducer test", () => {
  it("without params, should return initial state", () => {
    expect(userDetails(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test UserGetDetailTypes.SUCCESS", () => {
    const changedData = {
      content: { test: "test" },
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = userDetails(undefined, {
      payload: changedData,
      type: UserGetDetailTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test UserGetDetailTypes.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = userDetails(undefined, {
      payload: "Error",
      type: UserGetDetailTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
