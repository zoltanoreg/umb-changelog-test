import { UserSetDetailsTypes } from "../../../actions/user/set-details/types";
import { userSetDetails } from "../../../reducers/user/UserSetDetailsReducer";

const initialState = {
  data: [],
  error: false,
  loading: false,
};

describe("user set details reducer test", () => {
  it("should test UserSetDetailsTypes.REQUEST", () => {
    const changedData = {
      data: { content: [{ test: "test" }] },
    };
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = userSetDetails(undefined, {
      payload: changedData,
      type: UserSetDetailsTypes.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test UserSetDetailsTypes.SUCCESS", () => {
    const changedData = {
      data: { content: [{ test: "test" }] },
    };

    const expectedNewState = {
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = userSetDetails(undefined, {
      payload: changedData,
      type: UserSetDetailsTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test UserSetDetailsTypes.ERROR", () => {
    const changedData = {
      data: { content: [{ test: "Error" }] },
    };
    const expectedNewState = {
      ...changedData,
      error: true,
      loading: false,
    };

    const generatedNewState = userSetDetails(undefined, {
      payload: changedData,
      type: UserSetDetailsTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
