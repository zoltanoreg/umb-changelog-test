import { AnyAction } from "redux";

import { GetUserListType } from "../../../actions/user/get-list/types";
import { userList } from "../../../reducers/user/UserListReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("user list reducer test", () => {
  it("without params, should return initial state", () => {
    expect(userList(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test GetUserListType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = userList(undefined, {
      type: GetUserListType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetUserListType.SUCCESS", () => {
    const changedData = {
      data: { content: [{ test: "test" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = userList(undefined, {
      payload: changedData,
      type: GetUserListType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetUserListType.ERROR", () => {
    const changedData = {
      data: { content: [{ test: "Error" }] },
    };
    const expectedNewState = {
      ...initialState,
      error: changedData,
      loading: false,
    };

    const generatedNewState = userList(undefined, {
      payload: changedData,
      type: GetUserListType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
