import { CurrentUserTypes } from "../../../actions/user/current/types";
import { currentUser } from "../../../reducers/user/CurrentUserReducer";

describe("current user reducer test", () => {
  it("should test CurrentUserTypes.SUCCESS", () => {
    const changedData = {
      data: { data: { content: { test: "test" } } },
      headers: {
        ["access-token"]: "test",
      },
    };

    const expectedNewState = {
      authenticated: true,
      data: {
        ...changedData.data.data.content,
        jwt: changedData.headers["access-token"],
      },
      error: false,
      loading: false,
    };

    const generatedNewState = currentUser(undefined, {
      payload: changedData,
      type: CurrentUserTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test CurrentUserTypes.ERROR", () => {
    const changedData = {
      data: { data: { content: { test: "test" } } },
    };

    const expectedNewState = {
      authenticated: false,
      data: changedData,
      error: true,
      loading: false,
    };

    const generatedNewState = currentUser(undefined, {
      payload: changedData,
      type: CurrentUserTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
