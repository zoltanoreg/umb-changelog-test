import { AnyAction } from "redux";

import { TGetBrokerTypeDetails } from "../../../actions/broker-type/get-details/types";
import { brokerTypeDetails } from "../../../reducers/broker-type/BrokerTypeDetailsReducer";

const initialState = {
  content: [],
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("broker type details reducer test", () => {
  it("without params, should return initial state", () => {
    expect(brokerTypeDetails(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test TGetBrokerTypeDetails.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = brokerTypeDetails(undefined, {
      type: TGetBrokerTypeDetails.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TGetBrokerTypeDetails.SUCCESS", () => {
    const changedData = {
      data: { content: [{ test1: "test1" }, { test2: "test2" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = brokerTypeDetails(undefined, {
      payload: changedData,
      type: TGetBrokerTypeDetails.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TGetBrokerTypeDetails.ERROR", () => {
    const changedData = {
      data: { content: [{ test1: "test1" }, { test2: "test2" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: true,
      loading: false,
    };

    const generatedNewState = brokerTypeDetails(undefined, {
      payload: changedData,
      type: TGetBrokerTypeDetails.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
