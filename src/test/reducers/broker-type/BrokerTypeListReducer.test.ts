import { AnyAction } from "redux";

import { BrokerTypeListType } from "../../../actions/broker-type/get-filtered-list/types";
import { IGetBrokerTypeListData } from "../../../models/IBrokerTypeListingTableConfig";
import { brokerTypeList } from "../../../reducers/broker-type/BrokerTypeListReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("broker type list reducer test", () => {
  it("without params, should return initial state", () => {
    expect(brokerTypeList(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test BrokerTypeListType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = brokerTypeList(undefined, {
      type: BrokerTypeListType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test BrokerTypeListType.SUCCESS", () => {
    const changedData: IGetBrokerTypeListData = {
      content: [
        {
          brokerType: "test",
          brokerTypeId: "test",
          brokerTypeName: "test",
          createdAt: "test",
          createdBy: [{ fullName: "test", userId: "test" }],
          modifiedAt: "test",
          modifiedBy: [
            {
              fullName: "test",
              userId: "test",
            },
          ],
          pathToCa: "test",
          pathToCbaIca: "test",
          pathToPrivate: "test",
          systemType: "test",
        },
      ],
      count: 1,
      loading: true,
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = brokerTypeList(undefined, {
      payload: changedData,
      type: BrokerTypeListType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test BrokerTypeListType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = brokerTypeList(undefined, {
      payload: "Error",
      type: BrokerTypeListType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
