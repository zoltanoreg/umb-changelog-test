import { AnyAction } from "redux";

import { AllBrokerTypeListType } from "../../../actions/broker-type/get-all/types";
import { IGetBrokerTypeListData } from "../../../models/IBrokerTypeListingTableConfig";
import { allBrokerType } from "../../../reducers/broker-type/AllBrokerTypeReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("all broker type reducer test", () => {
  it("without params, should return initial state", () => {
    expect(allBrokerType(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test BrokerTypeListType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = allBrokerType(undefined, {
      type: AllBrokerTypeListType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test AllBrokerTypeListType.SUCCESS", () => {
    const changedData: IGetBrokerTypeListData = {
      content: [
        {
          brokerType: "test",
          brokerTypeId: "test",
          brokerTypeName: "test",
          createdAt: "test",
          createdBy: [{ fullName: "test", userId: "test" }],
          modifiedAt: "test",
          modifiedBy: [
            {
              fullName: "test",
              userId: "test",
            },
          ],
          pathToCa: "test",
          pathToCbaIca: "test",
          pathToPrivate: "test",
          systemType: "test",
        },
      ],
      count: 1,
      loading: true,
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = allBrokerType(undefined, {
      payload: changedData,
      type: AllBrokerTypeListType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test AllBrokerTypeListType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = allBrokerType(undefined, {
      payload: "Error",
      type: AllBrokerTypeListType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
