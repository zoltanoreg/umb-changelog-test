import { GetPayloadTypes } from "../../../actions/payload/get/types";
import { payloadGet } from "../../../reducers/payload/PayloadGetReducer";

const initialState = {
  content: [],
  error: false,
  loading: false,
};

describe("payloadGet reducer test", () => {
  it("should test GetPayloadTypes.REQUEST", () => {
    const changedData = {
      content: [],
    };
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = payloadGet(undefined, {
      payload: changedData,
      type: GetPayloadTypes.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetPayloadTypes.SUCCESS", () => {
    const changedData = {
      data: { content: [] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = payloadGet(undefined, {
      payload: changedData,
      type: GetPayloadTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetPayloadTypes.ERROR", () => {
    const changedData = {
      data: { content: [] },
    };
    const expectedNewState = {
      ...changedData.data,
      error: true,
      loading: false,
    };

    const generatedNewState = payloadGet(undefined, {
      payload: changedData,
      type: GetPayloadTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
