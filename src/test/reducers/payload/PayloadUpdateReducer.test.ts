import { UpdatePayloadTypes } from "../../../actions/payload/update/types";
import { payloadUpdate } from "../../../reducers/payload/PayloadUpdateReducer";

const initialState = {
  content: [],
  error: false,
  loading: false,
};

describe("payloadUpdate reducer test", () => {
  it("should test UpdatePayloadTypes.REQUEST", () => {
    const changedData = {
      content: [],
    };
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = payloadUpdate(undefined, {
      payload: changedData,
      type: UpdatePayloadTypes.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test UpdatePayloadTypes.SUCCESS", () => {
    const changedData = {
      data: { content: [] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = payloadUpdate(undefined, {
      payload: changedData,
      type: UpdatePayloadTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test UpdatePayloadTypes.ERROR", () => {
    const changedData = {
      data: { content: [] },
    };
    const expectedNewState = {
      ...changedData.data,
      error: true,
      loading: false,
    };

    const generatedNewState = payloadUpdate(undefined, {
      payload: changedData,
      type: UpdatePayloadTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
