import { UploadPayloadTypes } from "../../../actions/payload/upload/types";
import { payloadUpload } from "../../../reducers/payload/PayloadUploadReducer";

const initialState = {
  content: [],
  error: false,
  loading: false,
};

describe("payloadUpload reducer test", () => {
  it("should test UploadPayloadTypes.REQUEST", () => {
    const changedData = {
      content: [],
    };
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = payloadUpload(undefined, {
      payload: changedData,
      type: UploadPayloadTypes.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test UploadPayloadTypes.SUCCESS", () => {
    const changedData = {
      data: { content: [] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = payloadUpload(undefined, {
      payload: changedData,
      type: UploadPayloadTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test UploadPayloadTypes.ERROR", () => {
    const changedData = {
      data: { content: [] },
    };
    const expectedNewState = {
      ...changedData.data,
      error: true,
      loading: false,
    };

    const generatedNewState = payloadUpload(undefined, {
      payload: changedData,
      type: UploadPayloadTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
