import { PopupActions } from "../../../actions/popup/types";
import { popupData } from "../../../reducers/popup/PopupReducer";

const initialState = {
  content: "",
  open: false,
  title: "",
  type: "",
};

describe("popup reducer test", () => {
  it("should test PopupActions.OPEN", () => {
    const changedData = {
      content: "test",
      title: "test",
      type: "Info",
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      open: true,
    };

    const generatedNewState = popupData(undefined, {
      payload: changedData,
      type: PopupActions.OPEN,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test PopupActions.CLOSE", () => {
    const expectedNewState = {
      ...initialState,
      open: false,
      type: "",
    };

    const generatedNewState = popupData(undefined, {
      type: PopupActions.CLOSE,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
