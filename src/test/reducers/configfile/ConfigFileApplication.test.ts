import { AnyAction } from "redux";

import { GetConfigApplicationType } from "../../../actions/configfile/application/types";
import { configFileApplication } from "../../../reducers/configfile/ConfigFileApplication";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("configFileApplication reducer test", () => {
  it("without params, should return initial state", () => {
    expect(configFileApplication(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test GetConfigApplicationType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = configFileApplication(undefined, {
      type: GetConfigApplicationType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetConfigApplicationType.SUCCESS", () => {
    const changedData = {
      content: [],
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = configFileApplication(undefined, {
      payload: changedData,
      type: GetConfigApplicationType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetConfigApplicationType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = configFileApplication(undefined, {
      payload: "Error",
      type: GetConfigApplicationType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
