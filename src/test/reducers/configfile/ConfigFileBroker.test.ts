import { AnyAction } from "redux";

import { GetConfigBrokerType } from "../../../actions/configfile/broker/types";
import { configFileBroker } from "../../../reducers/configfile/ConfigFileBroker";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("configFileBroker reducer test", () => {
  it("without params, should return initial state", () => {
    expect(configFileBroker(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test GetConfigBrokerType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = configFileBroker(undefined, {
      type: GetConfigBrokerType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetConfigBrokerType.SUCCESS", () => {
    const changedData = {
      content: [],
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = configFileBroker(undefined, {
      payload: changedData,
      type: GetConfigBrokerType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetConfigBrokerType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = configFileBroker(undefined, {
      payload: "Error",
      type: GetConfigBrokerType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
