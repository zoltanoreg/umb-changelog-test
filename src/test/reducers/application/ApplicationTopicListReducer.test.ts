import { AnyAction } from "redux";

import { AppTopicListType } from "../../../actions/application/get-topics/types";
import { IGetTopicListContent } from "../../../models/ITopicConfig";
import { applicationTopicList } from "../../../reducers/application/ApplicationTopicListReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("applicationTopicList reducer test", () => {
  it("without params, should return initial state", () => {
    expect(applicationTopicList(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test AppTopicListType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = applicationTopicList(undefined, {
      type: AppTopicListType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test AppTopicListType.SUCCESS", () => {
    // tslint:disable-next-line:no-object-literal-type-assertion
    const changedData = {
      content: [
        {
          appClient: [{ appClientId: "test", appName: "test", appVersion: "test" }],
          createdBy: [{ fullName: "test", userId: "test" }],
          modifiedBy: [{ fullName: "test", userId: "test" }],
          topicId: "test",
          topicName: "test",
        },
      ] as IGetTopicListContent[],
      count: 2,
      loading: true,
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = applicationTopicList(undefined, {
      payload: changedData,
      type: AppTopicListType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test AppTopicListType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = applicationTopicList(undefined, {
      payload: "Error",
      type: AppTopicListType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
