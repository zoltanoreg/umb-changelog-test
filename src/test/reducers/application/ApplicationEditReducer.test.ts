import { EditApplicationTypes } from "../../../actions/application/edit/types";
import { applicationEdit } from "../../../reducers/application/ApplicationEditReducer";

const initialState = {
  content: [],
  error: false,
  loading: false,
};

describe("application edit reducer test", () => {
  it("should test EditApplicationTypes.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = applicationEdit(undefined, {
      payload: "",
      type: EditApplicationTypes.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test EditApplicationTypes.SUCCESS", () => {
    const changedData = {
      data: { content: [{ test: "test" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = applicationEdit(undefined, {
      payload: changedData,
      type: EditApplicationTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test EditApplicationTypes.ERROR", () => {
    const changedData = {
      data: { content: [{ test: "test" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: true,
      loading: false,
    };

    const generatedNewState = applicationEdit(undefined, {
      payload: changedData,
      type: EditApplicationTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
