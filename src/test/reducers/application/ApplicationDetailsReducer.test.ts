import { AnyAction } from "redux";

import { TGetApplicationDetails } from "../../../actions/application/get-details/types";
import { applicationDetails } from "../../../reducers/application/ApplicationDetailsReducer";

const initialState = {
  content: [],
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("application details reducer test", () => {
  it("without params, should return initial state", () => {
    expect(applicationDetails(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test TGetApplicationDetails.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = applicationDetails(undefined, {
      type: TGetApplicationDetails.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TGetApplicationDetails.SUCCESS", () => {
    const changedData = {
      data: { content: [{ test1: "test1" }, { test2: "test2" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = applicationDetails(undefined, {
      payload: changedData,
      type: TGetApplicationDetails.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TGetApplicationDetails.ERROR", () => {
    const changedData = {
      data: { content: [{ test1: "test1" }, { test2: "test2" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: true,
      loading: false,
    };

    const generatedNewState = applicationDetails(undefined, {
      payload: changedData,
      type: TGetApplicationDetails.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
