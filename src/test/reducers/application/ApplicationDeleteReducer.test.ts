import { AnyAction } from "redux";

import { TDeleteApplication } from "../../../actions/application/delete/types";
import { applicationDelete } from "../../../reducers/application/ApplicationDeleteReducer";

const initialState = {
  content: [],
  error: false,
  loading: true,
};

describe("application details reducer test", () => {
  it("should test TDeleteApplication.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = applicationDelete(undefined, {
      type: TDeleteApplication.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TDeleteApplication.SUCCESS", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: false,
    };

    const generatedNewState = applicationDelete(undefined, {
      payload: "0",
      type: TDeleteApplication.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TDeleteApplication.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = applicationDelete(undefined, {
      payload: "0",
      type: TDeleteApplication.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
