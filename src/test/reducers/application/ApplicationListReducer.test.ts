import { AnyAction } from "redux";

import { ApplicationListType } from "../../../actions/application/get-list/types";
import { IGetApplicationListData } from "../../../models/IAppListingTableConfig";
import { applicationList } from "../../../reducers/application/ApplicationListReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("application list reducer test", () => {
  it("without params, should return initial state", () => {
    expect(applicationList(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test ApplicationListType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = applicationList(undefined, {
      type: ApplicationListType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test ApplicationListType.SUCCESS", () => {
    // tslint:disable-next-line:no-object-literal-type-assertion
    const changedData = {
      content: [],
      count: 2,
    } as IGetApplicationListData;

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = applicationList(undefined, {
      payload: changedData,
      type: ApplicationListType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test ApplicationListType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = applicationList(undefined, {
      payload: "Error",
      type: ApplicationListType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
