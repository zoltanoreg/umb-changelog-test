import { CreateApplicationTypes } from "../../../actions/application/create/types";
import { applicationCreation } from "../../../reducers/application/ApplicationCreationReducer";

const initialState = {
  content: [],
  error: false,
  loading: false,
};

describe("application creation reducer test", () => {
  it("should test CreateApplicationTypes.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = applicationCreation(undefined, {
      payload: {},
      type: CreateApplicationTypes.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test CreateApplicationTypes.SUCCESS", () => {
    const changedData = {
      data: { content: [{ test1: "test1" }, { test2: "test2" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = applicationCreation(undefined, {
      payload: changedData,
      type: CreateApplicationTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test CreateApplicationTypes.ERROR", () => {
    const changedData = {
      data: { content: [{ test1: "test1" }, { test2: "test2" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: true,
      loading: false,
    };

    const generatedNewState = applicationCreation(undefined, {
      payload: changedData,
      type: CreateApplicationTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
