import { GetPublisherListTypes } from "../../../actions/application/get-publisher-list/types";
import { applicationPublisherList } from "../../../reducers/application/ApplicationPublisherListReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

describe("applicationPublisherList list reducer test", () => {
  it("should test GetPublisherListTypes.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = applicationPublisherList(undefined, {
      payload: {},
      type: GetPublisherListTypes.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetPublisherListTypes.SUCCESS", () => {
    const changedData = {
      content: [],
      count: 2,
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = applicationPublisherList(undefined, {
      payload: changedData,
      type: GetPublisherListTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetPublisherListTypes.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = applicationPublisherList(undefined, {
      payload: "Error",
      type: GetPublisherListTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
