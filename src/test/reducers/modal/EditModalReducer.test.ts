import { EditModalActions } from "../../../actions/modal/edit/types";
import { editModal } from "../../../reducers/modal/EditModalReducer";

const initialState = {
  open: false,
  type: "",
};

describe("edit modal reducer test", () => {
  it("should test EditModalActions.OPEN", () => {
    const changedData = {
      type: "OPEN",
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      open: true,
    };

    const generatedNewState = editModal(undefined, {
      payload: changedData,
      type: EditModalActions.OPEN,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test EditModalActions.CLOSE", () => {
    const expectedNewState = {
      ...initialState,
      open: false,
    };

    const generatedNewState = editModal(undefined, {
      type: EditModalActions.CLOSE,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
