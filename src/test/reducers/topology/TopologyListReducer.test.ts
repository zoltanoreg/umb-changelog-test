import { AnyAction } from "redux";

import { GetTopologyListType } from "../../../actions/topology/types";
import { topologyList } from "../../../reducers/topology/TopologyListReducer";

describe("topology reducer test", () => {
  const emptyObject: AnyAction = { type: "" };
  const initialState = {
    content: [],
    count: 0,
    error: false,
    loading: true,
  };

  it("without params, should return initial state", () => {
    expect(topologyList(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test GetTopologyListType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = topologyList(undefined, {
      type: GetTopologyListType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetTopologyListType.SUCCESS", () => {
    const changedData = {
      content: [],
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = topologyList(undefined, {
      payload: changedData,
      type: GetTopologyListType.SUCCESS,
    });
    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetTopologyListType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = topologyList(undefined, {
      payload: "Error",
      type: GetTopologyListType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
