import { EditTopicTypes } from "../../../actions/topic/edit/types";
import { topicEdit } from "../../../reducers/topic/TopicEditReducer";

const initialState = {
  content: [],
  error: false,
  loading: false,
};

describe("topic edit reducer test", () => {
  it("should test EditTopicTypes.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = topicEdit(undefined, {
      payload: "",
      type: EditTopicTypes.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test EditTopicTypes.SUCCESS", () => {
    const changedData = {
      data: { content: [{ test: "test" }] },
    };

    const expectedNewState = {
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = topicEdit(undefined, {
      payload: changedData,
      type: EditTopicTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test EditTopicTypes.ERROR", () => {
    const changedData = {
      data: { content: [{ test: "test" }] },
    };
    const expectedNewState = {
      ...changedData.data,
      error: true,
      loading: false,
    };

    const generatedNewState = topicEdit(undefined, {
      payload: changedData,
      type: EditTopicTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
