import { AnyAction } from "redux";

import { TopicClientListType } from "../../../actions/topic/get-clients/types";
import { IGetTopicListContent } from "../../../models/ITopicConfig";
import { topicClientList } from "../../../reducers/topic/TopicClientListReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("topicClientList reducer test", () => {
  it("without params, should return initial state", () => {
    expect(topicClientList(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test TopicClientListType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = topicClientList(undefined, {
      type: TopicClientListType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TopicClientListType.SUCCESS", () => {
    // tslint:disable-next-line:no-object-literal-type-assertion
    const changedData = {
      content: [
        {
          appClient: [{ appClientId: "test", appName: "test", appVersion: "test" }],
          createdBy: [{ fullName: "test", userId: "test" }],
          modifiedBy: [{ fullName: "test", userId: "test" }],
          topicId: "test",
          topicName: "test",
        },
      ] as IGetTopicListContent[],
      count: 2,
      loading: true,
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = topicClientList(undefined, {
      payload: changedData,
      type: TopicClientListType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TopicClientListType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = topicClientList(undefined, {
      payload: "Error",
      type: TopicClientListType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
