import { AnyAction } from "redux";

import { TopicListType } from "../../../actions/topic/get-list/types";
import { topicList } from "../../../reducers/topic/TopicListReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("topic list reducer test", () => {
  it("without params, should return initial state", () => {
    expect(topicList(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test TopicListType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = topicList(undefined, {
      type: TopicListType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TopicListType.SUCCESS", () => {
    const changedData = {
      content: [
        {
          appClient: [
            {
              appClientId: "test",
              appName: "test",
              appVersion: "test",
            },
          ],
          createdBy: [
            {
              fullName: "test",
              userId: "test",
            },
          ],
          modifiedBy: [
            {
              fullName: "test",
              userId: "test",
            },
          ],
          topicId: "test",
          topicName: "test",
        },
      ],
      count: 1,
      loading: true,
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = topicList(undefined, {
      payload: changedData,
      type: TopicListType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TopicListType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = topicList(undefined, {
      payload: "Error",
      type: TopicListType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
