import { AnyAction } from "redux";

import { FilterTopicListType } from "../../../actions/topic/filter-list/types";
import { filterTopicList } from "../../../reducers/topic/FilterTopicListReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("topic list reducer test", () => {
  it("without params, should return initial state", () => {
    expect(filterTopicList(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test TopicListType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = filterTopicList(undefined, {
      type: FilterTopicListType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TopicListType.SUCCESS", () => {
    const changedData = {
      content: [
        {
          appClient: [
            {
              appClientId: "test",
              appName: "test",
              appVersion: "test",
            },
          ],
          createdBy: [
            {
              fullName: "test",
              userId: "test",
            },
          ],
          modifiedBy: [
            {
              fullName: "test",
              userId: "test",
            },
          ],
          topicId: "test",
          topicName: "test",
        },
      ],
      count: 1,
      loading: true,
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = filterTopicList(undefined, {
      payload: changedData,
      type: FilterTopicListType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TopicListType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = filterTopicList(undefined, {
      payload: "Error",
      type: FilterTopicListType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
