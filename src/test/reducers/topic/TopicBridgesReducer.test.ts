import { AnyAction } from "redux";

import { TGetTopicBridges } from "../../../actions/topic/get-bridges/types";
import { topicBridges } from "../../../reducers/topic/TopicBridgesReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("topicBridges reducer test", () => {
  it("without params, should return initial state", () => {
    expect(topicBridges(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test TGetTopicBridges.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = topicBridges(undefined, {
      type: TGetTopicBridges.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TGetTopicBridges.SUCCESS", () => {
    const changedData = {
      content: [],
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = topicBridges(undefined, {
      payload: changedData,
      type: TGetTopicBridges.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TGetTopicBridges.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = topicBridges(undefined, {
      payload: "Error",
      type: TGetTopicBridges.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
