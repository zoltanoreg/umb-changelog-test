import { GetTopicPayloadTypes } from "../../../actions/topic/get-payload/types";
import { topicPayload } from "../../../reducers/topic/TopicPayloadReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: false,
};

describe("topic payload reducer test", () => {
  it("should test GetTopicPayloadTypes.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = topicPayload(undefined, {
      payload: "",
      type: GetTopicPayloadTypes.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetTopicPayloadTypes.SUCCESS", () => {
    const changedData = {
      content: [{ test: "test" }],
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = topicPayload(undefined, {
      payload: changedData,
      type: GetTopicPayloadTypes.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test GetTopicPayloadTypes.ERROR", () => {
    const changedData = {
      data: { content: [{ test: "test" }] },
    };
    const expectedNewState = {
      ...initialState,
      content: [],
      error: true,
      loading: false,
    };

    const generatedNewState = topicPayload(undefined, {
      payload: changedData,
      type: GetTopicPayloadTypes.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
