import { MQTTClientType } from "../../../actions/topic/get-mqtt-clients/types";
import { topicMQTTClients } from "../../../reducers/topic/TopicMqttClientsReducer";

const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: false,
};

describe("topicMQTTClients reducer test", () => {
  it("should test MQTTClientType.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = topicMQTTClients(undefined, {
      payload: "",
      type: MQTTClientType.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test MQTTClientType.SUCCESS", () => {
    const changedData = {
      content: [{ test: "test" }],
    };

    const expectedNewState = {
      ...initialState,
      ...changedData,
      error: false,
      loading: false,
    };

    const generatedNewState = topicMQTTClients(undefined, {
      payload: changedData,
      type: MQTTClientType.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test MQTTClientType.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = topicMQTTClients(undefined, {
      payload: "Error",
      type: MQTTClientType.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
