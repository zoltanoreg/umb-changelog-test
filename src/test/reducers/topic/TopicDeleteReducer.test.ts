import { AnyAction } from "redux";

import { TDeleteTopic } from "../../../actions/topic/delete/types";
import { topicDelete } from "../../../reducers/topic/TopicDeleteReducer";

const initialState = {
  content: [],
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("topic delete reducer test", () => {
  it("without params, should return initial state", () => {
    expect(topicDelete(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test TDeleteTopic.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = topicDelete(undefined, {
      type: TDeleteTopic.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TDeleteTopic.SUCCESS", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: false,
    };

    const generatedNewState = topicDelete(undefined, {
      payload: "0",
      type: TDeleteTopic.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TDeleteTopic.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = topicDelete(undefined, {
      payload: "0",
      type: TDeleteTopic.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
