import { AnyAction } from "redux";

import { TGetTopicDetails } from "../../../actions/topic/get-details/types";
import { topicDetails } from "../../../reducers/topic/TopicDetailsReducer";

const initialState = {
  content: {},
  error: false,
  loading: true,
};

const emptyObject: AnyAction = { type: "" };

describe("topic details reducer test", () => {
  it("without params, should return initial state", () => {
    expect(topicDetails(undefined, emptyObject)).toEqual(initialState);
  });

  it("should test TGetTopicDetails.REQUEST", () => {
    const expectedNewState = {
      ...initialState,
      error: false,
      loading: true,
    };

    const generatedNewState = topicDetails(undefined, {
      type: TGetTopicDetails.REQUEST,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TGetTopicDetails.SUCCESS", () => {
    const changedData = {
      data: {
        content: [{ test1: "test1" }, { test2: "test2" }],
      },
    };

    const expectedNewState = {
      ...initialState,
      ...changedData.data,
      error: false,
      loading: false,
    };

    const generatedNewState = topicDetails(undefined, {
      payload: changedData,
      type: TGetTopicDetails.SUCCESS,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });

  it("should test TGetTopicDetails.ERROR", () => {
    const expectedNewState = {
      ...initialState,
      error: true,
      loading: false,
    };

    const generatedNewState = topicDetails(undefined, {
      payload: "Error",
      type: TGetTopicDetails.ERROR,
    });

    expect(generatedNewState).toEqual(expectedNewState);
  });
});
