import deepEqual from "deep-equal";

import { axiosInstance } from "../../services/AxiosInstance";

const fetchMock = require("fetch-mock-jest");

describe("#AxiosInstance", () => {
  let instance;

  it("should instanciate an axios instance", async () => {
    fetchMock.get("/config.json", {
      apiUrl: "http://localhost:3000/api",
    });

    instance = await axiosInstance();

    expect(instance).toBeDefined();
    fetchMock.mockReset();
  });

  it("should return the same instance", async () => {
    const i2 = await axiosInstance();

    expect(deepEqual(instance, i2)).toBeTruthy();
  });

  it("should run interceptor on fulfill", () => {
    expect(instance.interceptors.response.handlers[0].fulfilled({ data: "foo" })).toMatchObject({
      data: "foo",
    });
  });

  it("should run interceptor on reject", () => {
    instance.interceptors.response.handlers[0]
      .rejected({
        response: {
          data: { message: "Page not found" },
          status: 404,
          statusText: "NotFound",
        },
      })
      .catch(e => {
        expect(e).toMatchObject({
          data: { message: "Page not found" },
          message: "NotFound",
          statusCode: 404,
        });
      });
  });
});
