export enum PermissionActionEnum {
  GRANT = "GRANT",
  DENY = "DENY",
}
