import { OrderType } from "./ITableConfig";

/* istanbul ignore file */

export interface IUserListingFilters {
  [key: string]: any;
  email: string;
  firstName: string;
  lastName: string;
  role: string;
}

export interface IUserNameFilter {
  [key: string]: any;
  fullName: string;
}

export interface IUserListingTableConfig {
  [key: string]: any;
  filter: IUserListingFilters | IUserNameFilter;
  limit: number;
  orderBy: string;
  orderType: OrderType;
  page: number;
  roles: any;
}
