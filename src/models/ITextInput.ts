export interface ITextInput {
  disabled?: boolean;
  label: string;
  name: string;
  placeholder: string;
  value: string;
  onChange?(e: React.ChangeEvent<HTMLInputElement>): void;
}

export interface IStatusAndValidText extends ITextInput {
  statusIcon?: boolean;
  valid: boolean;
}

export interface INumberInput {
  label: string;
  max?: number;
  min?: number;
  name: string;
  placeholder: string;
  valid: boolean;
  value: number;
  onChange(value: number): void;
}

export interface IStatusAndValidNumber extends INumberInput {
  statusIcon: boolean;
  valid: boolean;
}
