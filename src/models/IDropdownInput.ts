interface ILabelValue {
  label: JSX.Element | string | number;
  value: string | number;
}

export interface IDropdownInput {
  disabled?: boolean;
  label: string;
  name: string;
  options: ILabelValue[];
  selected: string;
  onChange(data: string): void;
}

export interface IStatusAndValid extends IDropdownInput {
  statusIcon: boolean;
  valid: boolean;
}
