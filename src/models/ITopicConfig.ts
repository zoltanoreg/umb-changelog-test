export enum topicStatusEnum {
  DRAFT = "DRAFT",
  ACTIVE = "ACTIVE",
  ONHOLD = "ON HOLD",
  DELETED = "DELETED",
}

export enum payloadFormatEnum {
  JSON = "JSON",
  PROTOBUF = "Protobuf",
}

export enum qosEnum {
  FIRE_AND_FORGET = 0,
  ATLEAST_ONCE = 1,
}

export enum retainRequiredEnum {
  YES = "YES",
  NO = "NO",
}
export interface ITopicListFilters {
  [key: string]: any;
}

export interface ITopicListingTableConfig {
  [key: string]: any;
  filter: ITopicListFilters;
  limit: number;
  orderBy: string;
  orderType: any;
  page: number;
}

export interface IGetTopicListData {
  content: IGetTopicListContent[];
  count: number;
  loading: boolean;
}

export interface IGetTopicDetailsData {
  content: IGetTopicListContent;
  count: number;
  loading: boolean;
}

export interface IGetTopicListContent {
  appClient: IGetTopicAppClient[];
  createdBy: IGetTopicListUser[];
  modifiedBy: IGetTopicListUser[];
  topicId: string;
  topicName: string;
}

export interface IGetTopicDetailsContent {
  appClient: IGetTopicAppClient[];
  createdBy: IGetTopicListUser[];
  modifiedBy: IGetTopicListUser[];
  topicId: string;
  topicName: string;
}

export interface IGetTopicAppClient {
  appClientId: string;
  appName: string;
  appVersion: string;
}

interface IGetTopicListUser {
  fullName: string;
  userId: string;
}

export enum offloadType {
  DEFERRED = "Deferred",
  LIVE = "Live",
}

export interface ITopic {
  appClientId: string;
  dependencies: string;
  description: string;
  featureName: string;
  isSecure: boolean;
  obfuscationRequirement: string;
  offloadType: offloadType;
  priority: number;
  qosLevel: number;
  serviceName: string;
  topicCategory: string;
  topicMqttClient: {
    accessLevel: string;
    frequency: string;
    retainRequired: boolean;
  };
  topicName: string;
  topicPayloadId: string;
  topicType: string;
  ttl: number;
  versionId: string;
}

export interface IPayload {
  currentPayloadId?: string;
  payloadFile: any;
  payloadFormat: string;
  payloadType: string;
}
