export const READ = "READ";
export const ADMINISTRATOR = "ADMINISTRATOR";
export const APPLICATION_OWNER = "APPLICATION_OWNER";
export const UMBT_IT_SUPPORT_TEAM = "UMBT_IT_SUPPORT_TEAM";
export const UMBT_OPERATIONAL_SUPPORT_TEAM = "UMBT_OPERATIONAL_SUPPORT_TEAM";

export const getUserRole = (role: string) => {
  let userRole = "";
  switch (role) {
    case ADMINISTRATOR:
      userRole = "Administrator";
      break;
    case APPLICATION_OWNER:
      userRole = "Application Owner";
      break;
    case READ:
      userRole = "Read Only";
      break;
    case UMBT_IT_SUPPORT_TEAM:
      userRole = "UMBT IT support team";
      break;
    case UMBT_OPERATIONAL_SUPPORT_TEAM:
      userRole = "UMBT Operational support team";
      break;
    default:
      userRole = role;
  }

  return userRole;
};
