export enum configTabEnum {
  APP,
  BROKER,
}
export interface IConfigListFilters {
  [key: string]: any;
}

export interface IConfigListingTableConfig {
  [key: string]: any;
  filter: IConfigListFilters;
  limit: number;
  orderBy: string;
  orderType: string;
  page: number;
}
