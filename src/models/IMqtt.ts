export interface IMqtt {
  appClient: {
    appBrokerType: {
      brokerType: string;
      brokerTypeId: string;
    };
    appClientId: string;
    appName: string;
    appOwner: {
      fullName: string;
      userId: string;
    };
    appVersion: string;
  };
  brokerTopology: {
    brokerTopologyId: string;
    brokerTopologyName: string;
    parentBrokerType: {
      brokerType: string;
      brokerTypeId: string;
    };
    remoteBrokerType: {
      brokerType: string;
      brokerTypeId: string;
    };
  };
  clientType: string;
  createdAt: string;
  modifiedAt: string;
  modifiedBy: {
    fullName: string;
    userId: string;
  };
  topic: {
    appClient: {
      appBrokerType: {
        brokerType: string;
        brokerTypeId: string;
      };
      appClientId: string;
      appName: string;
      appOwner: {
        fullName: string;
        userId: string;
      };
      appVersion: string;
    };
    status: string;
    topicId: string;
    topicName: string;
  };
  topicMqttClientId: string;
}
