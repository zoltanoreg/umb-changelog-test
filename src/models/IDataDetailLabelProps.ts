export interface IDataDetailLabelProps {
  className: string;
  link?: string;
  title: string;
  value: string;
}
