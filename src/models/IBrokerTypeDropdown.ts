export interface IBrokerTypeDropdown {
  label?: string;
  selected: any;
  type?: string;
  onChange(e: any): void;
}
