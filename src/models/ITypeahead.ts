export interface ITypeahead {
  /**
   * If it's provided and rule is set to true, Typeahead will check for the given key in the object.
   */
  autoFillOnExactMatchOnly?: {
    key: string;
    rule: boolean;
  };

  /**
   * Config object to work with.
   */
  config: {
    /**
     * Set default config object which will be passed to endpoint.
     */
    default: {
      /**
       * Filter depending on what endpoint is called.
       */
      filter: any;
      /**
       * Limit of query.
       */
      limit?: number;
      /**
       * Property to order by.
       */
      orderBy: string;
      /**
       * Order of list.
       */
      orderType?: "ASC" | "DESC";
      /**
       * Restrictions
       */
      roles?: string[];
      /**
       * Status of data passed to endpoint.
       */

      statuses?: string[];
    };
    /**
     *  Provide this function what to update upon change in filter object.
     */
    setFilter(
      value: any
    ): {
      [key: string]: any;
    };
  };
  /**
   * Disable typeahead.
   */
  disabled?: boolean;
  /**
   *  Automation class; this is going to be used as placeholder if placeholder is not provided.
   */
  name: string;
  /**
   *  Used at automation class generation, specifies current page.
   */
  pageSelector: string;
  /**
   * "{key1} {key2}" add a pattern like this, those keys will be replaced by real value in suggestion list.
   */
  pattern: string;
  /**
   * Placeholder on typeahead.
   */
  placeholder?: string;

  /**
   * Suggestion list config.
   */
  suggestion: {
    /**
     * List, prop from store.
     */
    list: any;
    /**
     * Dispatch action, called on change.
     */
    getList(config: any): void;
    /**
     * What should happen on suggestion selection.
     */
    select(value: any): string;
  };
  /**
   * Selected value.
   */
  value: string | { [key: string]: any };
  /**
   * If value is an object this is a mandatory field.
   */
  valueToSetOnLoad?: string;
  /**
   * What to show on chip (extra information as well via function) or just use (p: any) => p.fieldName.
   */
  chipLabel?(props: any): string;
  /**
   * onChange function.
   */
  handleCreate?(value: any): void;
  /**
   * onChange function.
   */
  handleTypeahead(value: any): void;
  /**
   * Provide extra check on deciding if chip or input should be shown.
   */
  hasSelected?(value: any): boolean;
}
