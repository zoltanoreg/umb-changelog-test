export interface IFlexContainerPropTypes {
  justifyContent:
    | "flex-start"
    | "flex-end"
    | "center"
    | "space-between"
    | "space-around"
    | "space-evenly";
}

export interface IVerticalSpacerPropTypes {
  width?: string;
}

export interface ISpacerPropTypes {
  height?: string;
}

export interface IColor {
  error?: boolean;
  success?: boolean;
  warning?: boolean;
}

export interface IAnimation {
  scaleFrom?: string;
  speed?: string;
}
