export interface IDropdownOption {
  label: string | number;
  value: string | number;
}
