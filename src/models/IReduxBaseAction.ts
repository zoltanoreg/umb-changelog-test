export interface IReduxBaseAction<T> {
  payload?: any;
  type: T;
}
