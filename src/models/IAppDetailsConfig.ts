export enum tabAppDetailsEnum {
  PUBLISH,
  SUBSCRIBE,
  DETAILS,
}
export enum topicType {
  SUBSCRIBE = "SUBSCRIBE",
  PUBLISH = "PUBLISH",
}

export enum authTypeEnum {
  CREDENTIALS = "Credentials",
  CERTIFICATE = "Client certificate",
}
