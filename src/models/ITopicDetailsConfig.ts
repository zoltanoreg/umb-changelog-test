export enum tabEnum {
  BRIDGES,
  DETAILS,
  PAYLOAD,
  PUBLISHERS,
  SUBSCRIBERS,
  TOPOLOGY,
}

export interface IAppBrokerType {
  brokerType: string;
  brokerTypeId: string;
}

export interface IGenericSimpleUser {
  fullName: string;
  userId: string;
}

export interface IAppClient {
  appBrokerType: IAppBrokerType;
  appClientId: string;
  appName: string;
  appOwner: IGenericSimpleUser;
  appVersion: string;
  modifiedAt: string;
}

export interface ITopicDetailsContent {
  appClient: IAppClient;
  createdAt: string;
  createdBy: IGenericSimpleUser;
  dependencies: string;
  description: string;
  featureName: string;
  isSecure: boolean;
  modifiedAt: string;
  modifiedBy: IGenericSimpleUser;
  obfuscationRequirement: number;
  offloadType: string;
  priority: number;
  qosLevel: number;
  retainRequired: boolean;
  serviceName: string;
  status: string;
  topicCategory: string;
  topicId: string;
  topicName: string;
  topicType: string;
  ttl: number;
  versionId: string;
}
