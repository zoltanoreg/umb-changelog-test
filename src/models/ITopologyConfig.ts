import { OrderType } from "./ITableConfig";

export interface ITopologyTableConfig {
  [key: string]: any;
  limit: number;
  orderBy: string;
  orderType: OrderType;
  page: number;
}
