import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";

import { combinedReducer } from "./reducers";

const initialState = {};

const middleware = [thunk];

const store = createStore(
  combinedReducer,
  initialState,
  compose(
    applyMiddleware(...middleware),
    (window as any).__REDUX_DEVTOOLS_EXTENSION__
      ? (window as any).__REDUX_DEVTOOLS_EXTENSION__()
      : (f: any) => f
  )
);

export default store;
