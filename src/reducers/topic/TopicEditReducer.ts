import { EditTopicTypes, TSetEditTopicActions } from "../../actions/topic/edit/types";

interface ITopicEditState {
  content: any;
  error: boolean;
  loading: boolean;
}

const initialState: ITopicEditState = {
  content: [],
  error: false,
  loading: false,
};

export const topicEdit = (
  state: ITopicEditState = initialState,
  action: TSetEditTopicActions
): ITopicEditState => {
  switch (action.type) {
    case EditTopicTypes.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case EditTopicTypes.SUCCESS:
      return {
        ...action.payload.data,
        error: false,
        loading: false,
      };
    case EditTopicTypes.ERROR:
      return {
        ...action.payload.data,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
