import { TopicListType, TTopicListActions } from "../../actions/topic/get-list/types";
import { IGetTopicListContent } from "../../models/ITopicConfig";

interface ITopicListState {
  content: IGetTopicListContent[];
  count: number;
  error: boolean;
  loading: boolean;
}

const initialState: ITopicListState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

export const topicList = (
  state: ITopicListState = initialState,
  action: TTopicListActions
): ITopicListState => {
  switch (action.type) {
    case TopicListType.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case TopicListType.SUCCESS:
      return {
        ...state,
        ...action.payload,
        error: false,
        loading: false,
      };
    case TopicListType.ERROR:
      return {
        ...state,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
