import {
  TopicClientListType,
  TTopicClientListActions,
} from "../../actions/topic/get-clients/types";
import { IGetTopicListContent } from "../../models/ITopicConfig";

interface ITopicClientListState {
  content: IGetTopicListContent[];
  count: number;
  error: boolean;
  loading: boolean;
}

const initialState: ITopicClientListState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

export const topicClientList = (
  state: ITopicClientListState = initialState,
  action: TTopicClientListActions
): ITopicClientListState => {
  switch (action.type) {
    case TopicClientListType.CLEAR:
      return initialState;
    case TopicClientListType.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case TopicClientListType.SUCCESS:
      return {
        ...state,
        ...action.payload,
        error: false,
        loading: false,
      };
    case TopicClientListType.ERROR:
      return {
        ...state,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
