import {
  IGetTopicBridgesActionType,
  TGetTopicBridges,
} from "../../actions/topic/get-bridges/types";

interface ITopicListState {
  content: any[];
  count: number;
  error: boolean;
  loading: boolean;
}

const initialState: ITopicListState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

export const topicBridges = (
  state: ITopicListState = initialState,
  action: IGetTopicBridgesActionType
): ITopicListState => {
  switch (action.type) {
    case TGetTopicBridges.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case TGetTopicBridges.SUCCESS:
      return {
        ...state,
        ...action.payload,
        error: false,
        loading: false,
      };
    case TGetTopicBridges.ERROR:
      return {
        ...state,
        error: true,
        loading: false,
      };
    case TGetTopicBridges.CLEAR:
      return {
        ...state,
        content: [],
        error: false,
        loading: false,
      };
    default:
      return state;
  }
};
