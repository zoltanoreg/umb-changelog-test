import {
  TGetUserDetailsActionType,
  UserGetDetailTypes,
} from "../../actions/user/get-details/types";

interface IUserDetailsState {
  content: any;
  error: boolean;
  loading: boolean;
}

const initialState: IUserDetailsState = {
  content: {},
  error: false,
  loading: true,
};

export const userDetails = (
  state: IUserDetailsState = initialState,
  action: TGetUserDetailsActionType
): IUserDetailsState => {
  switch (action.type) {
    case UserGetDetailTypes.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case UserGetDetailTypes.SUCCESS:
      return {
        ...state,
        ...action.payload,
        error: false,
        loading: false,
      };
    case UserGetDetailTypes.ERROR:
      return {
        ...state,
        content: {},
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
