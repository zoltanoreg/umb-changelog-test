import { GetUserListType, TUserListActions } from "../../actions/user/get-list/types";

interface IUserListState {
  content: any[];
  count: number;
  error: boolean;
  loading: boolean;
}

// tslint:disable-next-line:no-object-literal-type-assertion
const initialState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
} as IUserListState;

export const userList = (
  state: IUserListState = initialState,
  action: TUserListActions
): IUserListState => {
  switch (action.type) {
    case GetUserListType.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case GetUserListType.SUCCESS:
      return {
        ...action.payload.data,
        error: false,
        loading: false,
      };
    case GetUserListType.ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};
