import {
  IGetApplicationDetailsActionType,
  TGetApplicationDetails,
} from "../../actions/application/get-details/types";

interface IApplicationDetailsState {
  content: any;
  error: boolean;
  loading: boolean;
}
const initialState: IApplicationDetailsState = {
  content: [],
  error: false,
  loading: true,
};

export const applicationDetails = (
  state: IApplicationDetailsState = initialState,
  action: IGetApplicationDetailsActionType
): IApplicationDetailsState => {
  switch (action.type) {
    case TGetApplicationDetails.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case TGetApplicationDetails.SUCCESS:
      return {
        ...action.payload.data,
        error: false,
        loading: false,
      };
    case TGetApplicationDetails.ERROR:
      return {
        ...action.payload.data,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
