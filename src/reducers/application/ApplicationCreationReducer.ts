import {
  CreateApplicationTypes,
  TSetCreateApplicationActions,
} from "../../actions/application/create/types";

interface IApplicationCreationState {
  content: any;
  error: boolean;
  loading: boolean;
}
const initialState: IApplicationCreationState = {
  content: [],
  error: false,
  loading: false,
};

export const applicationCreation = (
  state: IApplicationCreationState = initialState,
  action: TSetCreateApplicationActions
): IApplicationCreationState => {
  switch (action.type) {
    case CreateApplicationTypes.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case CreateApplicationTypes.SUCCESS:
      return {
        ...action.payload.data,
        error: false,
        loading: false,
      };
    case CreateApplicationTypes.ERROR:
      return {
        ...action.payload.data,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
