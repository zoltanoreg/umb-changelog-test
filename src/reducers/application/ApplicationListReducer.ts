import { ApplicationListType, TApplicationActions } from "../../actions/application/get-list/types";

interface IApplicationState {
  content: any[];
  count: number;
  error: boolean;
  loading: boolean;
}

const initialState: IApplicationState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

export const applicationList = (
  state: IApplicationState = initialState,
  action: TApplicationActions
): IApplicationState => {
  switch (action.type) {
    case ApplicationListType.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case ApplicationListType.SUCCESS:
      return {
        ...state,
        ...action.payload,
        error: false,
        loading: false,
      };
    case ApplicationListType.ERROR:
      return {
        ...state,
        content: [],
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
