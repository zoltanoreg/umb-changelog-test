import {
  GetPublisherListTypes,
  TGetPublisherListActions,
} from "../../actions/application/get-publisher-list/types";
import { IGetTopicListContent } from "../../models/ITopicConfig";

interface ITopicListState {
  content: IGetTopicListContent[];
  count: number;
  error: boolean;
  loading: boolean;
}

const initialState: ITopicListState = {
  content: [],
  count: 0,
  error: false,
  loading: true,
};

export const applicationPublisherList = (
  state: ITopicListState = initialState,
  action: TGetPublisherListActions
): ITopicListState => {
  switch (action.type) {
    case GetPublisherListTypes.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case GetPublisherListTypes.SUCCESS:
      return {
        ...state,
        ...action.payload,
        error: false,
        loading: false,
      };
    case GetPublisherListTypes.ERROR:
      return {
        ...state,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
