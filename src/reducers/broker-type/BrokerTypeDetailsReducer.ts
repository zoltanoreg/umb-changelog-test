import {
  IGetBrokerTypeDetailsActionType,
  TGetBrokerTypeDetails,
} from "../../actions/broker-type/get-details/types";

interface IBrokerTypeDetailsState {
  content: any;
  error: boolean;
  loading: boolean;
}
const initialState: IBrokerTypeDetailsState = {
  content: [],
  error: false,
  loading: true,
};

export const brokerTypeDetails = (
  state: IBrokerTypeDetailsState = initialState,
  action: IGetBrokerTypeDetailsActionType
): IBrokerTypeDetailsState => {
  switch (action.type) {
    case TGetBrokerTypeDetails.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case TGetBrokerTypeDetails.SUCCESS:
      return {
        ...action.payload.data,
        error: false,
        loading: false,
      };
    case TGetBrokerTypeDetails.ERROR:
      return {
        ...action.payload.data,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
