import { combineReducers } from "redux";

import { applicationCreation } from "./application/ApplicationCreationReducer";
import { applicationDelete } from "./application/ApplicationDeleteReducer";
import { applicationDetails } from "./application/ApplicationDetailsReducer";
import { applicationEdit } from "./application/ApplicationEditReducer";
import { applicationList } from "./application/ApplicationListReducer";
import { applicationPublisherList } from "./application/ApplicationPublisherListReducer";
import { applicationTopicList } from "./application/ApplicationTopicListReducer";
import { allBrokerType } from "./broker-type/AllBrokerTypeReducer";
import { brokerTypeDetails } from "./broker-type/BrokerTypeDetailsReducer";
import { brokerTypeList } from "./broker-type/BrokerTypeListReducer";
import { configFileApplication } from "./configfile/ConfigFileApplication";
import { configFileBroker } from "./configfile/ConfigFileBroker";
import { configFileGenerate } from "./configfile/ConfigFileGenerate";
import { editModal } from "./modal/EditModalReducer";
import { payloadGet } from "./payload/PayloadGetReducer";
import { payloadUpload } from "./payload/PayloadUploadReducer";
import { popupData } from "./popup/PopupReducer";
import { filterTopicList } from "./topic/FilterTopicListReducer";
import { topicBridges } from "./topic/TopicBridgesReducer";
import { topicClientList } from "./topic/TopicClientListReducer";
import { topicDelete } from "./topic/TopicDeleteReducer";
import { topicDetails } from "./topic/TopicDetailsReducer";
import { topicEdit } from "./topic/TopicEditReducer";
import { topicList } from "./topic/TopicListReducer";
import { topicMQTTClients } from "./topic/TopicMqttClientsReducer";
import { topicPayload } from "./topic/TopicPayloadReducer";
import { topologyList } from "./topology/TopologyListReducer";
import { currentUser } from "./user/CurrentUserReducer";
import { userDetails } from "./user/UserDetailsReducer";
import { userList } from "./user/UserListReducer";
import { userSetDetails } from "./user/UserSetDetailsReducer";

export const combinedReducer = combineReducers({
  allBrokerType,
  applicationCreation,
  applicationDelete,
  applicationDetails,
  applicationEdit,
  applicationList,
  applicationPublisherList,
  applicationTopicList,
  brokerTypeDetails,
  brokerTypeList,
  configFileApplication,
  configFileBroker,
  configFileGenerate,
  currentUser,
  editModal,
  filterTopicList,
  payloadGet,
  payloadUpload,
  popupData,
  topicBridges,
  topicClientList,
  topicDelete,
  topicDetails,
  topicEdit,
  topicList,
  topicMQTTClients,
  topicPayload,
  topologyList,
  userDetails,
  userList,
  userSetDetails,
});

export type RootState = ReturnType<typeof combinedReducer>;
