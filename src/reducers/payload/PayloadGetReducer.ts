import { GetPayloadTypes, TGetPayloadActions } from "../../actions/payload/get/types";

interface IPayloadGetState {
  content: any;
  error: boolean;
  loading: boolean;
}

const initialState: IPayloadGetState = {
  content: [],
  error: false,
  loading: false,
};

export const payloadGet = (
  state: IPayloadGetState = initialState,
  action: TGetPayloadActions
): IPayloadGetState => {
  switch (action.type) {
    case GetPayloadTypes.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case GetPayloadTypes.SUCCESS:
      return {
        ...action.payload.data,
        error: false,
        loading: false,
      };
    case GetPayloadTypes.ERROR:
      return {
        ...action.payload.data,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
