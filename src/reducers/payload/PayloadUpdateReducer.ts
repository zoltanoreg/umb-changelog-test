import { TUpdatePayloadActions, UpdatePayloadTypes } from "../../actions/payload/update/types";

interface IUpdatePayloadState {
  content: any;
  error: boolean;
  loading: boolean;
}

const initialState: IUpdatePayloadState = {
  content: [],
  error: false,
  loading: false,
};

export const payloadUpdate = (
  state: IUpdatePayloadState = initialState,
  action: TUpdatePayloadActions
): IUpdatePayloadState => {
  switch (action.type) {
    case UpdatePayloadTypes.REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case UpdatePayloadTypes.SUCCESS:
      return {
        ...action.payload.data,
        error: false,
        loading: false,
      };
    case UpdatePayloadTypes.ERROR:
      return {
        ...action.payload.data,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
