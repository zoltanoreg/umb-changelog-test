import { Input } from "next-components";
import styled from "styled-components";

export const STypeaheadContainer = styled.div`
  position: relative;
`;

export const STypeaheadListContainer = styled.div`
  background-color: white;
  max-height: calc(4.5 * 40px);
  overflow: scroll;
  position: absolute;
  width: 100%;
  z-index: 2;
  box-shadow: 0px 2px 8px 0px rgba(0, 0, 0, 0.12);
`;

export const STypeaheadListItem = styled.div`
  box-shadow: inset 0 -1px 0px 0px #ededed, inset 0 0 0 2px transparent;
  cursor: pointer;
  padding: 11px 16px;

  &:hover  {
    background: #fafafa;
  }
`;

export const STypeaheadInput = styled(Input)`
  width: 100%;
`;
