import { ToolMenu } from "next-components";
import styled from "styled-components";

import { CloudBar } from "../components/next-component-fork";

export const HeaderContainer = styled.div`
  height: 100vh;
  display: flex;
  justify-content: stretch;
  top: 0;
  position: absolute;
`;

export const SCloudBar = styled(CloudBar)`
  z-index: 3;
`;

export const SToolMenu = styled(ToolMenu)`
  z-index: 2;
`;
