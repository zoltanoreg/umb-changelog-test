import { Heading, Text } from "next-components";
import styled, { createGlobalStyle } from "styled-components";

import { IFlexContainerPropTypes } from "../models/IStyles";

export const GlobalStyle = createGlobalStyle`
  body {
    background-color:#f9f9f9;
    margin: 0px;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
`;

export const SContainer = styled.div`
  position: relative;
  width: calc(100% - 108px);
  margin-left: 108px;
  box-sizing: border-box;
`;

export const SPageContainer = styled.div`
  position: relative;
  padding-left: 30px;
  padding-right: 30px;
`;

export const SPageHeading = styled(Heading)`
  padding-top: 30px;
  margin-bottom: 30px;
`;

export const SOverlay = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  display: flex;
`;

export const SOverlayCenterAlign = styled(SOverlay)`
  justify-content: center;
  align-items: center;
`;

export const STallText = styled(Text)`
  display: flex;
  line-height: 40px;
`;

export const STextDataTitle = styled.div`
  display: flex;
  font-weight: bold;
  padding-right: 10px;
`;

export const STextDataValue = styled.div`
  display: flex;
  flex-grow: 0;
`;

export const SFlexContainer = styled("div")<IFlexContainerPropTypes>`
  display: flex;
  justify-content: ${(props: IFlexContainerPropTypes) => props.justifyContent};
`;

export const SFlexItem = styled.div`
  display: flex;
  align-items: center;
`;

export const SActiveLink = styled.span`
  color: #2a7fc9;
  &:hover  {
    color: #21649f;
    text-decoration: underline;
`;

export const SNextCenteredContainer = styled.div`
  left: 50%;
  max-height: 100%;
  overflow: scroll;
  position: absolute;
  top: 50%;
  transform: translate(-50%, -50%);
`;

export const STextDataValueDropdown = styled.div`
  display: flex;
  flex-grow: 0;
  min-width: 50%;
`;

export const FilterWrapper = styled.div``;
