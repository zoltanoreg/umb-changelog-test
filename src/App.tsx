import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter, withRouter } from "react-router-dom";

import { Header } from "../src/components/Header";

import { Authentication } from "./Authentication";
import { Popup } from "./components/Popup";
import { RouteAuthenticated } from "./components/RouteAuthenticated";
import { RouteUnauthenticated } from "./components/RouteUnauthenticated";
import store from "./store";
import { GlobalStyle, SContainer } from "./styles/styles";

const HeaderWithRouter = withRouter(Header);

const App: React.FC = () => (
  <Provider store={store}>
    <GlobalStyle />
    <>
      <BrowserRouter>
        {/* HEADER */}
        <HeaderWithRouter />
        {/* INFO POPUP */}
        <Popup />
        {/* BODY */}
        <SContainer>
          <Authentication
            authenticated={RouteAuthenticated}
            unauthenticated={RouteUnauthenticated}
          />
        </SContainer>
      </BrowserRouter>
    </>
  </Provider>
);

export default App;
