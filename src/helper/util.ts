import moment from "moment-timezone";

export const getFormattedDate = (value: string) =>
  moment(value)
    .tz("America/Los_Angeles")
    .format("MM.DD.YYYY HH:mm");

export const isCurrentUserRole = (currentUser: any, roles: any[]) => {
  if (currentUser && currentUser.data) {
    return roles.includes(currentUser.data.role);
  }

  return false;
};
