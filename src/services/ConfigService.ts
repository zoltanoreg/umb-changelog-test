import { queryFilterHelper } from "../helper/queryFilterHelper";

import { axiosInstance } from "./AxiosInstance";

const createPaginationQuery = (config: any) =>
  `limit=${config.limit}&offset=${config.page}&orderBy=${config.orderBy}&orderType=${config.orderType}`;

const getConfigBroker = async (config: any) => {
  const filter = queryFilterHelper(config);
  const pagination = createPaginationQuery(config);
  const query = `?${pagination}${filter}`;
  const axios = await axiosInstance();

  return axios.get(`/configFile/broker${query}`);
};

const getConfigApplication = async (config: any) => {
  const filter = queryFilterHelper(config);
  const pagination = createPaginationQuery(config);
  const query = `?${pagination}${filter}`;
  const axios = await axiosInstance();

  return axios.get(`/configFile/application${query}`);
};

const generateConfig = async () => {
  const axios = await axiosInstance();

  return axios.post(`/configFile/generate`, {});
};

const s3ProxyUrl = async (s3RawUrl: string) => {
  const escapedUrl = s3RawUrl.replace("/", "%2F");
  const axios = await axiosInstance();

  await axios({
    method: "GET",
    responseType: "blob",
    timeout: 30000,
    url: `/configFile/download/${escapedUrl}`,
  })
    .then((response: any) => {
      const blob = new Blob([response.data], { type: response.data.type });
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      const contentDisposition = response.headers["content-disposition"];
      let fileName = "unknown";
      if (contentDisposition) {
        const fileNameMatch = contentDisposition.match(/filename=(.+)/);
        if (fileNameMatch.length === 2) {
          fileName = fileNameMatch[1];
        }
      }
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();
      link.remove();
      window.URL.revokeObjectURL(url);
    })
    .catch((err: any) => {
      console.info("ERROR:", err);
    });
};

export const ConfigService = {
  generateConfig,
  getConfigApplication,
  getConfigBroker,
  s3ProxyUrl,
};
