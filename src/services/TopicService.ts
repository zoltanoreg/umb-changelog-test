import { queryFilterHelper } from "../helper/queryFilterHelper";
import { IBrokerTypeListingTableConfig } from "../models/IBrokerTypeListingTableConfig";
import { IPayload, ITopic } from "../models/ITopicConfig";

import { axiosInstance } from "./AxiosInstance";

const getQueryString = (conf: IBrokerTypeListingTableConfig) => {
  const pagination = `limit=${conf.limit}&offset=${conf.page}&orderBy=${conf.orderBy}&orderType=${conf.orderType}`;

  return `?${pagination}${queryFilterHelper(conf)}`;
};

const getTopicList = async (conf: IBrokerTypeListingTableConfig) => {
  const query = getQueryString(conf);
  const axios = await axiosInstance();

  return axios.get(`/topics${query}`);
};

const getTopicDetails = async (topicId: string) => {
  const axios = await axiosInstance();

  return axios.get(`/topics/${topicId}`);
};

const deleteTopic = async (topicId: string) => {
  const axios = await axiosInstance();

  return axios.delete(`/topics/${topicId}`);
};

const updateTopic = async (config: any, id: any) => {
  const postData = config;
  const axios = await axiosInstance();

  return axios.put(`/topics/${id}`, postData);
};

const getTopicClientList = async (conf: any) => {
  const query = getQueryString(conf);
  const axios = await axiosInstance();

  return axios.get(`/mqtt${query}`);
};

const createTopic = async (config: ITopic) => {
  const postData = {
    appClientId: config.appClientId,
    dependencies: config.dependencies,
    description: config.description,
    featureName: config.featureName,
    isSecure: config.isSecure,
    obfuscationRequirement: config.obfuscationRequirement,
    offloadType: config.offloadType,
    priority: config.priority,
    qosLevel: config.qosLevel,
    serviceName: config.serviceName,
    topicCategory: config.topicCategory,
    topicMqttClient: config.topicMqttClient,
    topicName: config.topicName,
    topicPayloadId: config.topicPayloadId,
    topicType: config.topicType,
    ttl: config.ttl,
    versionId: config.versionId,
  };
  const axios = await axiosInstance();

  return axios.post(`/topics/`, postData);
};

const uploadPayload = async (config: IPayload) => {
  const requestOptions = { headers: { "Content-Type": "multipart/form-data" } };

  const formData = new FormData();
  formData.append("payloadFormat", config.payloadFormat);
  formData.append("payload", config.payloadFile);
  if (config.currentPayloadId) {
    formData.append("currentPayloadId", config.currentPayloadId);
  }
  const axios = await axiosInstance();

  return axios.post(`/topics/payload`, formData, requestOptions);
};

const getPayload = async (payloadId: any) => {
  const axios = await axiosInstance();

  return axios.get(`/topics/payload/${payloadId}`);
};

const updatePayload = async (config: any) => {
  const axios = await axiosInstance();

  return axios.post(`/topics/changepayload`, config);
};

const getBridges = async (conf: any) => {
  const pagination = `limit=${conf.limit}&offset=${conf.page}&orderBy=${conf.orderBy}&orderType=${conf.orderType}`;
  const query = `?${pagination}${queryFilterHelper(conf)}`;
  const axios = await axiosInstance();

  return axios.get(`/topics/bridges/${query}`);
};

export const TopicService = {
  createTopic,
  deleteTopic,
  getBridges,
  getPayload,
  getTopicClientList,
  getTopicDetails,
  getTopicList,
  updatePayload,
  updateTopic,
  uploadPayload,
};
