import { queryFilterHelper } from "../helper/queryFilterHelper";
import { IApplicationListingTableConfig } from "../models/IAppListingTableConfig";
import { OrderType } from "../models/ITableConfig";

import { axiosInstance } from "./AxiosInstance";

const getQueryString = (conf: IApplicationListingTableConfig) => {
  const pagination = `limit=${conf.limit}&offset=${conf.page}&orderBy=${conf.orderBy}&orderType=${conf.orderType}`;

  return `?${pagination}${queryFilterHelper(conf)}`;
};

const getApplicationList = async (conf: IApplicationListingTableConfig) => {
  const query = getQueryString(conf);

  const axios = await axiosInstance();

  return axios.get(`/applications${query}`);
};

const getApplicationDetails = async (appId: string) => {
  const axios = await axiosInstance();

  return axios.get(`/applications/${appId}`);
};

const createApplication = async (config: any) => {
  const postData = {
    appBrokerType: config.brokerType.brokerTypeId,
    appName: config.appName,
    appOwner: config.user.userId,
    appVersion: config.appVersion,
    authenticationType: config.authenticationType,
    connRetryDelaySec: config.connRetryDelaySec,
    numConnRetries: config.numConnRetries,
    password: config.password,
    pathClientCa: config.pathClientCa,
    pathToCa: config.pathToCa,
    status: "Active",
    systemName: config.systemName,
    userName: config.userName,
  };

  const axios = await axiosInstance();

  return axios.post(`/applications/`, postData);
};

const updateApplication = async (config: any) => {
  const postData = {
    appBrokerType: config.brokerType.brokerTypeId,
    appName: config.appName,
    appOwner: config.user.userId,
    appVersion: config.appVersion,
    authenticationType: config.authenticationType,
    connRetryDelaySec: config.connRetryDelaySec,
    numConnRetries: config.numConnRetries,
    password: config.password,
    pathClientCa: config.pathClientCa,
    pathToCa: config.pathToCa,
    status: "Active",
    systemName: config.systemName,
    userName: config.userName,
  };

  const axios = await axiosInstance();

  return axios.put(`/applications/${config.appClientId}`, postData);
};

const deleteApplication = async (applicationId: any) => {
  const axios = await axiosInstance();

  return axios.delete(`/applications/${applicationId}`);
};

const getApplicationTopicList = async (conf: any) => {
  const query = getQueryString(conf);

  const axios = await axiosInstance();

  return axios.get(`/mqtt${query}`);
};

export const ApplicationService = {
  createApplication,
  deleteApplication,
  getApplicationDetails,
  getApplicationList,
  getApplicationTopicList,
  updateApplication,
};
