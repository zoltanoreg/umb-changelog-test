/* istanbul ignore file */
import Axios, { AxiosInstance } from "axios";
import jwt from "jsonwebtoken";

import { clearSession, isExpired, redirect } from "../helper/sessionHelper";
import { UserService } from "../services/UserService";

let instance: AxiosInstance;
export const axiosInstance = async () => {
  if (!instance) {
    const fetchConfig = await fetch(`${process.env.PUBLIC_URL}/config.json`);
    const configJson = await fetchConfig.json();

    instance = Axios.create({
      baseURL: configJson.apiUrl,
    });

    instance.interceptors.request.use(
      async (config: any) => {
        if (process.env.NODE_ENV === "test") {
          return config;
        }

        const refreshToken = localStorage.getItem("UMB_RT") || "";
        const accessToken = localStorage.getItem("UMB_AT") || "";
        const clientId = localStorage.getItem("UMB_CLIENT_ID") || "";
        const pacAppClientId = localStorage.getItem("UMB_PAC_APP_CLIENT_ID") || "";
        let accessDetails: any;

        try {
          accessDetails = jwt.decode(accessToken) || {};
        } catch (e) {
          console.info("JWT malformed", e);
          redirect();
        }

        if (process.env.REACT_APP_ENV === "fethr") {
          config.headers.Authorization = `Bearer ${accessToken}`;

          return config;
        }

        if ((!accessDetails.exp || isExpired(accessDetails.exp)) && !refreshToken) {
          redirect();
        }

        if(refreshToken && !clientId && !pacAppClientId) {
          redirect();
        }

        if (!accessDetails.exp || isExpired(accessDetails.exp)) {
          return UserService.refreshToken(clientId || pacAppClientId, refreshToken)
            .then(({ data }: { data: any }) => {
              const expiration =
                +(new Date().getTime() / 1000).toFixed(0) + +data.AuthenticationResult.ExpiresIn;

              if (
                data.AuthenticationResult.AccessToken === "" ||
                isExpired(expiration.toString())
              ) {
                clearSession();

                redirect();

                return;
              }

              localStorage.setItem("UMB_AT", data.AuthenticationResult.AccessToken);

              config.headers.Authorization = `Bearer ${data.AuthenticationResult.AccessToken}`;

              return config;
            })
            .catch((error: any) => {
              console.error("Refresh failure:", error);
              clearSession();

              redirect();
            });
        }

        config.headers.Authorization = `Bearer ${accessToken}`;

        // Token is valid
        return config;
      },
      async (error: any) => Promise.reject(error)
    );

    // INTERCEPT AXIOS AND HANDLE ERROR
    instance.interceptors.response.use(
      response => response,
      async error => {
        // Redirect anything with Unauthorized response
        if (error.response.status === 401) {
          redirect();
        }

        return Promise.reject(error.response && error.response.data);
      }
    );
  }

  return instance;
};
