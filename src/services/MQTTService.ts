import { queryFilterHelper } from "../helper/queryFilterHelper";

import { axiosInstance } from "./AxiosInstance";

const createSubscriber = async (config: any) => {
  const postData = {
    appClientId: config.appClientId,
    bridge: config.bridge,
    clientType: "Subscriber",
    topicId: config.topic.topicId,
  };

  const axios = await axiosInstance();

  return axios.post(`/mqtt/`, postData);
};

const createPublisher = async (config: any) => {
  const postData = {
    appClientId: config.appClientId,
    clientType: "Publisher",
    retainRequired: config.retainRequired,
    topicId: config.topicId,
  };

  const axios = await axiosInstance();

  return axios.post(`/mqtt/`, postData);
};

const getMqttClients = async (conf: any) => {
  const filter = queryFilterHelper(conf);

  const axios = await axiosInstance();

  return axios.get(`/mqtt/?${filter}`);
};

const deleteAssociation = async (topicMqttClientId: string) => {
  const axios = await axiosInstance();

  return axios.delete(`/mqtt/${topicMqttClientId}`);
};

const deleteAssociationBulk = async (data: { mqttIds: string[] }) => {
  const axios = await axiosInstance();

  return axios.put(`/mqttbulkdelete`, data);
};

export const MQTTService = {
  createPublisher,
  createSubscriber,
  deleteAssociation,
  deleteAssociationBulk,
  getMqttClients,
};
