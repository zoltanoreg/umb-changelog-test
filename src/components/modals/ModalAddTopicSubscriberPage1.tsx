import { Text } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getTopicList } from "../../actions/topic/get-list/actions";
import { getMqttClients } from "../../actions/topic/get-mqtt-clients/actions";
import { topicStatusEnum } from "../../models/ITopicConfig";
import { RootState } from "../../reducers";
import { SFlexItem, SVerticalSpacer } from "../../styles/styles";
import { Checkbox, CheckboxOptions } from "../Checkbox";
import { StatusIcon } from "../StatusIcon";
import { Typeahead } from "../Typeahead";

// tslint:disable-next-line: cyclomatic-complexity
export const ModalAddTopicSubscriberPage1 = (props: any) => {
  const { applicationDetails, topicList, topicMQTTClients } = useSelector((state: RootState) => ({
    applicationDetails: state.applicationDetails,
    topicList: state.topicList,
    topicMQTTClients: state.topicMQTTClients,
  }));

  const dispatch = useDispatch();

  const [updatedConfig, setUpdatedConfig] = useState();

  useEffect(() => {
    if (props.config && updatedConfig && props.onChange) {
      props.onChange(updatedConfig);
    }
  }, [updatedConfig]);

  useEffect(() => {
    // User selects item from list
    dispatch(
      getMqttClients({
        filter: {
          clientType: "PUBLISHER",
          topicId: props.config.topic.topicId,
        },
      })
    );
  }, [props.config.topic.topicId]);

  useEffect(() => {
    if (!props.config.topic.topicId) {
      return undefined;
    }
    if (
      (topicMQTTClients.content.length === 1 &&
        topicMQTTClients.content[0].appClient.appBrokerType.brokerType ===
          applicationDetails.content.appBrokerType.brokerType) ||
      (topicMQTTClients.content.length === 0 &&
        props.config.topic.brokerType === applicationDetails.content.appBrokerType.brokerType)
    ) {
      setUpdatedConfig({ ...props.config, bridgeDisabled: false });
    } else {
      setUpdatedConfig({ ...props.config, bridgeDisabled: true, bridge: true });
    }
  }, [topicMQTTClients.content]);

  const handleCreateChipLabel = ({ value }: { value: any }) => {
    let label = "";

    if (value && value.topicName) {
      label = value.topicName;
      if (value.topicStatus === topicStatusEnum.ONHOLD) {
        label += " ---- Topic is on hold ⚠️";
      }
      if (value.topicStatus === topicStatusEnum.DRAFT) {
        label += " ---- Topic is a draft ⚠️";
      }
    }

    return label;
  };

  const checkStatus = () =>
    (props.config.topic.topicStatus === topicStatusEnum.DRAFT &&
      props.config.appClientId === props.config.topic.appClientId) ||
    props.config.topic.topicStatus === topicStatusEnum.ACTIVE;

  return (
    <>
      <Text>
        My application:{" "}
        <span>{applicationDetails.content && applicationDetails.content.appName}</span>
      </Text>
      <Text>
        Broker type:{" "}
        <span>
          {applicationDetails.content && applicationDetails.content.appBrokerType.brokerType}
        </span>
      </Text>
      <Text variant="captionText" className="ta-modal-input-caption-topic">
        Topic{" "}
        <StatusIcon
          className={
            props.config && props.config.topic && props.config.topic.topicId && checkStatus()
              ? "ta-status-icon-success"
              : "ta-status-icon-fail"
          }
          check={props.config && props.config.topic && props.config.topic.topicId && checkStatus()}
        />
      </Text>

      <div className="ta-modal-input-topic-name">
        <Typeahead
          pageSelector="addTopicSubscriber"
          autoFillOnExactMatchOnly={{
            key: "topicName",
            rule: props.autoFillOnExactMatchOnly,
          }}
          disabled={props.disabled}
          config={{
            default: {
              filter: {
                appClientId: undefined,
                brokerType: "",
                topicId: undefined,
                topicName: "",
                topicStatus: "",
              },
              orderBy: "topicName",
              statuses: [topicStatusEnum.ACTIVE, topicStatusEnum.DRAFT, topicStatusEnum.ONHOLD],
            },
            setFilter: (topicName: string) => ({
              topicName,
            }),
          }}
          suggestion={{
            getList: (config: any) => dispatch(getTopicList(config)),
            list: topicList,
            select: (topic: any) => `${topic.topicName}`,
          }}
          name="topic"
          chipLabel={handleCreateChipLabel}
          value={props.config && props.config.topic}
          hasSelected={(p: any) => p && p.topicId === undefined}
          valueToSetOnLoad="topicName"
          handleTypeahead={topic => {
            setUpdatedConfig({
              ...props.config,
              topic: {
                appClientId:
                  topic.appClient && topic.appClient.appClientId
                    ? topic.appClient.appClientId
                    : topic.appClientId,
                brokerType: topic.appClient && topic.appClient.appBrokerType.brokerType,
                topicId: topic.topicId,
                topicName: topic.topicName,
                topicStatus: topic.status,
              },
            });
          }}
          pattern="{topicName} - {status}"
        />
      </div>

      <SFlexItem>
        <div className="ta-modal-input-createBridge">
          <Checkbox
            disabled={props.config.bridgeDisabled || !props.config.topic.topicId}
            onClick={() => {
              setUpdatedConfig({ ...props.config, bridge: !props.config.bridge });

              return;
            }}
            marked={props.config.bridge ? CheckboxOptions.selected : CheckboxOptions.unselected}
          />
        </div>
        <SVerticalSpacer />
        <Text variant="captionText" className="ta-modal-input-caption-createBridge">
          I want to create a bridge
        </Text>
      </SFlexItem>
    </>
  );
};
