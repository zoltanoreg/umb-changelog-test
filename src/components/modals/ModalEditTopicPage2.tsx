import { NumberInput, Text, Textarea } from "next-components";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import { offloadType, qosEnum, topicStatusEnum } from "../../models/ITopicConfig";
import { APPLICATION_OWNER } from "../../models/UserTypes";
import { SFlexItem, SVerticalSpacer } from "../../styles/styles";
import { Checkbox, CheckboxOptions } from "../Checkbox";

import { DropdownInput } from "./Dropdownlnput";

export const ModalEditTopicPage2 = (props: any) => {
  const { currentUser } = useSelector((state: any) => ({
    currentUser: state.currentUser,
  }));

  const [cursorLocation, setCursorLocation] = useState(0);
  const [activeInput, setActiveInput] = useState();
  const [updatedConfig, setUpdatedConfig] = useState();

  const priorities: any[] = [
    {
      label: <span className="ta-dropdown-priority-1">1</span>,
      value: "1",
    },
    {
      label: <span className="ta-dropdown-priority-2">2</span>,
      value: "2",
    },
  ];

  const qosLevelList: any[] = [
    {
      label: <span className="ta-dropdown-qosLevel-0">{qosEnum.FIRE_AND_FORGET}</span>,
      value: qosEnum.FIRE_AND_FORGET,
    },
    {
      label: <span className="ta-dropdown-qosLevel-1">{qosEnum.ATLEAST_ONCE}</span>,
      value: qosEnum.ATLEAST_ONCE,
    },
  ];

  const offloadTypeList: any[] = [
    {
      label: <span className="ta-dropdown-offloadType-deferred">{offloadType.DEFERRED}</span>,
      value: offloadType.DEFERRED,
    },
    {
      label: <span className="ta-dropdown-offloadType-live">{offloadType.LIVE}</span>,
      value: offloadType.LIVE,
    },
  ];

  const handleTextInput = (e: any, field: string, maxChar?: number) => {
    setActiveInput(e.target);
    setCursorLocation(e.target.selectionStart);
    if (e.target.value === " ") {
      return;
    }
    if (e.target.value.length > (maxChar || 250)) {
      return;
    }
    setUpdatedConfig({ ...props.config, [field]: e.target.value });
  };

  const handleNumberInput = (value: any, field: string) => {
    let fieldValue = value;
    if (isNaN(value)) {
      fieldValue = 0;
    }
    if (value < 0) {
      fieldValue = Math.abs(value);
    }
    setUpdatedConfig({ ...props.config, [field]: fieldValue });
  };

  const handleCheckboxClick = (e: any) => {
    setUpdatedConfig({ ...props.config, isSecure: !props.config.isSecure });
  };

  const handlePriorityDropdown = (value: any) => {
    setUpdatedConfig({ ...props.config, priority: value });
  };

  const handleQosDropdown = (value: any) => {
    setUpdatedConfig({ ...props.config, qosLevel: value });
  };

  const handleOffloadChange = (value: any) => {
    setUpdatedConfig({ ...props.config, offloadType: value });
  };

  useEffect(() => {
    if (props.config && updatedConfig) {
      props.onChange(updatedConfig);
    }
  }, [updatedConfig]);

  useEffect(() => {
    if (activeInput && cursorLocation) {
      activeInput.setSelectionRange(cursorLocation, cursorLocation);
    }
  }, [props.config]);

  return (
    <>
      <Text className="ta-modal-text-topic">
        Topic: <span>{props.config && props.config.topicName}</span>
      </Text>

      <DropdownInput
        label="Priority:"
        name="priority"
        onChange={handlePriorityDropdown}
        options={priorities}
        selected={props.config && props.config.priority}
        disabled={
          currentUser.data.role === APPLICATION_OWNER &&
          props.config.status !== topicStatusEnum.DRAFT
        }
      />

      <SFlexItem>
        <div className="ta-modal-input-secure">
          <Checkbox
            onClick={handleCheckboxClick}
            marked={props.config.isSecure ? CheckboxOptions.selected : CheckboxOptions.unselected}
            disabled={
              currentUser.data.role === APPLICATION_OWNER &&
              props.config.status !== topicStatusEnum.DRAFT
            }
          />
        </div>
        <SVerticalSpacer />
        <Text variant="captionText" className="ta-modal-input-caption-secure">
          Secure
        </Text>
      </SFlexItem>

      <DropdownInput
        label="Offload type:"
        name="offload"
        onChange={handleOffloadChange}
        options={offloadTypeList}
        selected={props.config && props.config.offloadType}
        disabled={
          currentUser.data.role === APPLICATION_OWNER &&
          props.config.status !== topicStatusEnum.DRAFT
        }
      />

      <Text variant="captionText" className="ta-modal-input-caption-ttl">
        TTL:
      </Text>
      <NumberInput
        value={props.config.ttl}
        min={0}
        disabled={
          currentUser.data.role === APPLICATION_OWNER &&
          props.config.status !== topicStatusEnum.DRAFT
        }
        onChange={(value: any) => {
          handleNumberInput(value, "ttl");
        }}
      />

      <DropdownInput
        label="QoS level:"
        name="qosLevel"
        onChange={handleQosDropdown}
        options={qosLevelList}
        selected={props.config && props.config.qosLevel}
        disabled={
          currentUser.data.role === APPLICATION_OWNER &&
          props.config.status !== topicStatusEnum.DRAFT
        }
      />

      <Text variant="captionText" className="ta-modal-input-caption-description">
        Description:
      </Text>
      <Textarea
        className="ta-modal-input-description"
        placeholder="Description"
        value={props.config.description}
        onChange={(e: any) => {
          handleTextInput(e, "description", 250);
        }}
        disabled={
          currentUser.data.role === APPLICATION_OWNER &&
          props.config.status !== topicStatusEnum.DRAFT
        }
      />
    </>
  );
};
