import { ButtonPrimary } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { createSubscriber } from "../../actions/application/create-subscriber/actions";
import { getApplicationTopicList } from "../../actions/application/get-topics/actions";
import { topicStatusEnum } from "../../models/ITopicConfig";
import { SFlexContainer, SSpacer } from "../../styles/styles";

import { ModalAddTopicSubscriberPage1 } from "./ModalAddTopicSubscriberPage1";
import { ModalCreateBase } from "./ModalCreateBase";

export const ModalAddTopicSubscriber = (props: any) => {
  const dispatch = useDispatch();

  const [config, setConfig] = useState({
    appClientId: "",
    bridge: true,
    bridgeDisabled: true,
    topic: {
      appClientId: undefined,
      brokerType: "",
      topicId: undefined,
      topicName: "",
      topicStatus: "",
    },
  });

  const [hasAppClient, setHasAppClient] = useState(false);
  const [topicExistsAndSuitableToSubscribe, setTopicExistsAndSuitableToSubscribe] = useState(false);

  const { applicationDetails, editModal } = useSelector((state: any) => ({
    applicationDetails: state.applicationDetails,
    editModal: state.editModal,
  }));

  const handleSubmit = () => {
    dispatch(
      createSubscriber(config, () => {
        dispatch(getApplicationTopicList(editModal.topicFilter));
      })
    );
    setConfig({ ...config });
  };

  useEffect(() => {
    if (applicationDetails.content.appClientId !== undefined) {
      setConfig({
        ...config,
        appClientId: applicationDetails.content.appClientId,
      });
    }
  }, [applicationDetails.content.appClientId]);

  useEffect(() => {
    setHasAppClient(config.appClientId !== "");
    setTopicExistsAndSuitableToSubscribe(
      config.topic.topicName !== "" &&
        !!config.topic.topicStatus &&
        ((config.topic.topicStatus === topicStatusEnum.DRAFT &&
          config.appClientId === config.topic.appClientId) ||
          config.topic.topicStatus === topicStatusEnum.ACTIVE)
    );
  }, [config]);

  // Set back to default bridge checked status on typeahead clear
  useEffect(() => {
    if (!config.topic.topicId && !config.bridge) {
      setConfig({ ...config, bridge: true });
    }
  }, [config.topic.topicId]);

  return (
    <ModalCreateBase title="Add new topic to subscribe">
      <ModalAddTopicSubscriberPage1
        appData={editModal.appData}
        config={{ ...config }}
        onChange={(updatedConfig: any) => {
          setConfig(updatedConfig);
        }}
      />

      <SSpacer height="50px" />

      <SFlexContainer justifyContent="flex-end">
        <ButtonPrimary
          className="ta-modal-save-button"
          onClick={handleSubmit}
          disabled={!(hasAppClient && topicExistsAndSuitableToSubscribe)}
        >
          Save
        </ButtonPrimary>
      </SFlexContainer>
    </ModalCreateBase>
  );
};
