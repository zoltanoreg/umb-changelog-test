import { ButtonPrimary } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { createPublisher } from "../../actions/application/create-publisher/actions";
import { getApplicationTopicList } from "../../actions/application/get-topics/actions";
import { createTopic } from "../../actions/topic/create/actions";
import {
  offloadType,
  payloadFormatEnum,
  qosEnum,
  topicStatusEnum,
} from "../../models/ITopicConfig";
import { SFlexContainer } from "../../styles/styles";

import { ModalAddTopicPublisherPage1 } from "./ModalAddTopicPublisherPage1";
import { ModalCreateBase } from "./ModalCreateBase";

export const ModalAddTopicPublisher = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [isEditing, setIsEditing] = useState(false);
  const { editModal, payloadUpload } = useSelector((state: any) => ({
    editModal: state.editModal,
    payloadUpload: state.payloadUpload,
  }));

  const [currentPage, setCurrentPage] = useState(0);

  const [config, setConfig] = useState({
    description: "",
    featureName: "",
    isSecure: false,
    offloadType: offloadType.DEFERRED,
    payloadConfig: {
      payloadFile: File,
      payloadFileName: "",
      payloadFormat: payloadFormatEnum.JSON,
      payloadType: "",
      topicPayloadId: "",
    },
    priority: "1",
    qosLevel: qosEnum.ATLEAST_ONCE,
    retainRequired: true,
    schema: "",
    topic: {
      appClientId: undefined,
      topicId: undefined,
      topicName: "",
      topicStatus: "",
    },
    ttl: 0,
  });

  const [validPage, setValidPage] = useState([false]);

  const handlePageChange = (updatedConfig: any) => {
    setConfig(updatedConfig);
  };

  const handleSetValidPage = (index: number, value: boolean) => {
    const updatedPage = [...validPage];
    updatedPage[index] = value;
    setValidPage(updatedPage);
  };

  const checkValidity = () => {
    const isTopicNotSuitableToPublish =
      config.topic.topicStatus === topicStatusEnum.ONHOLD ||
      (config.topic.topicStatus === topicStatusEnum.DRAFT &&
        config.topic.appClientId !== editModal.appData.appClientId);
    if (
      config.topic &&
      config.topic.topicId &&
      !isTopicNotSuitableToPublish
    ) {
      handleSetValidPage(0, true);
    } else {
      handleSetValidPage(0, false);
    }
  };

  const handleSubmitExistingTopic = () => {
    dispatch(
      createPublisher(
        {
          appClientId: editModal.appData.appClientId,
          retainRequired: config.retainRequired,
          topicId: config.topic.topicId,
        },
        () => {
          dispatch(getApplicationTopicList(editModal.topicFilter));
        }
      )
    );
  };

  useEffect(() => {
    checkValidity();
  }, [config]);

  return (
    <ModalCreateBase title="Add new topic to publish">
      {currentPage === 0 && (
        <ModalAddTopicPublisherPage1
          appData={editModal.appData}
          config={{ ...config }}
          onChange={(updatedConfig: any) => {
            handlePageChange(updatedConfig);
          }}
        />
      )}
      <SFlexContainer justifyContent="space-between">
        <SFlexContainer justifyContent="flex-start">
          {currentPage !== 0 && (
            <ButtonPrimary
              className="ta-modal-previous-button"
              onClick={() => {
                setCurrentPage(currentPage - 1);
              }}
              disabled={currentPage === 0 || isEditing}
            >
              Previous
            </ButtonPrimary>
          )}
        </SFlexContainer>
        <SFlexContainer justifyContent="flex-end">
          <ButtonPrimary
            className="ta-modal-next-button"
            onClick={handleSubmitExistingTopic}
            disabled={!validPage[currentPage]}
          >
            Save
          </ButtonPrimary>
        </SFlexContainer>
      </SFlexContainer>
    </ModalCreateBase>
  );
};
