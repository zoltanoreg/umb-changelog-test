import Editor, { monaco } from "@monaco-editor/react";
import registerLanguage from "monaco-proto-lint";
import { ButtonPrimary } from "next-components";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";

import { updatePayload } from "../../actions/payload/update/actions";
import { getTAClass, TA_TYPES } from "../../helper/taHelper";
import { payloadFormatEnum } from "../../models/ITopicConfig";
import { SFlexContainer } from "../../styles/styles";

interface IPayloadSchemaEditor {
  payloadData: {
    payloadFormat: any;
    payloadSchema: any;
  };
  payloadId?: string;
  callback(response: any): void;
  cancel(): void;
}

export const PayloadSchemaEditor = ({
  payloadId,
  payloadData,
  cancel,
  callback,
}: IPayloadSchemaEditor) => {
  const [payloadSchema, setPayloadSchema] = useState(payloadData.payloadSchema);
  const editorRef = useRef();
  const dispatch = useDispatch();

  useEffect(() => {
    monaco
      .init()
      .then(monacoInst => {
        monacoInst.languages.json.jsonDefaults.setDiagnosticsOptions({
          enableSchemaRequest: true,
          validate: true,
        });

        // Register "protobuf" as language and theme
        registerLanguage(monacoInst);
      })
      .catch(error => {
        console.error("An error occurred during initialization of Monaco: ", error);
      });
  }, []);

  const handleEditorDidMount = (_: any, editor: any) => {
    editorRef.current = editor;
  };

  const handleUpload = () => {
    // Const syntaxErrors = (window as any).monaco.editor.getModelMarkers({}); // Works only for JSON

    const editorValue = (editorRef as any).current
      ? (editorRef as any).current.getValue()
      : payloadData.payloadSchema;

    dispatch(
      updatePayload(
        {
          currentPayloadId: payloadId,
          payload: editorValue,
          payloadFormat: payloadData.payloadFormat,
        },
        callback
      )
    );
  };

  const editorOptions = {
    editorDidMount: handleEditorDidMount,
    height: 400,
    language: payloadData.payloadFormat === payloadFormatEnum.PROTOBUF ? "protobuf" : "json",
    options: {
      minimap: {
        enabled: false,
      },
      selectOnLineNumbers: true,
    },
    theme: "vs-dark",
    value: payloadSchema,
  };

  return (
    <>
      <Editor {...editorOptions} />

      <SFlexContainer justifyContent="space-between">
        <SFlexContainer justifyContent="flex-start">
          <ButtonPrimary
            className={getTAClass("payloadSchemaEditor", TA_TYPES.BUTTON, "cancel")}
            onClick={cancel}
          >
            Cancel
          </ButtonPrimary>
        </SFlexContainer>
        <SFlexContainer justifyContent="flex-end">
          <ButtonPrimary
            className={getTAClass("payloadSchemaEditor", TA_TYPES.BUTTON, "update")}
            onClick={handleUpload}
          >
            Update
          </ButtonPrimary>
        </SFlexContainer>
      </SFlexContainer>
    </>
  );
};
