import { ButtonPrimary } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { createPublisher } from "../../actions/application/create-publisher/actions";
import { getApplicationTopicList } from "../../actions/application/get-topics/actions";
import { createTopic } from "../../actions/topic/create/actions";
import {
  offloadType,
  payloadFormatEnum,
  qosEnum,
  retainRequiredEnum,
  topicStatusEnum,
} from "../../models/ITopicConfig";
import { SFlexContainer } from "../../styles/styles";

import { ModalCreateBase } from "./ModalCreateBase";
import { ModalCreateTopicPage1 } from "./ModalCreateTopicPage1";
import { ModalCreateTopicPage2 } from "./ModalCreateTopicPage2";

export const ModalCreateTopic = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [isEditing, setIsEditing] = useState(false);
  const { editModal, payloadUpload } = useSelector((state: any) => ({
    editModal: state.editModal,
    payloadUpload: state.payloadUpload,
  }));

  const [currentPage, setCurrentPage] = useState(0);

  const [config, setConfig] = useState({
    appData: {
      appClientId: "",
      appName: "",
      appVersion: "",
    },
    description: "",
    featureName: "",
    isSecure: false,
    offloadType: offloadType.DEFERRED,
    payloadConfig: {
      payloadFile: File,
      payloadFileName: "",
      payloadFormat: payloadFormatEnum.JSON,
      payloadType: "",
      topicPayloadId: "",
    },
    priority: "1",
    qosLevel: qosEnum.ATLEAST_ONCE,
    retainRequired: retainRequiredEnum.YES,
    schema: "",
    topic: {
      topicId: undefined,
      topicName: "",
      topicStatus: "",
    },
    ttl: 0,
  });

  const [validPage, setValidPage] = useState([false, true]);
  const [isTopicNameValid, setIsTopicNameValid] = useState(false);

  const handlePageChange = (updatedConfig: any) => {
    setConfig(updatedConfig);
  };

  const handleSetValidPage = (index: number, value: boolean) => {
    const updatedPage = [...validPage];
    updatedPage[index] = value;
    setValidPage(updatedPage);
  };

  const checkValidity = () => {
    const isTopicOnHold = config.topic.topicStatus === topicStatusEnum.ONHOLD;

    if (
      config.retainRequired &&
      config.topic.topicName &&
      !isTopicOnHold &&
      isTopicNameValid &&
      config.appData.appClientId
    ) {
      handleSetValidPage(0, true);
    } else {
      handleSetValidPage(0, false);
    }
  };

  const handleSubmitNewTopic = () => {
    dispatch(
      createTopic(
        {
          appClientId: config.appData.appClientId,
          dependencies: "",
          description: config.description,
          featureName: config.featureName,
          isSecure: config.isSecure,
          obfuscationRequirement: "",
          offloadType: config.offloadType,
          priority: config.priority,
          qosLevel: config.qosLevel,
          serviceName: "",
          topicCategory: "",
          topicMqttClient: {
            accessLevel: "",
            frequency: "",
            retainRequired: config.retainRequired === retainRequiredEnum.YES,
          },
          topicName: config.topic && config.topic.topicName,
          topicPayloadId: config.payloadConfig.topicPayloadId,
          topicType: "",
          ttl: config.ttl,
          versionId: "1.0",
        },
        (topicId: any) => {
          history.push(`/topics/${topicId}`);
        }
      )
    );
  };

  useEffect(() => {
    checkValidity();
  }, [config, isTopicNameValid]);

  return (
    <ModalCreateBase title="Create new topic">
      {currentPage === 0 && (
        <ModalCreateTopicPage1
          appData={editModal.appData}
          config={{ ...config }}
          onChange={(updatedConfig: any) => {
            handlePageChange(updatedConfig);
          }}
          setIsTopicNameValid={setIsTopicNameValid}
          isTopicNameValid={isTopicNameValid}
        />
      )}
      {currentPage === 1 && (
        <ModalCreateTopicPage2
          appData={editModal.appData}
          config={{ ...config }}
          onChange={(updatedConfig: any) => {
            handlePageChange(updatedConfig);
          }}
          isEditing={isEditing}
          onToggleEditing={setIsEditing}
        />
      )}
      <SFlexContainer justifyContent="space-between">
        <SFlexContainer justifyContent="flex-start">
          {currentPage !== 0 && (
            <ButtonPrimary
              className="ta-modal-previous-button"
              onClick={() => {
                setCurrentPage(currentPage - 1);
              }}
              disabled={currentPage === 0 || isEditing}
            >
              Previous
            </ButtonPrimary>
          )}
        </SFlexContainer>
        <SFlexContainer justifyContent="flex-end">
          {currentPage === 0 ? (
            <ButtonPrimary
              className="ta-modal-next-button"
              onClick={() => {
                setCurrentPage(currentPage + 1);
              }}
              disabled={!validPage[currentPage]}
            >
              Next
            </ButtonPrimary>
          ) : (
            <ButtonPrimary
              className="ta-modal-save-button"
              onClick={handleSubmitNewTopic}
              disabled={payloadUpload.loading || isEditing || !validPage[currentPage]}
            >
              Save
            </ButtonPrimary>
          )}
        </SFlexContainer>
      </SFlexContainer>
    </ModalCreateBase>
  );
};
