import { Text } from "next-components";
import React, { useEffect, useState } from "react";

import { topicStatusEnum } from "../../models/ITopicConfig";
import { SFlexItem, SSpacer, SVerticalSpacer } from "../../styles/styles";
import { Checkbox, CheckboxOptions } from "../Checkbox";
import { StatusIcon } from "../StatusIcon";
import { TypeaheadTopics } from "../TypeaheadTopics";

export const ModalAddTopicPublisherPage1 = (props: any) => {
  const [updatedConfig, setUpdatedConfig] = useState();

  const handleRetainCheckbox = () => {
    setUpdatedConfig({ ...props.config, retainRequired: !props.config.retainRequired });
  };

  const handleTypeaheadInput = (topicData: any) => {
    setUpdatedConfig({ ...props.config, topic: { ...topicData } });
  };

  useEffect(() => {
    if (props.config && updatedConfig) {
      props.onChange(updatedConfig);
    }
  }, [updatedConfig]);

  return (
    <>
      <Text>
        My application: <span>{props.appData && props.appData.appName}</span>
      </Text>

      <Text>
        Application broker type:{" "}
        <span>
          {props.appData && props.appData.appBrokerType && props.appData.appBrokerType.brokerType}
        </span>
      </Text>

      <Text variant="captionText" className="ta-modal-input-caption-topic-name">
        Topic name{" "}
        <StatusIcon
          className={
            props.config &&
            props.config.topic &&
            props.config.topic.topicId &&
            ((props.config.topic.topicStatus === topicStatusEnum.DRAFT &&
              props.appData.appClientId === props.config.topic.appClientId) ||
              props.config.topic.topicStatus === topicStatusEnum.ACTIVE)
              ? "ta-status-icon-success"
              : "ta-status-icon-fail"
          }
          check={
            props.config &&
            props.config.topic &&
            props.config.topic.topicId &&
            ((props.config.topic.topicStatus === topicStatusEnum.DRAFT &&
              props.appData.appClientId === props.config.topic.appClientId) ||
              props.config.topic.topicStatus === topicStatusEnum.ACTIVE)
          }
        />
      </Text>
      <TypeaheadTopics
        pageSelector="addTopicPublisher"
        className="ta-modal-input-topic-name"
        value={props.config && props.config.topic}
        onChange={handleTypeaheadInput}
        autoFillOnExactMatchOnly
      />

      <SFlexItem>
        <div className="ta-modal-input-retainRequired">
          <Checkbox
            onClick={handleRetainCheckbox}
            marked={
              props.config.retainRequired ? CheckboxOptions.selected : CheckboxOptions.unselected
            }
          />
        </div>
        <SVerticalSpacer />
        <Text variant="captionText" className="ta-modal-input-caption-retainRequired">
          Retain required
        </Text>
      </SFlexItem>
      <SSpacer height="50px" />
    </>
  );
};
