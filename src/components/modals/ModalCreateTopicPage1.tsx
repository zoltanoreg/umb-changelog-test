import { NumberInput, Text, Textarea } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getFilteredTopicList } from "../../actions/topic/filter-list/actions";
import { getTaClassedOptions } from "../../helper/dropdown";
import { useDebounce } from "../../hooks/useDebounce";
import { OrderType } from "../../models/ITableConfig";
import { offloadType, qosEnum, topicStatusEnum } from "../../models/ITopicConfig";
import { RootState } from "../../reducers";
import { SFlexItem, SVerticalSpacer } from "../../styles/styles";
import { Checkbox, CheckboxOptions } from "../Checkbox";
import { StatusIcon } from "../StatusIcon";
import { TypeaheadApplication } from "../TypeaheadApplication";

import { DropdownInput } from "./Dropdownlnput";
import { TextInput } from "./TextInput";

export const ModalCreateTopicPage1 = (props: any) => {
  const [cursorLocation, setCursorLocation] = useState(0);
  const [activeInput, setActiveInput] = useState();
  const [updatedConfig, setUpdatedConfig] = useState();
  const priorities: any[] = [{ label: "1", value: "1" }, { label: "2", value: "2" }];

  const dispatch = useDispatch();
  const offloadTypeList: any[] = [
    { label: "Deferred", value: offloadType.DEFERRED },
    { label: "Live", value: offloadType.LIVE },
  ];

  const qosLevelList: any[] = [
    { label: "0", value: qosEnum.FIRE_AND_FORGET },
    { label: "1", value: qosEnum.ATLEAST_ONCE },
  ];

  const { filterTopicList } = useSelector((state: RootState) => ({
    filterTopicList: state.filterTopicList,
  }));

  const validateTextInput = (e: any, maxChar?: number) => {
    setActiveInput(e.target);
    setCursorLocation(e.target.selectionStart);
    if (e.target.value === " ") {
      return false;
    }
    if (e.target.value.length > (maxChar || 250)) {
      return false;
    }

    return true;
  };

  const debounceFilter = useDebounce(props.config.topic ? props.config.topic.topicName : "", 300);
  useEffect(() => {
    // Check on topic name change if topic name does exist
    const config = {
      filter: {
        topicName: props.config.topic && props.config.topic.topicName,
      },
      limit: 10,
      orderBy: "topicName",
      orderType: OrderType.ASC,
      page: 0,
    };

    dispatch(getFilteredTopicList(config));
  }, [debounceFilter]);

  useEffect(() => {
    if (
      !props.config.topic ||
      !props.config.topic.topicName ||
      filterTopicList.content.filter(
        (topic: any) =>
          topic.topicName === props.config.topic.topicName &&
          topic.status !== topicStatusEnum.DELETED
      ).length
    ) {
      props.setIsTopicNameValid(false);
    } else {
      props.setIsTopicNameValid(true);
    }
  }, [filterTopicList]);

  const handleTextInput = (e: any, field: string, maxChar?: number) => {
    if (!validateTextInput(e, maxChar)) {
      return;
    }
    setUpdatedConfig({ ...props.config, [field]: e.target.value });
  };

  const handleNumberInput = (value: any, field: string) => {
    let fieldValue = value;
    if (isNaN(value)) {
      fieldValue = 0;
    }
    if (value < 0) {
      fieldValue = Math.abs(value);
    }
    setUpdatedConfig({ ...props.config, [field]: fieldValue });
  };

  const handlePriorityDropdown = (value: any) => {
    setUpdatedConfig({ ...props.config, priority: value });
  };

  const handleCheckboxClick = (e: any) => {
    setUpdatedConfig({ ...props.config, isSecure: !props.config.isSecure });
  };

  const handleOffloadChange = (value: any) => {
    setUpdatedConfig({ ...props.config, offloadType: value });
  };

  const handleQosDropdown = (value: any) => {
    setUpdatedConfig({ ...props.config, qosLevel: value });
  };

  useEffect(() => {
    if (props.config && updatedConfig) {
      props.onChange(updatedConfig);
    }
  }, [updatedConfig]);

  useEffect(() => {
    if (activeInput && cursorLocation) {
      activeInput.setSelectionRange(cursorLocation, cursorLocation);
    }
  }, [props.config]);

  const handleTypeaheadInput = (app: any) => {
    setUpdatedConfig({
      ...props.config,
      appData: {
        appClientId: app.appClientId,
        appName: app.appName,
        appVersion: app.appVersion,
      },
    });
  };

  return (
    <>
      <TextInput
        label="Topic:"
        name="topic"
        placeholder="Topic"
        valid={props.isTopicNameValid}
        value={props.config.topic && props.config.topic.topicName}
        onChange={e => {
          if (!validateTextInput(e)) {
            return;
          }
          setUpdatedConfig({
            ...props.config,
            topic: {
              ...props.config.topic,
              topicName: e.target.value,
            },
          });
        }}
      />

      <Text variant="captionText" className="ta-modal-input-caption-topic-owner">
        Topic owner{" "}
        <StatusIcon
          className={
            props.config && props.config.appData && props.config.appData.appName
              ? "ta-status-icon-success"
              : "ta-status-icon-fail"
          }
          check={props.config && props.config.appData && props.config.appData.appName}
        />
      </Text>

      <TypeaheadApplication
        pageSelector="create-topic"
        value={`${
          props.config.appData && props.config.appData.appName
            ? `${props.config.appData.appName} ${props.config.appData.appVersion}`
            : ""
        }`}
        onChange={handleTypeaheadInput}
        disabled={false}
      />

      <DropdownInput
        label="Priority:"
        name="priority"
        onChange={handlePriorityDropdown}
        options={getTaClassedOptions(priorities)}
        selected={props.config && props.config.priority}
      />

      <SFlexItem>
        <div className="ta-modal-input-secure">
          <Checkbox
            onClick={handleCheckboxClick}
            marked={props.config.isSecure ? CheckboxOptions.selected : CheckboxOptions.unselected}
          />
        </div>
        <SVerticalSpacer />
        <Text variant="captionText" className="ta-modal-input-caption-secure">
          Secure
        </Text>
      </SFlexItem>

      <DropdownInput
        label="Offload type:"
        name="offload"
        onChange={handleOffloadChange}
        options={getTaClassedOptions(offloadTypeList)}
        selected={props.config && props.config.offloadType}
      />

      <Text variant="captionText" className="ta-modal-input-caption-ttl">
        TTL:
      </Text>
      <NumberInput
        value={props.config.ttl}
        min={0}
        onChange={(value: any) => {
          handleNumberInput(value, "ttl");
        }}
      />

      <DropdownInput
        label="QoS level:"
        name="qosLevel"
        onChange={handleQosDropdown}
        options={getTaClassedOptions(qosLevelList)}
        selected={props.config && props.config.qosLevel}
      />

      <Text variant="captionText" className="ta-modal-input-caption-description">
        Description:
      </Text>
      <Textarea
        className="ta-modal-input-description"
        placeholder="Description"
        value={props.config.description}
        onChange={(e: any) => {
          handleTextInput(e, "description", 250);
        }}
      />
    </>
  );
};
