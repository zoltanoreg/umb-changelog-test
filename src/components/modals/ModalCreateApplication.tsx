import { ButtonPrimary, ProgressIndicatorCircular, Text } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { createApplication } from "../../actions/application/create/actions";
import { ADMINISTRATOR } from "../../models/UserTypes";
import { SAnimatedContent, SFlexContainer, SFlexItem, SModalStackItem } from "../../styles/styles";
import { BrokerTypeDropdown } from "../BrokerTypeDropdown";
import { StatusIcon } from "../StatusIcon";
import { TypeaheadUser } from "../TypeaheadUser";

import { DropdownInput } from "./Dropdownlnput";
import { ModalCreateBase } from "./ModalCreateBase";
import { NumberInput } from "./NumberInput";
import { TextInput } from "./TextInput";

export const ModalCreateApplication = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const numConnRetriesValues = {
    default: -1,
    max: 65535,
    min: -1,
  };
  const connRetryDelaySecValues = {
    default: 0,
    max: 65535,
    min: 0,
  };

  const { currentUser } = useSelector((state: any) => ({
    currentUser: state.currentUser,
  }));

  const { applicationCreation } = useSelector((state: any) => ({
    applicationCreation: state.applicationCreation,
  }));

  enum authTypeEnum {
    CREDENTIALS = "Credentials",
    CERTIFICATE = "Client certificate",
  }

  const authTypeOptions = [
    {
      label: <span className="ta-auth-table-dropdown-cred">{authTypeEnum.CREDENTIALS}</span>,
      value: authTypeEnum.CREDENTIALS,
    },
    {
      label: <span className="ta-auth-table-dropdown-cert">{authTypeEnum.CERTIFICATE}</span>,
      value: authTypeEnum.CERTIFICATE,
    },
  ];

  const [authType, setAuthType] = useState(authTypeEnum.CREDENTIALS);
  const [formValid, setFormValid] = useState(false);

  const [config, setConfig] = useState({
    appName: "",
    appVersion: "",
    brokerType: "",
    connRetryDelaySec: connRetryDelaySecValues.default,
    fullName: "",
    numConnRetries: numConnRetriesValues.default,
    password: "",
    pathClientCa: "",
    pathToCa: "",
    systemName: "",
    user: {
      email: "",
      firstName: "",
      lastName: "",
      role: "",
      userId: "",
    },
    userName: "",
  });

  useEffect(() => {
    setConfig({ ...config, fullName: currentUser.data.fullName });
  }, []);

  useEffect(() => {
    setConfig({
      ...config,
      fullName: `${currentUser.data.firstName} ${currentUser.data.lastName}`,
      user: {
        email: currentUser.data.email,
        firstName: currentUser.data.firstName,
        lastName: currentUser.data.lastName,
        role: currentUser.data.role,
        userId: currentUser.data.userId,
      },
    });
  }, [currentUser]);

  const handleTextInput = (e: React.ChangeEvent<HTMLInputElement>, field: string) => {
    if (e.target.value === " ") {
      return;
    }
    setConfig({ ...config, [field]: e.target.value });
  };

  const handleNumberInput = (value: number, field: string) => {
    if (isNaN(value)) {
      return;
    }
    setConfig({ ...config, [field]: value });
  };

  const handleTypeaheadInput = (user: any) => {
    const fullName = user.firstName ? `${user.firstName} ${user.lastName}` : "";
    setConfig({ ...config, user, fullName });
  };

  const handleAuthDropdown = (value: authTypeEnum): void => {
    setAuthType(value);
    setConfig({ ...config, pathToCa: "", pathClientCa: "", userName: "", password: "" });
  };

  const handleFilterDropdown = (value: any): any => {
    setConfig({ ...config, brokerType: value });
  };

  const handleSubmit = () => {
    dispatch(
      createApplication({ ...config, authenticationType: authType }, (appId: any) => {
        history.push(`/applicationClients/${appId}`);
      })
    );
  };

  const isValidNumConnRetries = (value: number) =>
    !isNaN(value) && value >= numConnRetriesValues.min && value <= numConnRetriesValues.max;
  const isValidConnRetryDelaySec = (value: number) =>
    !isNaN(value) && value >= connRetryDelaySecValues.min && value <= connRetryDelaySecValues.max;

  const checkFormFill = () => {
    let authValid = false;

    if (authType === authTypeEnum.CERTIFICATE) {
      authValid = !!config.pathToCa && !!config.pathClientCa;
    }

    if (authType === authTypeEnum.CREDENTIALS) {
      authValid =
        !!config.userName &&
        !!config.password &&
        (config.password.length > 0 && config.password.length <= 100);
    }

    const numConnRetriesValid = isValidNumConnRetries(config.numConnRetries);
    const connRetryDelaySecValid = isValidConnRetryDelaySec(config.connRetryDelaySec);

    setFormValid(
      !!config.appName &&
        !!config.appVersion &&
        !!config.brokerType &&
        !!config.user.userId &&
        !!config.systemName &&
        authValid &&
        numConnRetriesValid &&
        connRetryDelaySecValid
    );
  };

  useEffect(() => {
    checkFormFill();
  }, [config]);

  return (
    <ModalCreateBase title="New Application">
      <TextInput
        label="App name"
        name="app-name"
        placeholder="App name"
        valid={config.appName.length > 0}
        value={config.appName}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          handleTextInput(e, "appName");
        }}
      />

      <TextInput
        label="App version"
        name="app-version"
        placeholder="App version"
        valid={config.appVersion.length > 0}
        value={config.appVersion}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          handleTextInput(e, "appVersion");
        }}
      />

      <Text variant="captionText" className="ta-modal-input-caption-broker-type">
        Broker Type{" "}
        <StatusIcon
          className={
            Object.keys(config.brokerType).length > 0
              ? "ta-status-icon-success"
              : "ta-status-icon-fail"
          }
          check={Object.keys(config.brokerType).length > 0}
        />
      </Text>
      <div className="ta-modal-input-broker-type">
        <BrokerTypeDropdown onChange={handleFilterDropdown} selected={config.brokerType} />
      </div>

      <Text variant="captionText" className="ta-modal-input-caption-app-owner">
        Application owner{" "}
        <StatusIcon
          className={
            config.user && config.user.userId !== ""
              ? "ta-status-icon-success"
              : "ta-status-icon-fail"
          }
          check={config.user && config.user.userId !== ""}
        />
      </Text>
      <TypeaheadUser
        pageSelector="createApplication"
        value={config.fullName}
        onChange={handleTypeaheadInput}
        disabled={currentUser.data.role !== ADMINISTRATOR}
      />

      <TextInput
        label="System name"
        name="system-name"
        placeholder="System name"
        valid={config.systemName.length > 0}
        value={config.systemName}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          handleTextInput(e, "systemName");
        }}
      />

      <NumberInput
        label="Number of connection retries"
        name="number-of-connection-retries"
        placeholder="Number of connection retries"
        valid={isValidNumConnRetries(config.numConnRetries)}
        value={config.numConnRetries}
        min={numConnRetriesValues.min}
        max={numConnRetriesValues.max}
        onChange={(value: number) => {
          handleNumberInput(value, "numConnRetries");
        }}
      />

      <NumberInput
        label="Connection retry delay (sec)"
        name="connection-retry-delay"
        placeholder="Connection retry delay (sec)"
        valid={isValidConnRetryDelaySec(config.connRetryDelaySec)}
        value={config.connRetryDelaySec}
        min={connRetryDelaySecValues.min}
        max={connRetryDelaySecValues.max}
        onChange={(value: number) => {
          handleNumberInput(value, "connRetryDelaySec");
        }}
      />

      <DropdownInput
        label="Authentication type"
        name="auth-type"
        onChange={handleAuthDropdown}
        options={authTypeOptions}
        selected={authType}
      />
      {authType === authTypeEnum.CREDENTIALS && (
        <>
          <SAnimatedContent>
            <SModalStackItem className="ta-modal-auth-container-cred">
              <TextInput
                label="User name"
                name="username"
                placeholder="User name"
                valid={config.userName.length > 0}
                value={config.userName}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  handleTextInput(e, "userName");
                }}
              />

              <TextInput
                label="Password"
                name="password"
                placeholder="Password"
                valid={config.password.length > 0 && config.password.length <= 100}
                value={config.password}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  handleTextInput(e, "password");
                }}
              />
            </SModalStackItem>
          </SAnimatedContent>
        </>
      )}

      {authType === authTypeEnum.CERTIFICATE && (
        <>
          <SAnimatedContent>
            <SModalStackItem className="ta-modal-auth-container-cert">
              <TextInput
                label="Path to CA"
                name="path-to-ca"
                placeholder="Path to CA"
                valid={config.pathToCa.length > 0}
                value={config.pathToCa}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  handleTextInput(e, "pathToCa");
                }}
              />

              <TextInput
                label="Path client CA"
                name="path-client"
                placeholder="Path client CA"
                valid={config.pathClientCa.length > 0}
                value={config.pathClientCa}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  handleTextInput(e, "pathClientCa");
                }}
              />
            </SModalStackItem>
          </SAnimatedContent>
        </>
      )}

      <SFlexContainer justifyContent="space-around">
        <SFlexItem>
          {applicationCreation.loading ? (
            <ProgressIndicatorCircular size="small" />
          ) : (
            <ButtonPrimary
              onClick={handleSubmit}
              disabled={!formValid}
              className="ta-modal-submit-button"
            >
              Save
            </ButtonPrimary>
          )}
        </SFlexItem>
      </SFlexContainer>
    </ModalCreateBase>
  );
};
