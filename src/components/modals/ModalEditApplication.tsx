import deepEqual from "deep-equal";
import { ButtonPrimary, ProgressIndicatorCircular, Text } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { EditApplication } from "../../actions/application/edit/actions";
import { getApplicationDetails } from "../../actions/application/get-details/actions";
import { editModalCloseAction } from "../../actions/modal/edit/actions";
import { getUserDetails } from "../../actions/user/get-details/actions";
import { authTypeEnum } from "../../models/IAppDetailsConfig";
import { ADMINISTRATOR } from "../../models/UserTypes";
import { SAnimatedContent, SFlexContainer, SFlexItem, SModalStackItem } from "../../styles/styles";
import { BrokerTypeDropdown } from "../BrokerTypeDropdown";
import { StatusIcon } from "../StatusIcon";
import { TypeaheadUser } from "../TypeaheadUser";

import { DropdownInput } from "./Dropdownlnput";
import { ModalCreateBase } from "./ModalCreateBase";
import { NumberInput } from "./NumberInput";
import { TextInput } from "./TextInput";

interface IEditUserConfig {
  appClientId: string;
  appName: string;
  appVersion: string;
  brokerType: {
    brokerType: string;
    brokerTypeId: string;
    brokerTypeName: string;
    systemType: string;
  };
  connRetryDelaySec: number;
  fullName: string;
  numConnRetries: number;
  password: string;
  pathClientCa: string;
  pathToCa: string;
  systemName: string;
  user: {
    email: string;
    firstName: string;
    lastName: string;
    role: string;
    userId: string;
  };
  userName: string;
}

export const ModalEditApplication = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const numConnRetriesValues = {
    default: -1,
    max: 65535,
    min: -1,
  };
  const connRetryDelaySecValues = {
    default: 0,
    max: 65535,
    min: 0,
  };

  const { currentUser } = useSelector((state: any) => ({
    currentUser: state.currentUser,
  }));
  const { editModal } = useSelector((state: any) => ({
    editModal: state.editModal,
  }));
  const { userDetails } = useSelector((state: any) => ({
    userDetails: state.userDetails,
  }));
  const { applicationEdit } = useSelector((state: any) => ({
    applicationEdit: state.applicationEdit,
  }));

  const [dirty, setDirty] = useState(false);
  const [originalConfig, setOriginalConfig] = useState();
  const authTypeOptions = [
    {
      label: <span className="ta-auth-table-dropdown-cred">{authTypeEnum.CREDENTIALS}</span>,
      value: authTypeEnum.CREDENTIALS,
    },
    {
      label: <span className="ta-auth-table-dropdown-cert">{authTypeEnum.CERTIFICATE}</span>,
      value: authTypeEnum.CERTIFICATE,
    },
  ];

  const emptyConfig = {
    appClientId: "",
    appName: "",
    appVersion: "",
    brokerType: {
      brokerType: "",
      brokerTypeId: "",
      brokerTypeName: "",
      systemType: "",
    },
    connRetryDelaySec: connRetryDelaySecValues.default,
    fullName: "",
    numConnRetries: numConnRetriesValues.default,
    password: "",
    pathClientCa: "",
    pathToCa: "",
    systemName: "",
    user: {
      email: "",
      firstName: "",
      lastName: "",
      role: "",
      userId: "",
    },
    userName: "",
  };

  const [authType, setAuthType] = useState(authTypeEnum.CREDENTIALS);
  const [formValid, setFormValid] = useState(true);

  const [config, setConfig] = useState<IEditUserConfig>(emptyConfig);

  const fillForm = (userData: any) => {
    let newConfig: IEditUserConfig = { ...emptyConfig };

    if (editModal.appData) {
      setAuthType(editModal.appData.authenticationType);

      const newUser = {
        email: userData.email,
        firstName: userData.firstName,
        lastName: userData.lastName,
        role: userData.role,
        userId: userData.userId,
      };

      newConfig = {
        ...config,
        appClientId: editModal.appData.appClientId,
        appName: editModal.appData.appName,
        appVersion: editModal.appData.appVersion,
        brokerType: editModal.appData.appBrokerType,
        connRetryDelaySec: editModal.appData.connRetryDelaySec,
        fullName: editModal.appData.appOwner.fullName,
        numConnRetries: editModal.appData.numConnRetries,
        pathClientCa: editModal.appData.pathClientCa,
        pathToCa: editModal.appData.pathToCa,
        systemName: editModal.appData.systemName,
        user: newUser,
        userName: editModal.appData.userName,
      };
    }
    setConfig({ ...newConfig });
    setOriginalConfig({ ...newConfig });
    setDirty(false);
  };

  useEffect(() => {
    if (editModal && currentUser && currentUser.data.role === ADMINISTRATOR) {
      dispatch(
        getUserDetails(editModal.appData.appOwner.userId, (userData: any) => {
          fillForm(userData.content);
        })
      );
    } else {
      fillForm(currentUser.data);
    }
  }, [editModal, currentUser]);

  const handleTextInput = (e: React.ChangeEvent<HTMLInputElement>, field: string) => {
    if (e.target.value === " ") {
      return;
    }
    setConfig({ ...config, [field]: e.target.value });
  };

  const handleNumberInput = (value: number, field: string) => {
    if (isNaN(value)) {
      return;
    }
    setConfig({ ...config, [field]: value });
  };

  const handleTypeaheadInput = (user: any) => {
    const fullName = user.firstName ? `${user.firstName} ${user.lastName}` : "";
    setConfig({ ...config, user, fullName });
  };

  const handleFilterDropdown = (value: any): any => {
    setConfig({ ...config, brokerType: value });
  };

  const handleAuthDropdown = (value: authTypeEnum): void => {
    setAuthType(value);
    setConfig({ ...config, pathToCa: "", pathClientCa: "", userName: "", password: "" });
  };

  const submitSuccessCallback = (appClientId: any) => {
    dispatch(editModalCloseAction());
    dispatch(getApplicationDetails(appClientId));
  };

  const handleSubmit = () => {
    const password = config.password ? config.password : editModal.appData.password;
    dispatch(
      EditApplication({ ...config, password, authenticationType: authType }, (appClientId: any) => {
        submitSuccessCallback(appClientId);
      })
    );
  };

  const isValidNumConnRetries = (value: number) =>
    !isNaN(value) && value >= numConnRetriesValues.min && value <= numConnRetriesValues.max;
  const isValidConnRetryDelaySec = (value: number) =>
    !isNaN(value) && value >= connRetryDelaySecValues.min && value <= connRetryDelaySecValues.max;

  const checkFormFill = () => {
    let authValid = false;

    if (authType === authTypeEnum.CERTIFICATE) {
      authValid = !!config.pathToCa && !!config.pathClientCa;
    }

    if (authType === authTypeEnum.CREDENTIALS) {
      authValid = !!config.userName && config.password.length <= 100;
    }

    const numConnRetriesValid = isValidNumConnRetries(config.numConnRetries);
    const connRetryDelaySecValid = isValidConnRetryDelaySec(config.connRetryDelaySec);

    setFormValid(
      !!config.appName &&
        !!config.appVersion &&
        !!config.brokerType &&
        !!config.user.userId &&
        !!config.systemName &&
        authValid &&
        numConnRetriesValid &&
        connRetryDelaySecValid
    );
  };

  useEffect(() => {
    checkFormFill();

    if (!dirty) {
      const isEqual = deepEqual(config, originalConfig);
      setDirty(!isEqual);
    }
  }, [config]);

  return (
    <ModalCreateBase title="Edit Application">
      <TextInput
        label="App name"
        name="app-name"
        placeholder="App name"
        valid={config.appName.length > 0}
        value={config.appName}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          handleTextInput(e, "appName");
        }}
      />

      <TextInput
        label="App version"
        name="app-version"
        placeholder="App version"
        valid={config.appVersion.length > 0}
        value={config.appVersion}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          handleTextInput(e, "appVersion");
        }}
      />

      <Text variant="captionText" className="ta-modal-input-caption-broker-type">
        Broker Type{" "}
        <StatusIcon
          className={
            Object.keys(config.brokerType).length > 0
              ? "ta-status-icon-success"
              : "ta-status-icon-fail"
          }
          check={Object.keys(config.brokerType).length > 0}
        />
      </Text>
      <div className="ta-modal-input-broker-type">
        <BrokerTypeDropdown onChange={handleFilterDropdown} selected={config.brokerType} />
      </div>

      <Text variant="captionText" className="ta-modal-input-caption-app-owner">
        Application owner{" "}
        <StatusIcon
          className={
            config.user && config.user.userId !== ""
              ? "ta-status-icon-success"
              : "ta-status-icon-fail"
          }
          check={config.user && config.user.userId !== ""}
        />
      </Text>
      <TypeaheadUser
        pageSelector="editApplication"
        value={config.fullName}
        onChange={handleTypeaheadInput}
        disabled={currentUser.data.role !== ADMINISTRATOR}
      />

      <TextInput
        label="System name"
        name="system-name"
        placeholder="System name"
        valid={config.systemName.length > 0}
        value={config.systemName}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          handleTextInput(e, "systemName");
        }}
      />

      <NumberInput
        label="Number of connection retries"
        name="number-of-connection-retries"
        placeholder="Number of connection retries"
        valid={isValidNumConnRetries(config.numConnRetries)}
        value={config.numConnRetries}
        min={numConnRetriesValues.min}
        max={numConnRetriesValues.max}
        onChange={(value: number) => {
          handleNumberInput(value, "numConnRetries");
        }}
      />

      <NumberInput
        label="Connection retry delay (sec)"
        name="connection-retry-delay"
        placeholder="Connection retry delay (sec)"
        valid={isValidConnRetryDelaySec(config.connRetryDelaySec)}
        value={config.connRetryDelaySec}
        min={connRetryDelaySecValues.min}
        max={connRetryDelaySecValues.max}
        onChange={(value: number) => {
          handleNumberInput(value, "connRetryDelaySec");
        }}
      />

      <DropdownInput
        label="Authentication type"
        name="auth-type"
        onChange={handleAuthDropdown}
        options={authTypeOptions}
        selected={authType}
      />
      {authType === authTypeEnum.CREDENTIALS && (
        <>
          <SAnimatedContent>
            <SModalStackItem className="ta-modal-auth-container-cred">
              <TextInput
                label="User name"
                name="username"
                placeholder="User name"
                valid={config.userName.length > 0}
                value={config.userName}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  handleTextInput(e, "userName");
                }}
              />

              <TextInput
                label="Password (Current password will be kept if field left empty)"
                name="password"
                placeholder="New password"
                valid={config.password.length <= 100}
                value={config.password}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  handleTextInput(e, "password");
                }}
              />
            </SModalStackItem>
          </SAnimatedContent>
        </>
      )}

      {authType === authTypeEnum.CERTIFICATE && (
        <>
          <SAnimatedContent>
            <SModalStackItem className="ta-modal-auth-container-cert">
              <TextInput
                label="Path to CA"
                name="path-to-ca"
                placeholder="Path to CA"
                valid={config.pathToCa.length > 0}
                value={config.pathToCa}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  handleTextInput(e, "pathToCa");
                }}
              />

              <TextInput
                label="Path client CA"
                name="path-client"
                placeholder="Path client CA"
                valid={config.pathClientCa.length > 0}
                value={config.pathClientCa}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  handleTextInput(e, "pathClientCa");
                }}
              />
            </SModalStackItem>
          </SAnimatedContent>
        </>
      )}

      <SFlexContainer justifyContent="space-around">
        <SFlexItem>
          {applicationEdit.loading ? (
            <ProgressIndicatorCircular size="small" />
          ) : (
            <ButtonPrimary
              onClick={handleSubmit}
              disabled={!formValid || !dirty}
              className="ta-modal-submit-button"
            >
              Save
            </ButtonPrimary>
          )}
        </SFlexItem>
      </SFlexContainer>
    </ModalCreateBase>
  );
};
