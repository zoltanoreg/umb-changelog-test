import { ButtonPrimary } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { editModalCloseAction } from "../../actions/modal/edit/actions";
import { editTopic } from "../../actions/topic/edit/actions";
import { getTopicDetails } from "../../actions/topic/get-details/actions";
import { offloadType, payloadFormatEnum, qosEnum } from "../../models/ITopicConfig";
import { SFlexContainer } from "../../styles/styles";

import { ModalCreateBase } from "./ModalCreateBase";
import { ModalEditTopicPage1 } from "./ModalEditTopicPage1";
import { ModalEditTopicPage2 } from "./ModalEditTopicPage2";
import { ModalEditTopicPage3 } from "./ModalEditTopicPage3";

export const ModalEditTopic = (props: any) => {
  const dispatch = useDispatch();

  const { editModal, payloadGet, payloadUpload } = useSelector((state: any) => ({
    editModal: state.editModal,
    payloadGet: state.payloadGet,
    payloadUpload: state.payloadUpload,
  }));

  const [currentPage, setCurrentPage] = useState(0);
  const [isEditing, setIsEditing] = useState(false);

  const [config, setConfig] = useState({
    appData: {
      appClientId: "",
      appName: "",
    },
    description: "",
    featureName: "",
    isSecure: false,
    offloadType: offloadType.DEFERRED,
    payloadData: {
      payloadFile: File,
      payloadFileName: "",
      payloadFormat: "",
      payloadSchema: "",
    },
    payloadFormat: payloadFormatEnum.JSON,
    payloadType: "",
    priority: "1",
    qosLevel: qosEnum.ATLEAST_ONCE,
    schema: "",
    status: "",
    topicId: "",
    topicName: "",
    topicPayloadId: "",
    ttl: 0,
  });

  const [validPage, setValidPage] = useState([false, true, false]);

  const handlePageChange = (updatedConfig: any) => {
    setConfig(updatedConfig);
  };

  const handleSetValidPage = (index: number, value: boolean) => {
    const updatedPage = [...validPage];
    updatedPage[index] = value;
    setValidPage(updatedPage);
  };

  const checkValidity = () => {
    if (config.topicName && config.appData && config.appData.appName) {
      handleSetValidPage(0, true);
    } else {
      handleSetValidPage(0, false);
    }
  };

  const handleSubmit = () => {
    dispatch(
      editTopic(
        {
          dependencies: "",
          description: config.description,
          featureName: config.featureName,
          isSecure: config.isSecure,
          obfuscationRequirement: "",
          offloadType: config.offloadType,
          priority: config.priority,
          qosLevel: config.qosLevel,
          serviceName: "",
          topicCategory: "",
          topicMqttClient: {
            accessLevel: "",
            frequency: "",
            retainRequired: "",
          },
          topicName: config.topicName,
          topicOwner: config.appData && config.appData.appClientId,
          topicPayloadId: config.topicPayloadId,
          topicType: "",
          ttl: config.ttl,
          versionId: "1.0",
        },
        config.topicId,
        (topicId: any) => {
          dispatch(getTopicDetails(topicId));
          dispatch(editModalCloseAction());
        }
      )
    );
  };

  useEffect(() => {
    checkValidity();
  }, [config]);

  useEffect(() => {
    setConfig({
      appData: { ...editModal.appData },
      description: editModal.topicData.description,
      featureName: editModal.topicData.featureName,
      isSecure: editModal.topicData.isSecure,
      offloadType: editModal.topicData.offloadType,
      payloadData: config.payloadData,
      payloadFormat: payloadFormatEnum.JSON,
      payloadType: "",
      priority: editModal.topicData.priority,
      qosLevel: editModal.topicData.qosLevel,
      schema: "",
      status: editModal.topicData.status,
      topicId: editModal.topicData.topicId,
      topicName: editModal.topicData.topicName,
      topicPayloadId: editModal.topicData.topicPayloadId,
      ttl: editModal.topicData.ttl,
    });
  }, [editModal]);

  return (
    <ModalCreateBase title="Edit topic">
      {currentPage === 0 && (
        <ModalEditTopicPage1
          config={{ ...config }}
          onChange={(updatedConfig: any) => {
            handlePageChange(updatedConfig);
          }}
        />
      )}
      {currentPage === 1 && (
        <ModalEditTopicPage2
          config={{ ...config }}
          onChange={(updatedConfig: any) => {
            handlePageChange(updatedConfig);
          }}
        />
      )}
      {currentPage === 2 && (
        <ModalEditTopicPage3
          config={{ ...config }}
          onChange={(updatedConfig: any) => {
            handlePageChange(updatedConfig);
          }}
          isEditing={isEditing}
          onToggleEditing={setIsEditing}
        />
      )}

      <SFlexContainer justifyContent="space-between">
        <SFlexContainer justifyContent="flex-start">
          {currentPage !== 0 && (
            <ButtonPrimary
              className="ta-modal-previous-button"
              onClick={() => {
                setCurrentPage(currentPage - 1);
              }}
              disabled={currentPage === 0 || isEditing}
            >
              Previous
            </ButtonPrimary>
          )}
        </SFlexContainer>
        <SFlexContainer justifyContent="flex-end">
          {currentPage !== 2 ? (
            <ButtonPrimary
              className="ta-modal-next-button"
              onClick={() => {
                setCurrentPage(currentPage + 1);
              }}
              disabled={!validPage[currentPage]}
            >
              Next
            </ButtonPrimary>
          ) : (
            <ButtonPrimary
              className="ta-modal-save-button"
              onClick={handleSubmit}
              disabled={(payloadGet.loading && payloadUpload.loading) || isEditing}
            >
              Save
            </ButtonPrimary>
          )}
        </SFlexContainer>
      </SFlexContainer>
    </ModalCreateBase>
  );
};
