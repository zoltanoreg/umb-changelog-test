import { Text } from "next-components";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";


import { topicStatusEnum } from "../../models/ITopicConfig";
import { ADMINISTRATOR, APPLICATION_OWNER } from "../../models/UserTypes";
import { StatusIcon } from "../StatusIcon";
import { TypeaheadApplication } from "../TypeaheadApplication";

import { TextInput } from "./TextInput";

export const ModalEditTopicPage1 = (props: any) => {
  const { currentUser } = useSelector((state: any) => ({
    currentUser: state.currentUser,
  }));

  const [cursorLocation, setCursorLocation] = useState(0);
  const [activeInput, setActiveInput] = useState();
  const [updatedConfig, setUpdatedConfig] = useState();

  const handleTextInput = (e: any, field: string, maxChar?: number) => {
    setActiveInput(e.target);
    setCursorLocation(e.target.selectionStart);
    if (e.target.value === " ") {
      return;
    }
    if (e.target.value.length > (maxChar || 250)) {
      return;
    }
    setUpdatedConfig({ ...props.config, [field]: e.target.value });
  };

  const handleTypeaheadInput = (app: any) => {
    setUpdatedConfig({ ...props.config, appData: app });
  };

  useEffect(() => {
    if (props.config && updatedConfig) {
      props.onChange(updatedConfig);
    }
  }, [updatedConfig]);

  useEffect(() => {
    if (activeInput && cursorLocation) {
      activeInput.setSelectionRange(cursorLocation, cursorLocation);
    }
  }, [props.config]);

  return (
    <>
      <Text variant="captionText" className="ta-modal-input-caption-topic-name">
        Application{" "}
        <StatusIcon
          className={
            props.config && props.config.appData.appName
              ? "ta-status-icon-success"
              : "ta-status-icon-fail"
          }
          check={props.config && props.config.appData && props.config.appData.appName}
        />
      </Text>

      <TypeaheadApplication
        pageSelector="editTopic"
        value={`${props.config.appData.appName}${props.config.appData.appVersion ? " " : ""}${
          props.config.appData.appVersion
        }`}
        onChange={handleTypeaheadInput}
        disabled={currentUser.data.role !== ADMINISTRATOR}
      />

      <Text>
        Application broker type:{" "}
        <span>
          {props.config.appData &&
            props.config.appData.appBrokerType &&
            props.config.appData.appBrokerType.brokerType}
        </span>
      </Text>

      <TextInput
        label="Topic name"
        name="topic-name"
        placeholder="Topic name"
        valid={props.config && props.config.topicName}
        value={props.config && props.config.topicName}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          handleTextInput(e, "topicName", 250);
        }}
        disabled={
          currentUser.data.role === APPLICATION_OWNER &&
          props.config.status !== topicStatusEnum.DRAFT
        }
      />
    </>
  );
};
