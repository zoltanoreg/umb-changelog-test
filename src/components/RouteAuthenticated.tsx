import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";

import { getRedirectUrl } from "../helper/redirectHelper";
import {
  ADMINISTRATOR,
  APPLICATION_OWNER,
  READ,
  UMBT_IT_SUPPORT_TEAM,
  UMBT_OPERATIONAL_SUPPORT_TEAM,
} from "../models/UserTypes";
import { RootState } from "../reducers";

import { ApplicationClients } from "./pages/ApplicationClients";
import { ApplicationClientsDetails } from "./pages/ApplicationClientsDetails";
import { BrokerTypes } from "./pages/BrokerTypes";
import { BrokerTypesDetails } from "./pages/BrokerTypesDetails";
import { ConfigurationFiles } from "./pages/ConfigurationFiles";
import { Error } from "./pages/Error";
import { TopicDetails } from "./pages/TopicDetails";
import { Topics } from "./pages/Topics";
import { UserDetails } from "./pages/Users/UserDetails";
import { Users } from "./pages/Users/Users";
import PrivateRoute from "./PrivateRoute";
import { Topology } from "./Topology";

export const RouteAuthenticated = () => {
  const { currentUser } = useSelector((state: RootState) => ({
    currentUser: state.currentUser,
  }));

  return (
    <Switch>
      <PrivateRoute
        exact
        path="/applicationClients"
        component={ApplicationClients}
        userLevelAccess={[ADMINISTRATOR, APPLICATION_OWNER, READ]}
      />
      <PrivateRoute
        exact
        path="/applicationClients/:appId"
        component={ApplicationClientsDetails}
        userLevelAccess={[ADMINISTRATOR, APPLICATION_OWNER, READ]}
      />
      <PrivateRoute
        exact
        path="/topics"
        component={Topics}
        userLevelAccess={[ADMINISTRATOR, APPLICATION_OWNER, READ]}
      />
      <PrivateRoute
        exact
        path="/topics/:topicId"
        component={TopicDetails}
        userLevelAccess={[ADMINISTRATOR, APPLICATION_OWNER, READ]}
      />
      <PrivateRoute
        exact
        path="/brokerTypes"
        component={BrokerTypes}
        userLevelAccess={[ADMINISTRATOR, APPLICATION_OWNER, READ]}
      />
      <PrivateRoute
        exact
        path="/brokerTypes/:brokerTypeId"
        component={BrokerTypesDetails}
        userLevelAccess={[ADMINISTRATOR, APPLICATION_OWNER]}
      />
      <PrivateRoute
        exact
        path="/users"
        component={Users}
        userLevelAccess={[ADMINISTRATOR, UMBT_IT_SUPPORT_TEAM]}
      />
      <PrivateRoute
        exact
        path="/users/:userId"
        component={UserDetails}
        userLevelAccess={[
          ADMINISTRATOR,
          APPLICATION_OWNER,
          READ,
          UMBT_IT_SUPPORT_TEAM,
          UMBT_OPERATIONAL_SUPPORT_TEAM,
        ]}
      />
      <PrivateRoute
        exact
        path="/configurationFiles"
        component={ConfigurationFiles}
        userLevelAccess={[ADMINISTRATOR, APPLICATION_OWNER, READ, UMBT_OPERATIONAL_SUPPORT_TEAM]}
      />
      <PrivateRoute
        exact
        path="/topology"
        component={Topology}
        userLevelAccess={[ADMINISTRATOR, APPLICATION_OWNER]}
      />
      <Route exact path="/error" component={Error} />

      <Redirect from="/" to={getRedirectUrl(currentUser)} />
    </Switch>
  );
};
