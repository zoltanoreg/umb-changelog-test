import deepEqual from "deep-equal";
import { IconDelete16, ProgressIndicatorLinear } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { getTopicsForPublisher } from "../actions/application/get-topics-for-publisher/actions";
import { getAllBrokerType } from "../actions/broker-type/get-all/actions";
import { popupOpenAction } from "../actions/popup/actions";
import { deleteAssociationBulk } from "../actions/topic/delete-association-bulk/actions";
import { deleteAssociation } from "../actions/topic/delete-association/actions";
import { appTopicListClearAction, getTopicClientList } from "../actions/topic/get-clients/actions";
import { getFormattedDate } from "../helper/util";
import { FilterType, IFilterConfig } from "../models/IFilterConfig";
import { ITableConfig, ITableHeaderCells, ITableRow, OrderType } from "../models/ITableConfig";
import { ADMINISTRATOR } from "../models/UserTypes";
import { RootState } from "../reducers";
import { SActiveLink, SOverlay, SSpacer } from "../styles/styles";

import { Filter } from "./Filter";
import { Table } from "./Table";

export const TopicClientList = (props: {
  subscriberCount?: number;
  type: "PUBLISHER" | "SUBSCRIBER";
}) => {
  const [selectedRows, setSelectedRows] = useState(Array<ITableRow>());
  const history = useHistory();

  const navigateToPage = (url: string): void => {
    history.push(url);
  };

  const headCells: ITableHeaderCells[] = [
    {
      id: "mqttClientAppName",
      label: "App Name",
      onClickCell: (row: ITableRow) => {
        navigateToPage(`/applicationClients/${row.appClient.appClientId}`);
      },
      value: (row: ITableRow) => <SActiveLink>{row.appClient.appName}</SActiveLink>,
      width: "2fr",
    },
    {
      id: "mqttClientAppVersion",
      label: "App Version",
      value: (row: ITableRow) => row.appClient.appVersion,
    },
    {
      id: "mqttClientBrokerType",
      label: "Broker Type",
      value: (row: ITableRow) => row.appClient.appBrokerType.brokerType,
    },
    {
      id: "mqttClientAppOwnerUserFullName",
      label: "Application Owner",
      value: (row: ITableRow) => row.appClient.appOwner.fullName,
    },
    {
      id: "mqttClientModifiedByFullName",
      label: "Modified By",
      value: (row: ITableRow) => row.modifiedBy.fullName,
    },
    {
      id: "mqttClientModifiedAt",
      label: "Modified at",
      value: (row: ITableRow) => getFormattedDate(row.modifiedAt),
    },
    { id: "delete", label: "Delete" },
  ];

  const dispatch = useDispatch();

  const [brokerTypes, setBrokerTypes]: any[] = useState([{}]);

  const { allBrokerType, topicDetails, topicClientList, currentUser } = useSelector(
    (state: RootState) => ({
      allBrokerType: state.allBrokerType,
      currentUser: state.currentUser,
      topicClientList: state.topicClientList,
      topicDetails:
        state.topicDetails && state.topicDetails.content && state.topicDetails.content.topic,
    })
  );

  const emptyFilterObject = {
    clientType: props.type,
    modifiedDate: { startDate: null, endDate: null },
    mqttConnectedAppClientBrokerType: "",
    mqttConnectedAppClientId: "",
    mqttConnectedAppClientName: "",
    mqttConnectedAppClientOwnerFullName: "",
    mqttConnectedAppClientVersion: "",
    topicId: "",
  };

  const [config, setConfig] = useState({
    filter: { ...emptyFilterObject },
    limit: 10,
    orderBy: "topicName",
    orderType: OrderType.ASC,
    page: 0,
  });

  useEffect(() => {
    dispatch(
      getAllBrokerType({
        filter: {},
        limit: 0,
        orderBy: "brokerTypeName",
        orderType: "ASC",
        page: 0,
      })
    );

    return () => {
      dispatch(appTopicListClearAction());
    };
  }, []);

  useEffect(() => {
    if (config.filter.topicId !== "") {
      dispatch(getTopicClientList(config));
    }
  }, [config]);

  useEffect(() => {
    setBrokerTypes(
      allBrokerType.content.map((broker: any) => ({
        label: <span className={`ta-dropdown-${broker.brokerType}`}>{broker.brokerType}</span>,
        value: broker.brokerType,
      }))
    );
  }, [allBrokerType]);

  const askForConfirmation = (topicMqttClientId: string, onlyPublisher: boolean, bulk: boolean) => {
    const confirmMsg1 =
      "You are trying to remove the association between the Application client and the Topic. Configuration file will be updated accordingly, do you want to continue?";
    const confirmMsg2 =
      "You are trying to remove the last publisher to the topic. Last publisher can only be removed if there are no subscribers to the topic. Do you want to continue?";

    if (!bulk || (bulk && onlyPublisher)) {
      dispatch(
        popupOpenAction({
          confirmText: "ConfirmText",
          content: onlyPublisher ? confirmMsg2 : confirmMsg1,
          onConfirm: () => {
            dispatch(
              deleteAssociation(topicMqttClientId, () => {
                setConfig({ ...config, page: 0 });
              })
            );
          },
          title: `Please confirm`,
          type: "Confirmation",
        })
      );
    } else {
      dispatch(
        deleteAssociation(topicMqttClientId, () => {
          setConfig({ ...config, page: 0 });
        })
      );
    }
  };

  const throwConfirmationPopup = (topicMqttClientId: string, bulk: boolean = false) => {
    if (props.type === "PUBLISHER") {
      askForConfirmation(topicMqttClientId, topicClientList.count === 1, bulk);
    } else {
      askForConfirmation(topicMqttClientId, false, bulk);
    }
  };

  const [isAdmin, setIsAdmin] = useState(false);
  useEffect(() => {
    if (currentUser && topicClientList) {
      setIsAdmin(currentUser.data.role === ADMINISTRATOR);
    }
  }, [topicClientList, currentUser]);

  const paginationCallback = (page: number) => {
    setConfig({ ...config, page });
  };

  const sortCallback = (column: string, direction: OrderType) => {
    setConfig({ ...config, orderBy: column, orderType: direction });
  };

  const batchDelete = (selRows: ITableRow[]) => {
    const mqttIds = selRows.map((row: ITableRow) => row.topicMqttClientId);
    if (selRows.length === 1) {
      throwConfirmationPopup(mqttIds[0], true);
    } else {
      dispatch(
        deleteAssociationBulk({ mqttIds }, () => {
          setConfig({ ...config, page: 0 });
        })
      );
    }
  };

  const isOnlyPublisherWithSubscriber = (selRows: ITableRow[]) => {
    if (
      selRows.length === topicClientList.count &&
      selRows.length !== 1 &&
      props.type === "PUBLISHER" &&
      props.subscriberCount &&
      props.subscriberCount > 0
    ) {
      return true;
    }

    return false;
  };

  const tableProps: ITableConfig = {
    batchAction: {
      actions: [{ title: "Delete", onClick: batchDelete, disabled: isOnlyPublisherWithSubscriber }],
      label: (selRows: ITableRow[]) => {
        const selectedMsg = `${selRows.length} item${selRows.length > 1 ? "s" : ""} selected`;

        if (isOnlyPublisherWithSubscriber(selRows)) {
          return `${selectedMsg}, at least one publisher MUST exist if there are subscribers`;
        }

        return selectedMsg;
      },
    },
    deleteCell: {
      as: "a",
      disabled: (row: ITableRow) =>
        !isAdmin && currentUser.data.userId !== row.appClient.appOwner.userId,
      onClick: (row: ITableRow) => {
        if (isAdmin || currentUser.data.userId === row.appClient.appOwner.userId) {
          throwConfirmationPopup(row.topicMqttClientId);
        }
      },
    },
    head: {
      cells: headCells,
    },
    list: topicClientList,
    name: "topicClients",
    paginationConfig: {
      limit: config.limit,
      onPageChange: paginationCallback,
      page: config.page,
    },
    sortConfig: {
      onSort: sortCallback,
      orderBy: config.orderBy,
      orderType: config.orderType,
    },
  };

  const filterConfig: IFilterConfig = {
    items: [
      {
        name: "mqttConnectedAppClientName",
        placeholder: "App Name",
        taClass: "appName",
      },
      {
        name: "mqttConnectedAppClientVersion",
        placeholder: "App Version",
        taClass: "appVersion",
      },
      {
        data: brokerTypes,
        name: "mqttConnectedAppClientBrokerType",
        placeholder: "Broker Type",
        taClass: "brokerType",
        type: FilterType.DROPDOWN,
      },
      {
        name: "mqttConnectedAppClientOwnerFullName",
        placeholder: "Owner",
        taClass: "owner",
      },
      {
        name: "modifiedDate",
        placeholder: "Modified",
        taClass: "modifiedDate",
        type: FilterType.DATEPICKER,
      },
    ],
    pageName: "topicClients",
    returnFilter: (f: typeof emptyFilterObject) => {
      const newFilterObject = {
        ...emptyFilterObject,
        topicId: topicDetails && topicDetails.topicId,
        ...f,
      };

      if (!deepEqual(config.filter, newFilterObject)) {
        setConfig({
          ...config,
          filter: newFilterObject,
          page: 0,
        });
      }
    },
  };

  return (
    <>
      <Filter {...filterConfig} />
      {/* FILTER */}

      <SSpacer />

      {/* LOADING */}
      {topicClientList.loading && (
        <SOverlay>
          <ProgressIndicatorLinear />
        </SOverlay>
      )}
      {/* TABLE */}
      <Table {...tableProps}></Table>
    </>
  );
};
