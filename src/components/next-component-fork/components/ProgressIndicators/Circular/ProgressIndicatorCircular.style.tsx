import React from "react";
import styled from "styled-components";

import { ProgressIndicatorSize } from "./IProgressIndicatorCircular";

export const ProgressImage = styled((props: any) => <img {...props} />)`
  width: ${(props: any) => (props.size === ProgressIndicatorSize.DEFAULT ? "100px" : "16px")};
`;
