export enum ProgressIndicatorSize {
  DEFAULT = "default",
  SMALL = "small",
}

export interface IProgressIndicatorCircular {
  size?: ProgressIndicatorSize;
}
