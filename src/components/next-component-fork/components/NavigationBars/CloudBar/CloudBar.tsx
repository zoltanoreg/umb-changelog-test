import React, { RefObject, useCallback, useEffect, useState } from "react";

import useOutsideClickDetector from "../../../../../hooks/useOutsideClickDetector";

import { Container, LargeSideBarContainer } from "./CloudBar.style";
import { ICloudBar } from "./ICloudBar";
import SideBar from "./SideBar";

const CloudBar = ({
  cloudHomeUrl,
  user,
  notificationCount,
  lastUsedTools,
  otherTools,
  ...props
}: ICloudBar) => {
  const [isLargeSideBarExisting, setLargeSideBarExisting] = useState(false);
  const [isLargeSideBarExpanded, setLargeSideBarExpanded] = useState(false);

  const toggleShowAll = useCallback(() => {
    if (isLargeSideBarExisting) {
      setLargeSideBarExpanded(false);
    } else {
      setLargeSideBarExisting(true);
    }
  }, [isLargeSideBarExisting, setLargeSideBarExisting, setLargeSideBarExpanded]);

  useEffect(() => {
    if (isLargeSideBarExisting) {
      setLargeSideBarExpanded(true);
    }
  }, [isLargeSideBarExisting, setLargeSideBarExisting]);

  const onTransitionEnd = useCallback(
    evt => {
      if (evt.propertyName === "transform") {
        if (!isLargeSideBarExpanded && isLargeSideBarExisting) {
          setLargeSideBarExisting(false);
        }
      }
    },
    [isLargeSideBarExpanded, isLargeSideBarExisting, setLargeSideBarExisting]
  );

  const outsideClickRef = useOutsideClickDetector(toggleShowAll);

  return (
    <Container {...props}>
      <SideBar
        cloudHomeUrl={cloudHomeUrl}
        user={user}
        notificationCount={notificationCount || 0}
        lastUsedTools={lastUsedTools}
        isLarge={false}
        onShowAllToggle={toggleShowAll}
      />
      {isLargeSideBarExisting && (
        <LargeSideBarContainer expanded={isLargeSideBarExpanded} onTransitionEnd={onTransitionEnd}>
          <SideBar
            ref={outsideClickRef}
            cloudHomeUrl={cloudHomeUrl}
            user={user}
            notificationCount={notificationCount || 3}
            lastUsedTools={lastUsedTools}
            otherTools={otherTools}
            isLarge={true}
            onShowAllToggle={toggleShowAll}
          />
        </LargeSideBarContainer>
      )}
    </Container>
  );
};

export default CloudBar;
