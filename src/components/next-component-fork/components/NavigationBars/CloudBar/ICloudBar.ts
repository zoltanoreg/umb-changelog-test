import React from 'react';

export interface ITools {
  icon: string;
  name: string;
  path: string;
  selected: boolean;
}

export interface ICloudBar extends React.HTMLAttributes<{}> {
  cloudHomeUrl: string;
  lastUsedTools?: ITools[];
  notificationCount?: number;
  otherTools?: ITools[];
  user: {
    name: string;
    notificationsLink: any;
    profileLink: any;
    settingsLink: any;
  };
}

export interface ISideBar {
  cloudHomeUrl?: string;
  isLarge: boolean;
  lastUsedTools?: any[];
  notificationCount: number;
  otherTools?: any[];
  ref: React.Ref<HTMLDivElement>;
  user?: any;
  onShowAllToggle(): void;
}

export interface ISideBarItem extends React.HTMLAttributes<HTMLAnchorElement> {
  as?: any;
  badge?: any;
  href?: string;
  icon: any;
  isLarge?: boolean;
  label?: any;
  selected?: boolean;
  showLabel?: boolean;
  target?: string;
}

export interface ISideBarItemNode extends React.HTMLAttributes<HTMLDivElement> {
  as?: any;
  badge?: any;
  icon: any;
  isLarge?: boolean;
  label?: any;
  selected?: boolean;
  showLabel?: boolean;
  target?: string;
}

export interface ISideBarCloudHomeItem extends React.HTMLAttributes<HTMLAnchorElement> {
  href?: any;
  isLarge?: boolean;
}
