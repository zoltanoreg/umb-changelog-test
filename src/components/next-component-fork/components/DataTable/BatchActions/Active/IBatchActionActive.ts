export interface IBatchActionActiveAction {
  disabled: boolean;
  icon?: JSX.Element;
  title: string;
  onClick?(e: any): void;
}

export interface IBatchActionActive {
  actions?: IBatchActionActiveAction[];
  label: string;
  onCancel(): void;
}
