import deepEqual from "deep-equal";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getConfigBroker } from "../../../actions/configfile/broker/actions";
import { getFormattedDate } from "../../../helper/util";
import { FilterType, IFilterConfig } from "../../../models/IFilterConfig";
import {
  ITableConfig,
  ITableHeaderCells,
  ITableRow,
  OrderType,
} from "../../../models/ITableConfig";
import { RootState } from "../../../reducers";
import { ConfigService } from "../../../services/ConfigService";
import { SActiveLink, SSpacer } from "../../../styles/styles";
import { Filter } from "../../Filter";
import { Table } from "../../Table";

export const ConfigurationBrokerList = (props: any) => {
  const dispatch = useDispatch();
  const { configFileGenerate } = useSelector((state: RootState) => ({
    configFileGenerate: state.configFileGenerate,
  }));

  const [brokerTypes, setBrokerTypes]: any[] = useState([{}]);

  const { allBrokerType, configFileBroker } = useSelector((state: RootState) => ({
    allBrokerType: state.allBrokerType,
    configFileBroker: state.configFileBroker,
  }));

  const emptyFilterObject = {
    brokerType: "",
    createdDate: { startDate: undefined, endDate: undefined },
    fileName: "",
    fileVersion: "",
  };

  const [config, setConfig] = useState({
    filter: { ...emptyFilterObject },
    limit: 10,
    orderBy: "fileName",
    orderType: OrderType.ASC,
    page: 0,
  });

  const headCells: ITableHeaderCells[] = [
    {
      id: "fileName",
      label: "File name",
      value: (row: ITableRow) => (
        <SActiveLink onClick={() => ConfigService.s3ProxyUrl(row.fileName)}>
          {row.fileName}
        </SActiveLink>
      ),
    },
    { id: "fileVersion", label: "File version" },
    { id: "brokerType", label: "Broker type", width: "2fr" },
    {
      id: "createdAt",
      label: "Created at",
      value: (row: ITableRow) => getFormattedDate(row.createdAt),
    },
  ];

  useEffect(() => {
    if (!configFileGenerate.loading && !configFileGenerate.error) {
      dispatch(getConfigBroker(config));
    }
  }, [configFileGenerate]);

  useEffect(() => {
    dispatch(getConfigBroker(config));
  }, [config]);

  const handleChangePage = (page: number): void => {
    setConfig({ ...config, page });
  };

  useEffect(() => {
    setBrokerTypes(
      allBrokerType.content.map((broker: any) => ({
        label: <span className={`ta-dropdown-${broker.brokerType}`}>{broker.brokerType}</span>,
        value: broker.brokerType,
      }))
    );
  }, [allBrokerType]);

  const filterConfig: IFilterConfig = {
    items: [
      {
        name: "fileName",
        placeholder: "File Name",
        taClass: "fileName",
      },
      {
        name: "fileVersion",
        placeholder: "File Version",
        taClass: "fileVersion",
      },
      {
        data: brokerTypes,
        name: "brokerType",
        placeholder: "Broker Type",
        taClass: "brokerType",
        type: FilterType.DROPDOWN,
      },
      {
        name: "createdDate",
        placeholder: "Created",
        taClass: "createdDate",
        type: FilterType.DATEPICKER,
      },
    ],
    pageName: "configurationBrokerList",
    returnFilter: (filter: any) => {
      if (!deepEqual(config.filter, filter)) {
        setConfig({ ...config, page: 0, filter: { ...filter } });
      }
    },
  };

  const paginationCallback = (page: number) => {
    setConfig({ ...config, page });
  };

  const sortCallback = (column: string, direction: OrderType) => {
    setConfig({ ...config, orderBy: column, orderType: direction });
  };

  const tableProps: ITableConfig = {
    head: {
      cells: headCells,
    },
    list: {
      ...configFileBroker,
    },
    name: "applications",
    paginationConfig: {
      limit: config.limit,
      onPageChange: paginationCallback,
      page: config.page,
    },
    sortConfig: {
      onSort: sortCallback,
      orderBy: config.orderBy,
      orderType: config.orderType,
    },
  };

  return (
    <>
      <SSpacer />
      {/* FILTER */}
      <Filter {...filterConfig} />

      <SSpacer />

      {/* TABLE */}
      <Table {...tableProps}></Table>
    </>
  );
};
