import deepEqual from "deep-equal";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getAllBrokerType } from "../../../actions/broker-type/get-all/actions";
import { getConfigApplication } from "../../../actions/configfile/application/actions";
import { getFormattedDate } from "../../../helper/util";
import { FilterType, IFilterConfig } from "../../../models/IFilterConfig";
import {
  ITableConfig,
  ITableHeaderCells,
  ITableRow,
  OrderType,
} from "../../../models/ITableConfig";
import { RootState } from "../../../reducers";
import { ConfigService } from "../../../services/ConfigService";
import { SActiveLink, SSpacer } from "../../../styles/styles";
import { Filter } from "../../Filter";
import { Table } from "../../Table";

export const ConfigurationAppList = (props: any) => {
  const dispatch = useDispatch();
  const { configFileGenerate } = useSelector((state: RootState) => ({
    configFileGenerate: state.configFileGenerate,
  }));

  const [brokerTypes, setBrokerTypes]: any[] = useState([{}]);

  const { configFileApplication, allBrokerType } = useSelector((state: RootState) => ({
    allBrokerType: state.allBrokerType,
    configFileApplication: state.configFileApplication,
  }));

  const emptyFilterObject = {
    appName: "",
    appVersion: "",
    brokerType: "",
    createdDate: { startDate: null, endDate: null },
    fileName: "",
    fileVersion: "",
  };

  const [config, setConfig] = useState({
    filter: { ...emptyFilterObject },
    limit: 10,
    orderBy: "appName",
    orderType: OrderType.ASC,
    page: 0,
  });

  // Get configs only once
  useEffect(() => {
    dispatch(
      getAllBrokerType({
        filter: {},
        limit: 0,
        orderBy: "brokerTypeName",
        orderType: "ASC",
        page: 0,
      })
    );
  }, []);

  // Get configs after "generate config"
  useEffect(() => {
    if (!configFileGenerate.loading && !configFileGenerate.error) {
      dispatch(getConfigApplication(config));
    }
  }, [configFileGenerate]);

  useEffect(() => {
    dispatch(getConfigApplication(config));
  }, [config]);

  const headCells: ITableHeaderCells[] = [
    { id: "appName", label: "App Name" },
    { id: "appVersion", label: "Version" },
    { id: "brokerType", label: "Broker type" },
    {
      id: "fileName",
      label: "File name",
      value: (row: ITableRow) => (
        <SActiveLink onClick={() => ConfigService.s3ProxyUrl(row.fileName)}>
          {row.fileName}
        </SActiveLink>
      ),
    },
    { id: "fileVersion", label: "File version" },
    {
      id: "createdAt",
      label: "Created at",
      value: (row: ITableRow) => getFormattedDate(row.createdAt),
    },
  ];

  useEffect(() => {
    setBrokerTypes(
      allBrokerType.content.map((broker: any) => ({
        label: <span className={`ta-dropdown-${broker.brokerType}`}>{broker.brokerType}</span>,
        value: broker.brokerType,
      }))
    );
  }, [allBrokerType]);

  const filterConfig: IFilterConfig = {
    items: [
      {
        name: "appName",
        placeholder: "App Name",
        taClass: "appName",
      },
      {
        name: "appVersion",
        placeholder: "ver#",
        taClass: "version",
      },
      {
        name: "fileName",
        placeholder: "File Name",
        taClass: "fileName",
      },
      {
        name: "fileVersion",
        placeholder: "File Version",
        taClass: "fileVersion",
      },
      {
        data: brokerTypes,
        name: "brokerType",
        placeholder: "Broker Type",
        taClass: "brokerType",
        type: FilterType.DROPDOWN,
      },
      {
        name: "createdDate",
        placeholder: "Created",
        taClass: "createdDate",
        type: FilterType.DATEPICKER,
      },
    ],
    pageName: "configurationAppList",
    returnFilter: (filter: any) => {
      if (!deepEqual(config.filter, filter)) {
        setConfig({ ...config, page: 0, filter: { ...filter } });
      }
    },
  };

  const paginationCallback = (page: number) => {
    setConfig({ ...config, page });
  };

  const sortCallback = (column: string, direction: OrderType) => {
    setConfig({ ...config, orderBy: column, orderType: direction });
  };

  const tableProps: ITableConfig = {
    head: {
      cells: headCells,
    },
    list: {
      ...configFileApplication,
    },
    name: "application",
    paginationConfig: {
      limit: config.limit,
      onPageChange: paginationCallback,
      page: config.page,
    },
    sortConfig: {
      onSort: sortCallback,
      orderBy: config.orderBy,
      orderType: config.orderType,
    },
  };

  return (
    <>
      <SSpacer />
      <Filter {...filterConfig} />
      {/* FILTER */}
      <SSpacer />

      {/* TABLE */}
      <Table {...tableProps}></Table>
    </>
  );
};
