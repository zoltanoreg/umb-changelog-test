import {
  ButtonPrimary,
  Grid,
  GridItem,
  ProgressIndicatorCircular,
  Tab,
  TabContainer,
} from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { generateConfig } from "../../actions/configfile/generate/actions";
import { configTabEnum } from "../../models/IConfigurationFilesConfig";
import { ADMINISTRATOR, UMBT_OPERATIONAL_SUPPORT_TEAM } from "../../models/UserTypes";
import { RootState } from "../../reducers";
import { SPageContainer, SPageHeading } from "../../styles/styles";

import { ConfigurationAppList } from "./ConfigurationFiles/ConfigurationAppList";
import { ConfigurationBrokerList } from "./ConfigurationFiles/ConfigurationBrokerList";

export const ConfigurationFiles = () => {
  const dispatch = useDispatch();
  const { currentUser, configFileGenerate } = useSelector((state: RootState) => ({
    configFileGenerate: state.configFileGenerate,
    currentUser: state.currentUser,
  }));

  const [isAdmin, setIsAdmin] = useState(false);
  const [isOperationalSupport, setIsOperationalSupport] = useState(false);
  const [activeTab, setActiveTab] = useState(configTabEnum.APP);

  useEffect(() => {
    if (currentUser) {
      setIsAdmin(currentUser.data.role === ADMINISTRATOR);
      setIsOperationalSupport(currentUser.data.role === UMBT_OPERATIONAL_SUPPORT_TEAM);
    }
  }, [currentUser]);

  const tabList = [
    { class: "ta-config-tab-apps", id: configTabEnum.APP, title: "Application Clients" },
    { class: "ta-config-tab-brokers", id: configTabEnum.BROKER, title: "Brokers" },
  ];

  const handleTabChange = (tabId: configTabEnum) => {
    setActiveTab(tabId);
  };

  const handleGenerateConfigs = () => {
    dispatch(generateConfig());
  };

  return (
    <SPageContainer>
      <SPageHeading>Configuration Files</SPageHeading>
      {/* TABS */}
      <Grid columns="1fr 2fr">
        <GridItem>
          <TabContainer>
            {tabList.map((tab: any, index: any) => (
              <Tab
                key={index}
                className={tab.class}
                active={activeTab === tab.id}
                onClick={() => {
                  handleTabChange(tab.id);
                }}
              >
                {tab.title}
              </Tab>
            ))}
          </TabContainer>
        </GridItem>
        <GridItem style={{ justifySelf: "end" }}>
          <ButtonPrimary
            className="ta-config-generate-btn"
            onClick={handleGenerateConfigs}
            disabled={!(isAdmin || isOperationalSupport) || configFileGenerate.loading}
          >
            {!configFileGenerate.loading && <>Generate all config files</>}
            {configFileGenerate.loading && <ProgressIndicatorCircular size="small" />}
          </ButtonPrimary>
        </GridItem>
      </Grid>

      {activeTab === configTabEnum.APP && <ConfigurationAppList />}
      {activeTab === configTabEnum.BROKER && <ConfigurationBrokerList />}

      {/* TAB CONTENT */}
    </SPageContainer>
  );
};
