import moment from "moment-timezone";
import { Grid, GridItem } from "next-components";
import React from "react";

import { getTAClass, TA_TYPES } from "../../../helper/taHelper";
import { ITopicDetailsContent } from "../../../models/ITopicDetailsConfig";
import { SSpacer } from "../../../styles/styles";
import { DataDetailLabel } from "../../DataDetailLabel";

export const TopicDetailsTabDetails = (props: { data: ITopicDetailsContent }) => {
  const topicData = { ...props.data };

  enum fieldTypeEnum {
    DATE,
    STRING,
  }

  const topicDetailsColumns = [
    [
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "priority"),
        title: "Priority",
        type: fieldTypeEnum.STRING,
        value: topicData.priority,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "isSecure"),
        title: "Secure",
        type: fieldTypeEnum.STRING,
        value: topicData.isSecure ? "Yes" : "No",
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "topicType"),
        title: "Topic Type",
        type: fieldTypeEnum.STRING,
        value: topicData.topicType,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "topicCategory"),
        title: "Topic Category",
        type: fieldTypeEnum.STRING,
        value: topicData.topicCategory,
      },
    ],
    [
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "featured"),
        title: "Feature name",
        type: fieldTypeEnum.STRING,
        value: topicData.featureName,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "serviceName"),
        title: "Service name",
        type: fieldTypeEnum.STRING,
        value: topicData.serviceName,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "ttl"),
        title: "TTL",
        type: fieldTypeEnum.STRING,
        value: topicData.ttl,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "offload"),
        title: "Offload type",
        type: fieldTypeEnum.STRING,
        value: topicData.offloadType,
      },
    ],
    [
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "qosLevel"),
        title: "QoS level",
        type: fieldTypeEnum.STRING,
        value: topicData.qosLevel,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "obfuscation"),
        title: "Obfuscation",
        type: fieldTypeEnum.STRING,
        value: topicData.obfuscationRequirement,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "dependencies"),
        title: "Dependencies",
        type: fieldTypeEnum.STRING,
        value: topicData.dependencies,
      },
    ],
    [
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "createdBy"),
        title: "CreatedBy",
        type: fieldTypeEnum.STRING,
        value: topicData.createdBy && topicData.createdBy.fullName,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "createdAt"),
        title: "CreatedAt",
        type: fieldTypeEnum.DATE,
        value: topicData.createdAt,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "modifiedBy"),
        title: "ModifiedBy",
        type: fieldTypeEnum.STRING,
        value: topicData.modifiedBy && topicData.modifiedBy.fullName,
      },
      {
        class: getTAClass("topicDetails", TA_TYPES.TEXT, "modifiedAt"),
        title: "ModifiedAt",
        type: fieldTypeEnum.DATE,
        value: topicData.modifiedAt,
      },
    ],
  ];

  return (
    <>
      <Grid>
        <GridItem>{topicData.description}</GridItem>
      </Grid>
      <SSpacer />

      <Grid columns="repeat(4, 1fr)">
        {topicDetailsColumns.map((column: any, colIndex: any) => (
          <GridItem key={colIndex}>
            {column.map((field: any, index: any) => (
              <DataDetailLabel
                title={field.title}
                value={
                  field.type === fieldTypeEnum.STRING
                    ? field.value
                    : moment(field.value)
                        .tz("America/Los_Angeles")
                        .format("MM.DD.YYYY HH:mm")
                }
                className={field.class}
                key={index}
              ></DataDetailLabel>
            ))}
          </GridItem>
        ))}
      </Grid>
    </>
  );
};
