import { Grid, GridItem } from "next-components";
import React from "react";

import { getTAClass, TA_TYPES } from "../../../helper/taHelper";
import { DataDetailLabel } from "../../DataDetailLabel";

export const TopicDetailsMainDetails = (props: { data: any }) => (
  <Grid columns="1">
    <GridItem>
      <DataDetailLabel
        title="Status"
        value={props.data && props.data.status}
        className={getTAClass("topicDetails", TA_TYPES.TEXT, "status")}
      ></DataDetailLabel>
      <DataDetailLabel
        title="Topic Owner"
        value={`${props.data && props.data.appClient.appName} ${props.data &&
          props.data.appClient.appVersion}`}
        className={getTAClass("topicDetails", TA_TYPES.TEXT, "owner")}
        link={`/applicationClients/${props.data && props.data.appClient.appClientId}`}
      ></DataDetailLabel>
      <DataDetailLabel
        title="Topic Owner Admin"
        value={
          props.data &&
          props.data.appClient &&
          props.data.appClient.appOwner &&
          props.data.appClient.appOwner.fullName
        }
        className={getTAClass("topicDetails", TA_TYPES.TEXT, "ownerAdmin")}
      ></DataDetailLabel>
    </GridItem>
  </Grid>
);
