import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { IGetTopicListData } from "../../../models/ITopicConfig";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import { RootState } from "../../../reducers";
import { TopicService } from "../../../services/TopicService";
import { TopicClientList } from "../../TopicClientList";

export const TopicDetailsPublishList = (props: any) => {
  const dispatch = useDispatch();

  const [isOwner, setIsOwner] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [subCount, setSubCount] = useState(0);

  const { topicDetails } = useSelector((state: RootState) => ({
    topicDetails: state.topicDetails,
  }));

  useEffect(() => {
    TopicService.getTopicClientList({
      filter: {
        clientType: "SUBSCRIBER",
        topicId: topicDetails.content.topic.topicId,
      },
      limit: 1,
      orderBy: "topicName",
      orderType: "ASC",
    })
      .then(({ data }: { data: { data: IGetTopicListData } }) => {
        setSubCount(data.data.count);
      })
      .catch(e => {
        console.error(e);
      });
  }, [topicDetails]);

  useEffect(() => {
    if (props.userData && props.appContent) {
      setIsAdmin(props.userData.role === ADMINISTRATOR);
      setIsOwner(
        props.appContent.appOwner && props.appContent.appOwner.userId === props.userData.userId
      );
    }
  }, [props]);

  return <TopicClientList type="PUBLISHER" subscriberCount={subCount} />;
};
