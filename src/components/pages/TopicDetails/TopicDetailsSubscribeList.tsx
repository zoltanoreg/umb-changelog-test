import React, { useEffect, useState } from "react";

import { ADMINISTRATOR } from "../../../models/UserTypes";
import { TopicClientList } from "../../TopicClientList";

export const TopicDetailsSubscriberList = (props: any) => {
  const [isOwner, setIsOwner] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    if (props.userData && props.appContent) {
      setIsAdmin(props.userData.role === ADMINISTRATOR);
      setIsOwner(
        props.appContent.appOwner && props.appContent.appOwner.userId === props.userData.userId
      );
    }
  }, [props]);

  return <TopicClientList type="SUBSCRIBER" />;
};
