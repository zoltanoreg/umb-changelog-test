import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import { editModalOpenAction } from "../../../actions/modal/edit/actions";
import { modalTypeEnum } from "../../../models/IEditModalConfig";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import { AppTopicsList } from "../../AppTopicsList";

export const TopicListPublish = (props: any) => {
  const dispatch = useDispatch();

  const [isActive, setIsActive] = useState(false);
  const [isOwner, setIsOwner] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    if (props.userData && props.appContent) {
      setIsAdmin(props.userData.role === ADMINISTRATOR);
      setIsOwner(
        props.appContent.appOwner && props.appContent.appOwner.userId === props.userData.userId
      );
      setIsActive(props.appContent.status === "ACTIVE");
    }
  }, [props]);

  const openNewPublisherModal = (topicFilter: any) => {
    dispatch(
      editModalOpenAction({
        appData: { ...props.appContent },
        topicFilter: { ...topicFilter },
        type: modalTypeEnum.CREATE_TOPIC_PUBLISH,
      })
    );
  };

  return (
    <AppTopicsList
      type="PUBLISHER"
      btnDisabled={!(isActive && (isOwner || isAdmin))}
      btnClick={(topicFilter: any) => {
        openNewPublisherModal(topicFilter);
      }}
    />
  );
};
