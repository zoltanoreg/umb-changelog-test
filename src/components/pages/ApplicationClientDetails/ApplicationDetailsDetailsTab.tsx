import moment from "moment-timezone";
import { Grid, GridItem } from "next-components";
import React from "react";

import { getTAClass, TA_TYPES } from "../../../helper/taHelper";
import { authTypeEnum } from "../../../models/IAppDetailsConfig";
import { STallText, STextDataTitle, STextDataValue } from "../../../styles/styles";

export const ApplicationDetailsDetailsTab = (props: any) => (
  <>
    <Grid columns="repeat(3, auto)">
      <GridItem>
        <STallText variant="uiText" className="ta-application-details-system-name">
          <STextDataTitle>System Name:</STextDataTitle>
          <STextDataValue>{props.appContent.systemName}</STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-application-details-authentication-type">
          <STextDataTitle>Authentication Type:</STextDataTitle>
          <STextDataValue>{props.appContent.authenticationType}</STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-application-details-num-conn-retries">
          <STextDataTitle>Number of connection retries:</STextDataTitle>
          <STextDataValue>{props.appContent.numConnRetries}</STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-application-details-conn-retry-delay">
          <STextDataTitle>Connection retry delay (sec):</STextDataTitle>
          <STextDataValue>{props.appContent.connRetryDelaySec}</STextDataValue>
        </STallText>
      </GridItem>
      <GridItem>
        {props.appContent.authenticationType === authTypeEnum.CREDENTIALS && (
          <>
            <STallText variant="uiText" className="ta-application-details-path-to-ca">
              <STextDataTitle>User Name:</STextDataTitle>
              <STextDataValue>{props.appContent.userName}</STextDataValue>
            </STallText>
            <STallText variant="uiText" className="ta-application-details-path-to-client-ca">
              <STextDataTitle>Password:</STextDataTitle>
              <STextDataValue title={props.appContent.password}>****</STextDataValue>
            </STallText>
          </>
        )}
        {props.appContent.authenticationType === authTypeEnum.CERTIFICATE && (
          <>
            <STallText variant="uiText" className="ta-application-details-path-to-ca">
              <STextDataTitle>Path to CA:</STextDataTitle>
              <STextDataValue>{props.appContent.pathToCa}</STextDataValue>
            </STallText>
            <STallText variant="uiText" className="ta-application-details-path-to-client-ca">
              <STextDataTitle>Path to Client CA:</STextDataTitle>
              <STextDataValue>{props.appContent.pathClientCa}</STextDataValue>
            </STallText>
          </>
        )}
      </GridItem>
      <GridItem>
        <STallText variant="uiText" className="ta-application-details-created-by">
          <STextDataTitle>Created by:</STextDataTitle>
          <STextDataValue>
            {props.appContent.createdBy && props.appContent.createdBy.fullName}
          </STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-application-details-created-at">
          <STextDataTitle>Created at:</STextDataTitle>
          <STextDataValue>
            {props.appContent.createdAt &&
              moment(props.appContent.createdAt)
                .tz("America/Los_Angeles")
                .format("MM.DD.YYYY HH:mm")}
          </STextDataValue>
        </STallText>
        <STallText
          variant="uiText"
          className={getTAClass("applicationDetails", TA_TYPES.INPUT, "modifiedBy")}
        >
          <STextDataTitle>Modified by:</STextDataTitle>
          <STextDataValue>
            {props.appContent.modifiedBy && props.appContent.modifiedBy.fullName}
          </STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-application-details-modified-at">
          <STextDataTitle>Modified at:</STextDataTitle>
          <STextDataValue>
            {props.appContent.modifiedAt &&
              moment(props.appContent.modifiedAt)
                .tz("America/Los_Angeles")
                .format("MM.DD.YYYY HH:mm")}
          </STextDataValue>
        </STallText>
      </GridItem>
    </Grid>
  </>
);
