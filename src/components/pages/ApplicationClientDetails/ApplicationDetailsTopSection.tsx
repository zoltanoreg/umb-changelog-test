import { ButtonPrimary, Grid, GridItem, Heading } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import { deleteApplication } from "../../../actions/application/delete/actions";
import { editModalOpenAction } from "../../../actions/modal/edit/actions";
import { popupOpenAction } from "../../../actions/popup/actions";
import { getTAClass, TA_TYPES } from "../../../helper/taHelper";
import { modalTypeEnum } from "../../../models/IEditModalConfig";
import { ADMINISTRATOR } from "../../../models/UserTypes";
import { SFlexContainer, SFlexItem, SSpacer, SVerticalSpacer } from "../../../styles/styles";
import { DataDetailLabel } from "../../DataDetailLabel";

export const ApplicationDetailsTopSection = (props: any) => {
  const dispatch = useDispatch();

  const [isActive, setIsActive] = useState(false);
  const [isOwner, setIsOwner] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    if (props.userData && props.appContent) {
      setIsAdmin(props.userData.role === ADMINISTRATOR);
      setIsOwner(
        props.appContent.appOwner && props.appContent.appOwner.userId === props.userData.userId
      );
      setIsActive(props.appContent.status === "ACTIVE");
    }
  }, [props]);

  const handleOpenEditModal = () => {
    dispatch(
      editModalOpenAction({ type: modalTypeEnum.EDIT_APP, appData: { ...props.appContent } })
    );
  };

  const handleDeleteApplication = () => {
    dispatch(deleteApplication(props.appContent.appClientId));
  };

  const askForConfirmation = () => {
    dispatch(
      popupOpenAction({
        confirmText: "ConfirmText",
        content: "Are you sure you want to delete this application?",
        onConfirm: handleDeleteApplication,
        title: `Please confirm`,
        type: "Confirmation",
      })
    );
  };

  return (
    <>
      <Grid>
        <GridItem>
          <SFlexContainer justifyContent="space-between">
            <SFlexItem>
              <Heading variant="heading2">
                <span className={getTAClass("applicationDetails", TA_TYPES.TITLE, "appName")}>
                  {props.appContent.appName}
                </span>{" "}
                <span className={getTAClass("applicationDetails", TA_TYPES.TITLE, "version")}>
                  {props.appContent.appVersion}
                </span>
              </Heading>
            </SFlexItem>
            <SFlexItem>
              <ButtonPrimary
                disabled={!(isActive && (isOwner || isAdmin))}
                className={getTAClass("applicationDetails", TA_TYPES.BUTTON, "edit")}
                onClick={handleOpenEditModal}
              >
                Edit
              </ButtonPrimary>
              <SVerticalSpacer />
              <ButtonPrimary
                disabled
                className={getTAClass("applicationDetails", TA_TYPES.BUTTON, "generate")}
              >
                Generate Config
              </ButtonPrimary>
              <SVerticalSpacer />
              <ButtonPrimary
                disabled={!(isActive && (isOwner || isAdmin))}
                className={getTAClass("applicationDetails", TA_TYPES.BUTTON, "delete")}
                onClick={askForConfirmation}
              >
                Delete
              </ButtonPrimary>
            </SFlexItem>
          </SFlexContainer>
        </GridItem>
      </Grid>
      <SSpacer />
      <Grid columns="repeat(1, auto)">
        <GridItem>
          <DataDetailLabel
            title="Broker Type"
            value={props.appContent.appBrokerType && props.appContent.appBrokerType.brokerType}
            className={getTAClass("applicationDetails", TA_TYPES.TEXT, "brokerType")}
          ></DataDetailLabel>
          <DataDetailLabel
            title="Owner"
            value={props.appContent.appOwner && props.appContent.appOwner.fullName}
            className={getTAClass("applicationDetails", TA_TYPES.TEXT, "owner")}
          ></DataDetailLabel>
          <DataDetailLabel
            title="Status"
            value={props.appContent && props.appContent.status}
            className={getTAClass("applicationDetails", TA_TYPES.TEXT, "status")}
          ></DataDetailLabel>
        </GridItem>
      </Grid>
    </>
  );
};
