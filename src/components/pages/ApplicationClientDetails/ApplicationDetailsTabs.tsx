import { Grid, GridItem, Tab, TabContainer } from "next-components";
import React, { useState } from "react";

import { tabAppDetailsEnum } from "../../../models/IAppDetailsConfig";

export const ApplicationDetailsTabs = (props: { activeTab: any; onChange: any }) => (
  <Grid columns="1fr 1fr 1fr">
    <GridItem>
      <TabContainer>
        <Tab
          className="ta-application-details-tab-topic-subscribe"
          onClick={() => {
            props.onChange(tabAppDetailsEnum.SUBSCRIBE);
          }}
          active={props.activeTab === tabAppDetailsEnum.SUBSCRIBE}
        >
          Topics to subscribe
        </Tab>
        <Tab
          className="ta-application-details-tab-topic-publish"
          onClick={() => {
            props.onChange(tabAppDetailsEnum.PUBLISH);
          }}
          active={props.activeTab === tabAppDetailsEnum.PUBLISH}
        >
          Topics to publish
        </Tab>
        <Tab
          className="ta-application-details-tab-details"
          onClick={() => {
            props.onChange(tabAppDetailsEnum.DETAILS);
          }}
          active={props.activeTab === tabAppDetailsEnum.DETAILS}
        >
          Details
        </Tab>
      </TabContainer>
    </GridItem>
  </Grid>
);
