import moment from "moment-timezone";
import { Grid, GridItem } from "next-components";
import React from "react";

import { STallText, STextDataTitle, STextDataValue } from "../../../styles/styles";

export const BrokerTypesDetailsTabDetails = ({ content }: { content: any }) => (
  <>
    <Grid columns="repeat(3, auto)">
      <GridItem>
        <STallText variant="uiText" className="ta-broker-type-details-broker-type">
          <STextDataTitle>Broker Type:</STextDataTitle>
          <STextDataValue>{content.brokerType}</STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-broker-type-details-system-type">
          <STextDataTitle>System Type:</STextDataTitle>
          <STextDataValue>{content.systemType}</STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-broker-type-details-max-in-flight-messages">
          <STextDataTitle>Max in flight messages:</STextDataTitle>
          <STextDataValue>{content.maxInFlightMessages}</STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-broker-type-details-disconnect-delay">
          <STextDataTitle>Disconnect delay:</STextDataTitle>
          <STextDataValue>{content.disconnectDelay}</STextDataValue>
        </STallText>
      </GridItem>
      <GridItem>
        <STallText variant="uiText" className="ta-broker-type-details-path-to-ca">
          <STextDataTitle>Path to CA:</STextDataTitle>
          <STextDataValue>{content.pathToCa}</STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-broker-type-details-path-to-private">
          <STextDataTitle>Path to Private:</STextDataTitle>
          <STextDataValue>{content.pathToPrivate}</STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-broker-type-details-path-to-cba-ica">
          <STextDataTitle>Path to CBA ICA:</STextDataTitle>
          <STextDataValue>{content.pathToCbaIca}</STextDataValue>
        </STallText>
      </GridItem>
      <GridItem>
        <STallText variant="uiText" className="ta-broker-type-details-created-by">
          <STextDataTitle>Created by:</STextDataTitle>
          <STextDataValue>{content.createdBy && content.createdBy.fullName}</STextDataValue>
        </STallText>
        <STallText variant="uiText" className="ta-broker-type-details-created-at">
          <STextDataTitle>Created at:</STextDataTitle>
          <STextDataValue>
            {content.createdAt &&
              moment(content.createdAt)
                .tz("America/Los_Angeles")
                .format("MM.DD.YYYY HH:mm")}
          </STextDataValue>
        </STallText>
      </GridItem>
    </Grid>
  </>
);
