import {
  ButtonIcon,
  ButtonPrimary,
  Grid,
  GridItem,
  Heading,
  IconArrowLeft32,
} from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import { editModalOpenAction } from "../../actions/modal/edit/actions";
import { popupOpenAction } from "../../actions/popup/actions";
import { deleteTopic } from "../../actions/topic/delete/actions";
import { editTopic } from "../../actions/topic/edit/actions";
import { getTopicDetails } from "../../actions/topic/get-details/actions";
import { getTAClass, TA_TYPES } from "../../helper/taHelper";
import { modalTypeEnum } from "../../models/IEditModalConfig";
import { topicStatusEnum } from "../../models/ITopicConfig";
import { tabEnum } from "../../models/ITopicDetailsConfig";
import { ADMINISTRATOR } from "../../models/UserTypes";
import { RootState } from "../../reducers";
import {
  SFlexContainer,
  SFlexItem,
  SOverlay,
  SPageContainer,
  SPageHeading,
  SSpacer,
  SVerticalSpacer,
} from "../../styles/styles";
import { ModalEditTopic } from "../modals/ModalEditTopic";
import { PageError, PageLoading } from "../PageState";

import { TopicBridgesGraph } from "./TopicDetails/TopicBridgesGraph";
import { TopicDetailsBridgesList } from "./TopicDetails/TopicBridgesList";
import { TopicDetailsMainDetails } from "./TopicDetails/TopicDetailsMainDetails";
import { TopicDetailsPayload } from "./TopicDetails/TopicDetailsPayload";
import { TopicDetailsPublishList } from "./TopicDetails/TopicDetailsPublishList";
import { TopicDetailsSubscriberList } from "./TopicDetails/TopicDetailsSubscribeList";
import { TopicDetailsTabDetails } from "./TopicDetails/TopicDetailsTabDetails";
import { TopicDetailsTabs } from "./TopicDetails/TopicDetailsTabs";

// tslint:disable-next-line: cyclomatic-complexity
export const TopicDetails = ({ match }: { match: any }) => {
  const dispatch = useDispatch();

  const [topicId, setTopicId] = useState("");
  const [isTopicsAppOwner, setIsTopicsAppOwner] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [isOnHold, setIsOnHold] = useState(false);
  const [isDraft, setIsDraft] = useState(false);
  const [activeTab, setActiveTab] = useState(tabEnum.DETAILS);

  const { currentUser, editModal, topicDetails } = useSelector((state: RootState) => ({
    currentUser: state.currentUser,
    editModal: state.editModal,
    topicDetails: state.topicDetails,
    // M topicPayloadId: state.topicDetails.topicPayloadId,
  }));

  useEffect(() => {
    if (match.params.topicId !== "" && typeof match.params.topicId !== "undefined") {
      setTopicId(match.params.topicId);
      dispatch(getTopicDetails(match.params.topicId));
    }
  }, [match.params.topicId]);

  useEffect(() => {
    if (
      currentUser &&
      topicDetails &&
      topicDetails.content &&
      topicDetails.content.topic &&
      !topicDetails.loading
    ) {
      setIsAdmin(currentUser.data.role === ADMINISTRATOR);
      setIsTopicsAppOwner(
        topicDetails.content.topic.appClient &&
          topicDetails.content.topic.appClient.appOwner &&
          topicDetails.content.topic.appClient.appOwner.userId === currentUser.data.userId
      );
      setIsDeleted(topicDetails.content.topic.status.toUpperCase() === topicStatusEnum.DELETED);
      setIsOnHold(topicDetails.content.topic.status.toUpperCase() === topicStatusEnum.ONHOLD);
      setIsDraft(topicDetails.content.topic.status.toUpperCase() === topicStatusEnum.DRAFT);
    }
  }, [topicDetails, currentUser]);

  const handleTabChange = (id: tabEnum) => {
    setActiveTab(id);
  };

  const handleDeleteSuccessCb = (topicIdUpdated: any) => {
    dispatch(getTopicDetails(topicIdUpdated));
  };

  const handleDeleteTopic = () => {
    dispatch(
      popupOpenAction({
        confirmText: "ConfirmText",
        content: "Are you sure you want to delete this topic?",
        onConfirm: () => {
          dispatch(deleteTopic(topicId, handleDeleteSuccessCb));
        },
        title: `Please confirm`,
        type: "Confirmation",
      })
    );
  };

  const changeStateSuccessCallback = () => {
    dispatch(getTopicDetails(topicId));
  };

  const handlePutOnHold = () => {
    dispatch(
      editTopic(
        {
          status: topicStatusEnum.ONHOLD,
        },
        topicId,
        changeStateSuccessCallback
      )
    );
  };

  const handleActivate = () => {
    dispatch(
      editTopic(
        {
          status: topicStatusEnum.ACTIVE,
        },
        topicId,
        changeStateSuccessCallback
      )
    );
  };

  const handleEditTopic = () => {
    dispatch(
      editModalOpenAction({
        appData: { ...topicDetails.content.topic.appClient },
        topicData: {
          ...topicDetails.content.topic,
          topicPayloadId: topicDetails.content.topicPayloadId,
        },
        type: modalTypeEnum.EDIT_TOPIC,
      })
    );
  };

  return (
    <>
      <SPageContainer>
        {/* HEADER */}
        <Grid columns="auto 1fr" alignItems="center" gap="10px">
          <GridItem>
            <Link to="/topics">
              <ButtonIcon size="default" icon={<IconArrowLeft32 />} />
            </Link>
          </GridItem>
          <GridItem>
            <SPageHeading>Topic Details</SPageHeading>
          </GridItem>
        </Grid>

        {topicDetails.loading && <PageLoading />}

        {topicDetails.error && <PageError />}

        {!topicDetails.loading && !topicDetails.error && (
          <>
            {/* CONTENT */}
            {/* PAGE TITLE AND BUTTONS */}
            <Grid>
              <GridItem>
                <SFlexContainer justifyContent="space-between">
                  <SFlexItem>
                    <Heading
                      variant="heading2"
                      className={getTAClass("topicDetails", TA_TYPES.TITLE, "topicName")}
                    >
                      {topicDetails &&
                        topicDetails.content &&
                        topicDetails.content.topic &&
                        topicDetails.content.topic.topicName}
                    </Heading>
                  </SFlexItem>
                  <SFlexItem>
                    <ButtonPrimary
                      className={getTAClass("topicDetails", TA_TYPES.BUTTON, "edit")}
                      disabled={!(isAdmin || isTopicsAppOwner) || isDeleted}
                      onClick={handleEditTopic}
                    >
                      Edit
                    </ButtonPrimary>
                    <SVerticalSpacer />
                    {isDraft || isOnHold ? (
                      <ButtonPrimary
                        className={getTAClass("topicDetails", TA_TYPES.BUTTON, "onhold")}
                        onClick={handleActivate}
                        disabled={!(isAdmin || isTopicsAppOwner) || isDeleted}
                      >
                        Activate
                      </ButtonPrimary>
                    ) : (
                      <ButtonPrimary
                        className={getTAClass("topicDetails", TA_TYPES.BUTTON, "onhold")}
                        onClick={handlePutOnHold}
                        disabled={!(isAdmin || isTopicsAppOwner) || isDeleted}
                      >
                        Put On Hold
                      </ButtonPrimary>
                    )}
                    <SVerticalSpacer />
                    <ButtonPrimary
                      className={getTAClass("topicDetails", TA_TYPES.BUTTON, "delete")}
                      onClick={handleDeleteTopic}
                      disabled={!(isAdmin || isTopicsAppOwner) || isDeleted}
                    >
                      Delete
                    </ButtonPrimary>
                  </SFlexItem>
                </SFlexContainer>
              </GridItem>
            </Grid>
            <SSpacer />

            {/* TOPIC DETAILS */}
            <TopicDetailsMainDetails data={topicDetails && topicDetails.content.topic} />

            <SSpacer />

            {/* TABS */}
            <TopicDetailsTabs activeTab={activeTab} onChange={handleTabChange} />
            <SSpacer />

            {/* TAB CONTENTS */}
            {activeTab === tabEnum.SUBSCRIBERS && <TopicDetailsSubscriberList />}
            {activeTab === tabEnum.PUBLISHERS && <TopicDetailsPublishList />}
            {activeTab === tabEnum.BRIDGES && (
              <TopicDetailsBridgesList data={topicDetails && topicDetails.content.topic} />
            )}
            {activeTab === tabEnum.DETAILS && (
              <TopicDetailsTabDetails data={topicDetails && topicDetails.content.topic} />
            )}
            {activeTab === tabEnum.PAYLOAD && (
              <TopicDetailsPayload
                payloadId={
                  topicDetails &&
                  topicDetails.content.topic &&
                  topicDetails.content.topic.topicPayloadId
                }
              />
            )}
            {activeTab === tabEnum.TOPOLOGY && (
              <TopicBridgesGraph data={topicDetails && topicDetails.content.topic} />
            )}
          </>
        )}
      </SPageContainer>

      {editModal.open && editModal.type === modalTypeEnum.EDIT_TOPIC && <ModalEditTopic />}
    </>
  );
};
