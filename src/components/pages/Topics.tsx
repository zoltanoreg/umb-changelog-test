import deepEqual from "deep-equal";
import { ProgressIndicatorLinear } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

import { editModalOpenAction } from "../../actions/modal/edit/actions";
import { getTopicList } from "../../actions/topic/get-list/actions";
import { getTAClass, TA_TYPES } from "../../helper/taHelper";
import { getFormattedDate } from "../../helper/util";
import { FilterType, ICustomElement, IFilterConfig } from "../../models/IFilterConfig";
import { ITableConfig, ITableHeaderCells, ITableRow, OrderType } from "../../models/ITableConfig";
import { topicStatusEnum } from "../../models/ITopicConfig";
import { READ } from "../../models/UserTypes";
import { RootState } from "../../reducers";
import {
  SActiveLink,
  SButtonIcon,
  SIconAdd32,
  SOverlay,
  SPageContainer,
  SPageHeading,
} from "../../styles/styles";
import { Filter } from "../Filter";
import { ModalCreateTopic } from "../modals/ModalCreateTopic";
import { Table } from "../Table";

export const Topics = (router: any) => {
  const dispatch = useDispatch();

  const SSpacer = styled.div`
    height: 20px;
  `;

  const { currentUser, editModal, topicList } = useSelector((state: RootState) => ({
    currentUser: state.currentUser,
    editModal: state.editModal,
    topicList: state.topicList,
  }));

  const statusOptions = [
    {
      label: <span className="ta-dropdown-draft">Draft</span>,
      value: topicStatusEnum.DRAFT,
    },
    {
      label: <span className="ta-dropdown-active">Active</span>,
      value: topicStatusEnum.ACTIVE,
    },
    {
      label: <span className="ta-dropdown-onhold">On Hold</span>,
      value: topicStatusEnum.ONHOLD,
    },
    {
      label: <span className="ta-dropdown-deleted">Deleted</span>,
      value: topicStatusEnum.DELETED,
    },
  ];

  const emptyFilterObject = {
    modifiedBy: "",
    modifiedDate: { startDate: null, endDate: null },
    status: "",
    topicName: "",
    topicOwner: "",
  };

  const [config, setConfig] = useState({
    filter: { ...emptyFilterObject },
    limit: 10,
    orderBy: "topicName",
    orderType: OrderType.ASC,
    page: 0,
  });

  useEffect(() => {
    dispatch(getTopicList(config));
  }, [config]);

  const navigateToPage = (url: string): void => {
    router.history.push(url);
  };

  const headCells: ITableHeaderCells[] = [
    {
      id: "topicName",
      label: "Topic Name",
      onClickCell: (row: ITableRow) => {
        navigateToPage(`topics/${row.topicId}`);
      },
      value: (row: ITableRow) => <SActiveLink>{row.topicName}</SActiveLink>,
    },
    {
      id: "topicOwner",
      label: "Topic Owner",
      onClickCell: (row: ITableRow) => {
        navigateToPage(`applicationClients/${row.appClient.appClientId}`);
      },
      value: (row: ITableRow) => (
        <SActiveLink>{`${row.appClient && row.appClient.appName} ${row.appClient &&
          row.appClient.appVersion}`}</SActiveLink>
      ),
    },
    { id: "status", label: "Status" },
    { id: "modifiedBy", label: "Modified by", value: (row: ITableRow) => row.modifiedBy.fullName },
    {
      id: "modifiedAt",
      label: "Modified at",
      value: (row: ITableRow) => getFormattedDate(row.modifiedAt),
    },
  ];

  const paginationCallback = (page: number) => {
    setConfig({ ...config, page });
  };

  const sortCallback = (column: string, direction: OrderType) => {
    setConfig({ ...config, orderBy: column, orderType: direction });
  };

  const tableProps: ITableConfig = {
    head: {
      cells: headCells,
    },
    list: {
      ...topicList,
    },
    name: "topics",
    paginationConfig: {
      limit: config.limit,
      onPageChange: paginationCallback,
      page: config.page,
    },
    sortConfig: {
      onSort: sortCallback,
      orderBy: config.orderBy,
      orderType: config.orderType,
    },
  };

  const handleOpenModal = () => {
    dispatch(editModalOpenAction({ type: "NEWTOPIC" }));
  };

  const filterConfig: IFilterConfig = {
    customElements: [
      {
        element: (
          <SButtonIcon
            size="default"
            icon={<SIconAdd32 />}
            onClick={handleOpenModal}
            className={getTAClass("topics", TA_TYPES.BUTTON, "createNew")}
            disabled={currentUser.data.role === READ}
          />
        ),
      },
    ],
    items: [
      {
        name: "topicName",
        placeholder: "Topic name",
        taClass: "topicName",
        type: FilterType.TEXT,
      },
      {
        name: "topicOwner",
        placeholder: "Topic owner",
        taClass: "topicOwner",
        type: FilterType.TEXT,
      },
      {
        data: statusOptions,
        name: "status",
        placeholder: "Status",
        taClass: "status",
        type: FilterType.DROPDOWN,
      },
      {
        name: "modifiedBy",
        placeholder: "Modified by",
        taClass: "modifiedBy",
        type: FilterType.TEXT,
      },
      {
        name: "modifiedDate",
        placeholder: "Modified",
        taClass: "modifiedDate",
        type: FilterType.DATEPICKER,
      },
    ],
    pageName: "topics",
    returnFilter: (filter: any) => {
      // Handle first load
      if (!deepEqual(config.filter, filter)) {
        setConfig({ ...config, page: 0, filter: { ...filter } });
      }
    },
  };

  return (
    <>
      {/* LOADING */}
      {topicList.loading && (
        <SOverlay>
          <ProgressIndicatorLinear />
        </SOverlay>
      )}

      <SPageContainer>
        <SPageHeading className="ta-topics-title">Topics</SPageHeading>

        <Filter {...filterConfig} />

        <SSpacer />
        <Table {...tableProps}></Table>
      </SPageContainer>

      {/* CREATE APPLICATION MODAL */}
      {editModal && editModal.open ? <ModalCreateTopic /> : ""}
    </>
  );
};
