import deepEqual from "deep-equal";
import {
  ButtonGhost,
  ButtonPrimary,
  DateRangePicker,
  Dropdown,
  Grid,
  GridItem,
  Input,
  ProgressIndicatorLinear,
} from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getApplicationList } from "../../actions/application/get-list/actions";
import { editModalOpenAction } from "../../actions/modal/edit/actions";
import { getTAClass, TA_TYPES } from "../../helper/taHelper";
import { getFormattedDate } from "../../helper/util";
import { useDebounce } from "../../hooks/useDebounce";
import { userListStatusEnum } from "../../models/IAppListingTableConfig";
import { ITableConfig, ITableHeaderCells, ITableRow, OrderType } from "../../models/ITableConfig";
import { READ } from "../../models/UserTypes";
import { RootState } from "../../reducers";
import {
  SButtonIcon,
  SFlexContainer,
  SFlexItem,
  SIconAdd32,
  SOverlay,
  SPageContainer,
  SPageHeading,
  SSpacer,
  SVerticalSpacer,
} from "../../styles/styles";
import { BrokerTypeDropdown } from "../BrokerTypeDropdown";
import { ModalCreateApplication } from "../modals/ModalCreateApplication";
import { Table } from "../Table";

export const ApplicationClients = (router: any) => {
  const dispatch = useDispatch();

  const { applicationList } = useSelector((state: RootState) => ({
    applicationList: state.applicationList,
  }));
  const { currentUser } = useSelector((state: RootState) => ({
    currentUser: state.currentUser,
  }));
  const { editModal } = useSelector((state: RootState) => ({
    editModal: state.editModal,
  }));

  const headCells: ITableHeaderCells[] = [
    { id: "appName", label: "App Name", width: "2fr" },
    { id: "appVersion", label: "Version" },
    {
      id: "appBrokerType",
      label: "Broker type",
      value: (row: ITableRow) => row.appBrokerType.brokerType,
    },
    { id: "appOwner", label: "App Owner", value: (row: ITableRow) => row.appOwner.fullName },
    { id: "createdBy", label: "Created By", value: (row: ITableRow) => row.createdBy.fullName },
    {
      id: "createdAt",
      label: "Created at",
      value: (row: ITableRow) => getFormattedDate(row.createdAt),
    },
    { id: "modifiedBy", label: "Modified by", value: (row: ITableRow) => row.modifiedBy.fullName },
    {
      id: "modifiedAt",
      label: "Modified at",
      value: (row: ITableRow) => getFormattedDate(row.modifiedAt),
    },
    { id: "status", label: "Status" },
    { id: "numConnRetries", label: "Number of connection retries" },
    { id: "connRetryDelaySec", label: "Connection retry delay (sec)" },
  ];

  const emptyFilterObject = {
    appBrokerType: "",
    appClientId: "",
    appName: "",
    appOwner: "",
    appVersion: "",
    createdDate: { startDate: null, endDate: null },
    modifiedDate: { startDate: null, endDate: null },
    status: userListStatusEnum.EMPTY,
  };

  const [config, setConfig] = useState({
    filter: { ...emptyFilterObject },
    limit: 10,
    orderBy: "appName",
    orderType: OrderType.ASC,
    page: 0,
  });

  const [filter, setFilter] = useState({ ...emptyFilterObject });

  const addFilterToConfig = () => {
    if (!deepEqual(config.filter, filter)) {
      setConfig({ ...config, page: 0, filter: { ...filter } });
    }
  };

  const debounceFilter = useDebounce(filter, 1000);
  useEffect(() => {
    if (debounceFilter && JSON.stringify(filter) !== JSON.stringify(config.filter)) {
      addFilterToConfig();
    }
  }, [debounceFilter]);

  const [filterDirty, setFilterDirty] = useState(false);
  useEffect(() => {
    JSON.stringify(filter) === JSON.stringify(emptyFilterObject)
      ? setFilterDirty(false)
      : setFilterDirty(true);
  }, [filter]);

  useEffect(() => {
    dispatch(getApplicationList(config));
  }, [config]);

  const openDetails = (row: ITableRow): void => {
    router.history.push(`applicationClients/${row.appClientId}`);
  };

  const handleFilterTextInput = (event: any, field: string): void => {
    if (event.target.value === " ") {
      return;
    } // Don't allow empty spaces
    const updatedFilter = { [field]: event.target.value };
    setFilter({ ...filter, ...updatedFilter });
  };

  const handleFilterDropdown = (value: string): void => {
    setFilter({ ...filter, appBrokerType: value });
    setConfig({ ...config, filter: { ...config.filter, appBrokerType: value } });
  };

  const handleClearFilter = (): void => {
    setFilter(emptyFilterObject);
    setConfig({ ...config, filter: { ...emptyFilterObject } });
  };

  const handleFilterKeyPress = (event: any) => {
    if (event.key === "Enter") {
      addFilterToConfig();
    }
  };

  const handleDatePicker = (date: any, type: string) => {
    if (type === "created") {
      setFilter({ ...filter, createdDate: { ...date } });
    }
    if (type === "modified") {
      setFilter({ ...filter, modifiedDate: { ...date } });
    }
  };

  const handleFilterMyApplications = () => {
    setFilter({ ...filter, appOwner: currentUser.data.fullName });
    addFilterToConfig();
  };

  const handleFilterStatus = (status: userListStatusEnum) => {
    setFilter({ ...filter, status });
    addFilterToConfig();
  };

  const handleOpenModal = () => {
    dispatch(editModalOpenAction({ type: "NEWAPP" }));
  };

  const paginationCallback = (page: number) => {
    setConfig({ ...config, page });
  };

  const sortCallback = (column: string, direction: OrderType) => {
    setConfig({ ...config, orderBy: column, orderType: direction });
  };

  const tableProps: ITableConfig = {
    head: {
      cells: headCells,
    },
    list: {
      ...applicationList,
      onClickRow: openDetails,
    },
    name: "application",
    paginationConfig: {
      limit: config.limit,
      onPageChange: paginationCallback,
      page: config.page,
    },
    sortConfig: {
      onSort: sortCallback,
      orderBy: config.orderBy,
      orderType: config.orderType,
    },
  };

  return (
    <>
      {/* LOADING */}
      {applicationList.loading && (
        <SOverlay>
          <ProgressIndicatorLinear />
        </SOverlay>
      )}
      <SPageContainer>
        <SPageHeading className="ta-application-title">Application Clients</SPageHeading>

        {/* FILTER */}
        <Grid columns="1fr 1fr 1fr 1fr 1fr 1fr">
          <GridItem>
            {/* FILTER - App Name */}
            <Input
              className={getTAClass("applicationClients", TA_TYPES.FILTER, "appName")}
              placeholder="App Name"
              value={filter.appName}
              onChange={(e: any) => {
                handleFilterTextInput(e, headCells[0].id);
              }}
              onKeyPress={handleFilterKeyPress}
            />
          </GridItem>
          <GridItem>
            {/* FILTER - App Version */}
            <Input
              className={getTAClass("applicationClients", TA_TYPES.FILTER, "version")}
              placeholder="ver#"
              value={filter.appVersion}
              onChange={(e: any) => {
                handleFilterTextInput(e, headCells[1].id);
              }}
              onKeyPress={handleFilterKeyPress}
            />
          </GridItem>
          <GridItem className={getTAClass("applicationClients", TA_TYPES.FILTER, "brokerType")}>
            {/* FILTER - Broker Type Dropdown */}
            <BrokerTypeDropdown
              label="Broker type"
              onChange={handleFilterDropdown}
              selected={filter.appBrokerType}
              type="filter"
            />
          </GridItem>
          <GridItem>
            {/* FILTER - Owner Name */}
            <Input
              className={getTAClass("applicationClients", TA_TYPES.FILTER, "appOwner")}
              placeholder="App Owner"
              value={filter.appOwner}
              onChange={(e: any) => {
                handleFilterTextInput(e, headCells[3].id);
              }}
              onKeyPress={handleFilterKeyPress}
            />
          </GridItem>
          <GridItem>
            {/* FILTER - Created From To */}
            <DateRangePicker
              className={getTAClass("applicationClients", TA_TYPES.FILTER, "createdDate")}
              startDatePlaceholderText="Created from"
              endDatePlaceholderText="Created to"
              startDate={filter.createdDate.startDate}
              endDate={filter.createdDate.endDate}
              onDatesChange={(date: any) => {
                handleDatePicker(date, "created");
              }}
              enableOutsideDays
              isOutsideRange={() => false}
            />
          </GridItem>
          <GridItem>
            {/* FILTER - Modified From To */}
            <DateRangePicker
              className={getTAClass("applicationClients", TA_TYPES.FILTER, "modifiedDate")}
              startDatePlaceholderText="Modified from"
              endDatePlaceholderText="Modified to"
              startDate={filter.modifiedDate.startDate}
              endDate={filter.modifiedDate.endDate}
              onDatesChange={(date: any) => {
                handleDatePicker(date, "modified");
              }}
              enableOutsideDays
              isOutsideRange={() => false}
            />
          </GridItem>
        </Grid>
        <SSpacer />
        <SFlexContainer justifyContent="space-between">
          <SFlexItem>
            <Grid columns="1fr 1fr">
              <GridItem className={getTAClass("applicationClients", TA_TYPES.FILTER, "status")}>
                <Dropdown
                  options={[
                    {
                      label: <span className="ta-dropdown-active">Active</span>,
                      value: userListStatusEnum.ACTIVE,
                    },
                    {
                      label: <span className="ta-dropdown-deleted">Deleted</span>,
                      value: userListStatusEnum.DELETED,
                    },
                  ]}
                  label="Status"
                  onChange={handleFilterStatus}
                  selected={filter.status}
                />
              </GridItem>
              <GridItem>
                <ButtonGhost
                  className={getTAClass("applicationClients", TA_TYPES.BUTTON, "myApplications")}
                  onClick={handleFilterMyApplications}
                >
                  My Applications
                </ButtonGhost>
              </GridItem>
            </Grid>
          </SFlexItem>
          <SFlexItem>
            <ButtonPrimary
              className={getTAClass("applicationClients", TA_TYPES.BUTTON, "clearFilter")}
              onClick={handleClearFilter}
              disabled={!filterDirty}
            >
              Clear filter
            </ButtonPrimary>
            <SVerticalSpacer />
            <SButtonIcon
              size="default"
              icon={<SIconAdd32 />}
              onClick={handleOpenModal}
              className={getTAClass("applicationClients", TA_TYPES.BUTTON, "createNew")}
              disabled={currentUser.data.role === READ}
            />
          </SFlexItem>
        </SFlexContainer>

        <SSpacer />

        <Table {...tableProps}></Table>
      </SPageContainer>

      {/* CREATE APPLICATION MODAL */}
      {editModal && editModal.open ? <ModalCreateApplication /> : ""}
    </>
  );
};
