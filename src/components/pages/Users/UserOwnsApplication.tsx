import React from "react";

import { SimpleApplicationList } from "../../SimpleApplicationList";

export const UserOwnsApplication = ({ userId, history }: { history: any; userId: string }) => (
  <>{userId !== "" && <SimpleApplicationList history={history} userId={userId} />}</>
);
