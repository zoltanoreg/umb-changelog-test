import deepEqual from "deep-equal";
import { ProgressIndicatorLinear } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getUserList } from "../../../actions/user/get-list/actions";
import { FilterType, IFilterConfig } from "../../../models/IFilterConfig";
import { ITableConfig, ITableRow, OrderType } from "../../../models/ITableConfig";
import { SOverlay, SPageContainer, SPageHeading, SSpacer } from "../../../styles/styles";
import { Filter } from "../../Filter";
import { Table } from "../../Table";

export const Users = (router: any) => {
  const dispatch = useDispatch();

  const { userList } = useSelector((state: any) => ({
    userList: state.userList,
  }));

  const headCells = [
    { id: "firstName", label: "First Name" },
    { id: "lastName", label: "Last Name" },
    { id: "email", label: "Email" },
    { id: "role", label: "Role" },
  ];

  const userRoles = [
    {
      label: <span className="ta-dropdown-all">All</span>,
      purelabel: "All",
      value: "",
    },
    {
      label: <span className="ta-dropdown-read_only">Read Only</span>,
      purelabel: "Read Only",
      value: "READ",
    },
    {
      label: <span className="ta-dropdown-application_owner">Application Owner</span>,
      purelabel: "Application Owner",
      value: "APPLICATION_OWNER",
    },
    {
      label: <span className="ta-dropdown-administrator">Administrator</span>,
      purelabel: "Administrator",
      value: "ADMINISTRATOR",
    },
    {
      label: <span className="ta-dropdown-umbt_it_support_team">UMBT IT support team</span>,
      purelabel: "UMBT IT support team",
      value: "UMBT_IT_SUPPORT_TEAM",
    },
    {
      label: (
        <span className="ta-dropdown-umbt_operational_support_team">
          UMBT Operational support team
        </span>
      ),
      purelabel: "UMBT Operational support team",
      value: "UMBT_OPERATIONAL_SUPPORT_TEAM",
    },
  ];

  const emptyFilterObject = {
    email: "",
    firstName: "",
    lastName: "",
    role: "",
  };

  const initialSortAndPagination = {
    limit: 10,
    orderBy: "firstName",
    orderType: OrderType.ASC,
    page: 0,
  };

  const [config, setConfig] = useState({
    filter: { ...emptyFilterObject },
    ...initialSortAndPagination,
    roles: [],
  });

  useEffect(() => {
    dispatch(getUserList(config));
  }, [config]);

  const openDetails = (userRow: ITableRow): void => {
    router.history.push(`users/${userRow.userId}`);
  };

  const paginationCallback = (page: number) => {
    setConfig({ ...config, page });
  };

  const sortCallback = (column: string, direction: OrderType) => {
    setConfig({ ...config, orderBy: column, orderType: direction });
  };

  const tableProps: ITableConfig = {
    head: {
      cells: headCells,
    },
    list: {
      ...userList,
      cellRules: [
        {
          columnName: "role",
          mask: {
            from: "value",
            source: userRoles,
            to: "purelabel",
          },
        },
      ],
      onClickRow: openDetails,
    },
    name: "users",
    paginationConfig: {
      limit: config.limit,
      onPageChange: paginationCallback,
      page: config.page,
    },
    sortConfig: {
      onSort: sortCallback,
      orderBy: config.orderBy,
      orderType: config.orderType,
    },
  };

  const filterConfig: IFilterConfig = {
    items: [
      {
        name: "firstName",
        placeholder: "First Name",
        taClass: "firstName",
      },
      {
        name: "lastName",
        placeholder: "Last Name",
        taClass: "lastName",
      },
      {
        name: "email",
        placeholder: "Email",
        taClass: "email",
      },
      {
        data: userRoles,
        name: "role",
        taClass: "role",
        type: FilterType.DROPDOWN,
      },
    ],
    pageName: "users",
    returnFilter: (filter: any) => {
      if (!deepEqual(config.filter, filter)) {
        setConfig({ ...config, page: 0, filter: { ...filter } });
      }
    },
  };

  return (
    <>
      <SPageContainer>
        {/* LOADING */}
        {userList.loading && (
          <SOverlay>
            <ProgressIndicatorLinear />
          </SOverlay>
        )}

        <SPageHeading className="ta-users-title">Users</SPageHeading>
        <Filter {...filterConfig} />
        <SSpacer />
        <Table {...tableProps}></Table>
      </SPageContainer>
    </>
  );
};
