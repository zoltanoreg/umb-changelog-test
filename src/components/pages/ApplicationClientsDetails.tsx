import { ButtonIcon, Grid, GridItem, IconArrowLeft32 } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import { getApplicationDetails } from "../../actions/application/get-details/actions";
import { tabAppDetailsEnum } from "../../models/IAppDetailsConfig";
import { modalTypeEnum } from "../../models/IEditModalConfig";
import { RootState } from "../../reducers";
import { SPageContainer, SPageHeading, SSpacer } from "../../styles/styles";
import { ModalAddTopicPublisher } from "../modals/ModalAddTopicPublisher";
import { ModalAddTopicSubscriber } from "../modals/ModalAddTopicSubscriber";
import { ModalEditApplication } from "../modals/ModalEditApplication";
import { PageError, PageLoading } from "../PageState";

import { ApplicationDetailsDetailsTab } from "./ApplicationClientDetails/ApplicationDetailsDetailsTab";
import { ApplicationDetailsTabs } from "./ApplicationClientDetails/ApplicationDetailsTabs";
import { ApplicationDetailsTopSection } from "./ApplicationClientDetails/ApplicationDetailsTopSection";
import { TopicListPublish } from "./ApplicationClientDetails/TopicListPublish";
import { TopicListSubscribe } from "./ApplicationClientDetails/TopicListSubscribe";

export const ApplicationClientsDetails = ({ match }: { match: any }) => {
  const dispatch = useDispatch();

  const { applicationDetails, currentUser, editModal } = useSelector((state: RootState) => ({
    applicationDetails: state.applicationDetails,
    currentUser: state.currentUser,
    editModal: state.editModal,
  }));

  const [appId, setAppId] = useState("");
  const [activeTab, setActiveTab] = useState(tabAppDetailsEnum.PUBLISH);

  useEffect(() => {
    if (match.params.appId !== "" && typeof match.params.appId !== "undefined") {
      setAppId(match.params.appId);
      dispatch(getApplicationDetails(match.params.appId));
    }
  }, [match.params.appId]);

  return (
    <>
      <SPageContainer>
        {/* HEADER */}
        <Grid columns="auto 1fr" alignItems="center" gap="10px">
          <GridItem>
            <Link to="/applicationClients" className="ta-application-details-button-back">
              <ButtonIcon size="default" icon={<IconArrowLeft32 />} />
            </Link>
          </GridItem>
          <GridItem>
            <SPageHeading className="ta-application-title">Application Client Details</SPageHeading>
          </GridItem>
        </Grid>

        {applicationDetails.loading && <PageLoading />}

        {applicationDetails.error && <PageError />}

        {!applicationDetails.loading && !applicationDetails.error && (
          <>
            {/* APP DETAILS */}
            {applicationDetails.content && currentUser.data && (
              <ApplicationDetailsTopSection
                appContent={applicationDetails.content}
                userData={currentUser.data}
              />
            )}

            <SSpacer />

            {/* APP TABS */}
            <ApplicationDetailsTabs
              activeTab={activeTab}
              onChange={(tab: tabAppDetailsEnum) => {
                setActiveTab(tab);
              }}
            />

            <SSpacer />

            {/* TABLE */}
            {activeTab === tabAppDetailsEnum.SUBSCRIBE && applicationDetails.content && (
              <TopicListSubscribe
                userData={currentUser.data}
                appContent={applicationDetails.content}
              />
            )}
            {activeTab === tabAppDetailsEnum.PUBLISH && applicationDetails.content && (
              <TopicListPublish
                userData={currentUser.data}
                appContent={applicationDetails.content}
              />
            )}
            {activeTab === tabAppDetailsEnum.DETAILS &&
              applicationDetails.content &&
              currentUser.data && (
                <ApplicationDetailsDetailsTab
                  appContent={applicationDetails.content}
                  userData={currentUser.data}
                />
              )}
          </>
        )}
      </SPageContainer>

      {editModal.open && editModal.type === modalTypeEnum.EDIT_APP && <ModalEditApplication />}
      {editModal.open && editModal.type === modalTypeEnum.CREATE_TOPIC_PUBLISH && (
        <ModalAddTopicPublisher />
      )}
      {editModal.open && editModal.type === modalTypeEnum.CREATE_TOPIC_SUBSCRIBE && (
        <ModalAddTopicSubscriber />
      )}
    </>
  );
};
