import { Chip } from "next-components";
import React, { useEffect, useState } from "react";

import { useDebounce } from "../hooks/useDebounce";
import useOutsideClickDetector from "../hooks/useOutsideClickDetector";
import { ITypeahead } from "../models/ITypeahead";
import {
  STypeaheadContainer,
  STypeaheadInput,
  STypeaheadListContainer,
  STypeaheadListItem,
} from "../styles/styles";

export const Typeahead = (props: ITypeahead) => {
  const [dirty, setDirty] = useState(false);
  const [suggestionOpen, setSuggestionOpen] = useState(false);
  const [inputValue, setInputValue] = useState("");

  // Create config from provided config object
  const [config, setConfig] = useState({
    ...props.config.default,
    limit: 100,
    orderType: "ASC",
    page: 0,
  });

  const debounceFilter = useDebounce(config, 150);
  useEffect(() => {
    if (dirty) {
      props.suggestion.getList(config);
    }
  }, [debounceFilter]);

  useEffect(() => {
    if (props.value && typeof props.value !== "string" && !props.valueToSetOnLoad) {
      throw new Error("Please set 'valueToSetOnLoad' property if 'value' is an object.");
    }

    setInputValue(
      props.valueToSetOnLoad
        ? props.value
          ? (props.value as any)[props.valueToSetOnLoad]
          : props.value
        : props.value
    );
  }, [props.value]);

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setDirty(true);
    setInputValue(e.target.value);

    // On clear button
    if (e.target.value === "") {
      props.config.setFilter(props.config.default.filter);
      setInputValue("");
    }

    if (e.target.value.length > 1) {
      setConfig({
        ...config,
        filter: {
          ...config.filter,
          ...props.config.setFilter(e.target.value),
        },
      });
      setSuggestionOpen(true);
    }

    if (e.target.value.length <= 1) {
      setSuggestionOpen(false);
    }

    if (props.handleCreate) {
      props.handleCreate(e.target.value);
    }
  };

  const handleSelectSuggestion = (value: any): void => {
    setDirty(true);
    setInputValue(props.suggestion.select(value));
    setConfig({
      ...config,
      filter: {
        ...config.filter,
        ...props.config.setFilter(value),
      },
    });
    setSuggestionOpen(false);
    props.handleTypeahead(value);
  };

  const autoFill = () => {
    setSuggestionOpen(false);

    if (
      props.autoFillOnExactMatchOnly &&
      inputValue !== "" &&
      props.suggestion.list.content.length === 1
    ) {
      if (props.autoFillOnExactMatchOnly.rule) {
        if (props.suggestion.list.content[0][props.autoFillOnExactMatchOnly.key] === inputValue) {
          handleSelectSuggestion(props.suggestion.list.content[0]);
        }
      } else {
        handleSelectSuggestion(props.suggestion.list.content[0]);
      }
    }
  };

  const handleClear = () => {
    setDirty(true);
    // Backspace
    props.handleTypeahead(props.config.default.filter);
    setInputValue("");
    setSuggestionOpen(false);
  };

  const handleKeyDown = (e: any) => {
    if (e.keyCode === 8) {
      handleClear();
    }

    if (e.keyCode === 9 || e.keyCode === 13) {
      autoFill();
    }
  };

  const outsideClickRef = useOutsideClickDetector(autoFill);

  const getLabel = (data: any) => {
    const keys = props.pattern.match(/[^{\}]+(?=})/g); // Keys to replace is {elem}
    let label = props.pattern;
    if (keys) {
      keys.forEach((key: string) => {
        const re = new RegExp(`{${key}}`, "gi");
        label = label.replace(re, data[key]);
      });
    }

    return label;
  };

  return (
    // tslint:disable-next-line: ban-ts-ignore
    // @ts-ignore: Unreachable code error
    <STypeaheadContainer ref={outsideClickRef}>
      {props.value === "" ||
      props.value === " " ||
      (props.hasSelected && props.hasSelected(props.value)) ? (
        <STypeaheadInput
          className={`ta-${props.pageSelector}-typeahead-${props.name}`}
          placeholder={
            props.placeholder && props.placeholder !== ""
              ? props.placeholder
              : props.name.charAt(0).toUpperCase() + props.name.slice(1)
          }
          value={inputValue}
          onChange={handleInput}
          onKeyDown={handleKeyDown}
          disabled={props.disabled || false}
        />
      ) : (
        <Chip
          className={`ta-${props.pageSelector}-typeahead-${props.name}-chip`}
          tabIndex="0"
          label={props.chipLabel ? props.chipLabel(props) : props.value}
          disabled={props.disabled || false}
          size="default"
          onClick={!props.disabled && handleClear}
        />
      )}
      {suggestionOpen && (
        <STypeaheadListContainer
          className={`ta-${props.pageSelector}-typeahead-${props.name}-list`}
        >
          {props.suggestion.list.content.map((data: any, index: any) => (
            <STypeaheadListItem
              className={`ta-${props.pageSelector}-typeahead-${props.name}-list-elem-${index}`}
              key={index}
              onClick={() => {
                handleSelectSuggestion(data);
              }}
            >
              {getLabel(data)}
            </STypeaheadListItem>
          ))}
          {props.suggestion.list.content.length === 0 && (
            <STypeaheadListItem className={`ta-typeahead-${props.name}-list-elem-0`}>
              No match.
            </STypeaheadListItem>
          )}
        </STypeaheadListContainer>
      )}
    </STypeaheadContainer>
  );
};
