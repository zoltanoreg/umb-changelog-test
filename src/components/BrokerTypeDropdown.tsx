import { Dropdown } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getAllBrokerType } from "../actions/broker-type/get-all/actions";
import { IBrokerTypeDropdown } from "../models/IBrokerTypeDropdown";
import { RootState } from "../reducers";

export const BrokerTypeDropdown = (props: IBrokerTypeDropdown) => {
  const dispatch = useDispatch();

  const { allBrokerType } = useSelector((state: RootState) => ({
    allBrokerType: state.allBrokerType,
  }));

  const [brokerTypes, setBrokerTypes]: any[] = useState([{}]);

  useEffect(() => {
    dispatch(
      getAllBrokerType({
        filter: {},
        limit: 0,
        orderBy: "brokerTypeName",
        orderType: "ASC",
        page: 0,
      })
    );
  }, []);

  useEffect(() => {
    setBrokerTypes(
      allBrokerType.content.map((broker: any) => ({
        label: <span className={`ta-dropdown-${broker.brokerType}`}>{broker.brokerType}</span>,
        value:
          props.type === "filter"
            ? broker.brokerType
            : {
                brokerType: broker.brokerType,
                brokerTypeId: broker.brokerTypeId,
                brokerTypeName: broker.brokerTypeName,
                systemType: broker.systemType,
              },
      }))
    );
  }, [allBrokerType]);

  return (
    <Dropdown
      options={brokerTypes}
      label={props.label || props.selected.brokerType}
      onChange={(e: Event) => {
        props.onChange(e);
      }}
      selected={props.selected}
    />
  );
};
