import React, { useEffect, useState } from "react";
import { Edge, Network, Node } from "react-vis-network";

interface IRawData {
  from: string;
  label: string;
  to: string;
}

interface IGraph {
  rawData: IRawData[];
  undirectional?: boolean;
}

interface INode {
  id: string;
  label: string;
}

const getNodes = (rawData: IRawData[]) => {
  const nodes = [];
  for (const row of rawData) {
    if (!nodes.filter((r: INode) => r.id === row.from).length) {
      nodes.push({
        id: row.from,
        label: row.from,
      });
    }
    if (!nodes.filter((r: INode) => r.id === row.to).length) {
      nodes.push({
        id: row.to,
        label: row.to,
      });
    }
  }

  return nodes;
};

const getLinks = (rawData: IRawData[], isUndirectional: boolean = false) => {
  // tslint:disable-next-line:no-parameter-reassignment
  rawData = rawData.map((row: IRawData) => ({
    arrows: isUndirectional ? { to: false } : row.label === "out" ? "from" : "to",
    from: row.from,
    label: row.label,
    to: row.to,
  }));

  if (rawData.length === 1 && rawData[0].from === rawData[0].to) {
    return [];
  }

  return rawData;
};

export const TopologyGraphVis = (props: IGraph) => {
  const [nodes, setNodes]: any[] = useState([]);
  const [edges, setEdges]: any[] = useState([]);

  useEffect(() => {
    if (props.rawData.length > 0) {
      setNodes(getNodes(props.rawData));
      setEdges(getLinks(props.rawData, props.undirectional));
    }
  }, [props]);

  return (
    <>
      {nodes.length ? (
        <Network height={1000}>
          {nodes.map((node: INode) => (
            <Node key={node.id} id={node.id} label={node.label} />
          ))}
          {edges.map((edge: any, index: number) => (
            <Edge
              key={index}
              id={index}
              from={edge.from}
              to={edge.to}
              label={edge.label}
              arrows={edge.arrows}
            />
          ))}
        </Network>
      ) : (
        ""
      )}
    </>
  );
};

const isIdenticalGraph = (prevProps: any, nextProps: any) => {
  const prewRawData = prevProps.rawData;
  const nextRawData = nextProps.rawData;
  const haveSameLength = prewRawData.length === nextRawData.length;
  let isIdentical = true;
  for (const item of nextRawData) {
    if (!prewRawData.filter((raw: any) => JSON.stringify(raw) === JSON.stringify(item)).length) {
      isIdentical = false;
    }
  }

  return haveSameLength && isIdentical;
};
export const MemoizedTopologyGraphVis = React.memo(TopologyGraphVis, isIdenticalGraph);
