import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { getTopicList } from "../actions/topic/get-list/actions";
import { topicStatusEnum } from "../models/ITopicConfig";
import { ITopicDetailsContent } from "../models/ITopicDetailsConfig";
import { RootState } from "../reducers";
import {
  STypeaheadContainer,
  STypeaheadInput,
  STypeaheadListContainer,
  STypeaheadListItem,
} from "../styles/styles";

import { Typeahead } from "./Typeahead";

export const TypeaheadTopics = (props: any) => {
  const { topicList } = useSelector((state: RootState) => ({
    topicList: state.topicList,
  }));

  const dispatch = useDispatch();

  const handleCreateChipLabel = ({ value }: { value: any }) => {
    let label = "";

    if (value && value.topicName) {
      label = value.topicName;
      if (value.topicStatus === topicStatusEnum.ONHOLD) {
        label += " ---- Topic is on hold ⚠️";
      }
      if (value.topicStatus === topicStatusEnum.DRAFT) {
        label += " ---- Topic is a draft ⚠️";
      }
    }

    return label;
  };

  return (
    <Typeahead
      pageSelector={props.pageSelector}
      autoFillOnExactMatchOnly={{
        key: "topicName",
        rule: props.autoFillOnExactMatchOnly,
      }}
      disabled={props.disabled}
      config={{
        default: {
          filter: {
            appClientId: undefined,
            topicId: undefined,
            topicName: "",
            topicStatus: "",
          },
          orderBy: "topicName",
          statuses: [topicStatusEnum.ACTIVE, topicStatusEnum.ONHOLD, topicStatusEnum.DRAFT],
        },
        setFilter: (topicName: string) => ({
          topicName,
        }),
      }}
      suggestion={{
        getList: (config: any) => dispatch(getTopicList(config)),
        list: topicList,
        select: (topic: any) => `${topic.topicName}`,
      }}
      name="topic"
      chipLabel={handleCreateChipLabel}
      value={props.value}
      hasSelected={(p: any) => p && p.topicId === undefined}
      valueToSetOnLoad="topicName"
      handleTypeahead={topic => {
        props.onChange({
          appClientId:
            topic.appClient && topic.appClient.appClientId
              ? topic.appClient.appClientId
              : topic.appClientId,
          topicId: topic.topicId,
          topicName: topic.topicName,
          topicStatus: topic.status,
        });
      }}
      handleCreate={topic => {
        props.onChange({
          appClientId: undefined,
          topicId: undefined,
          topicName: topic,
          topicStatus: undefined,
        });
      }}
      pattern="{topicName} - {status}"
    />
  );
};
