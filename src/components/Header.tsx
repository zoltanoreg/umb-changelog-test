import { OverlayFullscreen, ToolMenuItem } from "next-components";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import packageJson from "../../package.json";
import { isCurrentUserRole } from "../helper/util";
import {
  ADMINISTRATOR,
  APPLICATION_OWNER,
  READ,
  UMBT_IT_SUPPORT_TEAM,
  UMBT_OPERATIONAL_SUPPORT_TEAM,
} from "../models/UserTypes";
import { HeaderContainer, SCloudBar, SToolMenu } from "../styles/styles";

export const Header = (router: any) => {
  const { currentUser } = useSelector((state: any) => ({
    currentUser: state.currentUser,
  }));
  const [isClosed, setIsClosed] = useState(true);

  const handleClick = (e: React.MouseEvent<HTMLElement>, target: string) => {
    e.preventDefault();
    router.history.push(target);
    setIsClosed(true);
  };

  const menuElements = [
    {
      className: "ta-menu-item-application_clients",
      pathname: "/applicationClients",
      roles: [ADMINISTRATOR, APPLICATION_OWNER, READ],
      title: "Application Clients",
    },
    {
      className: "ta-menu-item-topics",
      pathname: "/topics",
      roles: [ADMINISTRATOR, APPLICATION_OWNER, READ],
      title: "Topics",
    },
    {
      className: "ta-menu-item-broker_types",
      pathname: "/brokerTypes",
      roles: [ADMINISTRATOR, APPLICATION_OWNER, READ],
      title: "Broker types",
    },
    {
      className: "ta-menu-item-configuration_files",
      pathname: "/configurationFiles",
      roles: [ADMINISTRATOR, APPLICATION_OWNER, READ, UMBT_OPERATIONAL_SUPPORT_TEAM],
      title: "Configuration Files",
    },
    {
      className: "ta-menu-item-users",
      href: "/users",
      pathname: "/users",
      roles: [ADMINISTRATOR, UMBT_IT_SUPPORT_TEAM],
      title: "Users",
    },
    {
      className: "ta-menu-item-topology",
      href: "/topology",
      pathname: "/topology",
      roles: [ADMINISTRATOR, APPLICATION_OWNER],
      title: "Topology",
    },
  ];

  return (
    <HeaderContainer>
      {!isClosed ? <OverlayFullscreen opacity={0.2} /> : <></>}
      <SCloudBar
        className="ta-header-cloudbar"
        cloudHomeUrl="#"
        notificationCount={0}
        user={{
          name: "User",
          notificationsLink: "#",
          profileLink: (props: any) => <Link to={`/users/${currentUser.data ? currentUser.data.userId : ''}`}>{props.children}</Link>,
          settingsLink: "#",
        }}
      />
      <SToolMenu
        className={
          isClosed
            ? "ta-header-tools ta-header-tools-closed"
            : "ta-header-tools ta-header-tools-open"
        }
        title={`UMB Tool ${packageJson.version}`}
        handleClosed={() => {
          setIsClosed(!isClosed);
        }}
        handleCloudbar={() => {
          return;
        }}
        isClosed={isClosed}
      >
        {menuElements.map(
          (menu: any) =>
            isCurrentUserRole(currentUser, menu.roles) && (
              <ToolMenuItem
                className={menu.className}
                as="a"
                href={menu.href ? menu.href : "#"}
                title={menu.title}
                onClick={(e: React.MouseEvent<HTMLElement>) => {
                  handleClick(e, menu.pathname);
                }}
                isActive={router.location.pathname.indexOf(menu.pathname) >= 0}
              />
            )
        )}
      </SToolMenu>
    </HeaderContainer>
  );
};
