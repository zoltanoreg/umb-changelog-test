import { ModalConfirmation, ModalInfo } from "next-components";
import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { popupCloseAction } from "../actions/popup/actions";
import { RootState } from "../reducers";

export const Popup = () => {
  const dispatch = useDispatch();

  const { popupData } = useSelector((state: RootState) => ({
    popupData: state.popupData,
  }));

  const closePopup = () => {
    dispatch(popupCloseAction());
  };

  return (
    <>
      {popupData.open && (
        <div className="ta-modal-info">
          {popupData.type === "Confirmation" ? (
            <ModalConfirmation
              title={<span className="ta-modal-confirmation-title">{popupData.title}</span>}
              text={<span className="ta-modal-confirmation-content">{popupData.content}</span>}
              onCancel={closePopup}
              onConfirm={popupData.onConfirm}
            />
          ) : (
            <ModalInfo
              title={<span className="ta-modal-info-title">{popupData.title}</span>}
              text={<span className="ta-modal-info-content">{popupData.content}</span>}
              onClose={closePopup}
            />
          )}
        </div>
      )}
    </>
  );
};
