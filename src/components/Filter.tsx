import { ButtonPrimary, DateRangePicker, Dropdown, Grid, GridItem, Input } from "next-components";
import React, { useEffect, useState } from "react";

import { Moment } from "../../node_modules/moment";
import { getTAClass, TA_TYPES } from "../helper/taHelper";
import { useDebounce } from "../hooks/useDebounce";
import { FilterType, ICustomElement, IFilterConfig, IFilterItem } from "../models/IFilterConfig";
import { DEFAULT_WIDTH } from "../models/ITableConfig";
import { FilterWrapper } from "../styles/styles";

export const Filter = (props: IFilterConfig) => {
  const [filter, setFilter] = useState();
  const [emptyFilter, setEmptyFilter] = useState();
  const [columnsRatio, setColumnsRatio] = useState();

  const [filterDirty, setFilterDirty] = useState(false);

  const createFilterObject = () =>
    props.items.reduce(
      (acc: any, cur: IFilterItem) => ({
        ...acc,
        [cur.name]:
          cur.type === FilterType.DATEPICKER ? { startDate: undefined, endDate: undefined } : "",
      }),
      {}
    );

  useEffect(() => {
    setFilter(createFilterObject());
    setEmptyFilter(createFilterObject());

    if (!props.columns) {
      const ratio = props.items.map((headCell: IFilterItem) => headCell.width || DEFAULT_WIDTH);
      // Clear button
      ratio.push(DEFAULT_WIDTH);
      if (props.customElements) {
        props.customElements.forEach((element: ICustomElement) =>
          ratio.push(element.width ? element.width : DEFAULT_WIDTH)
        );
      }
      setColumnsRatio(ratio.join(" "));
    } else {
      setColumnsRatio(props.columns);
    }
  }, []);

  useEffect(() => {
    JSON.stringify(filter) === JSON.stringify(emptyFilter)
      ? setFilterDirty(false)
      : setFilterDirty(true);
  }, [filter]);

  const handleFilterTextInput = (
    event: React.ChangeEvent<HTMLInputElement>,
    field: string
  ): void => {
    if (event.target.value === " ") {
      return;
    } // Don't allow empty spaces
    const updatedFilter = { [field]: event.target.value };
    setFilter({ ...filter, ...updatedFilter });
  };

  const returnFilter = (f?: typeof emptyFilter) => {
    props.returnFilter(f ? f : filter);
  };

  const handleFilterDropdown = (field: string, value: string): void => {
    const f = { ...filter, [field]: value };
    setFilter(f);
    returnFilter(f);
  };

  const handleClearFilter = (): void => {
    setFilter(emptyFilter);
    returnFilter(emptyFilter);
  };

  const handleFilterKeyPress = (event: KeyboardEvent) => {
    if (event.key === "Enter") {
      returnFilter();
    }
  };

  const handleDatePicker = (field: string, date: any) => {
    setFilter({ ...filter, [field]: { ...date } });
  };

  const debounceFilter = useDebounce(filter, 1000);
  useEffect(() => {
    if (debounceFilter && JSON.stringify(filter) && JSON.stringify(emptyFilter)) {
      returnFilter();
    }
  }, [debounceFilter]);

  return (
    <FilterWrapper>
      {filter ? (
        <Grid columns={columnsRatio}>
          {props.items.map((item: IFilterItem, index: number) => {
            switch (item.type) {
              case FilterType.DROPDOWN:
                return (
                  <GridItem
                    key={`${item.name}-${index}`}
                    className={getTAClass(props.pageName, TA_TYPES.FILTER, item.taClass)}
                  >
                    <Dropdown
                      options={item.data}
                      onChange={(e: any) => {
                        handleFilterDropdown(item.name, e);
                      }}
                      selected={filter[item.name]}
                      label={item.placeholder}
                    />
                  </GridItem>
                );
              case FilterType.DATEPICKER:
                return (
                  <GridItem key={`${item.name}-${index}`}>
                    <DateRangePicker
                      className={getTAClass(props.pageName, TA_TYPES.FILTER, item.taClass)}
                      startDatePlaceholderText={`${item.placeholder || item.name} from`}
                      endDatePlaceholderText={`${item.placeholder || item.name} to`}
                      startDate={filter[item.name].startDate}
                      endDate={filter[item.name].endDate}
                      onDatesChange={(e: any) => {
                        handleDatePicker(item.name, e);
                      }}
                      enableOutsideDays
                      isOutsideRange={() => false}
                    />
                  </GridItem>
                );
              default:
                return (
                  <GridItem key={`${item.name}-${index}`}>
                    <Input
                      className={getTAClass(props.pageName, TA_TYPES.FILTER, item.taClass)}
                      placeholder={item.placeholder || item.name}
                      value={filter[item.name]}
                      onChange={(e: any) => {
                        handleFilterTextInput(e, item.name);
                      }}
                      onKeyPress={handleFilterKeyPress}
                    />
                  </GridItem>
                );
            }
          })}
          <GridItem>
            <ButtonPrimary
              onClick={handleClearFilter}
              disabled={!filterDirty}
              className={getTAClass(props.pageName, TA_TYPES.BUTTON, "clearFilter")}
            >
              Clear filter
            </ButtonPrimary>
          </GridItem>
          {props.customElements
            ? props.customElements.map((customElement: ICustomElement) => customElement.element)
            : ""}
        </Grid>
      ) : (
        ""
      )}
    </FilterWrapper>
  );
};
