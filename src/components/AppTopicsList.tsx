import deepEqual from "deep-equal";
import { ButtonPrimary, GridItem, ProgressIndicatorLinear } from "next-components";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { getTopicsForPublisher } from "../actions/application/get-topics-for-publisher/actions";
import {
  appTopicListClearAction,
  getApplicationTopicList,
} from "../actions/application/get-topics/actions";
import { getAllBrokerType } from "../actions/broker-type/get-all/actions";
import { popupOpenAction } from "../actions/popup/actions";
import { deleteAssociationBulk } from "../actions/topic/delete-association-bulk/actions";
import { deleteAssociation } from "../actions/topic/delete-association/actions";
import { getTAClass, TA_TYPES } from "../helper/taHelper";
import { getFormattedDate } from "../helper/util";
import { FilterType, IFilterConfig } from "../models/IFilterConfig";
import { ITableConfig, ITableHeaderCells, ITableRow, OrderType } from "../models/ITableConfig";
import { topicStatusEnum } from "../models/ITopicConfig";
import { ADMINISTRATOR } from "../models/UserTypes";
import { RootState } from "../reducers";
import { ApplicationService } from "../services/ApplicationService";
import { SActiveLink, SOverlay, SSpacer } from "../styles/styles";

import { Filter } from "./Filter";
import { Table } from "./Table";

export const AppTopicsList = (props: {
  btnDisabled: boolean;
  type: "PUBLISHER" | "SUBSCRIBER";
  btnClick(config: any): void;
}) => {
  const history = useHistory();

  const navigateToPage = (url: string): void => {
    history.push(url);
  };

  const statusOptions = [
    {
      label: <span className="ta-dropdown-draft">Draft</span>,
      value: topicStatusEnum.DRAFT,
    },
    {
      label: <span className="ta-dropdown-active">Active</span>,
      value: topicStatusEnum.ACTIVE,
    },
    {
      label: <span className="ta-dropdown-onhold">On Hold</span>,
      value: topicStatusEnum.ONHOLD,
    },
    {
      label: <span className="ta-dropdown-deleted">Deleted</span>,
      value: topicStatusEnum.DELETED,
    },
  ];

  const headCells: ITableHeaderCells[] = [
    {
      id: "topicName",
      label: "Topic Name",
      onClickCell: (row: ITableRow) => {
        navigateToPage(`/topics/${row.topic.topicId}`);
      },
      value: (row: ITableRow) => <SActiveLink>{row.topic.topicName}</SActiveLink>,
      width: "3fr",
    },
    {
      id: "status",
      label: "Topic Status",
      value: (row: ITableRow) => row.topic.status,
    },
    {
      id: "topicOwnerAppClientName",
      label: "Topic Owner",
      onClickCell: (row: ITableRow) => {
        navigateToPage(`/applicationClients/${row.topic.appClient.appClientId}`);
      },
      value: (row: ITableRow) => <SActiveLink>{row.topic.appClient.appName}</SActiveLink>,
    },
    {
      id: "topicOwnerAppClientVersion",
      label: "Topic Owner Version",
      value: (row: ITableRow) => row.topic.appClient.appVersion,
    },
    {
      id: "topicOwnerAppClientBrokerType",
      label: "Topic Owner Broker",
      value: (row: ITableRow) => row.topic.appClient.appBrokerType.brokerType,
    },
    {
      id: "mqttClientBridgeFrom",
      label: "Bridged from",
      value: (row: ITableRow) =>
        row.brokerTopology && row.brokerTopology.parentBrokerType.brokerType,
    },
    {
      id: "mqttClientBridgeTo",
      label: "Bridged to",
      value: (row: ITableRow) =>
        row.brokerTopology && row.brokerTopology.remoteBrokerType.brokerType,
    },
    {
      id: "mqttClientCreatedAt",
      label: "Created at",
      value: (row: ITableRow) => getFormattedDate(row.createdAt),
    },
    {
      id: "mqttClientModifiedAt",
      label: "Modified at",
      value: (row: ITableRow) => getFormattedDate(row.modifiedAt),
    },
    { id: "delete", label: "Delete" },
  ];

  const dispatch = useDispatch();
  const [brokerTypes, setBrokerTypes]: any[] = useState([{}]);

  useEffect(() => {
    dispatch(
      getAllBrokerType({
        filter: {},
        limit: 0,
        orderBy: "brokerTypeName",
        orderType: "ASC",
        page: 0,
      })
    );
  }, []);

  const { allBrokerType, applicationDetails, applicationTopicList, currentUser } = useSelector(
    (state: RootState) => ({
      allBrokerType: state.allBrokerType,
      applicationDetails: state.applicationDetails,
      applicationTopicList: state.applicationTopicList,
      currentUser: state.currentUser,
    })
  );

  const emptyFilterObject = {
    clientType: props.type,
    modifiedDate: { startDate: undefined, endDate: undefined },
    mqttConnectedAppClientId: "",
    topicAppOwnerBrokerType: "",
    topicAppOwnerName: "",
    topicAppOwnerVersion: "",
    topicName: "",
    topicStatus: "",
  };

  const [config, setConfig] = useState({
    filter: { ...emptyFilterObject },
    limit: 10,
    orderBy: "topicName",
    orderType: OrderType.ASC,
    page: 0,
  });

  useEffect(() => {
    setBrokerTypes(
      allBrokerType.content.map((broker: any) => ({
        label: <span className={`ta-dropdown-${broker.brokerType}`}>{broker.brokerType}</span>,
        value: broker.brokerType,
      }))
    );
  }, [allBrokerType]);

  // Wait for the first filter set
  useEffect(() => {
    if (config.filter.mqttConnectedAppClientId !== "") {
      dispatch(getApplicationTopicList(config));
    }
  }, [config]);

  useEffect(
    () => () => {
      dispatch(appTopicListClearAction());
    },
    []
  );

  const deleteMqttAssociation = (topicMqttClientId: string) => {
    dispatch(
      deleteAssociation(topicMqttClientId, () => {
        setConfig({ ...config, page: 0 });
      })
    );
  };

  const notifyAboutLastPublisher = () => {
    dispatch(
      popupOpenAction({
        content:
          "You are trying to remove the last publisher to the topic. Please remove all the subscribers or assign another publisher first.",
        title: "Error",
        type: "Info",
      })
    );
  };

  const notifyAboutLastPublisherBulkDelete = (
    numberOfCase: number,
    numberOfselected: number,
    isSuccessful: boolean
  ) => {
    if (isSuccessful) {
      dispatch(
        popupOpenAction({
          content: `${numberOfCase}/${numberOfselected} associations deleted successfully`,
          title: "Success",
          type: "Info",
        })
      );
    } else {
      dispatch(
        popupOpenAction({
          content: `You have tried to remove the last publisher for ${numberOfCase} out of ${numberOfselected} topics selected. Please remove all the subscriptions or assign another publisher first.`,
          title: "Error",
          type: "Info",
        })
      );
    }
  };

  const askForConfirmation = (
    topicMqttClientId: string,
    isLastPublisher: boolean = false,
    hasSubscribers: boolean = false
  ) => {
    const confirmMsg1 =
      "You are trying to remove the association between the Application client and the Topic. Configuration file will be updated accordingly, do you want to continue?";
    const confirmMsg2 =
      "You are trying to remove the last publisher to the topic. Last publisher can only be removed if there are no subscribers to the topic. Do you want to continue?";

    dispatch(
      popupOpenAction({
        confirmText: "ConfirmText",
        content: isLastPublisher ? confirmMsg2 : confirmMsg1,
        onConfirm: () => {
          if (isLastPublisher && hasSubscribers) {
            notifyAboutLastPublisher();
          } else {
            deleteMqttAssociation(topicMqttClientId);
          }
        },
        title: `Please confirm`,
        type: "Confirmation",
      })
    );
  };

  const getPromiseOfApplicationTopicList = (
    currentClientType: string,
    currentTopicId: string
  ): Promise<any> =>
    ApplicationService.getApplicationTopicList({
      filter: {
        clientType: currentClientType,
        topicId: currentTopicId,
        topicStatus: "ACTIVE",
      },
      limit: 100,
      orderBy: "topicName",
      orderType: OrderType.ASC,
      page: 0,
    });

  const getErrorMessageAndThrowPopup = (row: ITableRow, isConfirmNeeded: boolean) => {
    const topicMqttClientId: string = row.topicMqttClientId;
    const selectedTopicId: string = row.topic.topicId;

    if (props.type === "PUBLISHER") {
      // Get the publisher list
      dispatch(
        getTopicsForPublisher(
          {
            filter: {
              clientType: "PUBLISHER",
              topicId: selectedTopicId,
              topicStatus: "ACTIVE",
            },
            limit: 100,
            orderBy: "topicName",
            orderType: OrderType.ASC,
            page: 0,
          },
          (isLastPublisher: boolean) => {
            if (!(isLastPublisher || isConfirmNeeded)) {
              deleteMqttAssociation(topicMqttClientId);
            } else {
              getPromiseOfApplicationTopicList("SUBSCRIBER", selectedTopicId)
                .then((response: any) => {
                  askForConfirmation(
                    topicMqttClientId,
                    isLastPublisher,
                    response.data.data.count > 0
                  );
                })
                .catch((e: any) => {
                  console.error(e);
                });
            }
          }
        )
      );
    } else {
      if (isConfirmNeeded) {
        askForConfirmation(topicMqttClientId);
      } else {
        deleteMqttAssociation(topicMqttClientId);
      }
    }
  };

  const [isAdmin, setIsAdmin] = useState(false);
  useEffect(() => {
    if (currentUser && applicationDetails) {
      setIsAdmin(currentUser.data.role === ADMINISTRATOR);
    }
  }, [applicationDetails, currentUser]);

  const paginationCallback = (page: number) => {
    setConfig({ ...config, page });
  };

  const sortCallback = (column: string, direction: OrderType) => {
    setConfig({ ...config, orderBy: column, orderType: direction });
  };

  const batchDelete = (selRows: ITableRow[]) => {
    if (selRows.length === 1) {
      getErrorMessageAndThrowPopup(selRows[0], false);
    } else {
      const mqttIds = selRows.map((row: ITableRow) => row.topicMqttClientId);

      if (props.type === "PUBLISHER") {
        const topicIds = selRows.map((row: ITableRow) => row.topic.topicId);
        // tslint:disable-next-line: array-type
        const promises: Promise<any>[] = [];
        topicIds.forEach((currentTopicId: string) =>
          promises.push(getPromiseOfApplicationTopicList("PUBLISHER", currentTopicId))
        );
        Promise.all(promises)
          .then(responsesOfLastPublishers => {
            const mqttIdsOfLastPublishers: string[] = [];
            const mqttIdsOfNotLastPublishers: string[] = [];
            const topicIdsOfLastPublishers: string[] = [];
            const mqttIdsOfLastPublishersWithSubscribers: string[] = [];
            const mqttIdsOfLastPublishersWithoutSubscribers: string[] = [];
            const mqttIdsOfAlreadyDeleted: string[] = [];

            responsesOfLastPublishers.forEach((response, index) => {
              const isExistingMqttAppClient = response.data.data.content.filter(
                (mqtt: any) => mqtt.appClient.appClientId === applicationDetails.content.appClientId
              ).length;
              if (isExistingMqttAppClient) {
                if (response.data.data.count === 1) {
                  mqttIdsOfLastPublishers.push(mqttIds[index]);
                  topicIdsOfLastPublishers.push(topicIds[index]);
                } else if (response.data.data.count > 1) {
                  mqttIdsOfNotLastPublishers.push(mqttIds[index]);
                }
              } else {
                mqttIdsOfAlreadyDeleted.push(mqttIds[index]);
              }
            });

            // tslint:disable-next-line: array-type
            const promisesForPotentialSubscibersOfLastPublishers: Promise<any>[] = [];
            topicIdsOfLastPublishers.forEach((topicId: string) => {
              promisesForPotentialSubscibersOfLastPublishers.push(
                getPromiseOfApplicationTopicList("SUBSCRIBER", topicId)
              );
            });
            Promise.all(promisesForPotentialSubscibersOfLastPublishers)
              .then(responsesOfExistingSubscribers => {
                responsesOfExistingSubscribers.forEach((response, index) => {
                  if (response.data.data.count > 0) {
                    mqttIdsOfLastPublishersWithSubscribers.push(mqttIdsOfLastPublishers[index]);
                  } else {
                    mqttIdsOfLastPublishersWithoutSubscribers.push(mqttIdsOfLastPublishers[index]);
                  }
                });

                const deletableAssociations = mqttIdsOfNotLastPublishers.concat(
                  mqttIdsOfLastPublishersWithoutSubscribers
                );
                const notDeletableAssociations = mqttIdsOfAlreadyDeleted.concat(
                  mqttIdsOfLastPublishersWithSubscribers
                );

                if (
                  !deletableAssociations.length &&
                  mqttIdsOfAlreadyDeleted.length !== selRows.length
                ) {
                  notifyAboutLastPublisherBulkDelete(
                    mqttIdsOfLastPublishersWithSubscribers.length,
                    selRows.length,
                    false
                  );
                } else {
                  dispatch(
                    deleteAssociationBulk({ mqttIds: deletableAssociations }, () => {
                      if (
                        mqttIdsOfAlreadyDeleted.length &&
                        mqttIdsOfAlreadyDeleted.length + deletableAssociations.length ===
                          selRows.length
                      ) {
                        notifyAboutLastPublisherBulkDelete(
                          deletableAssociations.length,
                          selRows.length,
                          true
                        );
                      } else if (deletableAssociations.length !== selRows.length) {
                        notifyAboutLastPublisherBulkDelete(
                          mqttIdsOfLastPublishersWithSubscribers.length,
                          selRows.length,
                          false
                        );
                      }
                      setConfig({ ...config, page: 0 });
                    })
                  );
                }
              })
              .catch(e => {
                console.error(e);
              });
          })
          .catch(e => {
            console.error(e);
          });
      } else {
        dispatch(
          deleteAssociationBulk({ mqttIds }, () => {
            setConfig({ ...config, page: 0 });
          })
        );
      }
    }
  };

  const tableProps: ITableConfig = {
    ...(isAdmin && {
      batchAction: {
        actions: [{ title: "Delete", onClick: batchDelete }],
        label: (selRows: ITableRow[]) =>
          `${selRows.length} item${selRows.length > 1 ? "s" : " is "} selected`,
      },
    }),
    deleteCell: {
      as: "a",
      disabled: (row: ITableRow) =>
        !isAdmin && currentUser.data.userId !== row.appClient.appOwner.userId,
      onClick: (row: ITableRow) => {
        if (isAdmin || currentUser.data.userId === applicationDetails.content.appOwner.userId) {
          getErrorMessageAndThrowPopup(row, true);
        }
      },
    },
    head: {
      cells: headCells,
    },
    list: {
      ...applicationTopicList,
    },
    name: "application-details",
    paginationConfig: {
      limit: config.limit,
      onPageChange: paginationCallback,
      page: config.page,
    },
    sortConfig: {
      onSort: sortCallback,
      orderBy: config.orderBy,
      orderType: config.orderType,
    },
  };

  const filterConfig: IFilterConfig = {
    customElements: [
      {
        element: (
          <GridItem justifySelf="end">
            <ButtonPrimary
              className={
                props.type === "PUBLISHER"
                  ? getTAClass("applicationDetails", TA_TYPES.BUTTON, "addPublisher")
                  : getTAClass("applicationDetails", TA_TYPES.BUTTON, "addSubscriber")
              }
              onClick={() => {
                props.btnClick(config);

                return;
              }}
              disabled={props.btnDisabled}
            >
              {props.type === "PUBLISHER" ? <>Add publisher</> : <>Add subscription</>}
            </ButtonPrimary>
          </GridItem>
        ),
      },
    ],
    items: [
      {
        name: "topicName",
        placeholder: "Topic Name",
        taClass: "topicName",
      },
      {
        data: statusOptions,
        name: "topicStatus",
        placeholder: "Topic Status",
        taClass: "topicStatus",
        type: FilterType.DROPDOWN,
      },
      {
        name: "topicAppOwnerName",
        placeholder: "Topic Owner",
        taClass: "topicOwner",
      },
      {
        name: "topicAppOwnerVersion",
        placeholder: "Topic Owner Version",
        taClass: "topicOwnerVersion",
      },
      {
        data: brokerTypes,
        name: "topicAppOwnerBrokerType",
        placeholder: "Broker type",
        taClass: "brokerType",
        type: FilterType.DROPDOWN,
      },
      {
        name: "modifiedDate",
        placeholder: "Modified",
        taClass: "modifiedDate",
        type: FilterType.DATEPICKER,
      },
    ],
    pageName: "applicationDetails",
    returnFilter: (filter: any) => {
      const newFilterObject = {
        ...emptyFilterObject,
        clientType: props.type,
        mqttConnectedAppClientId: applicationDetails.content.appClientId,
        ...filter,
      };

      if (!deepEqual(config.filter, newFilterObject)) {
        setConfig({
          ...config,
          filter: newFilterObject,
          page: 0,
        });
      }
    },
  };

  return (
    <>
      <Filter {...filterConfig} />
      {/* FILTER */}

      <SSpacer />

      {/* LOADING */}
      {applicationTopicList.loading && (
        <SOverlay>
          <ProgressIndicatorLinear />
        </SOverlay>
      )}
      {/* TABLE */}
      <Table {...tableProps} />
    </>
  );
};
