### UMB Changelog BSD

#### [v0.1.128](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.128..v0.1.127)

> 5 August 2020

#### [v0.1.127](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.127..v0.1.126)

> 31 July 2020

- ABC-303: Changelog [`#5`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/5)

#### [v0.1.126](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.126..v0.1.122)

> 29 July 2020

- ABC-503: Changelog [skip CI] [skip CI] [skip CI] [skip CI] [`#4`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/4)

#### [v0.1.122](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.122..v0.1.121)

> 9 July 2020

- ABC-502: Changelog [skip CI] [`#3`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/3)

#### [v0.1.121](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.121..v0.1.120)

> 9 July 2020

- ABC-501: Changelog 501 [skip CI] [`#2`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/2)

#### [v0.1.120](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.120..v0.1.116)

> 7 July 2020

- ABC-500: Changelog [`#1`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/1)
- Story/UMBT-989 [`#302`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/302)
- UMBT-1623: Operational user now can navigate to profile page [`#301`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/301)
- UMBT-1620: move dropdown on edit modal to the top [`#300`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/300)
- UMBT-1626: redirects user to the same page [`#299`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/299)
- Hotfix/UMBT-1622 1625 [`#298`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/298)
- Feature/UMBT-1466 [`#292`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/292)
- Feature/UMBT-1464-1465 [`#291`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/291)
- UMBT-1578: fork CloudBar, implement bottom links to accept both element and string [`#290`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/290)

#### [v0.1.116](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.116..v0.1.111)

> 23 June 2020

- UMBT-1569: use given client id from access token to refresh access token [`#289`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/289)
- UMBT-1473: pass current payload id in schema editing [skip CI] [`#271`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/271)
- UMBT-1568: missing ta classes on edit topic modal [`#285`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/285)
- UMBT-1533: Fix I want to create bridge checkbox is greyed out on local subscription issue [`#279`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/279)
- UMBT-1553: checkbox checked attribute relates to marked state [`#283`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/283)

#### [v0.1.111](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.111..v0.1.110)

> 16 June 2020

- Story/UMBT-1483 qos level value range update [`#278`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/278)
- Feature/UMBT-1502 qos level [`#276`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/276)

#### [v0.1.110](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.110..v0.1.109)

> 16 June 2020

- UMBT-1527: Allow creating new topic with previously deleted topic's name [`#273`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/273)

#### [v0.1.109](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.109..v0.1.108)

> 15 June 2020

- Story/UMBT-1446 [skip CI] [`#269`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/269)
- UMBT-1512: removed retain required dropdown from Edit Topic modal [`#270`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/270)

#### [v0.1.108](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.108..v0.1.107)

> 11 June 2020

- Story/UMBT-1392 [`#260`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/260)
- UMBT-1497: check the new filter object with the full filter object [`#268`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/268)
- UMBT-1497: avoid loading twice after filtering [`#266`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/266)

#### [v0.1.107](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.107..v0.1.105)

> 8 June 2020

- UMBT-1413: Add create new topic button [skip CI] [`#259`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/259)
- UMBT-1489: multipart form data header not needed in couple cases [`#262`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/262)
- UMBT-156: jwt handling [skip CI] [`#258`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/258)
- UMBT-1486: parse malformed token can throw error [`#261`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/261)
- UMBT-156: jwt handling [`#248`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/248)

#### [v0.1.105](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.105..v0.1.104)

> 29 May 2020

- UMBT-1389: read api config from public/config.json [`#240`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/240)

#### [v0.1.104](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.104..v0.1.103)

> 25 May 2020

- Story/UMBT-962 [skip CI] [`#247`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/247)
- UMBT-1451: fix plural on messages [`#246`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/246)
- UMBT-1348: new fields on BrokerType detail page [`#241`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/241)

#### [v0.1.103](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.103..v0.1.102)

> 25 May 2020

- UMBT-1352: RetainRequired, isSecure on TopicDetail page [`#244`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/244)
- UMBT-1444: copy fix [`#245`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/245)

#### [v0.1.102](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.102..v0.1.101)

> 18 May 2020

- UMBT-1249: Fix: Encrypted password is displayed on Application Edit page [`#238`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/238)

#### [v0.1.101](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.101..v0.1.100)

> 14 May 2020

- Story/UMBT-949 [`#239`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/239)
- UMBT-1397: Reduce the width of Number of connection retries and Connection retry (sec) columns [`#237`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/237)
- Feature/UMBT-1371 [`#235`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/235)
- Feature/UMBT-1370 [`#234`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/234)
- UMBT-1369: put two columns into table [`#233`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/233)

#### [v0.1.100](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.100..v0.1.99)

> 14 April 2020

- UMBT-960 [`#228`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/228)
- UMBT-1355: copy fix [`#232`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/232)
- UMBT-1342: wrong payloadId was saved [`#230`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/230)
- UMBT-1320: use response as schema [`#229`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/229)
- UMBT-1283: payload schema editing [`#227`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/227)

#### [v0.1.99](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.99..v0.1.98)

> 31 March 2020

- UMBT-1308: broker type page should load only once [`#226`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/226)

#### [v0.1.98](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.98..v0.1.97)

> 31 March 2020

- UMBT-734: loading refactor [`#225`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/225)
- UMBT-734: loading refactor [`#223`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/223)

#### [v0.1.97](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.97..v0.1.96)

> 30 March 2020

- UMBT-1197: BE no longer needs client type for bulk delete, change label on batch bar to item [`#224`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/224)
- UMBT-1197: BE no longer needs client type for bulk delete, change label on batch bar to item [`#205`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/205)

#### [v0.1.96](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.96..v0.1.95)

> 30 March 2020

- UMBT-1254: bulk delete if there is a last publisher [`#219`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/219)
- UMBT-1303: fix message on deleting already removed mqtts [`#222`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/222)
- UMBT-1298: fix pagination after deleting rows [`#221`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/221)
- UMBT-1254: bulk delete if there is a last publisher [`#216`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/216)
- UMBT-972: Bulk delete on application client page with administrator role [`#208`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/208)

#### [v0.1.95](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.95..v0.1.94)

> 26 March 2020

- Story/UMBT-958 [`#215`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/215)
- UMBT-1251: allow main screen of broker types to READ [`#212`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/212)
- UMBT-1260: fix sort by app name [`#213`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/213)
- UMBT-1250: Send only one request [`#214`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/214)
- UMBT-977: app client list on brokertypedetails [`#209`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/209)
- UMBT-978: Create Broker Details page - Topics Published tab, [`#210`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/210)
- UMBT-980: Create Broker Details page - Details Tab [`#207`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/207)
- UMBT-1179: Create Broker Type Details page [`#206`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/206)
- UMBT-976: Update Broker Type Listing page [`#204`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/204)

#### [v0.1.94](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.94..v0.1.93)

> 17 March 2020

- UMBT-1191: Remove the Application client version # from the topic string [`#202`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/202)

#### [v0.1.93](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.93..v0.1.92)

> 13 March 2020

- Story/UMBT-990 [`#193`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/193)

#### [v0.1.92](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.92..v0.1.91)

> 12 March 2020

- UMBT-1066: topic bulk delete fn, tests [`#196`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/196)
- UMBT-1147: warning message logic fix on bulk update [`#198`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/198)
- Hotfix/UMBT-1150 [`#200`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/200)
- Feature/UMBT-1066 [`#194`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/194)

#### [v0.1.91](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.91..v0.1.90)

> 9 March 2020

- UMBT-955: Fix Application Details page refreshing issue [`#191`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/191)

#### [v0.1.90](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.90..v0.1.89)

> 9 March 2020

- UMBT-1001: Fix Typeahead sometimes returns incorrect results issue [`#192`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/192)

#### [v0.1.89](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.89..v0.1.88)

> 28 February 2020

- UMBT-1039: tests for modals [`#188`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/188)

#### [v0.1.88](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.88..v0.1.87)

> 28 February 2020

- UMBT-881: call backend each time config changes on configuration listings [`#186`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/186)

#### [v0.1.87](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.87..v0.1.86)

> 28 February 2020

- UMBT-959: add Topology page [`#180`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/180)

#### [v0.1.86](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.86..v0.1.85)

> 28 February 2020

- Story/UMBT-956 [`#185`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/185)
- UMBT-1082: sonar scanner on PRs [`#187`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/187)

#### [v0.1.85](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.85..v0.1.84)

> 28 February 2020

- UMBT-1082: sonar scanner on PRs [`#187`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/187)

#### [v0.1.84](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.84..v0.1.83)

> 27 February 2020

- Feature/UMBT-936 2 refactors [`#184`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/184)
- UMBT-899: Refactor: Service url assembly [`#176`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/176)
- UMBT-936: testing filter helper [`#179`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/179)
- UMBT-896: ta classes [`#175`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/175)
- UMBT-896: ta classes [`#175`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/175)

#### [v0.1.83](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.83..v0.1.82)

> 21 February 2020

- UMBT-1018: enable create bridge [`#177`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/177)

#### [v0.1.82](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.82..v0.1.81)

> 20 February 2020

#### [v0.1.81](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.81..v0.1.80)

> 19 February 2020

- UMBT-995: fix 'Feature name' instead of 'Featured name' [`#174`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/174)

#### [v0.1.80](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/v0.1.80..0.1.79)

> 17 February 2020

- Feature/UMBT-936 refactors [`#172`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/172)
- UMBT-936: develop pipeline update [`#173`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/173)
- Feature/UMBT-886 [`#143`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/143)
- WIP: UMBT-888: filter refactor [`#167`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/167)
- UMBT-889: typeahead refactor [`#152`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/152)
- Revert "UMBT-985: pagination switch-off (pull request #169)" [`#170`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/170)
- UMBT-985: pagination switch-off [`#169`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/169)
- UMBT-942: Seperate Application client details in a separate tab [`#157`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/157)

#### [0.1.79](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/0.1.79..0.1.78)

> 10 February 2020

- UMBT-946: Fix ta classes on Topic Details - Bridges Table [`#156`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/156)
- UMBT-894: Refactor: Modal dropdown [`#154`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/154)
- UMBT-915: checkbox fix on topic subscribe modal [`#153`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/153)

#### [0.1.78](http://bitbucket.org/zoltanoreg/umb-changelog-test/compare/0.1.78..0.1.77)

> 6 February 2020

- UMBT-944: fix styling issue with the body margin typo [`#155`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/155)
- UMBT-892: Seperate Details pages label into component [`#145`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/145)

#### 0.1.77

> 6 February 2020

- UMBT-915: checkbox fix on topic subscribe modal [`#153`](http://bitbucket.org/zoltanoreg/umb-changelog-test/pull-requests/153)
