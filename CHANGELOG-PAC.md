### UMB Changelog PAC

#### v0.1.128

> 5 August 2020
5 August 2020
> 2020-08-05
2020-08-05

#### v0.1.127

> 31 July 2020
31 July 2020
> 2020-07-31
2020-07-31

- ABC-303: Changelog

#### v0.1.126

> 29 July 2020
29 July 2020
> 2020-07-29
2020-07-29

- ABC-503: Changelog [skip CI] [skip CI] [skip CI] [skip CI]

#### v0.1.122

> 9 July 2020
9 July 2020
> 2020-07-09
2020-07-09

- ABC-502: Changelog [skip CI]

#### v0.1.121

> 9 July 2020
9 July 2020
> 2020-07-09
2020-07-09

- ABC-501: Changelog 501 [skip CI]

#### v0.1.120

> 7 July 2020
7 July 2020
> 2020-07-07
2020-07-07

- ABC-500: Changelog
- Story/UMBT-989
- UMBT-1623: Operational user now can navigate to profile page
- UMBT-1620: move dropdown on edit modal to the top
- UMBT-1626: redirects user to the same page
- Hotfix/UMBT-1622 1625
- Feature/UMBT-1466
- Feature/UMBT-1464-1465
- UMBT-1578: fork CloudBar, implement bottom links to accept both element and string

#### v0.1.116

> 23 June 2020
23 June 2020
> 2020-06-23
2020-06-23

- UMBT-1569: use given client id from access token to refresh access token
- UMBT-1473: pass current payload id in schema editing [skip CI]
- UMBT-1568: missing ta classes on edit topic modal
- UMBT-1533: Fix I want to create bridge checkbox is greyed out on local subscription issue
- UMBT-1553: checkbox checked attribute relates to marked state

#### v0.1.111

> 16 June 2020
16 June 2020
> 2020-06-16
2020-06-16

- Story/UMBT-1483 qos level value range update
- Feature/UMBT-1502 qos level

#### v0.1.110

> 16 June 2020
16 June 2020
> 2020-06-16
2020-06-16

- UMBT-1527: Allow creating new topic with previously deleted topic's name

#### v0.1.109

> 15 June 2020
15 June 2020
> 2020-06-15
2020-06-15

- Story/UMBT-1446 [skip CI]
- UMBT-1512: removed retain required dropdown from Edit Topic modal

#### v0.1.108

> 11 June 2020
11 June 2020
> 2020-06-11
2020-06-11

- Story/UMBT-1392
- UMBT-1497: check the new filter object with the full filter object
- UMBT-1497: avoid loading twice after filtering

#### v0.1.107

> 8 June 2020
8 June 2020
> 2020-06-08
2020-06-08

- UMBT-1413: Add create new topic button [skip CI]
- UMBT-1489: multipart form data header not needed in couple cases
- UMBT-156: jwt handling [skip CI]
- UMBT-1486: parse malformed token can throw error
- UMBT-156: jwt handling

#### v0.1.105

> 29 May 2020
29 May 2020
> 2020-05-29
2020-05-29

- UMBT-1389: read api config from public/config.json

#### v0.1.104

> 25 May 2020
25 May 2020
> 2020-05-25
2020-05-25

- Story/UMBT-962 [skip CI]
- UMBT-1451: fix plural on messages
- UMBT-1348: new fields on BrokerType detail page

#### v0.1.103

> 25 May 2020
25 May 2020
> 2020-05-25
2020-05-25

- UMBT-1352: RetainRequired, isSecure on TopicDetail page
- UMBT-1444: copy fix

#### v0.1.102

> 18 May 2020
18 May 2020
> 2020-05-18
2020-05-18

- UMBT-1249: Fix: Encrypted password is displayed on Application Edit page

#### v0.1.101

> 14 May 2020
14 May 2020
> 2020-05-14
2020-05-14

- Story/UMBT-949
- UMBT-1397: Reduce the width of Number of connection retries and Connection retry (sec) columns
- Feature/UMBT-1371
- Feature/UMBT-1370
- UMBT-1369: put two columns into table

#### v0.1.100

> 14 April 2020
14 April 2020
> 2020-04-14
2020-04-14

- UMBT-960
- UMBT-1355: copy fix
- UMBT-1342: wrong payloadId was saved
- UMBT-1320: use response as schema
- UMBT-1283: payload schema editing

#### v0.1.99

> 31 March 2020
31 March 2020
> 2020-03-31
2020-03-31

- UMBT-1308: broker type page should load only once

#### v0.1.98

> 31 March 2020
31 March 2020
> 2020-03-31
2020-03-31

- UMBT-734: loading refactor
- UMBT-734: loading refactor

#### v0.1.97

> 30 March 2020
30 March 2020
> 2020-03-30
2020-03-30

- UMBT-1197: BE no longer needs client type for bulk delete, change label on batch bar to item
- UMBT-1197: BE no longer needs client type for bulk delete, change label on batch bar to item

#### v0.1.96

> 30 March 2020
30 March 2020
> 2020-03-30
2020-03-30

- UMBT-1254: bulk delete if there is a last publisher
- UMBT-1303: fix message on deleting already removed mqtts
- UMBT-1298: fix pagination after deleting rows
- UMBT-1254: bulk delete if there is a last publisher
- UMBT-972: Bulk delete on application client page with administrator role

#### v0.1.95

> 26 March 2020
26 March 2020
> 2020-03-26
2020-03-26

- Story/UMBT-958
- UMBT-1251: allow main screen of broker types to READ
- UMBT-1260: fix sort by app name
- UMBT-1250: Send only one request
- UMBT-977: app client list on brokertypedetails
- UMBT-978: Create Broker Details page - Topics Published tab,
- UMBT-980: Create Broker Details page - Details Tab
- UMBT-1179: Create Broker Type Details page
- UMBT-976: Update Broker Type Listing page

#### v0.1.94

> 17 March 2020
17 March 2020
> 2020-03-17
2020-03-17

- UMBT-1191: Remove the Application client version # from the topic string

#### v0.1.93

> 13 March 2020
13 March 2020
> 2020-03-13
2020-03-13

- Story/UMBT-990

#### v0.1.92

> 12 March 2020
12 March 2020
> 2020-03-12
2020-03-12

- UMBT-1066: topic bulk delete fn, tests
- UMBT-1147: warning message logic fix on bulk update
- Hotfix/UMBT-1150
- Feature/UMBT-1066

#### v0.1.91

> 9 March 2020
9 March 2020
> 2020-03-09
2020-03-09

- UMBT-955: Fix Application Details page refreshing issue

#### v0.1.90

> 9 March 2020
9 March 2020
> 2020-03-09
2020-03-09

- UMBT-1001: Fix Typeahead sometimes returns incorrect results issue

#### v0.1.89

> 28 February 2020
28 February 2020
> 2020-02-28
2020-02-28

- UMBT-1039: tests for modals

#### v0.1.88

> 28 February 2020
28 February 2020
> 2020-02-28
2020-02-28

- UMBT-881: call backend each time config changes on configuration listings

#### v0.1.87

> 28 February 2020
28 February 2020
> 2020-02-28
2020-02-28

- UMBT-959: add Topology page

#### v0.1.86

> 28 February 2020
28 February 2020
> 2020-02-28
2020-02-28

- Story/UMBT-956
- UMBT-1082: sonar scanner on PRs

#### v0.1.85

> 28 February 2020
28 February 2020
> 2020-02-28
2020-02-28

- UMBT-1082: sonar scanner on PRs

#### v0.1.84

> 27 February 2020
27 February 2020
> 2020-02-27
2020-02-27

- Feature/UMBT-936 2 refactors
- UMBT-899: Refactor: Service url assembly
- UMBT-936: testing filter helper
- UMBT-896: ta classes
- UMBT-896: ta classes

#### v0.1.83

> 21 February 2020
21 February 2020
> 2020-02-21
2020-02-21

- UMBT-1018: enable create bridge

#### v0.1.82

> 20 February 2020
20 February 2020
> 2020-02-20
2020-02-20

#### v0.1.81

> 19 February 2020
19 February 2020
> 2020-02-19
2020-02-19

- UMBT-995: fix 'Feature name' instead of 'Featured name'

#### v0.1.80

> 17 February 2020
17 February 2020
> 2020-02-17
2020-02-17

- Feature/UMBT-936 refactors
- UMBT-936: develop pipeline update
- Feature/UMBT-886
- WIP: UMBT-888: filter refactor
- UMBT-889: typeahead refactor
- Revert "UMBT-985: pagination switch-off (pull request #169)"
- UMBT-985: pagination switch-off
- UMBT-942: Seperate Application client details in a separate tab

#### 0.1.79

> 10 February 2020
10 February 2020
> 2020-02-10
2020-02-10

- UMBT-946: Fix ta classes on Topic Details - Bridges Table
- UMBT-894: Refactor: Modal dropdown
- UMBT-915: checkbox fix on topic subscribe modal

#### 0.1.78

> 6 February 2020
6 February 2020
> 2020-02-06
2020-02-06

- UMBT-944: fix styling issue with the body margin typo
- UMBT-892: Seperate Details pages label into component

#### 0.1.77

> 6 February 2020
6 February 2020
> 2020-02-06
2020-02-06

- UMBT-915: checkbox fix on topic subscribe modal
