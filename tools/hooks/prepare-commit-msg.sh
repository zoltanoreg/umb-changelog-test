#!/bin/sh
INPUT_FILE=".git/COMMIT_EDITMSG";
START_LINE=`head -n1 $INPUT_FILE`;
PATTERN="(^[A-Za-z0-9-]{2,10}-[0-9]{1,6}(:|[[:space:]]-[[:space:]]|[[:space:]]).)|(^[Mm]erge.)";
RED='\033[0;31m';
GREEN='\033[0;32m';
NC='\033[0m';

if ! [[ "$START_LINE" =~ $PATTERN ]]; then
  echo $INPUT_FILE;
  echo "😫💔 ${RED}[ERROR]${NC} Please use the following pattern: 'ABC-123: Descriptive message in present tense'";
  exit 1;
else
  echo "🙂❤️ ${GREEN}[VALID]${NC} Valid commit message!";
  exit 0;
fi